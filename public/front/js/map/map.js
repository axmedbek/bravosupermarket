moment.locale('az');

window.addEventListener('DOMContentLoaded', function () {
    'use strict';

    let see_more = $('#map_see_more').text();
    let go_back = $('#map_go_back').text();
    let route_trans = $('#map_route').text();
    let weekly_trans = $('#map_weekly').text();

    let phone_trans = $('#map_phone').text();
    let open_now_trans = $('#map_open_now').text();
    let closed_trans = $('#map_closed').text();
    let need_info_trans = $('#map_need_more_info').text();
    let store_info_trans = $('#map_store_info').text();


    if (document.querySelector('#map') !== null) {
        initMap('Bravo supermarket', false);
    }


    function initMap(query, status, location) {

        var request = {
            query: query,
            sensor: false,
            fields: ['name', 'geometry', 'phone', 'opening_hours'],
            type: ["grocery_or_supermarket", "food", "point_of_interest", "store", "establishment"],
            componentRestrictions: {country: 'az'}
        };

        // if (location) {
        //     mapOptions.zoom = 16;
        // }

        var myLatlng = new google.maps.LatLng(40.410519, 49.867317);

        var mapOptions = {
            zoom: 12,
            center: myLatlng
        };

        var map = new google.maps.Map(document.getElementById('map'), mapOptions);

        // var service = new google.maps.places.PlacesService(map);

        $.get($('#map_api_stores_url').text(), response => {
            response.data.map(result => {
                var open_status = "";
                var is_open = true;

                if (!result.work_times) {
                    is_open = true;
                } else {
                    if (result.work_times !== "24/7") {
                        var currentTime = moment(moment(), 'hh:mm');
                        var startTime = moment(result.work_times.split("-")[0].trim(), 'hh:mm');
                        var endTime = moment(result.work_times.split("-")[1].trim(), 'hh:mm');

                        if (currentTime.isBefore(startTime) || currentTime.isAfter(endTime)) {
                            is_open = false;
                        }
                        else {
                            is_open = true;
                        }
                    } else {
                        is_open = true;
                    }
                }

                if (is_open) {
                    open_status += "<p class=\"store-find-collection__info store-find-collection__info--opened\">\n" +
                        "                                                <svg class=\"icon icon-open store-find-collection__info-svg\">\n" +
                        "                                                    <use xlink:href=\"/front/images/sprite/sprite.svg#open\"></use>\n" +
                        "                                                </svg>\n" +
                        "                                                "+open_now_trans+"\n" +
                        "                                            </p>"
                } else {
                    open_status += "<p class=\"store-find-collection__info store-find-collection__info--closed\">\n" +
                        "                                                <svg class=\"icon icon-closed store-find-collection__info-svg\">\n" +
                        "                                                    <use xlink:href=\"/front/images/sprite/sprite.svg#closed\"></use>\n" +
                        "                                                </svg>"+closed_trans+"\n" +
                        "                                            </p>";
                }

                markerCreator(result, map, status, location);
                $('#side_location_list').append("<div class=\"store-find-collection__row\">\n" +
                    "                                    <h2 class=\"store-find-collection__title\">" + result.name + "</h2>\n" +
                    "                                    <p class=\"store-find-collection__address\">" + result.address + "</p>\n" +
                    "                                    <div class=\"store-find-collection__status-box\">\n" +
                    "                                        <div class=\"store-find-collection__status-box-row\">\n" +
                    open_status +
                    "                                            <a data-place-id=\"" + result.store_id + "\" class=\"store-find-collection__info store-find-collection__info--link store_see_more_btn\"\n" +
                    "                                               href=\"javascript:void(0)\">\n" +
                    "                                                <svg class=\"icon icon-info store-find-collection__info-svg\">\n" +
                    "                                                    <use xlink:href=\"/front/images/sprite/sprite.svg#info\"></use>\n" +
                    "                                                </svg>\n" +
                    "                                                "+store_info_trans+" </a>\n" +
                    "                                        </div>\n" +
                    "                                        <div class=\"store-find-collection__status-box-row\">\n" +
                    "                                            <a data-place-id=\"" + result.store_id + "\" class=\"primary-btn primary-btn--green store_see_more_btn\" href=\"javascript:void(0)\">"+see_more+"</a>\n" +
                    "                                        </div>\n" +
                    "                                    </div>\n" +
                    "                                </div>")
            })
        });

    }

    function markerCreator(item, map, status, location) {
        var coords = new google.maps.LatLng(item.latitude, item.longitude,21);

        new google.maps.Marker({
            position: coords,
            map: map,
            icon: '/front/images/main/icon/pin.svg',
        });

        if (status) {
            var userCoords = new google.maps.LatLng(location.latitude, location.longitude);
            new google.maps.Marker({
                position: userCoords,
                map: map,
                icon: '/front/images/main/icon/location.svg',
            });

            map.setCenter(userCoords);
            map.setZoom(16);
        }
    }


    var timer;

    $('input[name="store-find-form-search"]').on('keyup', function () {
        var myLatlng = new google.maps.LatLng(40.410519, 49.867317);

        var mapOptions = {
            zoom: 12,
            center: myLatlng
        };

        var map = new google.maps.Map(document.getElementById('map'), mapOptions);

        clearTimeout(timer);
        timer = setTimeout(() => {
            $('#side_location_list').html('');
            searchMapContainerCreator($('#map_api_search_url').text()+'?query='+$(this).val(),map);
        }, 1500)
    });


    function searchMapContainerCreator(link,map,status,location){
        $.get(link, response => {
            response.data.map(result => {
                var open_status = "";
                var is_open = true;

                if (!result.work_times) {
                    is_open = true;
                } else {
                    if (result.work_times !== "24/7") {
                        var currentTime = moment(moment(), 'hh:mm');
                        var startTime = moment(result.work_times.split("-")[0].trim(), 'hh:mm');
                        var endTime = moment(result.work_times.split("-")[1].trim(), 'hh:mm');

                        if (currentTime.isBefore(startTime) || currentTime.isAfter(endTime)) {
                            is_open = false;
                        }
                        else {
                            is_open = true;
                        }
                    } else {
                        is_open = true;
                    }
                }

                if (is_open) {
                    open_status += "<p class=\"store-find-collection__info store-find-collection__info--opened\">\n" +
                        "                                                <svg class=\"icon icon-open store-find-collection__info-svg\">\n" +
                        "                                                    <use xlink:href=\"/front/images/sprite/sprite.svg#open\"></use>\n" +
                        "                                                </svg>\n" +
                        "                                                "+open_now_trans+"\n" +
                        "                                            </p>"
                } else {
                    open_status += "<p class=\"store-find-collection__info store-find-collection__info--closed\">\n" +
                        "                                                <svg class=\"icon icon-closed store-find-collection__info-svg\">\n" +
                        "                                                    <use xlink:href=\"/front/images/sprite/sprite.svg#closed\"></use>\n" +
                        "                                                </svg>"+closed_trans+"\n" +
                        "                                            </p>";
                }

                markerCreator(result, map, status, location);
                $('#side_location_list').append("<div class=\"store-find-collection__row\">\n" +
                    "                                    <h2 class=\"store-find-collection__title\">" + result.name + "</h2>\n" +
                    "                                    <p class=\"store-find-collection__address\">" + result.address + "</p>\n" +
                    "                                    <div class=\"store-find-collection__status-box\">\n" +
                    "                                        <div class=\"store-find-collection__status-box-row\">\n" +
                    open_status +
                    "                                            <a data-place-id=\"" + result.store_id + "\" class=\"store-find-collection__info store-find-collection__info--link store_see_more_btn\"\n" +
                    "                                               href=\"javascript:void(0)\">\n" +
                    "                                                <svg class=\"icon icon-info store-find-collection__info-svg\">\n" +
                    "                                                    <use xlink:href=\"/front/images/sprite/sprite.svg#info\"></use>\n" +
                    "                                                </svg>\n" +
                    "                                               "+store_info_trans+" </a>\n" +
                    "                                        </div>\n" +
                    "                                        <div class=\"store-find-collection__status-box-row\">\n" +
                    "                                            <a data-place-id=\"" + result.store_id + "\" class=\"primary-btn primary-btn--green store_see_more_btn\" href=\"javascript:void(0)\">"+see_more+"</a>\n" +
                    "                                        </div>\n" +
                    "                                    </div>\n" +
                    "                                </div>")
            })
        });
    }

    // function initInnerMap() {
    //     var myLatlng = new google.maps.LatLng(40.399763, 49.852098);
    //     var pos1 = new google.maps.LatLng(40.399763, 49.852098);
    //
    //
    //     var mapOptions = {
    //         zoom: 17,
    //         center: myLatlng
    //     }
    //     var map = new google.maps.Map(document.getElementById('map'), mapOptions);
    //
    //     var marker = new google.maps.Marker({
    //         position: pos1,
    //         map: map,
    //         icon: './images/main/icon/pin.svg',
    //     });
    //
    // }

    function googleSearchDivider(service, request, status, location, successCallback) {
        service.textSearch(request, successCallback);
    }


    function getLocation() {
        if (navigator.geolocation) {
            // timeout at 60000 milliseconds (60 seconds)
            var options = {timeout: 60000};
            navigator.geolocation.getCurrentPosition
            (showLocation, errorHandler, options);
        } else {
            alert("Sorry, browser does not support geolocation!");
        }
    }

    function showLocation(position) {
        var latitude = position.coords.latitude;
        var longitude = position.coords.longitude;
        $('#side_location_list').html('');

        // initMap('Bravo supermarket', true, {latitude: latitude, longitude: longitude});

        var myLatlng = new google.maps.LatLng(latitude, longitude);

        var request = {
            query: 'Bravo',
            name: 'Bravo',
            radius: '5000',
            type: ["grocery_or_supermarket","store"],
            location : {lat: latitude, lng: longitude},
            fields: ['name', 'formatted_address'],
            componentRestrictions: {country: 'az'}
        };

        var mapOptions = {
            zoom: 12,
            center: myLatlng
        };

        var map = new google.maps.Map(document.getElementById('map'), mapOptions);

        var service = new google.maps.places.PlacesService(map);

        service.nearbySearch(request,response => {
            if (response.length > 1) {
                var request = {
                    placeId: response[0].place_id,
                    fields: ['name','formatted_address']
                };
                service.getDetails(request,response => {
                    searchMapContainerCreator($('#map_api_search_url').text()+'?query='+response.formatted_address,map,true,{latitude: latitude,longitude: longitude});
                })
            }
        })
    }

    function errorHandler(err) {
        if (err.code === 1) {
            alert("Error: Access is denied!");
        } else if (err.code === 2) {
            alert("Error: Position is unavailable!");
        }
    }

    $('.store-find__current-loc-search').on('click', function () {
        getLocation();
    });

    $(document).on('click', '.store-find-collection__back-btn', function (e) {
        e.preventDefault();
        e.stopPropagation();

        $('#side_location_list').html('');
        initMap();
    });

    $(document).on('click', '.store_see_more_btn', function (e) {
        e.preventDefault();
        e.stopPropagation();

        var pos = "28 May metro";
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function (position) {
                pos = position.coords.latitude + ',' + position.coords.longitude;
            }, function () {
                alert("Location permission denied");
            });
        } else {
            // Browser doesn't support Geolocation
            alert("Location permission denied");
        }

        var place_id = $(this).attr('data-place-id');

        var request = {
            placeId: place_id,
            fields: ['name', 'rating', 'formatted_phone_number', 'geometry', 'opening_hours', 'formatted_address']
        };

        var myLatlng = new google.maps.LatLng(40.410519, 49.867317);

        var mapOptions = {
            zoom: 12,
            center: myLatlng
        };

        var map = new google.maps.Map(document.getElementById('map'), mapOptions);

        // var service = new google.maps.places.PlacesService(map);

        $('#side_location_list').html('');

        $.get($('#map_api_store_url').text().replace(':store_id', place_id), response => {
            var coords = new google.maps.LatLng(response.data.latitude, response.data.longitude);

            map.setZoom(16);
            map.setCenter(coords);

            new google.maps.Marker({
                position: coords,
                map: map,
                icon: '/front/images/main/icon/pin.svg',
            });

            var open_status = "";
            var is_open = true;

            if (!response.data.work_times) {
                is_open = true;
            } else {
                if (response.data.work_times !== "24/7") {
                    var currentTime = moment(moment(), 'hh:mm');
                    var startTime = moment(response.data.work_times.split("-")[0].trim(), 'hh:mm');
                    var endTime = moment(response.data.work_times.split("-")[1].trim(), 'hh:mm');

                    if (currentTime.isBefore(startTime) || currentTime.isAfter(endTime)) {
                        is_open = false;
                    }
                    else {
                        is_open = true;
                    }
                } else {
                    is_open = true;
                }
            }

            if (is_open) {
                open_status += "<p class=\"store-find-collection__info store-find-collection__info--work-info store-find-collection__info--opened\">\n" +
                    "                                                <svg class=\"icon icon-open store-find-collection__info-svg\">\n" +
                    "                                                    <use xlink:href=\"/front/images/sprite/sprite.svg#open\"></use>\n" +
                    "                                                </svg>\n" +
                    "                                                "+open_now_trans+"\n" +
                    "                                            </p>"
            } else {
                open_status += "<p class=\"store-find-collection__info store-find-collection__info--work-info store-find-collection__info--closed\">\n" +
                    "                                                <svg class=\"icon icon-closed store-find-collection__info-svg\">\n" +
                    "                                                    <use xlink:href=\"/front/images/sprite/sprite.svg#closed\"></use>\n" +
                    "                                                </svg>"+closed_trans+"\n" +
                    "                                            </p>";
            }


            $('#side_location_list').html('<div class="store-find-collection__row">\n' +
                '                                    <h2 class="store-find-collection__title">' + response.data.name + '</h2>\n' +
                '                                    <div class="store-find-collection__wrap">\n' +
                '                                        <p class="store-find-collection__address">' + response.data.address + '</p>\n' +
                '                                        <a class="store-find-collection__route-btn" href="https://www.google.com/maps/dir/?api=1&origin=' + pos + '&destination=' + response.data.latitude + ',' + response.data.longitude + '&travelmode=driver" target="_blank">\n' +
                '                                            <svg class="icon icon-route store-find-collection__route-icon">\n' +
                '                                                <use xlink:href="/front/images/sprite/sprite.svg#route"></use>\n' +
                '                                            </svg>'+route_trans+'</a>\n' +
                '                                    </div>\n' +
                '                                    <div class="store-find-collection__status-box store-find-collection__status-box--fxd-column">\n' +
                '                                        <div class="store-find-collection__status-box-row store-find-collection__status-box-row--fxd-row">\n' +
                '                                            <p class="store-find-collection__shedule store-find-collection__shedule--bold">'+weekly_trans+'</p>\n' +
                '                                            <p class="store-find-collection__shedule">' + response.data.work_times + '</p>' +
                open_status +
                '                                        </div>\n' +
                '                                        <div style="padding-top: 14px;" class="store-find-collection__status-box-row store-find-collection__status-box-row--fxd-row">\n' +
                '                                            <p class="store-find-collection__shedule store-find-collection__shedule--bold">'+phone_trans+'</p>\n' +
                '                                            <a class="store-find-collection__shedule store-find-collection__shedule--link" href="tel:' + response.data.phone + '">' + (response.data.phone ?? '') + '</a>\n' +
                '                                        </div>\n' +
                '                                        <div class="store-find-collection__status-box-row store-find-collection__status-box-row--fxd-row">\n' +
                '                                            <a class="store-find-collection__info store-find-collection__info--middle store-find-collection__info--link" href="tel:' + response.data.phone + '">\n' +
                '                                                <svg class="icon icon-info store-find-collection__info-svg">\n' +
                '                                                    <use xlink:href="/front/images/sprite/sprite.svg#info"></use>\n' +
                '                                                </svg>'+need_info_trans+'</a>\n' +
                '                                        </div>\n' +
                '                                        <div class="store-find-collection__status-box-row store-find-collection__status-box-row--fxd-row">\n' +
                '                                            <a class="store-find-collection__back-btn" href="javascript:void(0)">\n' +
                '                                                <svg class="icon icon-arrow store-find-collection__back-btn-icon">\n' +
                '                                                    <use xlink:href="/front/images/sprite/sprite.svg#arrow"></use>\n' +
                '                                                </svg>'+go_back+' </a>\n' +
                '                                        </div>\n' +
                '                                    </div>\n' +
                '                                </div>');
        });

    })
});
