<?php

return [
    'faqTargets' => [
      'praktikum'
    ],
    'menuTargets' => [
        'noTarget',
        'targetTop',
        'targetBottom',
        'targetFooter',
        'targetCareersTop',
        'targetJobOpportunities',
        'targetStoreLocation'
    ],
    'studentGroup' => [2015,2016,2017,2018,2019,2020,2021,2022,2023,2024,2025],
    'sliderTargets' => [
        'noTarget',
        'shopAtBravo',
        'targetBottom',
        'targetFooter'
    ],
    'bannerTargets' => [
        'noTarget',
        'shopAtBravo',
        'aboveBravoBanner',
        'bravoBanner',
        'belowBravoBanner',
        'media',
    ],
    'bannerStyles' => [
        'noText',
        'textLeft',
        'textRight',
    ],
    'azMonthNames' => [
        1 => 'Yanvar',
        2 => 'Fevral',
        3 => 'Mart',
        4 => 'Aprel',
        5 => 'May',
        6 => 'İyun',
        7 => 'İyul',
        8 => 'Avqust',
        9 => 'Sentyabr',
        10 => 'OKtyabr',
        11 => 'Noyabr',
        12 => 'Dekabr',
    ],
    'socials' => [
        'facebook' => 'https://www.facebook.com/BravoSupermarketAz/',
        'instagram' => 'https://www.instagram.com/bravosupermarketaz/?hl=en',
        'youtube' => 'https://www.youtube.com/channel/UCUDPswYmu2SCJQCr7qDdtwA',
        'twitter' => 'https://twitter.com/bravoazs?lang=en',
        'linkedin' => 'https://az.linkedin.com/company/bravo-supermarket-azerbaijan',
        'pinterest' => 'https://www.pinterest.com/BravoAZS/',
        'googleplus' => 'https://plus.google.com/106071940515925921197',
        'telegram' => 'https://t.me/bravosupermarketaz',
    ]

];
