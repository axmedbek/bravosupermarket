<?php

return [
    'see_more' => 'Ətraflı',
    'open_now' => 'Açıqdır',
    'closed' => 'Bağlıdır',
    'weekly' => 'İş saatları',
    'store_info' => 'Mağaza haqqında',
    'phone' => 'Telefon',
    'need_more_info' => 'Köməyə ehtiyac var ?',
    'route' => 'Yol tarifi',
    'go_back' => 'Geri',
    'weekin' => 'Bazar ertəsi - Cümə',
    'weekend' => 'Şənbə - Bazar',
    'starting_from' => '-dan başlayaraq',
];
