<?php

return [
    'see_more' => 'See more',
    'open_now' => 'Open now',
    'closed' => 'Closed',
    'weekly' => 'Work times',
    'store_info' => 'Store info',
    'phone' => 'Phone',
    'need_more_info' => 'Need more information ?',
    'route' => 'Route',
    'go_back' => 'Back',
    'weekin' => 'Monday - Friday',
    'weekend' => 'Saturday - Sunday',
    'starting_from' => 'Starting from',
];
