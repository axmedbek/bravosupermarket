@extends('admin.layouts.layout')


@section('styles')
    <style>

    </style>
@endsection

@section('content')

    <!-- Page header -->
    <div class="page-header page-header-light" style="border-left: 1px solid #ddd; border-right: 1px solid #ddd;">

        <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline py-2">
            <div class="d-flex">
                <div class="breadcrumb">
                    <a href="{{route('admin.dashboard')}}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> {{translate('breadcrumb.dashboard')}}</a>
                    <a href="{{route('admin.offers.index')}}" class="breadcrumb-item">{{translate('breadcrumb.offers')}}</a>
                    <span class="breadcrumb-item active">{{translate('breadcrumb.editOffer')}}</span>
                </div>
            </div>
        </div>
    </div>
    <!-- /page header -->
    @php
        $offerData = null;
        foreach ($offers as $offer){$offerData = $offer;}
    @endphp

    <div class="content p-0">
        <form action="{{route('admin.offers.update',['offer' => $offerData->offer_id])}}" method="post" data-reload="{{route('admin.offers.index')}}">
            @csrf
            @method('put')
            <div class="card border-top-0 mb-0 border-x-0">
                <div class="card-body px-0">
                    <ul class="nav nav-tabs nav-tabs-highlight">
                        @foreach(getLanguages() as $language)
                            <li class="nav-item"><a href="#tab-{{$language['key']}}" class="nav-link {{getLocale()===$language['key']?'active':''}}" data-toggle="tab">{{$language['name']}}</a></li>
                        @endforeach
                    </ul>

                    <div class="tab-content px-3">
                        @foreach(getLanguages() as $language)
                            <div class="tab-pane fade {{getLocale()===$language['key']?'active show':''}}" id="tab-{{$language['key']}}">
                                <div class="row">
                                    <div class="form-group col-lg-6 col-md-6 col-sm-6">
                                        <label for="title"><i class="icon-pencil title-icon"></i>{{translate('offers.title')}}</label>
                                        <div class="input-group">
                                            <input class="form-control" id="title" name="title[{{$language['key']}}]" value="{{$offers[$language['key']]->title}}">
                                        </div>
                                    </div>

                                    <div class="form-group col-lg-6 col-md-6 col-sm-6">
                                        <label for="text"><i class="icon-file-text title-icon"></i>{{translate('offers.text')}}</label>
                                        <div class="input-group">
                                            <input class="form-control" id="text" name="text[{{$language['key']}}]" value="{{$offers[$language['key']]->text}}">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>

                    <div class="row px-2 mx-0">


                        <div class="form-group col-lg-6 col-md-6 col-sm-6">
                            <label for="date"><i class="icon-calendar3 title-icon"></i>{{translate('offers.date')}}</label>
                            <div class="input-group">
                                <input class="form-control" id="date" name="date" value="{{humanDate($offer->date,'Y-m-d H:i:s','d.m.Y')??''}}">
                            </div>
                            @if($errors->has('date'))
                                <div class="text-danger">{{$errors->first('date')}}</div>
                            @endif
                        </div>

                        <div class="form-group col-lg-6 col-md-6 col-sm-6">
                            <label for="status"><i class="icon-file-check title-icon"></i>{{translate('offers.status')}}</label>
                            <div class="input-group" style="padding: 7px 7px 7px 0;">
                                <input class="form-control switchery" id="status" type="checkbox" name="status" {{$offer->status?'checked':''}}>
                            </div>
                        </div>
                        <div class="form-group col-lg-12 col-md-12 col-sm-12 mb-5">
                            <label for="offer_products"><i class="icon-grid-alt title-icon"></i>{{translate('offers.offerProducts')}}</label>
                            <div class="input-group">
                                <select class="form-control selectpicker" id="offer_products" name="offer_products[]" multiple>
                                    <option value="">{{translate('common.selectOfferProduct')}}</option>
                                    @foreach($offerProducts as $offerProduct)
                                        <option value="{{$offerProduct->offer_product_id}}" data-content="<img width='60' src='{{$offerProduct->media->url}}'/>&nbsp; {{$offerProduct->title}}" {{$offer->offer_products && checkInListGivenId($offerProduct->id,$offer->offer_products)?'selected':''}}>{{$offerProduct->title}}</option>
                                    @endforeach
                                </select>
                            </div>
                            @if($errors->has('offer_products'))
                                <div class="text-danger">{{$errors->first('offer_products')}}</div>
                            @endif
                        </div>

                        <div class="form-group col-lg-12 col-md-12 col-sm-6">
                            <div class="btn bg-slate" id="openMediaManager"><i class="icon-gallery"></i> {{translate('media.openMediaManager')}}</div>
                            <div class="media-area row">
                                <a href="{{$offerData->media->url}}" class="fancybox" data-fancybox="media">
                                    <div class="image" id="{{$offerData->media->id}}">
                                        @if($offerData->media->type==='video' && $offerData->media->mime_type==='youtube')
                                            <iframe src="{{generateYoutubeEmbedUrl($offerData->media->url)}}"></iframe>
                                        @else
                                            <img src="{{$offerData->media->url}}" alt="{{$offerData->title}}"/>
                                        @endif
                                        <div class="action deleteMedia">×</div>
                                        <div class="action editMedia"><i class="icon-crop"></i></div>
                                        <div class="action showMedia"><i class="icon-eye"></i></div>
                                        <input type="hidden" name="media[]" value="{{$offerData->media->id}}">
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>

                </div>

                <div class="card-footer">
                    <div class="text-right">
                        <button class="btn bg-slate submitAJAX">{{translate('offers.editOffer')}}</button>
                    </div>
                </div>
            </div>
        </form>
    </div>

@endsection

@section('scripts')
    <script>

        pashayev.datePicker('#date','DD.MM.YYYY');

        function beforeSubmit(){
            Object.entries(mediaTitles).forEach(([mediaId,mediaTitle]) => {
                let newMediaTitleTextInput = `<input type="hidden" name="mediaTitles[${mediaId}]" value="${mediaTitle}"/>`;
                newMediaTitleTextInput += `<input type="hidden" name="mediaTexts[${mediaId}]" value="${mediaTexts[mediaId]}"/>`;
                $('form').append(newMediaTitleTextInput);
            });
        }

        $('select.select2').select2();
    </script>
@endsection
