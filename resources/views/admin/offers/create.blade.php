@extends('admin.layouts.layout')


@section('styles')
    <style>

    </style>
@endsection

@section('content')

    <!-- Page header -->
    <div class="page-header page-header-light" style="border-left: 1px solid #ddd; border-right: 1px solid #ddd;">

        <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline py-2">
            <div class="d-flex">
                <div class="breadcrumb">
                    <a href="{{route('admin.dashboard')}}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> {{translate('breadcrumb.dashboard')}}</a>
                    <a href="{{route('admin.offers.index')}}" class="breadcrumb-item">{{translate('breadcrumb.offers')}}</a>
                    <span class="breadcrumb-item active">{{translate('breadcrumb.newOffer')}}</span>
                </div>
            </div>
        </div>
    </div>
    <!-- /page header -->

    <div class="content p-0">
        <form action="{{route('admin.offers.store')}}" method="post" data-reload="{{route('admin.offers.index')}}">
            @csrf
            <div class="card border-top-0 mb-0 border-x-0">
                <div class="card-body px-0">

                    <ul class="nav nav-tabs nav-tabs-highlight">
                        @foreach(getLanguages() as $language)
                            <li class="nav-item"><a href="#tab-{{$language['key']}}" class="nav-link {{getLocale()===$language['key']?'active':''}}" data-toggle="tab">{{$language['name']}}</a></li>
                        @endforeach
                    </ul>

                    <div class="tab-content px-3">
                        @foreach(getLanguages() as $language)
                            <div class="tab-pane fade {{getLocale()===$language['key']?'active show':''}}" id="tab-{{$language['key']}}">
                                <div class="row">
                                    <div class="form-group col-lg-6 col-md-6 col-sm-6">
                                        <label for="title"><i class="icon-pencil title-icon"></i>{{translate('offers.title')}}</label>
                                        <div class="input-group">
                                            <input class="form-control" id="title"  name="title[{{$language['key']}}]">
                                        </div>
                                    </div>

                                    <div class="form-group col-lg-6 col-md-6 col-sm-6">
                                        <label for="text"><i class="icon-file-text title-icon"></i>{{translate('offers.text')}}</label>
                                        <div class="input-group">
                                            <input class="form-control" id="text"  name="text[{{$language['key']}}]">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>

                    <div class="row px-2 mx-0">

                        <div class="form-group col-lg-6 col-md-6 col-sm-6">
                            <label for="date"><i class="icon-calendar3 title-icon"></i>{{translate('offers.date')}}</label>
                            <div class="input-group">
                                <input class="form-control" id="date" name="date">
                            </div>
                            @if($errors->has('date'))
                                <div class="text-danger">{{$errors->first('date')}}</div>
                            @endif
                        </div>

                        <div class="form-group col-lg-6 col-md-6 col-sm-6">
                            <label for="status"><i class="icon-file-check title-icon"></i>{{translate('offers.status')}}</label>
                            <div class="input-group" style="padding: 7px 7px 7px 0;">
                                <input class="form-control switchery" id="status" type="checkbox" name="status" checked>
                            </div>
                        </div>

                        <div class="form-group col-lg-12 col-md-12 col-sm-12 mb-5">
                            <label for="offer_products"><i class="icon-grid-alt title-icon"></i>{{translate('offers.offerProducts')}}</label>
                            <div class="input-group">
                                <select class="form-control selectpicker" id="offer_products" name="offer_products[]" multiple>
                                    <option value="">{{translate('common.selectOfferProduct')}}</option>
                                    @foreach($offerProducts as $offerProduct)
                                        <option value="{{$offerProduct->offer_product_id}}" data-content="<img width='60' src='{{$offerProduct->media->url}}'/>&nbsp; {{$offerProduct->title}}">{{$offerProduct->title}}</option>
                                    @endforeach
                                </select>
                            </div>
                            @if($errors->has('offer_products'))
                                <div class="text-danger">{{$errors->first('offer_products')}}</div>
                            @endif
                        </div>

                        <div class="form-group col-lg-12 col-md-12 col-sm-6">
                            <div><label for="openMediaManager"><i class="icon-image2 title-icon"></i>{{translate('common.offerImage')}}</label></div>
                            <div class="btn bg-slate" id="openMediaManager" data-media-select-type="single"><i class="icon-gallery"></i> {{translate('media.openMediaManager')}}</div>
                            <div class="media-area row"></div>
                        </div>
                    </div>

                </div>

                <div class="card-footer">
                    <div class="text-right">
                        <button class="btn bg-slate submitAJAX">{{translate('offers.addNewOffer')}}</button>
                    </div>
                </div>
            </div>
        </form>
    </div>

@endsection

@section('scripts')
    <script>

        pashayev.datePicker('#date','DD.MM.YYYY');
        $('select.selectpicker').selectpicker({
            showThumbnails:1
        });

        $('select.select2').select2();
    </script>
@endsection
