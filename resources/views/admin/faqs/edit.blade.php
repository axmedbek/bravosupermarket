@extends('admin.layouts.layout')


@section('styles')
    <style>
        #roles tr, #roles td, #roles th {
            border: 1px solid #ebebeb;
            padding: 10px;
        }

        #infos .row {
            border-bottom: 1px solid #ebebeb;
            margin: 10px 0;
        }

        #infos .row:last-child {
            border-bottom: 0;
        }

        .select2-selection--multiple .select2-search--inline:first-child .select2-search__field {
            padding-left: .5rem;
        }
    </style>
@endsection

@section('content')

    <!-- Page header -->
    <div class="page-header page-header-light" style="border-left: 1px solid #ddd; border-right: 1px solid #ddd;">

        <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline py-2">
            <div class="d-flex">
                <div class="breadcrumb">
                    <a href="{{route('admin.dashboard')}}" class="breadcrumb-item"><i
                            class="icon-home2 mr-2"></i> {{translate('breadcrumb.dashboard')}}</a>
                    <a href="{{route('admin.faqs.index')}}"
                       class="breadcrumb-item">{{translate('breadcrumb.faqs')}}</a>
                    <span class="breadcrumb-item active">{{translate('breadcrumb.newFaq')}}</span>
                </div>
            </div>
        </div>
    </div>
    <!-- /page header -->

    <div class="content p-0">
        <form action="{{route('admin.faqs.update',['faq' => $faq['id']])}}" method="post" data-reload="reload">
            @csrf
            @method('PUT')
            <div class="card border-top-0 mb-0">
                <div class="card-body">
                    <div class="row">
                        <div class="form-group col-lg-12 col-md-12 col-sm-12">
                            <ul class="nav nav-tabs nav-tabs-highlight">
                                @foreach(getLanguages() as $language)
                                    <li class="nav-item"><a href="#tab-{{$language['key']}}"
                                                            class="nav-link {{getLocale()===$language['key']?'active':''}}"
                                                            data-toggle="tab">{{$language['name']}}</a></li>
                                @endforeach
                            </ul>

                            <div class="tab-content px-3">
                                @foreach(getLanguages() as $language)
                                    <div class="tab-pane fade {{getLocale()===$language['key']?'active show':''}}"
                                         id="tab-{{$language['key']}}">
                                        <div class="row">
                                            <div class="form-group col-lg-12 col-md-12 col-sm-12">
                                                <label for="question-{{$language['key']}}">{{translate('menus.question')}}</label>
                                                <div class="input-group">
                                                     <textarea class="form-control" id="question-{{$language['key']}}" name="question[{{$language['key']}}]"
                                                               cols="30" rows="5">{{ $faq[$language['key']] ? $faq[$language['key']]->question : '' }}</textarea>
                                                </div>
                                                @if($errors->has('question'))
                                                    <div class="text-danger">{{$errors->first('question')}}</div>
                                                @endif
                                            </div>
                                            <div class="form-group col-lg-12 col-md-12 col-sm-12">
                                                <label for="answer-{{$language['key']}}">{{translate('menus.answer')}}</label>
                                                <div class="input-group">
                                                      <textarea class="form-control" id="answer-{{$language['key']}}" name="answer[{{$language['key']}}]"
                                                                cols="30" rows="5">{{ $faq[$language['key']] ? $faq[$language['key']]->answer : '' }}</textarea>
                                                </div>
                                                @if($errors->has('answer'))
                                                    <div class="text-danger">{{$errors->first('answer')}}</div>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>

                        <div class="form-group col-lg-12 col-md-12 col-sm-12">
                            <label for="parent">{{translate('menus.parent')}}</label>
                            <div class="input-group">
                                <select class="form-control select2" id="parent" name="parent">
                                    <option value="null">{{translate('options.noParent')}}</option>
                                    @foreach(\App\Faq::getFaqs() as $parentFaq)
                                        <option {{ $faq->parent_id == $parentFaq->faq_id ? "selected" : "" }} value="{{$parentFaq->faq_id}}">{{$parentFaq->question}}</option>
                                        @if($subfaqs = $parentFaq->subFaqs)
                                            @include('admin.partials.faqs.options', ['subfaqs' => $subfaqs,'layer' => 1,'parent_id' => $faq->parent_id])
                                        @endif
                                    @endforeach
                                </select>
                            </div>
                            @if($errors->has('parent_id'))
                                <div class="text-danger">{{$errors->first('parent_id')}}</div>
                            @endif
                        </div>

                        <div class="form-group col-lg-6 col-md-6 col-sm-6">
                            <label for="target">{{translate('faqs.target')}}</label>
                            <div class="input-group">
                                <select name="target[]" id="target" class="form-control selectpicker" data-container="body" multiple data-live-search="true" data-selected-text-format="count">
                                    @foreach(config('settings.faqTargets') as $target)
                                        <option value="{{$target}}" {{in_array($target,json_decode($faq->target))?'selected':''}}>{{translate("faqs.$target")}}</option>
                                    @endforeach
                                </select>
                            </div>
                            @if($errors->has('target'))
                                <div class="text-danger">{{$errors->first('target')}}</div>
                            @endif
                        </div>


                        <div class="form-group col-lg-6 col-md-6 col-sm-6">
                            <label for="order">{{translate('menus.order')}}</label>
                            <div class="input-group">
                                <input class="form-control" id="order" name="order" value="{{$faq ? $faq->order : getLastOrder('faqs')}}">
                            </div>
                            @if($errors->has('order'))
                                <div class="text-danger">{{$errors->first('order')}}</div>
                            @endif
                        </div>

                        <div class="form-group col-lg-6 col-md-6 col-sm-6">
                            <label for="status">{{translate('menus.status')}}</label>
                            <div class="input-group" style="padding: 7px 7px 7px 0;">
                                <input class="form-control switchery" id="status" type="checkbox" name="status" checked>
                            </div>
                        </div>

                    </div>


                </div>
                <div class="card-footer">
                    <div class="text-right">
                        <button class="btn bg-slate submitAJAX">{{translate('menus.newFaq')}}</button>
                    </div>
                </div>
            </div>
        </form>
    </div>

@endsection

@section('scripts')
    <script>
        $(function () {
            $('select.select2').select2();
            $('select.selectpicker').selectpicker();
            $('.form-check-input-styled').uniform();
            $('.switchery').each(function () {
                new Switchery($(this)[0])
            });
        });

        $(document).on('change', '#parent_id', function () {
            let column = 'parent_id';
            let value = $(this).val();
            let model = 'Menu';
            pashayev.request({
                url: '{{route('admin.generateOrderNumberInParent')}}',
                input: {column, value, model},
                success: function (response) {
                    $('#order').val(response.result.order);
                }
            });
        });
    </script>
@endsection
