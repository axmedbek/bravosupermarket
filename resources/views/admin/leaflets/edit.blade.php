@extends('admin.layouts.layout')


@section('styles')
    <style>
        .pdfmedia .image .action {
            position: absolute;
            cursor: pointer;
            opacity: .5;
            width: 30px;
            height: 30px;
            display: flex;
            align-items: center;
            justify-content: center;
            color: white;
            transition: 220ms;
            font-size: 20px;
            z-index: 2;
        }
        .pdfmedia .image {
            width: 150px;
            height: 150px;
            border: 1px solid #dedede;
            position: relative;
            margin: 30px 5px;
            box-shadow: 1px 1px 6px -3px #a0a0a0;
        }
        .pdfmedia .image img {
            width: 100%;
            height: 100%;
            object-fit: cover;
        }
    </style>
@endsection

@section('content')

    <!-- Page header -->
    <div class="page-header page-header-light" style="border-left: 1px solid #ddd; border-right: 1px solid #ddd;">

        <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline py-2">
            <div class="d-flex">
                <div class="breadcrumb">
                    <a href="{{route('admin.dashboard')}}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> {{translate('breadcrumb.dashboard')}}</a>
                    <a href="{{route('admin.leaflets.index')}}" class="breadcrumb-item">{{translate('breadcrumb.leaflets')}}</a>
                    <span class="breadcrumb-item active">{{$leaflets[getLocale()]->title}}</span>
                </div>
            </div>
        </div>
    </div>
    <!-- /page header -->

    @php
        $leafletData = null;
        foreach ($leaflets as $leaflet){$leafletData = $leaflet;}
    @endphp
    <div class="content p-0">
        <form action="{{route('admin.leaflets.update',['leaflet' => $leafletData->leaflet_id])}}" method="post" data-reload="{{route('admin.leaflets.index')}}">
            @csrf
            @method('PUT')
            <div class="card border-top-0 mb-0 border-x-0">
                @if(session()->has('success'))
                    {!! alert(session()->get('success')) !!}
                @endif
                <div class="card-body px-0">
                    <ul class="nav nav-tabs nav-tabs-highlight">
                        @foreach(getLanguages() as $language)
                            <li class="nav-item"><a href="#tab-{{$language['key']}}" class="nav-link {{getLocale()===$language['key']?'active':''}}" data-toggle="tab">{{$language['name']}}</a></li>
                        @endforeach
                        <li class="nav-item"><a href="#tab-gallery" class="nav-link" data-toggle="tab">{{translate('tabs.gallery')}}</a></li>
                        <li class="nav-item"><a href="#tab-settings" class="nav-link" data-toggle="tab">{{translate('tabs.settings')}}</a></li>
                    </ul>


                    <div class="tab-content px-3">
                        @foreach(getLanguages() as $language)
                            @php($leaflet = $leaflets[$language['key']]??null)
                            <div class="tab-pane fade {{getLocale()===$language['key']?'active show':''}}" id="tab-{{$language['key']}}">
                                <div class="row">
                                    <div class="form-group col-lg-12 col-md-12 col-sm-12">
                                        <label for="title-{{$language['key']}}"><i class="icon-pencil5 title-icon"></i>{{translate('leaflets.title')}}</label>
                                        <input class="form-control" id="title-{{$language['key']}}" name="title[{{$language['key']}}]" value="{{$leaflet->title??''}}">
                                        @if($errors->has('title'))
                                            <div class="text-danger">{{$errors->first('title')}}</div>
                                        @endif
                                    </div>



                                    <div class="form-group col-lg-12 col-md-12 col-sm-12">
                                        <label for="slug-{{$language['key']}}"><i class="icon-hyperlink title-icon"></i>{{translate('leaflets.slug')}}</label>
                                        <input class="form-control" id="slug-{{$language['key']}}" name="slug[{{$language['key']}}]" value="{{$leaflet->slug??''}}">
                                        @if($errors->has('slug'))
                                            <div class="text-danger">{{$errors->first('slug')}}</div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        @endforeach
                        <div class="tab-pane fade" id="tab-gallery">
                            <div class="row">
                                <div class="col-lg-6 col-md-6 col-sm-6">
                                    <div class="form-group col-lg-12 col-md-12 col-sm-6">
                                        <div class="btn bg-slate" id="openMediaManager"><i class="icon-gallery"></i> {{translate('media.openMediaManager')}}</div>
                                        <div class="media-area row">
                                            <empty class="d-none"><i class="icon-files-empty title-icon"></i>{{translate('common.nothingSelected')}}</empty>
                                            @if($leaflet->media)
                                                <a href="{{$leaflet->media->url}}" class="fancybox" data-fancybox="media">
                                                    <div class="image" id="{{$leaflet->media->id}}">
                                                        <img src="{{$leaflet->media->url}}">
                                                        <div class="action deleteMedia">×</div>
                                                        <div class="action editMedia"><i class="icon-crop"></i></div>
                                                        <div class="action showMedia"><i class="icon-eye"></i></div>
                                                        <input type="hidden" name="media[]" value="{{$leaflet->media->id}}">
                                                    </div>
                                                </a>
                                            @endif
                                        </div>
                                    </div>

                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6">
                                    <div class="form-group col-lg-12 col-md-12 col-sm-6">
                                        <div class="btn bg-slate" id="openMediaManagerPDF"><i class="icon-gallery"></i> {{translate('media.openMediaManager')}}</div>
                                        <div class="pdfmedia row">
                                            <empty class="d-none"><i class="icon-files-empty title-icon"></i>{{translate('common.nothingSelected')}}</empty>
                                            @if($leaflet->pdf)
                                                <a href="{{$leaflet->pdf->url}}" class="fancybox" data-fancybox="media">
                                                    <div class="image" id="{{$leaflet->pdf->id}}">
                                                        <img src="{{ asset('/panel/images/pdf.png')}}">
                                                        <div class="action deleteMedia">×</div>
                                                        <div class="action editMedia"><i class="icon-crop"></i></div>
                                                        <div class="action showMedia"><i class="icon-eye"></i></div>
                                                        <input type="hidden" name="media[]" value="{{$leaflet->pdf->id}}">
                                                    </div>
                                                </a>
                                            @endif
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="tab-settings">
                            <div class="row">


                                <div class="form-group col-lg-6 col-md-6 col-sm-12">
                                    <label for="date"><i class="icon-calendar3 title-icon"></i>{{translate('leaflets.start_at')}}</label>
                                    <div class="input-group">
                                        <input class="form-control" id="start" name="start_at" value="{{humanDate($leafletData->start_at)}}">
                                    </div>
                                    @if($errors->has('start_at'))
                                        <div class="text-danger">{{$errors->first('start_at')}}</div>
                                    @endif
                                </div>

                                <div class="form-group col-lg-6 col-md-6 col-sm-12">
                                    <label for="date"><i class="icon-calendar3 title-icon"></i>{{translate('leaflets.end_at')}}</label>
                                    <div class="input-group">
                                        <input class="form-control" id="end" name="end_at" value="{{humanDate($leafletData->end_at)}}">
                                    </div>
                                    @if($errors->has('end_at'))
                                        <div class="text-danger">{{$errors->first('end_at')}}</div>
                                    @endif
                                </div>




                                <div class="form-group col-lg-12 col-md-12 col-sm-12 pl-3">
                                    <label for="status"><i class="icon-file-check title-icon"></i>{{translate('pages.status')}}</label>
                                    <div class="input-group">
                                        <input class="form-control switchery" id="status" type="checkbox" name="status" {{$leafletData->status?'checked':''}}>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                </div>

                <div class="card-footer">
                    <div class="text-right">
                        <button class="btn bg-slate submitAJAX">{{translate('users.updateLeaflet')}}</button>
                    </div>
                </div>
            </div>
        </form>
    </div>

@endsection

@section('scripts')
    <script>
        {{--//appendedMedias = [{{$leafletData->media}}];--}}
        $('.post-content').each(function(){
            pashayev.editor($(this).attr('id'));
        });
        $('#date').daterangepicker({
            locale: {
                format: 'DD.MM.YYYY HH:mm:ss'
            },
            singleDatePicker: true,
            timePicker: true,
            startDate: moment(),
            timePicker24Hour: true,
            autoUpdateInput: false,
            autoApply: true
        },function(date){
            console.log(date)
            $('#date').val(date.format('DD.MM.YYYY HH:mm:ss'));
        });

        $('#category').select2();

        CKEDITOR.on('instanceReady', function(){
            $.each( CKEDITOR.instances, function(instance) {
                CKEDITOR.instances[instance].on("change", function(e) {
                    for ( instance in CKEDITOR.instances )
                        CKEDITOR.instances[instance].updateElement();
                });
            });
        });

        $(document).on('change','[id*=title]',function(){
            let text = $(this).val();
            let slugInput = $(this).parents('.row').find('[id*=slug]');
            $.post('{{route('admin.generateSlug')}}',{text},function(slug){
                slugInput.val(slug);
            })
        });

        $(document).on('click','#openMediaManagerPDF',function(){
            pashayev.selectMedia(medias => {
                let media = medias[0];
                let newMedia = `
                    <a href="${media.url}" class="fancybox" data-fancybox="media">
                        <div class="image" id="${media.id}">
                            <img src="{{ asset('/panel/images/pdf.png')}}"  alt="PDF"/>
                            <div class="action deleteMedia">×</div>
                            <div class="action editMedia"><i class="icon-crop"></i></div>
                            <div class="action showMedia"><i class="icon-eye"></i></div>
                            <input type="hidden" name="pdf_media_id[]" value="${media.id}">
                        </div>
                    </a>
                `;
                $('.pdfmedia').html(newMedia);
            });
        });
    </script>
@endsection
