@foreach($subtabs as $subtab)
    @php($preSpace = str_repeat("&emsp;&emsp;",$layer-1))
    <option value="{{$subtab->tab_id}}" {{isset($parent_id) && $subtab->tab_id===$parent_id?'selected':''}}>{!! $preSpace !!}&mdash;&emsp;{{$subtab->title}}</option>
    @if($subnexttabs = $subtab->subtabs)
        @include('admin.partials.tabs.options', ['subtabs' => $subnexttabs,'layer' => $layer+1])
    @endif
@endforeach
