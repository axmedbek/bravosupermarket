@foreach($submenus as $submenu)
    @php($preSpace = str_repeat("&emsp;&emsp;",$layer-1))
    <option value="{{$submenu->menu_id}}" {{isset($pageData) && $submenu->menu_id==$pageData->menu_id?'selected':''}}>{!! $preSpace !!}&mdash;&emsp;{{$submenu->name}}</option>
    @if($submenus = $submenu->subMenus)
        @include('admin.partials.menus.options', ['submenus' => $submenus,'layer' => $layer+1])
    @endif
@endforeach
