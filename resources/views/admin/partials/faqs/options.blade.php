@foreach($subfaqs as $subfaq)
    @php($preSpace = str_repeat("&emsp;&emsp;",$layer-1))
    <option value="{{$subfaq->faq_id}}" {{isset($parent_id) && $subfaq->faq_id==$parent_id?'selected':''}}>{!! $preSpace !!}&mdash;&emsp;{{substr($subfaq->question,0,30)}}</option>
    @if($subfaqs = $subfaq->subfaqs)
        @include('admin.partials.faqs.options', ['subfaqs' => $subfaqs,'layer' => $layer+1])
    @endif
@endforeach
