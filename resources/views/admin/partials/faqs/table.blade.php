@php($color = generateHEXColor($existColors))
@foreach($subfaqs as $subfaq)
    @php($preSpace = str_repeat("&emsp;&emsp;",$layer-1))
    <tr>
        @permission('delete-faqs')
        <td><input type="checkbox" class="form-check-input-styled" name="selecteds" value="{{$subfaq['faq_id']}}"></td>
        @endpermission
        <td>{{$subfaq['faq_id']}}</td>
        <td>{!! $preSpace !!}&mdash;&emsp;{{$subfaq['question']}}</td>
        <td>{{$subfaq['answer']}}</td>
        <td>{{$subfaq['parent_id']}}</td>
        <td>{!! $preSpace !!}&mdash;&emsp;<span class="p-1 text-white rounded" style="background-color:#{{$color}}">{{$subfaq['order']}}</span></td>
        <td>{!! getStatus($subfaq['status']) !!}</td>
        @permission('update-faqs|delete-faqs')
        <td class="text-center">
            <div class="list-icons">
                @permission('update-faqs')
                    <a href="{{route('admin.faqs.edit',['faq' => $subfaq['faq_id']])}}" class="dropdown-item mr-0 editData"><i class="icon-pencil mr-0 text-slate"></i> </a>
                @endpermission
                @permission('delete-faqs')
                    <a data-false class="dropdown-item mr-0 delete" data-model="faqs" data-type="faq" data-id="{{$subfaq['id']}}"><i class="icon-bin mr-0 text-danger"></i> </a>
                @endpermission
            </div>
        </td>
        @endpermission
    </tr>

    @if($subfaqs = $subfaq->subFaqs)
        @php($existColors[] = $color)
        @include('admin.partials.faqs.table', ['subfaqs' => $subfaqs,'layer' => $layer+1,'existColors' => $existColors])
    @endif
@endforeach
