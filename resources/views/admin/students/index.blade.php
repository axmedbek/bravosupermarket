@extends('admin.layouts.layout')


@section('styles')
    <style>
        #roles tr, #roles td, #roles th{
            border:1px solid #ebebeb;
            padding: 10px;
        }
    </style>
@endsection

@section('content')

    <div class="page-header page-header-light" style="border-left: 1px solid #ddd; border-right: 1px solid #ddd;">

        <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline py-2">
            <div class="d-flex">
                <div class="breadcrumb">
                    <a href="{{route('admin.dashboard')}}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> {{translate('breadcrumb.dashboard')}}</a>
                    <span class="breadcrumb-item active">{{translate('breadcrumb.students')}}</span>
                </div>
            </div>
            <div class="text-right">
                <a href="{{route('admin.students.create')}}" class="btn btn-labeled btn-labeled-right bg-slate">{{translate('breadcrumb.addStudent')}} <b><i class="icon-plus2"></i></b></a>
            </div>
        </div>
    </div>
    <!-- /page header -->
    <div class="content p-0">
        <div class="card border-top-0">
            <div class="card-header d-none"></div>

            @if(session()->has('success'))
                {!! alert(session()->get('success')) !!}
            @endif
            <div class="card-body">
                <table class="table datatable-pages dataTable no-footer" data-model="students" width="100%">
                    <thead>
                        <tr>
                            <th class="no-sort sorting_disabled" style="width: 20px;"><input type="checkbox" class="selectAll form-check-input-styled" data-target="selecteds"></th>
                            <th style="width: 100px">ID</th>
                            <th>{{translate('table.locale')}}</th>
                            <th>{{translate('table.name')}}</th>
                            <th>{{translate('table.title')}}</th>
                            <th>{{translate('table.author')}}</th>
                            <th>{{translate('table.group')}}</th>
                            <th>{{translate('table.date')}}</th>
                            <th>{{translate('table.status')}}</th>
                            <th class="text-center no-sort" style="width: 100px">{{translate('table.actions')}}</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($students as $student)
                            <tr>
                                <td><input type="checkbox" class="form-check-input-styled" name="selecteds" value="{{$student->student_id}}"></td>
                                <td>{{$student->student_id}}</td>
                                <td>{{$student->locale}}</td>
                                <td>{{$student->name}}</td>
                                <td>{{$student->title}}</td>
                                <td>{{$student->author->name}}</td>
                                <td>{{$student->group}}</td>
                                <td>{{humanDate($student->date)}}</td>
                                <td>{!! getStatus($student->status) !!}</td>

                                <td class="text-center">
                                    <div class="list-icons">
                                        <a href="{{route('admin.students.edit',['student' => $student->student_id])}}" class="dropdown-item mr-0"><i class="icon-pencil mr-0 text-slate"></i> </a>
                                        <a data-false class="dropdown-item mr-0 delete" data-model="students" data-type="student" data-id="{{$student['id']}}"><i class="icon-bin mr-0 text-danger"></i> </a>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                    <tfoot>
                    <tr>
                        <td style="padding: .75rem 0;" colspan="10"><button class="btn btn-danger btn-labeled btn-labeled-right deleteAll" disabled><span>{{translate('common.delete')}}</span><b><i class="icon-bin"></i></b></button></td>
                    </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>

    <style>
        .datatable-scroll {
            overflow-x: auto !important;
        }
    </style>
@endsection

@section('scripts')
    <script>
        $(function(){
            $('.datatable-pages').table(6,'desc');
        });

        $(document).on('click','.getLink',function () {
            let link = $(this).data('link');
            pashayev.copyClipboard(link);
            pashayev.notify({
                type:'success',
                text:pashayev.translate('common.linkCopied')
            });
        });
    </script>
@endsection
