@extends('admin.layouts.layout')


@section('styles')
    <style>
        #roles tr, #roles td, #roles th{
            border:1px solid #ebebeb;
            padding: 10px;
        }
    </style>
@endsection

@section('content')

    <div class="page-header page-header-light" style="border-left: 1px solid #ddd; border-right: 1px solid #ddd;">

        <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline py-2">
            <div class="d-flex">
                <div class="breadcrumb">
                    <a href="{{route('admin.dashboard')}}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> {{translate('breadcrumb.dashboard')}}</a>
                    <span class="breadcrumb-item active">{{translate('breadcrumb.cards')}}</span>
                </div>
            </div>
            @permission('create-cards')
            <div class="text-right">
                <a href="{{route('admin.cards.create')}}" class="btn btn-labeled btn-labeled-right bg-slate">{{translate('breadcrumb.addCard')}} <b><i class="icon-plus2"></i></b></a>
            </div>
            @endpermission
        </div>
    </div>
    <!-- /page header -->
    <div class="content p-0">
        <div class="card border-top-0">
            <div class="card-header d-none"></div>

            @if(session()->has('success'))
                {!! alert(session()->get('success')) !!}
            @endif
            <div class="card-body">
                <table class="table datatable-posts dataTable no-footer" data-model="banners" width="100%">
                    <thead>
                    <tr>
                        <th class="no-sort sorting_disabled" style=""></th>
                        <th class="no-sort sorting_disabled" style="width: 20px;"><input type="checkbox" class="selectAll form-check-input-styled" data-target="selecteds"></th>
                        <th style="width: 100px">ID</th>
                        <th>{{translate('table.title')}}</th>
                        <th>{{translate('table.target')}}</th>
                        <th>{{translate('table.style')}}</th>
                        <th>{{translate('table.author')}}</th>
                        <th>{{translate('table.order')}}</th>
                        <th>{{translate('table.status')}}</th>
                        @permission('update-cards|delete-cards')
                        <th class="text-center no-sort" style="width: 100px">{{translate('table.actions')}}</th>
                        @endpermission
                    </tr>
                    </thead>
                    <tbody class="drag-container" id="drag-dragger">
                    @foreach($cards as $card)
                        <tr id="{{$card->id}}">
                            <td style="width: 30px;padding:0 0 0 1.25rem"><i class="icon-move dragula-handle"></i></td>
                            <td><input type="checkbox" class="form-check-input-styled" name="selecteds" value="{{$card->id}}"></td>
                            <td>{{$card->id}}</td>
                            <td>{{$card->title}}</td>
                            <td>54</td>
                            <td>4565</td>
                            <td>{{$card->author->name}}</td>
                            <td>{{$card->order}}</td>
                            <td>{!! getStatus($card->status) !!}</td>
                            @permission('update-cards|delete-cards')
                            <td class="text-center">
                                <div class="list-icons">
                                    @permission('update-cards')
                                    <a href="{{route('admin.cards.edit',['card' => $card->card_id])}}" class="dropdown-item mr-0"><i class="icon-pencil mr-0 text-slate"></i> </a>
                                    @endpermission
                                    @permission('delete-cards')
                                    <a data-false class="dropdown-item mr-0 delete" data-model="cards" data-type="vacancy" data-id="{{$card->id}}"><i class="icon-bin mr-0 text-danger"></i> </a>
                                    @endpermission
                                </div>
                            </td>
                            @endpermission
                        </tr>
                    @endforeach
                    </tbody>
                    @permission('delete-cards')
                    <tfoot>
                    <tr>
                        <td style="padding: .75rem 0;" colspan="11"><button class="btn btn-danger btn-labeled btn-labeled-right deleteAll" disabled><span>{{translate('common.delete')}}</span><b><i class="icon-bin"></i></b></button></td>
                    </tr>
                    </tfoot>
                    @endpermission
                </table>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    <script>
        $(function(){
            $('.datatable-posts').table(6,'asc');
            let drag = dragula([document.getElementById('drag-dragger')], {
                mirrorContainer: document.querySelector('.drag-container')
            });

            drag.on('dragend',function(){
                let reorders = {};
                $('tbody tr').each((order,element) => {
                    let dataId = $(element).attr('id');
                    order++;
                    reorders[dataId] = order;
                });
                pashayev.startLoader();
                pashayev.request({
                    url:'{{route('admin.reorder')}}',
                    input:{
                        reorders,
                        model:'Card'
                    },
                    success:function(response){
                        pashayev.notify({
                            type:'success',
                            text:response.message
                        });
                        pashayev.stopLoader();
                    }
                });

            });

        });
    </script>
@endsection
