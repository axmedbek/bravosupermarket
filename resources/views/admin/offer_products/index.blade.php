@extends('admin.layouts.layout')


@section('styles')
    <style>
        #roles tr, #roles td, #roles th{
            border:1px solid #ebebeb;
            padding: 10px;
        }
    </style>
@endsection

@section('content')

    <div class="page-header page-header-light" style="border-left: 1px solid #ddd; border-right: 1px solid #ddd;">

        <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline py-2">
            <div class="d-flex">
                <div class="breadcrumb">
                    <a href="{{route('admin.dashboard')}}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> {{translate('breadcrumb.dashboard')}}</a>
                    <span class="breadcrumb-item active">{{translate('breadcrumb.offerProducts')}}</span>
                </div>
            </div>
            @permission('create-offer-products')
            <div class="text-right">
                <a href="{{route('admin.offer-products.create')}}" class="btn btn-labeled btn-labeled-right bg-slate">{{translate('breadcrumb.addOfferProduct')}} <b><i class="icon-plus2"></i></b></a>
            </div>
            @endpermission
        </div>
    </div>
    <!-- /page header -->
    <div class="content p-0">
        <div class="card border-top-0">
            <div class="card-header d-none"></div>

            @if(session()->has('success'))
                {!! alert(session()->get('success')) !!}
            @endif
            <div class="card-body">
                <table class="table datatable-posts dataTable no-footer" data-model="offers" width="100%">
                    <thead>
                    <tr>
                        <th class="no-sort sorting_disabled" style=""></th>
                        <th class="no-sort sorting_disabled" style="width: 20px;"><input type="checkbox" class="selectAll form-check-input-styled" data-target="selecteds"></th>
                        <th style="width: 100px">ID</th>
                        <th>{{translate('table.title')}}</th>
                        <th>{{translate('table.text')}}</th>
                        <th>{{translate('table.author')}}</th>
                        <th>{{translate('table.date')}}</th>
                        <th>{{translate('table.status')}}</th>
                        @permission('update-offer-products|delete-offer-products')
                        <th class="text-center no-sort" style="width: 100px">{{translate('table.actions')}}</th>
                        @endpermission
                    </tr>
                    </thead>
                    <tbody class="drag-container" id="drag-dragger">
                    @foreach($offerProducts as $offerProduct)
                        <tr id="{{$offerProduct->offer_product_id}}">
                            <td style="width: 30px;padding:0 0 0 1.25rem"><i class="icon-move dragula-handle"></i></td>
                            <td><input type="checkbox" class="form-check-input-styled" name="selecteds" value="{{$offerProduct->offer_product_id}}"></td>
                            <td>{{$offerProduct->offer_product_id}}</td>
                            <td>{{$offerProduct->title}}</td>
                            <td>{{$offerProduct->text}}</td>
                            <td>{{$offerProduct->author->name}}</td>
                            <td>{{humanDate($offerProduct->date,'Y-m-d H:i:s','d.m.Y')??''}}</td>
                            <td>{!! getStatus($offerProduct->status) !!}</td>

                            @permission('update-offer-products|delete-offer-products')
                            <td class="text-center">
                                <div class="list-icons">
                                    @permission('update-offer-products')
                                    <a href="{{route('admin.offer-products.edit',['offer_product' => $offerProduct->offer_product_id])}}" class="dropdown-item mr-0"><i class="icon-pencil mr-0 text-slate"></i> </a>
                                    @endpermission
                                    @permission('delete-offer-products')
                                    <a data-false class="dropdown-item mr-0 delete" data-model="offer-products" data-type="offer-product" data-id="{{$offerProduct->offer_product_id}}"><i class="icon-bin mr-0 text-danger"></i> </a>
                                    @endpermission
                                </div>
                            </td>
                            @endpermission
                        </tr>
                    @endforeach
                    </tbody>
                    @permission('delete-offer-products')
                    <tfoot>
                    <tr>
                        <td style="padding: .75rem 0;" colspan="9"><button class="btn btn-danger btn-labeled btn-labeled-right deleteAll" disabled><span>{{translate('common.delete')}}</span><b><i class="icon-bin"></i></b></button></td>
                    </tr>
                    </tfoot>
                    @endpermission
                </table>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    <script>
        $(function(){
            $('.datatable-posts').table(1,'asc');

            let drag = dragula([document.getElementById('drag-dragger')], {
                mirrorContainer: document.querySelector('.drag-container')
            });
            drag.on('dragend',function(){
                let reorders = {};
                $('tbody tr').each((order,element) => {
                    let dataId = $(element).attr('id');
                    order++;
                    reorders[dataId] = order;
                });
                pashayev.startLoader();
                pashayev.request({
                    url:'{{route('admin.reorder')}}',
                    input:{
                        reorders,
                        model:'OfferProduct',
                        key_id: 'offer_product_id'
                    },
                    success:function(response){
                        pashayev.notify({
                            type:'success',
                            text:response.message
                        });
                        pashayev.stopLoader();
                    }
                });

            });
        });
    </script>
@endsection
