@extends('admin.layouts.layout')


@section('styles')
    <style>

    </style>
@endsection

@section('content')

    <!-- Page header -->
    <div class="page-header page-header-light" style="border-left: 1px solid #ddd; border-right: 1px solid #ddd;">

        <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline py-2">
            <div class="d-flex">
                <div class="breadcrumb">
                    <a href="{{route('admin.dashboard')}}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> {{translate('breadcrumb.dashboard')}}</a>
                    <a href="{{route('admin.offer-products.index')}}" class="breadcrumb-item">{{translate('breadcrumb.offerProducts')}}</a>
                    <span class="breadcrumb-item active">{{translate('breadcrumb.editOfferProduct')}}</span>
                </div>
            </div>
        </div>
    </div>
    <!-- /page header -->
    @php
        $offerProductData = null;
        foreach ($offerProducts as $offerProduct){$offerProductData = $offerProduct;}
    @endphp

    <div class="content p-0">
        <form action="{{route('admin.offer-products.update',['offer_product' => $offerProductData->offer_product_id])}}" method="post" data-reload="{{route('admin.offer-products.index')}}">
            @csrf
            @method('put')
            <div class="card border-top-0 mb-0 border-x-0">
                <div class="card-body px-0">
                    <ul class="nav nav-tabs nav-tabs-highlight">
                        @foreach(getLanguages() as $language)
                            <li class="nav-item"><a href="#tab-{{$language['key']}}" class="nav-link {{getLocale()===$language['key']?'active':''}}" data-toggle="tab">{{$language['name']}}</a></li>
                        @endforeach
                    </ul>

                    <div class="tab-content px-3">
                        @foreach(getLanguages() as $language)
                            <div class="tab-pane fade {{getLocale()===$language['key']?'active show':''}}" id="tab-{{$language['key']}}">
                                <div class="row">
                                    <div class="form-group col-lg-6 col-md-6 col-sm-6">
                                        <label for="title"><i class="icon-pencil title-icon"></i>{{translate('offerProducts.title')}}</label>
                                        <div class="input-group">
                                            <input class="form-control" id="title" name="title[{{$language['key']}}]" value="{{$offerProducts[$language['key']]->title}}">
                                        </div>
                                    </div>

                                    <div class="form-group col-lg-6 col-md-6 col-sm-6">
                                        <label for="text"><i class="icon-file-text title-icon"></i>{{translate('offerProducts.text')}}</label>
                                        <div class="input-group">
                                            <input class="form-control" id="text" name="text[{{$language['key']}}]" value="{{$offerProducts[$language['key']]->text}}">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>

                    <div class="row px-2 mx-0">


                        <div class="form-group col-lg-6 col-md-6 col-sm-6">
                            <label for="date"><i class="icon-calendar3 title-icon"></i>{{translate('offerProducts.date')}}</label>
                            <div class="input-group">
                                <input class="form-control" id="date" name="date" value="{{humanDate($offerProduct->date,'Y-m-d H:i:s','d.m.Y')??''}}">
                            </div>
                            @if($errors->has('date'))
                                <div class="text-danger">{{$errors->first('date')}}</div>
                            @endif
                        </div>

                        <div class="form-group col-lg-6 col-md-6 col-sm-6">
                            <label for="status"><i class="icon-file-check title-icon"></i>{{translate('offerProducts.status')}}</label>
                            <div class="input-group" style="padding: 7px 7px 7px 0;">
                                <input class="form-control switchery" id="status" type="checkbox" name="status" {{$offerProduct->status?'checked':''}}>
                            </div>
                        </div>
                        <div class="form-group col-lg-12 col-md-12 col-sm-6">
                            <div class="btn bg-slate" id="openMediaManager"><i class="icon-gallery"></i> {{translate('media.openMediaManager')}}</div>
                            <div class="media-area row">
                                <a href="{{$offerProductData->media->url}}" class="fancybox" data-fancybox="media">
                                    <div class="image" id="{{$offerProductData->media->id}}">
                                        @if($offerProductData->media->type==='video' && $offerProductData->media->mime_type==='youtube')
                                            <iframe src="{{generateYoutubeEmbedUrl($offerProductData->media->url)}}"></iframe>
                                        @else
                                            <img src="{{$offerProductData->media->url}}" alt="{{$offerProductData->title}}"/>
                                        @endif
                                        <div class="action deleteMedia">×</div>
                                        <div class="action editMedia"><i class="icon-crop"></i></div>
                                        <div class="action showMedia"><i class="icon-eye"></i></div>
                                        <input type="hidden" name="media[]" value="{{$offerProductData->media->id}}">
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>

                </div>

                <div class="card-footer">
                    <div class="text-right">
                        <button class="btn bg-slate submitAJAX">{{translate('offerProducts.editOfferProduct')}}</button>
                    </div>
                </div>
            </div>
        </form>
    </div>

@endsection

@section('scripts')
    <script>

        pashayev.datePicker('#date','DD.MM.YYYY');

        function beforeSubmit(){
            Object.entries(mediaTitles).forEach(([mediaId,mediaTitle]) => {
                let newMediaTitleTextInput = `<input type="hidden" name="mediaTitles[${mediaId}]" value="${mediaTitle}"/>`;
                newMediaTitleTextInput += `<input type="hidden" name="mediaTexts[${mediaId}]" value="${mediaTexts[mediaId]}"/>`;
                $('form').append(newMediaTitleTextInput);
            });
        }

        $('select.select2').select2();
    </script>
@endsection
