@extends('admin.layouts.layout')


@section('styles')
    <style>
        #roles tr, #roles td, #roles th{
            border:1px solid #ebebeb;
            padding: 10px;
        }
    </style>
@endsection

@section('content')

    <div class="page-header page-header-light" style="border-left: 1px solid #ddd; border-right: 1px solid #ddd;">

        <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline py-2">
            <div class="d-flex">
                <div class="breadcrumb">
                    <a href="{{route('admin.dashboard')}}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> {{translate('breadcrumb.dashboard')}}</a>
                    <span class="breadcrumb-item ">{{translate('breadcrumb.vacancy')}}</span>
                    <span class="breadcrumb-item active">{{ $vacancy->title }}</span>
                </div>
            </div>
            @permission('export-vacancies')
                <div class="text-right">
                    <a href="{{route('admin.vacancies.export', $vacancy->id)}}" target="_blank" class="btn btn-labeled btn-labeled-right bg-success">{{translate('breadcrumb.export')}} <b><i class="icon-file-excel"></i></b></a>
                </div>
            @endpermission
        </div>
    </div>
    <!-- /page header -->
    <div class="content p-0">
        <div class="card border-top-0">
            <div class="card-header d-none"></div>

            @if(session()->has('success'))
                {!! alert(session()->get('success')) !!}
            @endif
            <div class="card-body">
                <table class="table datatable-posts dataTable no-footer" data-model="posts" width="100%">
                    <thead>
                        <tr>
                            <th style="width: 100px">ID</th>
                            <th>{{translate('table.name')}}</th>
                            <th>{{translate('table.phone')}}</th>
                            <th>{{translate('table.experience_store_management')}}</th>
                            <th>{{translate('table.working_now')}}</th>
                            <th>{{translate('table.description')}}</th>
                            <th>{{translate('table.file')}}</th>
                            <th>{{translate('table.date')}}</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($applies as $apply)
                            <tr>
                                <td>{{ $apply->id }}</td>
                                <td>{{ $apply->name }}</td>
                                <td>{{ $apply->phone }}</td>
                                <td>{{ translate('career.'.$apply->experience_store_management) }}</td>
                                <td>{{ translate('career.'.$apply->working_now) }}</td>
                                <td>{{ $apply->description }}</td>
                                <td>

                                    @if($apply->file_first and $apply->file_second)
                                        <div class="badge bg-white border-1 border-slate" style="margin:1px 0;">
                                            <i class="icon-download"></i> <a target="_blank" href="{{ asset('uploads/'.$apply->file_first) }}" class="text-slate font-weight-bold p-1 rounded">{{ translate('career.file') }}</a>
                                        </div>
                                        <div class="badge bg-white border-1 border-slate" style="margin:1px 0;">
                                            <i class="icon-download"></i> <a target="_blank" href="{{ asset('uploads/'.$apply->file_second) }}" class="text-slate font-weight-bold p-1 rounded">{{ translate('career.file') }}</a>
                                        </div>
                                    @elseif($apply->file_first)
                                        <div class="badge bg-white border-1 border-slate" style="margin:1px 0;">
                                            <i class="icon-download"></i> <a target="_blank" href="{{ asset('uploads/'.$apply->file_first) }}" class="text-slate font-weight-bold p-1 rounded">{{ translate('career.file') }}</a>
                                        </div>
                                        @elseif($apply->file_second)
                                        <div class="badge bg-white border-1 border-slate" style="margin:1px 0;">
                                            <i class="icon-download"></i> <a target="_blank" href="{{ asset('uploads/'.$apply->file_second) }}" class="text-slate font-weight-bold p-1 rounded">{{ translate('career.file') }}</a>
                                        </div>
                                        @else
                                            <div class="badge bg-white border-1 border-danger" style="margin:1px 0;">
                                                 <a  class="text-slate font-weight-bold p-1 rounded">{{ translate('career.no_file') }}</a>
                                            </div>
                                        @endif
                                </td>
                                <td>{{ $apply->created_at }}</td>

                            </tr>
                        @endforeach
                    </tbody>

                </table>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    <script>
        $(function(){
            $('.datatable-posts').table(6,'desc');
        });
    </script>
@endsection
