@extends('admin.layouts.layout')


@section('styles')
    <style>
        #roles tr, #roles td, #roles th{
            border:1px solid #ebebeb;
            padding: 10px;
        }
    </style>
@endsection

@section('content')

    <div class="page-header page-header-light" style="border-left: 1px solid #ddd; border-right: 1px solid #ddd;">

        <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline py-2">
            <div class="d-flex">
                <div class="breadcrumb">
                    <a href="{{route('admin.dashboard')}}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> {{translate('breadcrumb.dashboard')}}</a>
                    <span class="breadcrumb-item active">{{translate('breadcrumb.tabs')}}</span>
                </div>
            </div>
{{--            @permission('create-tabs')--}}
                <div class="text-right">
                    <a href="{{route('admin.tabs.create')}}" class="btn btn-labeled btn-labeled-right bg-slate">{{translate('breadcrumb.addTab')}} <b><i class="icon-plus2"></i></b></a>
                </div>
{{--            @endpermission--}}
        </div>
    </div>
    <!-- /page header -->
    <div class="content p-0">
        <div class="card border-top-0">
            <div class="card-header d-none"></div>

            @if(session()->has('success'))
                {!! alert(session()->get('success')) !!}
            @endif
            <div class="card-body">
                <table class="table datatable-with-row-group dataTable no-footer" data-group="5" data-model="pages">
                    <thead>
                        <tr>
                            <th class="no-sort sorting_disabled" style="width: 20px;"><input type="checkbox" class="selectAll form-check-input-styled" data-target="selecteds"></th>
                            <th style="width: 100px">ID</th>
                            <th>{{translate('table.title')}}</th>
                            <th>{{translate('table.parent')}}</th>
                            <th>{{translate('table.form')}}</th>
                            <th>{{translate('table.group')}}</th>
                            <th>{{translate('table.author')}}</th>
                            <th>{{translate('table.date')}}</th>
                            <th>{{translate('table.status')}}</th>
{{--                            @permission('update-pages|delete-pages')--}}
                                <th class="text-center no-sort" style="width: 100px">{{translate('table.actions')}}</th>
{{--                            @endpermission--}}
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($tabs as $tab)
                            <tr>
                                <td><input type="checkbox" class="form-check-input-styled" name="selecteds" value="{{$tab->page_id}}"></td>
                                <td>{{$tab->tab_id}}</td>
                                <td>{{$tab->title}}</td>
                                <td>{{$tab->parent_id ?? '-'}}</td>
                                <td>{{$tab->form_template}}</td>
                                <td>{{$tab->group}}</td>
                                <td>{{$tab->author->name}}</td>
                                <td>{{humanDate($tab->date)}}</td>
                                <td>{!! getStatus($tab->status) !!}</td>

                                @permission('update-pages|delete-pages')
                                    <td class="text-center">
                                        <div class="list-icons">
                                            @permission('update-pages')
                                                <a href="{{route('admin.tabs.edit',['tab' => $tab->tab_id])}}" class="dropdown-item mr-0"><i class="icon-pencil mr-0 text-slate"></i> </a>
                                            @endpermission
                                            @permission('delete-pages')
                                                <a data-false class="dropdown-item mr-0 delete" data-model="tabs" data-type="tab" data-id="{{$tab['id']}}"><i class="icon-bin mr-0 text-danger"></i> </a>
                                            @endpermission
                                        </div>
                                    </td>
                                @endpermission
                            </tr>
                        @endforeach
                    </tbody>
                    @permission('delete-pages')
                        <tfoot>
                            <tr>
                                <td style="padding: .75rem 0;" colspan="10"><button class="btn btn-danger btn-labeled btn-labeled-right deleteAll" disabled><span>{{translate('common.delete')}}</span><b><i class="icon-bin"></i></b></button></td>
                            </tr>
                        </tfoot>
                    @endpermission
                </table>
            </div>
        </div>
    </div>


    <style>
        .datatable-scroll {
            overflow-x: auto !important;
        }
    </style>
@endsection

@section('scripts')
    <script>
        $(function(){
            $('.datatable-pages').table(6,'desc');
        });
    </script>
@endsection
