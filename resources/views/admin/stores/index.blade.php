@extends('admin.layouts.layout')


@section('styles')
    <style>
        #roles tr, #roles td, #roles th{
            border:1px solid #ebebeb;
            padding: 10px;
        }
        table.dataTable tr.dtrg-group.dtrg-level-0 td {
            background: #607d8b40;
        }
    </style>
@endsection

@section('content')

    <div class="page-header page-header-light" style="border-left: 1px solid #ddd; border-right: 1px solid #ddd;">

        <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline py-2">
            <div class="d-flex">
                <div class="breadcrumb">
                    <a href="{{route('admin.dashboard')}}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> {{translate('breadcrumb.stores')}}</a>
                    <span class="breadcrumb-item active">{{translate('breadcrumb.stores')}}</span>
                </div>
            </div>
            @permission('create-stores')
                <div class="text-right">
                    <a href="{{route('admin.stores.create')}}" class="btn btn-labeled btn-labeled-right bg-slate addData">{{translate('breadcrumb.addStore')}} <b><i class="icon-plus2"></i></b></a>
                </div>
            @endpermission
        </div>
    </div>
    <!-- /page header -->
    <div class="content p-0">
        <div class="card border-top-0">
            <div class="card-header d-none"></div>

            @if(session()->has('success'))
                {!! alert(session()->get('success')) !!}
            @endif
            <div class="card-body">
                <table class="table datatable-basic-no-order dataTable no-footer" data-model="stores" width="100%">
                    <thead>
                        <tr>
                            @permission('delete-stores')
                                <th class="no-sort sorting_disabled" style="width: 20px;"><input type="checkbox" class="selectAll form-check-input-styled" data-target="selecteds"></th>
                            @endpermission
                            <th class="no-sort sorting_disabled" style="width: 100px">ID</th>
                            <th class="no-sort sorting_disabled">{{translate('table.name')}}</th>
                            <th class="no-sort sorting_disabled">{{translate('table.address')}}</th>
                            <th class="no-sort sorting_disabled">{{translate('table.phone')}}</th>
                            <th class="no-sort sorting_disabled">{{translate('table.work_times')}}</th>
                            <th class="no-sort sorting_disabled">{{translate('table.order')}}</th>
                            <th class="no-sort sorting_disabled">{{translate('table.status')}}</th>
                            @permission('update-stores|delete-stores')
                                <th class="text-center no-sort" style="width: 100px">{{translate('table.actions')}}</th>
                            @endpermission
                        </tr>
                    </thead>
                    <tbody>
                        @php($color = generateHEXColor())
                        @foreach($stores as $store)
                            <tr>
                                @permission('delete-stores')
                                    <td><input type="checkbox" class="form-check-input-styled" name="selecteds" value="{{$store->id}}"></td>
                                @endpermission
                                <td>{{$store->store_id}}</td>
                                <td>{{$store->name}}</td>
                                <td>{{$store->address}}</td>
                                <td>{{$store->phone}}</td>
                                <td>{{$store->work_times}}</td>
                                <td><span class="p-1 text-white rounded" style="background-color:#{{$color}}">{{$store->order}}</span></td>
                                <td>{!! getStatus($store->status) !!}</td>
                                @permission('update-stores|delete-stores')
                                    <td class="text-center">
                                        <div class="list-icons">
                                            @permission('update-stores')
                                                <a href="{{route('admin.stores.edit',['store' => $store])}}" class="dropdown-item mr-0 editData"><i class="icon-pencil mr-0 text-slate"></i> </a>
                                            @endpermission
                                            @permission('delete-stores')
                                                <a data-false class="dropdown-item mr-0 delete" data-model="stores" data-type="store" data-id="{{$store->id}}"><i class="icon-bin mr-0 text-danger"></i> </a>
                                            @endpermission
                                        </div>
                                    </td>
                                @endpermission
                            </tr>
                        @endforeach
                    </tbody>
                    @permission('delete-stores')
                        <tfoot>
                            <tr>
                                <td style="padding: .75rem 0;" colspan="9"><button class="btn btn-danger btn-labeled btn-labeled-right deleteAll" disabled><span>{{translate('common.delete')}}</span><b><i class="icon-bin"></i></b></button></td>
                            </tr>
                        </tfoot>
                    @endpermission
                </table>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    <script>

    </script>
@endsection
