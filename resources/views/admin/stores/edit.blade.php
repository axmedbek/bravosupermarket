@extends('admin.layouts.layout')


@section('styles')
    <style>
        #roles tr, #roles td, #roles th{
            border:1px solid #ebebeb;
            padding: 10px;
        }
        #infos .row {
            border-bottom: 1px solid #ebebeb;
            margin: 10px 0;
        }
        #infos .row:last-child {
            border-bottom: 0;
        }
        .select2-selection--multiple .select2-search--inline:first-child .select2-search__field {
            padding-left: .5rem;
        }
    </style>
@endsection

@section('content')

    <!-- Page header -->
    <div class="page-header page-header-light" style="border-left: 1px solid #ddd; border-right: 1px solid #ddd;">

        <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline py-2">
            <div class="d-flex">
                <div class="breadcrumb">
                    <a href="{{route('admin.dashboard')}}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> {{translate('breadcrumb.dashboard')}}</a>
                    <a href="{{route('admin.stores.index')}}" class="breadcrumb-item">{{translate('breadcrumb.stores')}}</a>
                    <span class="breadcrumb-item active">{{$store['name']}}</span>
                </div>
            </div>
        </div>
    </div>
    <!-- /page header -->

    <div class="content p-0">
        <form action="{{route('admin.stores.update',['store' => $store['id']])}}" method="post" data-reload="reload">
            @csrf
            @method('PUT')
            <div class="card border-top-0 mb-0">
                @if(session()->has('success'))
                    {!! alert(session()->get('success')) !!}
                @endif
                <div class="card-body px-0">
                    <ul class="nav nav-tabs nav-tabs-highlight">
                        @foreach(getLanguages() as $language)
                            <li class="nav-item">
                                <a href="#tab-{{$language['key']}}" class="nav-link {{getLocale()===$language['key']?'active':''}}" data-toggle="tab">{{$language['name']}}</a>
                            </li>
                        @endforeach
                    </ul>

                    <div class="tab-content px-3">
                        @foreach(getLanguages() as $language)
                            <div class="tab-pane fade {{getLocale()===$language['key']?'active show':''}}"
                                 id="tab-{{$language['key']}}">
                                <div class="row">
                                    <div class="form-group col-lg-6 col-md-6 col-sm-6">
                                        <label for="name-{{$language['key']}}">{{translate('stores.name')}}</label>
                                        <div class="input-group">
                                            <input class="form-control" id="name-{{$language['key']}}"
                                                   name="name[{{$language['key']}}]" value="{{ $store[$language['key']] ? $store[$language['key']]->name : '' }}">
                                        </div>
                                        @if($errors->has('name'))
                                            <div class="text-danger">{{$errors->first('name')}}</div>
                                        @endif
                                    </div>
                                    <div class="form-group col-lg-6 col-md-6 col-sm-6">
                                        <label
                                            for="address-{{$language['key']}}">{{translate('stores.address')}}</label>
                                        <div class="input-group">
                                            <textarea class="form-control"
                                                      id="address-{{$language['key']}}"
                                                      name="address[{{$language['key']}}]"
                                                      cols="30"
                                                      rows="5">{{ $store[$language['key']] ? $store[$language['key']]->address : '' }}</textarea>
                                        </div>
                                        @if($errors->has('address'))
                                            <div class="text-danger">{{$errors->first('address')}}</div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                    <div class="row px-2 mx-0">

                        <div class="row" style="border: 1px solid #e0d4d4;padding: 15px;margin: 15px;">
                            <div class="form-group col-lg-12 col-md-12 col-sm-12">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="input-group">
                                            <input class="form-control" id="google_map_url" name="google_map_url" placeholder="google map url"/>
                                            <button class="btn btn-success getCoordsBtn">Get Coordinates</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group col-lg-6 col-md-6 col-sm-6">
                                <label for="latitude">{{translate('stores.latitude')}}</label>
                                <div class="input-group">
                                    <input class="form-control" id="latitude" name="latitude" value="{{ $store->latitude }}"/>
                                </div>
                                @if($errors->has('latitude'))
                                    <div class="text-danger">{{$errors->first('latitude')}}</div>
                                @endif
                            </div>

                            <div class="form-group col-lg-6 col-md-6 col-sm-6">
                                <label for="longitude">{{translate('stores.longitude')}}</label>
                                <div class="input-group">
                                    <input class="form-control" id="longitude" name="longitude" value="{{ $store->longitude }}"/>
                                </div>
                                @if($errors->has('longitude'))
                                    <div class="text-danger">{{$errors->first('longitude')}}</div>
                                @endif
                            </div>
                        </div>

                        <div class="form-group col-lg-6 col-md-6 col-sm-6">
                            <label for="phone">{{translate('stores.phone')}}</label>
                            <div class="input-group">
                                <input class="form-control" id="phone" name="phone" value="{{ $store->phone }}"/>
                            </div>
                            @if($errors->has('phone'))
                                <div class="text-danger">{{$errors->first('phone')}}</div>
                            @endif
                        </div>


                        <div class="form-group col-lg-6 col-md-6 col-sm-6">
                            <label for="work_times">{{translate('stores.work_times')}}</label>
                            <div class="input-group">
                                <input class="form-control" id="work_times" name="work_times" value="{{ $store->work_times }}"/>
                            </div>
                            @if($errors->has('work_times'))
                                <div class="text-danger">{{$errors->first('work_times')}}</div>
                            @endif
                        </div>

                        <div class="form-group col-lg-6 col-md-6 col-sm-6">
                            <label for="code">{{translate('stores.code')}}</label>
                            <div class="input-group">
                                <input class="form-control" id="code" name="code" value="{{ $store->code }}"/>
                            </div>
                            @if($errors->has('code'))
                                <div class="text-danger">{{$errors->first('code')}}</div>
                            @endif
                        </div>


                        <div class="form-group col-lg-6 col-md-6 col-sm-6">
                            <label for="format">{{translate('stores.format')}}</label>
                            <div class="input-group">
                                <input class="form-control" id="format" name="format" value="{{ $store->format }}"/>
                            </div>
                            @if($errors->has('format'))
                                <div class="text-danger">{{$errors->first('format')}}</div>
                            @endif
                        </div>

                        <div class="form-group col-lg-6 col-md-6 col-sm-6">
                            <label for="order">{{translate('stores.order')}}</label>
                            <div class="input-group">
                                <input class="form-control" id="order" name="order" value="{{$store->order}}">
                            </div>
                            @if($errors->has('order'))
                                <div class="text-danger">{{$errors->first('order')}}</div>
                            @endif
                        </div>

                        <div class="form-group col-lg-6 col-md-6 col-sm-6">
                            <label for="status">{{translate('stores.status')}}</label>
                            <div class="input-group">
                                <input class="form-control switchery" id="status" type="checkbox" name="status" {{$store['status']?'checked':''}}>
                            </div>
                        </div>

                    </div>


                </div>
                <div class="card-footer">
                    <div class="text-right">
                        <button class="btn bg-slate submitAJAX">{{translate('stores.editStore')}}</button>
                    </div>
                </div>
            </div>
        </form>
    </div>

@endsection

@section('scripts')
    <script>
        $(function () {

            $('.getCoordsBtn').on('click',function (e) {
                e.preventDefault();
                e.stopPropagation();

                let google_map_url = $('#google_map_url').val();
                let coords = google_map_url.split('@')[1].split('z')[0].split(',');

                if(coords.length < 3) {
                    alert("Google Map Url Problem.Please be sure inserted url is correct");
                }
                else {
                    $('input[name="latitude"]').val(coords[0]);
                    $('input[name="longitude"]').val(coords[1]);
                }
            });


            $('select.select2').select2();
            $('select.selectpicker').selectpicker();
            $('.form-check-input-styled').uniform();
            $('.switchery').each(function () {
                new Switchery($(this)[0])
            });
        });
    </script>
@endsection
