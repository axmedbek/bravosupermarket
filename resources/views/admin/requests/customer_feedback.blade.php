@extends('admin.layouts.layout')


@section('styles')
    <style>
        #roles tr, #roles td, #roles th{
            border:1px solid #ebebeb;
            padding: 10px;
        }
    </style>
@endsection

@section('content')

    <div class="page-header page-header-light" style="border-left: 1px solid #ddd; border-right: 1px solid #ddd;">

        <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline py-2">
            <div class="d-flex">
                <div class="breadcrumb">
                    <a href="{{route('admin.dashboard')}}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> {{translate('breadcrumb.dashboard')}}</a>
                    <span class="breadcrumb-item active">{{translate('breadcrumb.customer_feedback_requests')}}</span>
                </div>
            </div>
        </div>
    </div>
    <!-- /page header -->
    <div class="content p-0">
        <div class="card border-top-0">
            <div class="card-header d-none"></div>

            @if(session()->has('success'))
                {!! alert(session()->get('success')) !!}
            @endif
            <div class="card-body">
                <table class="table datatable-pages dataTable no-footer" data-model="pages" width="100%">
                    <thead>
                    <tr>
                        <th style="width: 100px">ID</th>
                        <th>{{translate('forms.firstname')}}</th>
                        <th>{{translate('forms.lastname')}}</th>
                        <th>{{translate('forms.email')}}</th>
                        <th>{{translate('forms.phone')}}</th>
                        <th>{{translate('forms.files')}}</th>
                        <th>{{translate('forms.request_type')}}</th>
                        <th>{{translate('forms.request_subject')}}</th>
                        <th>{{translate('forms.message')}}</th>
                        <th>{{translate('table.date')}}</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($items as $item)
                        <tr>
                            <td>{{$item->id}}</td>
                            <td>{{$item->firstname}}</td>
                            <td>{{$item->lastname}}</td>
                            <td>{{$item->email}}</td>
                            <td>{{$item->phone}}</td>
                            <td>
                                @foreach($item->files('customer_feedback') as $file)
                                    <a target="_blank" style="margin-bottom: 10px;" href="{{ asset('uploads/'.$file->path) }}" class="btn btn-success">{{ $file->type }} [{{ $file->file_type }}]</a>
                                @endforeach
                            </td>
                            <td>{{$item->request_type}}</td>
                            <td>{{$item->subject}}</td>
                            <td>
                                <div style="width: 200px;max-height: 200px;overflow: auto;">
                                    {{$item->comment}}
                                </div>
                            </td>
                            <td>{{$item->created_at}}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <style>
        .datatable-scroll {
            overflow-x: auto !important;
        }
    </style>
@endsection

@section('scripts')
    <script>
        $(function(){
            $('.datatable-pages').table(6,'desc');
        });

        $(document).on('click','.getLink',function () {
            let link = $(this).data('link');
            pashayev.copyClipboard(link);
            pashayev.notify({
                type:'success',
                text:pashayev.translate('common.linkCopied')
            });
        });
    </script>
@endsection
