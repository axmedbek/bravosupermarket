@extends('admin.layouts.layout')


@section('styles')
    <style>
        #roles tr, #roles td, #roles th{
            border:1px solid #ebebeb;
            padding: 10px;
        }
    </style>
@endsection

@section('content')

    <div class="page-header page-header-light" style="border-left: 1px solid #ddd; border-right: 1px solid #ddd;">

        <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline py-2">
            <div class="d-flex">
                <div class="breadcrumb">
                    <a href="{{route('admin.dashboard')}}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> {{translate('breadcrumb.dashboard')}}</a>
                    <span class="breadcrumb-item active">{{translate('breadcrumb.b2b_requests')}}</span>
                </div>
            </div>
        </div>
    </div>
    <!-- /page header -->
    <div class="content p-0">
        <div class="card border-top-0">
            <div class="card-header d-none"></div>

            @if(session()->has('success'))
                {!! alert(session()->get('success')) !!}
            @endif
            <div class="card-body">
                <table class="table datatable-pages dataTable no-footer" data-model="pages" width="100%">
                    <thead>
                    <tr>
                        <th style="width: 100px">ID</th>
                        <th>{{translate('forms.company_name')}}</th>
                        <th>{{translate('forms.presentation_company')}}</th>
                        <th>{{translate('forms.relevant_person')}}</th>
                        <th>{{translate('forms.address')}}</th>
                        <th>{{translate('forms.district')}}</th>
                        <th>{{translate('forms.city')}}</th>
                        <th>{{translate('forms.country')}}</th>
                        <th>{{translate('forms.post_code')}}</th>
                        <th>{{translate('forms.email')}}</th>
                        <th>{{translate('forms.phone')}}</th>
                        <th>{{translate('forms.category')}}</th>
                        <th>{{translate('forms.family')}}</th>
                        <th>{{translate('table.date')}}</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($items as $item)
                        <tr>
                            <td>{{$item->id}}</td>
                            <td>{{$item->company_name}}</td>
                            <td>{{$item->company_info}}</td>
                            <td>{{$item->related_person}}</td>
                            <td>{{$item->address}}</td>
                            <td>{{$item->region}}</td>
                            <td>{{$item->city}}</td>
                            <td>{{$item->country}}</td>
                            <td>{{$item->zip_code}}</td>
                            <td>{{$item->email}}</td>
                            <td>{{$item->phone}}</td>
                            <td>{{$item->category}}</td>
                            <td>{{$item->product_family}}</td>
                            <td>{{$item->created_at}}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <style>
        .datatable-scroll {
            overflow-x: auto !important;
        }
    </style>
@endsection

@section('scripts')
    <script>
        $(function(){
            $('.datatable-pages').table(6,'desc');
        });

        $(document).on('click','.getLink',function () {
            let link = $(this).data('link');
            pashayev.copyClipboard(link);
            pashayev.notify({
                type:'success',
                text:pashayev.translate('common.linkCopied')
            });
        });
    </script>
@endsection
