var smartgrid = require('smart-grid');
 


/* It's principal settings in smart grid project */
var settings = {
    outputStyle: 'sass', /* less || scss || sass || styl */
    columns: 12, /* number of grid columns */
		offset: '20px', /* gutter width px || % */
    mobileFirst: false, /* mobileFirst ? 'min-width' : 'max-width' */
    container: {
        maxWidth: '1210px', /* max-width оn very large screen */
				fields: '45px' /* side fields */,
    },
    breakPoints: {
        xl: {
            width: '1210px', /* -> @media (max-width: 1210px) */
        },
        lg: {
            width: '1100px', /* -> @media (max-width: 1100px) */
        },
        md: {
            width: '960px'
        },
        sm: {
            width: '780px'
        },
        xs: {
            width: '639px'
        },
        xs_small: {
            width: '480px'
        }
        /* 
        We can create any quantity of break points.
 
        some_name: {
            width: 'Npx',
            fields: 'N(px|%|rem)',
            offset: 'N(px|%|rem)'
        }
        */
    }
};
 

smartgrid();
smartgrid('./app/precss', settings);