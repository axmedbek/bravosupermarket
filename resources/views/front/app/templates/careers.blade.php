@extends('front.app.careerLayout')
@section('main')
    <main class="main">
        <section class="breadcrumbs">
            <div class="container">
                <div class="row">
                    <div class="breadcrumbs_col">
                        <div class="breadcrumbs__nav">
                            <a class="breadcrumbs__item preloader-overlay preloader-overlay--vertical" href="#">
                                <svg class="icon icon-home breadcrumbs__icon">
                                    <use xlink:href="{{asset('front/images/sprite/sprite.svg#home')}}"></use>
                                </svg>
                            </a>
                            <span class="breadcrumbs__item breadcrumbs__item--inactive preloader-overlay preloader-overlay--vertical">
                            <svg class="icon icon-arrow breadcrumbs__icon breadcrumbs__icon--arrow">
                                <use xlink:href="{{asset('front/images/sprite/sprite.svg#arrow')}}"></use>
                            </svg>{{$page->title}}</span>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="inner-top-thumb inner-top-thumb--white-bg section--pb-inner">
            <div class="container container--full">
                <div class="row">
                    <div class="container-inner">
                        <div class="inner-top-thumb__box preloader preloader--v" style="background: url({{$media[0]->url}}) no-repeat center/cover"></div>
                    </div>
                </div>
            </div>
        </section>
        <section class="careers-info section--pb">
            <div class="container">
                <div class="row">
                    <div class="careers-info__col">
                        <h1 class="preloader preloader--v careers-info__title">{{$page->title}}</h1>
                        {!! $page->content !!}
                    </div>
                </div>
            </div>
        </section>
        <section class="careers-news section--mb-last">
            @php($images = ['ca1','ca2','ca3'])
            @php($menus = \App\Menu::getTargetMenus('targetCareersTop'))
            <div class="container">
                <div class="row">
                    <div class="careers-news__col">
                        <div class="preloader preloader--v careers-news-box">
                            <div class="careers-news-box__inner careers-news-box__inner--width-full">
                                <a class="careers-news-box__tile careers-news-box__tile--horizontal" href="{{route('page',['slug' => $menus[0]->page['slug'],'id' => $menus[0]->page['id']])}}">
                                    <div class="careers-news-box__img careers-news-box__img--horizontal">
                                        <img class="blazy blazy--style" src="./" data-src="{{asset("front/images/main/careers/{$images[0]}.jpg")}}" alt="bravo market"
                                        style="width: 350px;height: 300px;">
                                    </div>
                                    <div class="careers-news-box__descr" style="background: radial-gradient(50% 50% at 50% 50%, #5868FB 0%, #001AFF 100%)">
                                        <h4 class="careers-news-box__title">{{$menus[0]->page['title']}}</h4>
                                        <p class="careers-news-box__sub-title"></p>
                                        <div class="careers-news-box__arrow careers-news-box__arrow--left-bottom" style="border-right-color: #001AFF"></div>
                                    </div>
                                </a>
                                <a class="careers-news-box__tile careers-news-box__tile--horizontal" href="{{route('page',['slug' => $menus[1]->page['slug'],'id' => $menus[1]->page['id']])}}">
                                    <div class="careers-news-box__descr" style="background: radial-gradient(50% 50% at 50% 50%, #93E95E 0%, #5BBC1F 100%)">
                                        <h4 class="careers-news-box__title">{{$menus[1]->page['title']}}</h4>
                                        <p class="careers-news-box__sub-title"></p>
                                        <div class="careers-news-box__arrow careers-news-box__arrow--right-bottom" style="border-left-color: #5BBC1F"></div>
                                    </div>
                                    <div class="careers-news-box__img careers-news-box__img--horizontal">
                                        <img class="blazy blazy--style" src="./" data-src="{{asset("front/images/main/careers/{$images[1]}.jpg")}}" alt="bravo market"
                                             style="width: 350px;height: 300px;">
                                    </div>
                                </a>
                            </div>
                            <div class="careers-news-box__inner careers-news-box__inner--width-half">
                                <a class="careers-news-box__tile careers-news-box__tile--vertical" href="{{route('page',['slug' => $menus[2]->page['slug'],'id' => $menus[2]->page['id']])}}">
                                    <div class="careers-news-box__img careers-news-box__img--vertical">
                                        <img class="blazy blazy--style" src="./" data-src="{{asset("front/images/main/careers/{$images[2]}.jpg")}}" alt="bravo market"
                                             style="width: 350px;height: 300px;">
                                    </div>
                                    <div class="careers-news-box__descr" style="background: radial-gradient(50% 50% at 50% 50%, #FAE794 0%, #FBD630 100%)">
                                        <h4 class="careers-news-box__title">{{$menus[2]->page['title']}}</h4>
                                        <p class="careers-news-box__sub-title"></p>
                                        <div class="careers-news-box__arrow careers-news-box__arrow--left-top" style="border-bottom-color: #FBD630"></div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
{{--                <div class="row">--}}
{{--                    <div class="careers-news__col-half">--}}
{{--                        <a class="careers-news-post" href="#">--}}
{{--                            <div class="careers-news-post__thumb">--}}
{{--                                <img class="blazy blazy--style" src="./" data-src="{{asset('front/images/main/careers/a1.jpg')}}" alt="bravo market">--}}
{{--                            </div>--}}
{{--                            <div class="careers-news-post__content">--}}
{{--                                <h5 class="careers-news-post__title">Additional block</h5>--}}
{{--                            </div>--}}
{{--                        </a>--}}
{{--                    </div>--}}
{{--                    <div class="careers-news__col-half">--}}
{{--                        <a class="careers-news-post" href="#">--}}
{{--                            <div class="careers-news-post__thumb">--}}
{{--                                <img class="blazy blazy--style" src="./" data-src="{{asset('front/images/main/careers/a2.jpg')}}" alt="bravo market">--}}
{{--                            </div>--}}
{{--                            <div class="careers-news-post__content">--}}
{{--                                <h5 class="careers-news-post__title">Additional block</h5>--}}
{{--                            </div>--}}
{{--                        </a>--}}
{{--                    </div>--}}
{{--                </div>--}}
            </div>
        </section>
    </main>
@endsection
