@extends('front.app.layout')
@section('main')

    @php
        $leaflets = \App\Leaflet::with('media')
              ->with('pdf')
              ->where('status', 1)
              ->where('locale', getLocale())
              ->orderBy('id', 'desc')
              ->limit(2)
              ->get();
    @endphp
    <main class="main">
        <section class="breadcrumbs">
            <div class="container">
                <div class="row">
                    <div class="breadcrumbs_col">
                        <div class="breadcrumbs__nav">
                            <a class="breadcrumbs__item preloader-overlay preloader-overlay--vertical" href="#">
                                <svg class="icon icon-home breadcrumbs__icon">
                                    <use xlink:href="{{asset('front/images/sprite/sprite.svg#home')}}"></use>
                                </svg>
                            </a>
                            <span class="breadcrumbs__item breadcrumbs__item--inactive preloader-overlay preloader-overlay--vertical">
                                    <svg class="icon icon-arrow breadcrumbs__icon breadcrumbs__icon--arrow">
                                        <use xlink:href="{{asset('front/images/sprite/sprite.svg#arrow')}}"></use>
                                    </svg>{{ $page->title }}</span>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="inner-top-thumb section--mb-inner">
            <div class="container container--full">
                <div class="row">
                    <div class="container-inner">
                        <div class="inner-top-thumb__box preloader preloader--v" style="background: url({{$media[0]->url}}) no-repeat center/cover">
                            <h1 class="inner-top-thumb__title"> {{ $page->title }}
                            </h1>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="leaflet section--mb-inner">
            <div class="container">
                <div class="row">
                    <div class="section section--line">
                        <h2 class="section__title preloader preloader--v">{{ translate('leaflets.desc') }}</h2>
                    </div>
                </div>
                <div class="row">
                    @foreach($leaflets as $key=>$value)
                        <div class="leaflet__col">
                            <div class="leaflet-box">
                                <p class="leaflet-box__time preloader preloader--v">{{ date('d/m', strtotime($value->start_at)) }}-{{ date('d/m', strtotime($value->end_at)) }}</p>
                                <h3 class="leaflet-box__title preloader preloader--v">{{$value->title}}</h3>
                                <a class="leaflet-box__img preloader preloader--v" href="{{ route('leaflet.Get', $value->slug) }}">
                                    <img class="blazy blazy--style" src="{{asset("front/images/main/empty_img.png")}}" data-src="{{ $value->media ? $value->media->url : '' }}" alt="bravo market logo">
                                    <span class="leaflet-box__hover-btn">
                                        <span class="leaflet-box__hover-icon">
                                            <svg class="icon icon-pdf leaflet-box__read-icon">
                                                <use xlink:href="{{asset("front/images/sprite/sprite.svg#pdf")}}"></use>
                                            </svg>
                                        </span>
                                        <span class="leaflet-box__hover-text">{{ translate('leaflets.read_online') }}</span>
                                    </span>
                                </a>
                                @if ($value->pdf)
                                    <a class="leaflet-box__link preloader preloader--v" href="{{ $value->pdf->url }}" download>
                                        <svg class="icon icon-download leaflet-box__download-icon">
                                            <use xlink:href="{{asset("front/images/sprite/sprite.svg#download")}}"></use>
                                        </svg>{{ translate('leaflets.download_pdf') }}</a>
                                @endif
                            </div>
                        </div>
                    @endforeach

                </div>
            </div>
        </section>
    </main>
@endsection
