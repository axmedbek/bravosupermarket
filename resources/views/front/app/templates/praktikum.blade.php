@extends('front.app.careerLayout')
@section('main')
    <main class="main">
        <section class="breadcrumbs">
            <div class="container">
                <div class="row">
                    <div class="breadcrumbs_col">
                        <div class="breadcrumbs__nav">
                            <a class="breadcrumbs__item breadcrumbs__item--grey-color preloader-overlay preloader-overlay--vertical"
                               href="#">
                                <svg class="icon icon-home breadcrumbs__icon">
                                    <use xlink:href="./images/sprite/sprite.svg#home"></use>
                                </svg>
                            </a>
                            <a class="breadcrumbs__item breadcrumbs__item--grey-color preloader-overlay preloader-overlay--vertical"
                               href="#">
                                <svg class="icon icon-arrow breadcrumbs__icon breadcrumbs__icon--arrow">
                                    <use xlink:href="./images/sprite/sprite.svg#arrow"></use>
                                </svg>{{ app()->getLocale() == "az" ? "Karyera" : "Career" }}</a>
                            <span class="breadcrumbs__item breadcrumbs__item--grey-color
                            breadcrumbs__item--inactive preloader-overlay preloader-overlay--vertical">
                                <svg class="icon icon-arrow breadcrumbs__icon breadcrumbs__icon--arrow">
                                    <use xlink:href="./images/sprite/sprite.svg#arrow"></use>
                                </svg>{{ $page->title }}</span>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="inner-top-thumb inner-top-thumb--white-bg section--pb-inner">
            <div class="container container--full">
                <div class="row">
                    <div class="container-inner">
                        <div class="inner-top-thumb__box inner-top-thumb__box--overlay-none preloader preloader--v"
                             style="background: url({{$media[0]->url}}) no-repeat center/cover"></div>
                    </div>
                </div>
            </div>
        </section>
        <section class="careers-info section--pb">
            <div class="container">
                <div class="row">
                    <div class="careers-info__col">
                        {!! explode("separate from here",$page->content)[0] !!}
                    </div>
                </div>
            </div>
        </section>
        <section class="careers-news section--mb section--pb section--pt">
            <div class="container">
                <div class="row">
                    @php
                        $years = \App\Student::groupBy('group')->orderByDesc('id')->take(2)->get(['group']);
                    @endphp
                    @foreach($years as $key => $year)
                        <div class="careers-news__col-half">
                            <a class="careers-news-post" href="{{ route('get.students.group',$year['group']) }}">
                                <div class="careers-news-post__thumb careers-news-post__thumb--full">
                                    <img class="blazy blazy--style" src="./"
                                         data-src="{{ asset('front/images/main/careers/b'.++$key.'.jpg') }}" alt="bravo market">
                                </div>
                            </a>
                        </div>
                    @endforeach
                </div>
            </div>
        </section>
        <section class="news-accordion section--white-bg section--pt section--pb">
            <div class="container">
                <div class="row">
                    <div class="careers-info__col">
                        @foreach(\App\Faq::getFaqsWithType('praktikum') as $parentFaq)
                            <div class="accordion__inner">
                                <h5 class="accordion__descr accordion__descr--inner-title">
                                    <svg class="icon icon-arrow accordion__arrow ">
                                        <use xlink:href="{{asset('front/images/sprite/sprite.svg#arrow')}}"></use>
                                    </svg>
                                    {{ $parentFaq['question'] }}
                                </h5>
                                <p class="accordion__text accordion__text--inner">{{ $parentFaq['answer'] }}</p>
                            </div>
                        @endforeach
                    </div>
                </div>
                <div class="row">
                    <div class="careers-info__col">
                        {!! count(explode("separate from here",$page->content)) > 1 ? explode("separate from here",$page->content)[1] : '' !!}
                    </div>
                </div>
            </div>
        </section>
    </main>
@endsection
