@extends('front.app.layout')
@section('main')
    <main class="main">
        <section class="breadcrumbs">
            <div class="container">
                <div class="row">
                    <div class="breadcrumbs_col">
                        <div class="breadcrumbs__nav">
                            <a class="breadcrumbs__item preloader-overlay preloader-overlay--vertical" href="#">
                                <svg class="icon icon-home breadcrumbs__icon">
                                    <use xlink:href="./images/sprite/sprite.svg#home"></use>
                                </svg>
                            </a>
                            <span class="breadcrumbs__item breadcrumbs__item--inactive preloader-overlay preloader-overlay--vertical">
                                    <svg class="icon icon-arrow breadcrumbs__icon breadcrumbs__icon--arrow">
                                        <use xlink:href="./images/sprite/sprite.svg#arrow"></use>
                                    </svg>{{$page->title}}</span>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="inner-top-thumb section--mb-inner">
            <div class="container container--full">
                <div class="row">
                    <div class="container-inner">
                        <div class="inner-top-thumb__box preloader preloader--v" style="background: url({{$media[0]->url}}) no-repeat center/cover">
                            <h1 class="inner-top-thumb__title"> {{$page->title}}
                            </h1>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="about">
            <div class="container">
                <div class="row">
                    <aside class="about-aside">
                        <nav class="about-aside__nav">
                            <ul class="about-aside__menu">
                                <li class="about-aside__items preloader preloader--v">
                                    <span class="about-aside__link simple-tab__link--active" data-simple-tab-nav="about-tab-1">General</span>
                                </li>
                                <li class="about-aside__items preloader preloader--v">
                                    <span class="about-aside__link simple-aside__link--active" data-simple-tab-nav="about-tab-2">Our Quality Standards</span>
                                </li>
                            </ul>
                        </nav>
                    </aside>
                    <article class="about-post">
                        <div class="about-post__col preloader preloader--v simple-tab__content--active" data-simple-tab-content="about-tab-1">
                            <h2>Hədəf</h2>
                            <h6>Hədəfimiz nədir?</h6>
                            <p>Bizim ambisiyamız Azərbaycanda ərzaq məhsullarının pərakəndə satışı üzrə həmişə milli dəyərlərə bağlı olan və ölkəyə qürur verən ən etibarlı brend olmaqdır.</p>
                            <br>
                            <br>
                            <h2>Məqsəd</h2>
                            <h6>Biz hədəfimizə necə nail olmağı planlaşdırırıq?</h6>
                            <p>Müştərilərimizi ərzaq, qeyri-ərzaq məhsulları barədə məlumatlandırmaq və beynəlxalq standartlar səviyyəsində fəaliyyət göstərməklə yanaşı, bizim məqsədimiz yüksək keyfiyyətli innovativ məhsullar təklif etmək, həmçinin müştərilərimizi
                                ruhlandırmaq, müsbət mənada təəccübləndirmək və həmişə orta gəlirli ailələr üçün münasib qiymətlər təqdim etməkdir.</p>
                            <br>
                            <br>
                            <h2>Dəyərlər</h2>
                            <h6>Bizim məramımız nədir və biz necə davranırıq?</h6>
                            <span>Dəyərlərimiz inkişafa nail olmaqda bizə kömək edir. Bizim altı </span>
                            <a href="#">əsas dəyərimiz var: </a>
                            <br>
                            <br>
                            <img class="post-img blazy blazy--style" src="./images/main/empty_img.png" data-src="./images/main/about/about_post.png" alt="bravo market logo">
                            <br>
                            <br>
                            <br>
                            <br>
                        </div>
                        <div class="about-post__col preloader preloader--v" data-simple-tab-content="about-tab-2">
                            <h2>Komfortlu alış-veriş üçün hər şərait</h2>
                            <ul>
                                <li>İnnovativ mağaza dizaynı </li>
                                <li>Sıralar arasında genişlik</li>
                                <li>Problemsiz alış-veriş üçün müasir və geniş kassalar </li>
                            </ul>
                            <br>
                            <br>
                            <h2>Mağaza standardlarında mükəmməlllik</h2>
                            <ul>
                                <li>Daim yenilənən zəngin çeşid</li>
                                <li>Qida təhlükəsizliyinə və gigiyenaya zəmanət</li>
                                <li>Aydın promo və qiymət siyasəti</li>
                            </ul>
                            <br>
                            <br>
                            <h2>Qüsursuz xidmət</h2>
                            <ul>
                                <li>Mükəmməl xidmət üçün təlimli və təcrübəli işçi heyəti</li>
                                <li>Həmişə canla-başla xidmət</li>
                            </ul>
                            <br>
                            <br>
                        </div>
                    </article>
                </div>
            </div>
        </section>
    </main>
@endsection
