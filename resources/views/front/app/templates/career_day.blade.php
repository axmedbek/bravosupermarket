@extends('front.app.careerLayout')
@section('main')
    <main class="main">
        <section class="breadcrumbs">
            <div class="container">
                <div class="row">
                    <div class="breadcrumbs_col">
                        <div class="breadcrumbs__nav">
                            <a class="breadcrumbs__item breadcrumbs__item--grey-color preloader-overlay preloader-overlay--vertical"
                               href="#">
                                <svg class="icon icon-home breadcrumbs__icon">
                                    <use xlink:href="./images/sprite/sprite.svg#home"></use>
                                </svg>
                            </a>
                            <a class="breadcrumbs__item breadcrumbs__item--grey-color preloader-overlay preloader-overlay--vertical"
                               href="#">
                                <svg class="icon icon-arrow breadcrumbs__icon breadcrumbs__icon--arrow">
                                    <use xlink:href="./images/sprite/sprite.svg#arrow"></use>
                                </svg>{{ getLocale() == "az" ? "Karyera" : "Career" }}</a>
                            <span class="breadcrumbs__item breadcrumbs__item--grey-color
                            breadcrumbs__item--inactive preloader-overlay preloader-overlay--vertical">
                                <svg class="icon icon-arrow breadcrumbs__icon breadcrumbs__icon--arrow">
                                    <use xlink:href="./images/sprite/sprite.svg#arrow"></use>
                                </svg>{{ $page->title }}</span>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="inner-top-thumb inner-top-thumb--white-bg section--pb-inner">
            <div class="container container--full">
                <div class="row">
                    <div class="container-inner">
                        <div class="inner-top-thumb__box inner-top-thumb__box--overlay-none preloader preloader--v"
                             style="background: url({{$media[0]->url}}) no-repeat center/cover"></div>
                    </div>
                </div>
            </div>
        </section>
        <section class="careers-info section--pb">
            <div class="container">
                <div class="row">
                    <div class="careers-info__col">
                        {!! $page->content !!}
                    </div>
                </div>
            </div>
        </section>
        <section class="careers-news careers-news--bg-white section--pb-inner">
            <div class="container">
                <div class="row">
                    <div class="careers-news__col-half">
                        <a class="careers-news-post" href="javascript:void(0)">
                            <div class="careers-news-post__thumb">
                                <img class="blazy blazy--style" src="./" data-src="{{ asset('front/images/main/careers/a1.jpg') }}"
                                     alt="bravo market">
                            </div>
                            <div class="careers-news-post__content">
                                <h5 class="careers-news-post__title">{{ translate('careers.hipermarkets') }}</h5>
                                <ul class="careers-news-post__menu">
                                    <li class="careers-news-post__items">{{ translate('careers.hip_address1') }}</li>
                                    <li class="careers-news-post__items">{{ translate('careers.hip_address2') }}</li>
                                </ul>
                            </div>
                        </a>
                    </div>
                    <div class="careers-news__col-half">
                        <a class="careers-news-post" href="javascript:void(0)">
                            <div class="careers-news-post__thumb">
                                <img class="blazy blazy--style" src="./" data-src="{{ asset('front/images/main/careers/a2.jpg') }}"
                                     alt="bravo market">
                            </div>
                            <div class="careers-news-post__content">
                                <h5 class="careers-news-post__title">{{ translate('careers.super_stores') }}</h5>
                                <ul class="careers-news-post__menu">
                                    <li class="careers-news-post__items">{{ translate('careers.sup_address1') }}</li>
                                    <li class="careers-news-post__items">{{ translate('careers.sup_address2') }}</li>
                                </ul>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </section>
        <section class="careers-news section--pb">
            <div class="container container--full">
                <div class="row">
                    <div class="container-inner">
                        <div class="row">
                            <div class="career-news-title">
                                <h2 class="career-news-title__name">{{ translate('common.news') }}:</h2>
                            </div>
                        </div>
                        <div class="row">
                            @foreach(\App\Post::where('category_id',2)->where('locale',getLocale())->orderBy('id')->take(4)->get() as $new)
                                <div class="careers-news__col-half careers-news__col-half--mobile">
                                    <a class="careers-news-post" href="javascript:void(0)">
                                        <div class="careers-news-post__thumb careers-news-post__thumb--svg">
                                            <img class="blazy blazy--style" src="./"
                                                 data-src="{{ asset('front/images/main/icon/news.svg') }}"
                                                 alt="bravo market">
                                        </div>
                                        <div class="careers-news-post__content">
                                            <p class="careers-news-post__descr">{!! $new['content'] !!}</p>
                                        </div>
                                    </a>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>
@endsection
