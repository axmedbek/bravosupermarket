@extends('front.app.layout')
@section('main')
    <main class="main">
        <section class="breadcrumbs">
            <div class="container">
                <div class="row">
                    <div class="breadcrumbs_col">
                        <div class="breadcrumbs__nav">
                            <a class="breadcrumbs__item preloader-overlay preloader-overlay--vertical" href="#">
                                <svg class="icon icon-home breadcrumbs__icon">
                                    <use xlink:href="{{asset('front/images/sprite/sprite.svg#home')}}"></use>
                                </svg>
                            </a>
                            <span class="breadcrumbs__item breadcrumbs__item--inactive preloader-overlay preloader-overlay--vertical">
                                    <svg class="icon icon-arrow breadcrumbs__icon breadcrumbs__icon--arrow">
                                        <use xlink:href="{{asset('front/images/sprite/sprite.svg#arrow')}}"></use>
                                    </svg>{{$page->title}}</span>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="inner-top-thumb section--mb-inner">
            <div class="container container--full">
                <div class="row">
                    <div class="container-inner">
                        <div class="inner-top-thumb__box preloader preloader--v" style="background: url({{$media[0]->url}}) no-repeat center/cover">
                            <h1 class="inner-top-thumb__title"> {{$page->title}}
                            </h1>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="about">
            <div class="container">
                <div class="row">
                    <aside class="about-aside">
                        <nav class="about-aside__nav">
                            <ul class="about-aside__menu">
                                <li class="about-aside__items preloader preloader--v">
                                    <span class="about-aside__link simple-tab__link--active" data-simple-tab-nav="about-tab-1">Feedback Form </span>
                                </li>
                                <li class="about-aside__items preloader preloader--v">
                                    <span class="about-aside__link" data-simple-tab-nav="about-tab-2">FAQ</span>
                                </li>
                                <li class="about-aside__items preloader preloader--v">
                                    <span class="about-aside__link" data-simple-tab-nav="about-tab-3" data-simple-tab-submenu="true">Consumer Protection Act</span>
                                    <ul class="about-aside__menu-inner">
                                        <li class="about-aside__items preloader preloader--v">
                                            <span class="about-aside__link" data-simple-tab-inner-nav="about-tab-3">Geri qaytarma və Dəyişdirmə siyasəti</span>
                                        </li>
                                        <li class="about-aside__items preloader preloader--v">
                                            <span class="about-aside__link" data-simple-tab-inner-nav="about-tab-4">Geri qaytarılmayan mallar haqqında məlumat</span>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </nav>
                    </aside>
                    <article class="about-post">
                        <div class="about-post__col preloader preloader--v simple-tab__content--active" data-simple-tab-content="about-tab-1">
                            <h2>Feedback Form</h2>
                            <p> <i style="color: red">* </i>- ilə işarə olunan sətirlərin doldurulması mütləqdir</p>
                            <br>
                            <br>
                            <form class="contacts-form">
                                <div class="contacts-form__row">
                                    <div class="contacts-form__col contacts-form__col--half">
                                        <label class="contacts-form__label">Müraciətin növü </label>
                                        <div class="select-wrap__text select-wrap__text--full" data-select-wrap="feedback-form-select-1" data-select-focus="false">
                                            <input class="select-wrap__input" type="hidden" name="contacts-page-family" value="Choose your info" data-select-input="feedback-form-select-1">
                                            <div class="select-wrap__selected select-wrap__selected--grey" data-selected-item="feedback-form-select-1" data-selected-focus="false">Sual</div>
                                            <svg class="icon icon-arrow select-wrap__icon-svg select-wrap__icon-svg--arrow">
                                                <use xlink:href="{{asset('front/images/sprite/sprite.svg#arrow')}}"></use>
                                            </svg>
                                            <div class="select-wrap__option select-wrap__option--form-page" data-select-option="feedback-form-select-1" data-select-option-opened="false" data-select-option-scroll="true">
                                                <span class="select-wrap__option-item select-wrap__option-item--form-page" data-select-option-items="Təklif" data-select-option-input="feedback-form-select-1">Təklif </span>
                                                <span class="select-wrap__option-item select-wrap__option-item--form-page" data-select-option-items="İrad" data-select-option-input="feedback-form-select-1">İrad </span>
                                                <span class="select-wrap__option-item select-wrap__option-item--form-page" data-select-option-items="Təşəkkür" data-select-option-input="feedback-form-select-1">Təşəkkür</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="contacts-form__col contacts-form__col--half">
                                        <label class="contacts-form__label">Müraciətin mövzusu</label>
                                        <div class="select-wrap__text select-wrap__text--full" data-select-wrap="feedback-form-select-2" data-select-focus="false">
                                            <input class="select-wrap__input" type="hidden" name="contacts-page-family" value="Choose your info" data-select-input="feedback-form-select-2">
                                            <div class="select-wrap__selected select-wrap__selected--grey" data-selected-item="feedback-form-select-2" data-selected-focus="false">Məhsul çeşidi </div>
                                            <svg class="icon icon-arrow select-wrap__icon-svg select-wrap__icon-svg--arrow">
                                                <use xlink:href="{{asset('front/images/sprite/sprite.svg#arrow')}}"></use>
                                            </svg>
                                            <div class="select-wrap__option select-wrap__option--form-page" data-select-option="feedback-form-select-2" data-select-option-opened="false" data-select-option-scroll="true">
                                                <span class="select-wrap__option-item select-wrap__option-item--form-page" data-select-option-items="Məsulun keyfiyyəti" data-select-option-input="feedback-form-select-2">Məsulun keyfiyyəti </span>
                                                <span class="select-wrap__option-item select-wrap__option-item--form-page" data-select-option-items="Qiyməti" data-select-option-input="feedback-form-select-2">Qiyməti </span>
                                                <span class="select-wrap__option-item select-wrap__option-item--form-page" data-select-option-items="Kassa xidməti" data-select-option-input="feedback-form-select-2">Kassa xidməti</span>
                                                <span class="select-wrap__option-item select-wrap__option-item--form-page" data-select-option-items="Bonus və endirim kartları" data-select-option-input="feedback-form-select-2">Bonus və endirim kartları</span>
                                                <span class="select-wrap__option-item select-wrap__option-item--form-page" data-select-option-items="Digər" data-select-option-input="feedback-form-select-2">Digər</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="contacts-form__row">
                                    <div class="contacts-form__col contacts-form__col--half">
                                        <label class="contacts-form__label">Ad, soyad <i style="color: red">* </i></label>
                                        <input class="contacts-form__input" type="text" name="feedback-user-name" placeholder="example: Harry Hole" required>
                                    </div>
                                    <div class="contacts-form__col contacts-form__col--half">
                                        <label class="contacts-form__label">Elektron ünvan <i style="color: red">* </i></label>
                                        <input class="contacts-form__input" type="email" name="feedback-user-email" placeholder="example: Art@example.com" required>
                                    </div>
                                </div>
                                <div class="contacts-form__row">
                                    <div class="contacts-form__col contacts-form__col--half">
                                        <label class="contacts-form__label">Əlaqə nömrə <i style="color: red">* </i></label>
                                        <input class="contacts-form__input" type="number" name="feedback-user-phone" placeholder="example: +994756000000" required>
                                    </div>
                                </div>
                                <div class="contacts-form__row">
                                    <div class="contacts-form__col contacts-form__col--full">
                                        <label class="contacts-form__label">Müraciətinin təsviri</label>
                                        <textarea class="contacts-form__textarea" name="feedback-user-message" placeholder="example: Please help me contact with your company"></textarea>
                                    </div>
                                </div>
                                <div class="contacts-form__row">
                                    <div class="contacts-form__col contacts-form__col--full contacts-form__col--pined-file-title">
                                        <label class="contacts-form__label">Zəruri hallarda müraciəti təsvir edən foto/video görüntüləri aşağıda əlavə edə bilərsiniz.</label>
                                    </div>
                                    <div class="contacts-form__col contacts-form__col--half">
                                        <div class="contact-form-loaded">
                                            <input class="contact-form-loaded__input" type="file" name="contacts-page-presentation-file">
                                            <button class="contact-form-loaded__close">
                                                <svg class="icon icon-close contact-form-loaded__icon-svg">
                                                    <use xlink:href="{{asset('front/images/sprite/sprite.svg#close')}}"></use>
                                                </svg>
                                            </button>
                                            <div class="contact-form-loaded__inner">
                                                <div class="contact-form-loaded__img">
                                                    <img class="contact-form-loaded__icon blazy blazy--style" src="./" data-src="{{asset('front/images/main/icon/pdf_active.svg')}}" alt="bravo market">
                                                    <img class="contact-form-loaded__icon blazy blazy--style" src="./" data-src="{{asset('front/images/main/icon/pdf.svg')}}" alt="bravo market">
                                                </div>
                                                <div class="contact-form-loaded__content">
                                                    <h4 class="contact-form-loaded__title">Əlavə №1</h4>
                                                    <p class="contact-form-loaded__descr">Formats: .pdf .png .jpeg .jpg </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="contacts-form__col contacts-form__col--half">
                                        <div class="contact-form-loaded">
                                            <input class="contact-form-loaded__input" type="file" name="contacts-page-price-file">
                                            <button class="contact-form-loaded__close">
                                                <svg class="icon icon-close contact-form-loaded__icon-svg">
                                                    <use xlink:href="{{asset('front/images/sprite/sprite.svg#close')}}"></use>
                                                </svg>
                                            </button>
                                            <div class="contact-form-loaded__inner">
                                                <div class="contact-form-loaded__img">
                                                    <img class="contact-form-loaded__icon blazy blazy--style" src="./" data-src="{{asset('front/images/main/icon/pdf_active.svg')}}" alt="bravo market">
                                                    <img class="contact-form-loaded__icon blazy blazy--style" src="./" data-src="{{asset('front/images/main/icon/pdf.svg')}}" alt="bravo market">
                                                </div>
                                                <div class="contact-form-loaded__content">
                                                    <h4 class="contact-form-loaded__title">Əlavə №2</h4>
                                                    <p class="contact-form-loaded__descr">Formats: .pdf .png .jpeg .jpg </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="contacts-form__row">
                                    <div class="contacts-form__col contacts-form__col--half">
                                        <div class="contact-form-loaded">
                                            <input class="contact-form-loaded__input" type="file" name="contacts-page-cert-file-1">
                                            <button class="contact-form-loaded__close">
                                                <svg class="icon icon-close contact-form-loaded__icon-svg">
                                                    <use xlink:href="{{asset('front/images/sprite/sprite.svg#close')}}"></use>
                                                </svg>
                                            </button>
                                            <div class="contact-form-loaded__inner">
                                                <div class="contact-form-loaded__img">
                                                    <img class="contact-form-loaded__icon blazy blazy--style" src="./" data-src="{{asset('./front/images/main/icon/pdf_active.svg')}}" alt="bravo market">
                                                    <img class="contact-form-loaded__icon blazy blazy--style" src="./" data-src="{{asset('./front/images/main/icon/pdf.svg')}}" alt="bravo market">
                                                </div>
                                                <div class="contact-form-loaded__content">
                                                    <h4 class="contact-form-loaded__title">Əlavə №3</h4>
                                                    <p class="contact-form-loaded__descr">Formats: .pdf .png .jpeg .jpg </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="contacts-form__row contacts-form__row--send-btn">
                                    <div class="contacts-form__col contacts-form__col--half">
                                        <button class="contacts-form__send-btn primary-btn primary-btn--green">
                                            <svg class="icon icon-send contacts-form__send-icon">
                                                <use xlink:href="{{asset('front/images/sprite/sprite.svg#send')}}"></use>
                                            </svg>Send Form
                                        </button>
                                    </div>
                                </div>
                            </form>
                            <br>
                            <br>
                            <br>
                            <br>
                        </div>
                        <div class="about-post__col preloader preloader--v" data-simple-tab-content="about-tab-2">
                            <h2>FAQ</h2>
                            <div class="accordion">
                                <div class="accordion__title">
                                    <h5 class="accordion__descr accordion__descr--top accordion__descr--active">
                                        <svg class="icon icon-arrow accordion__arrow accordion__arrow--top">
                                            <use xlink:href="{{asset('front/images/sprite/sprite.svg#arrow')}}"></use>
                                        </svg>General Store information
                                    </h5>
                                    <div class="accordion__inner-wrap accordion-visible">
                                        <div class="accordion__inner">
                                            <h5 class="accordion__descr accordion__descr--inner-title">
                                                <svg class="icon icon-arrow accordion__arrow ">
                                                    <use xlink:href="{{asset('front/images/sprite/sprite.svg#arrow')}}"></use>
                                                </svg>What store formats do you have?
                                            </h5>
                                            <p class="accordion__text accordion__text--inner">Answer answer answer answer answer answer answer answer answer answer answer answer answer answer answer answer</p>
                                        </div>
                                        <div class="accordion__inner">
                                            <h5 class="accordion__descr accordion__descr--inner-title">
                                                <svg class="icon icon-arrow accordion__arrow ">
                                                    <use xlink:href="{{asset('front/images/sprite/sprite.svg#arrow')}}"></use>
                                                </svg>Where can I find the working hours of my local store?
                                            </h5>
                                            <p class="accordion__text accordion__text--inner">Answer answer answer answer answer answer answer answer answer answer answer answer answer answer answer answer</p>
                                        </div>
                                        <div class="accordion__inner">
                                            <h5 class="accordion__descr accordion__descr--inner-title">
                                                <svg class="icon icon-arrow accordion__arrow ">
                                                    <use xlink:href="{{asset('front/images/sprite/sprite.svg#arrow')}}"></use>
                                                </svg>Where is my nearest store?
                                            </h5>
                                            <p class="accordion__text accordion__text--inner">Answer answer answer answer answer answer answer answer answer answer answer answer answer answer answer answer</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="accordion__title">
                                    <h5 class="accordion__descr accordion__descr--top">
                                        <svg class="icon icon-arrow accordion__arrow accordion__arrow--top">
                                            <use xlink:href="{{asset('front/images/sprite/sprite.svg#arrow')}}"></use>
                                        </svg>Payment
                                    </h5>
                                    <div class="accordion__inner-wrap">
                                        <div class="accordion__inner">
                                            <h5 class="accordion__descr accordion__descr--inner-title">
                                                <svg class="icon icon-arrow accordion__arrow ">
                                                    <use xlink:href="{{asset('front/images/sprite/sprite.svg#arrow')}}"></use>
                                                </svg>Payment
                                            </h5>
                                            <p class="accordion__text accordion__text--inner">Answer answer answer answer answer answer answer answer answer answer answer answer answer answer answer answer</p>
                                        </div>
                                        <div class="accordion__inner">
                                            <h5 class="accordion__descr accordion__descr--inner-title">
                                                <svg class="icon icon-arrow accordion__arrow ">
                                                    <use xlink:href="{{asset('front/images/sprite/sprite.svg#arrow')}}"></use>
                                                </svg>What type and methods of payment can I use in-store?
                                            </h5>
                                            <p class="accordion__text accordion__text--inner">Answer answer answer answer answer answer answer answer answer answer answer answer answer answer answer answer</p>
                                        </div>
                                        <div class="accordion__inner">
                                            <h5 class="accordion__descr accordion__descr--inner-title">
                                                <svg class="icon icon-arrow accordion__arrow ">
                                                    <use xlink:href="{{asset('front/images/sprite/sprite.svg#arrow')}}"></use>
                                                </svg>I have a gift card, how can I use it? Can I use it partly?
                                            </h5>
                                            <p class="accordion__text accordion__text--inner">Answer answer answer answer answer answer answer answer answer answer answer answer answer answer answer answer</p>
                                        </div>
                                        <div class="accordion__inner">
                                            <h5 class="accordion__descr accordion__descr--inner-title">
                                                <svg class="icon icon-arrow accordion__arrow ">
                                                    <use xlink:href="{{asset('front/images/sprite/sprite.svg#arrow')}}"></use>
                                                </svg>I have an UMICO loyal bonus card savings; can I use them during payment?
                                            </h5>
                                            <p class="accordion__text accordion__text--inner">Answer answer answer answer answer answer answer answer answer answer answer answer answer answer answer answer</p>
                                        </div>
                                        <div class="accordion__inner">
                                            <h5 class="accordion__descr accordion__descr--inner-title">
                                                <svg class="icon icon-arrow accordion__arrow ">
                                                    <use xlink:href="{{asset('front/images/sprite/sprite.svg#arrow')}}"></use>
                                                </svg>How do we earn UMİCO bonus card`s points during payment?
                                            </h5>
                                            <p class="accordion__text accordion__text--inner">Answer answer answer answer answer answer answer answer answer answer answer answer answer answer answer answer</p>
                                        </div>
                                        <div class="accordion__inner">
                                            <h5 class="accordion__descr accordion__descr--inner-title">
                                                <svg class="icon icon-arrow accordion__arrow ">
                                                    <use xlink:href="{{asset('front/images/sprite/sprite.svg#arrow')}}"></use>
                                                </svg>I have a Bravo loyal pension card; can I use it during payment?
                                            </h5>
                                            <p class="accordion__text accordion__text--inner">Answer answer answer answer answer answer answer answer answer answer answer answer answer answer answer answer</p>
                                        </div>
                                        <div class="accordion__inner">
                                            <h5 class="accordion__descr accordion__descr--inner-title">
                                                <svg class="icon icon-arrow accordion__arrow ">
                                                    <use xlink:href="{{asset('front/images/sprite/sprite.svg#arrow')}}"></use>
                                                </svg>I seem to have been charged twice the same transaction. What should I do?
                                            </h5>
                                            <p class="accordion__text accordion__text--inner">Answer answer answer answer answer answer answer answer answer answer answer answer answer answer answer answer</p>
                                        </div>
                                        <div class="accordion__inner">
                                            <h5 class="accordion__descr accordion__descr--inner-title">
                                                <svg class="icon icon-arrow accordion__arrow ">
                                                    <use xlink:href="{{asset('front/images/sprite/sprite.svg#arrow')}}"></use>
                                                </svg>What shall I do if I have been overcharged?
                                            </h5>
                                            <p class="accordion__text accordion__text--inner">Answer answer answer answer answer answer answer answer answer answer answer answer answer answer answer answer</p>
                                        </div>
                                        <div class="accordion__inner">
                                            <h5 class="accordion__descr accordion__descr--inner-title">
                                                <svg class="icon icon-arrow accordion__arrow ">
                                                    <use xlink:href="{{asset('front/images/sprite/sprite.svg#arrow')}}"></use>
                                                </svg>Can I use both Loyal Cards during payment?
                                            </h5>
                                            <p class="accordion__text accordion__text--inner">Answer answer answer answer answer answer answer answer answer answer answer answer answer answer answer answer</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="accordion__title">
                                    <h5 class="accordion__descr accordion__descr--top">
                                        <svg class="icon icon-arrow accordion__arrow accordion__arrow--top">
                                            <use xlink:href="{{asset('front/images/sprite/sprite.svg#arrow')}}"></use>
                                        </svg>Product information. Availability of products
                                    </h5>
                                    <div class="accordion__inner-wrap">
                                        <div class="accordion__inner">
                                            <h5 class="accordion__descr accordion__descr--inner-title">
                                                <svg class="icon icon-arrow accordion__arrow ">
                                                    <use xlink:href="{{asset('front/images/sprite/sprite.svg#arrow')}}"></use>
                                                </svg>Payment
                                            </h5>
                                            <p class="accordion__text accordion__text--inner">Answer answer answer answer answer answer answer answer answer answer answer answer answer answer answer answer</p>
                                        </div>
                                        <div class="accordion__inner">
                                            <h5 class="accordion__descr accordion__descr--inner-title">
                                                <svg class="icon icon-arrow accordion__arrow ">
                                                    <use xlink:href="{{asset('front/images/sprite/sprite.svg#arrow')}}"></use>
                                                </svg>What type and methods of payment can I use in-store?
                                            </h5>
                                            <p class="accordion__text accordion__text--inner">Answer answer answer answer answer answer answer answer answer answer answer answer answer answer answer answer</p>
                                        </div>
                                        <div class="accordion__inner">
                                            <h5 class="accordion__descr accordion__descr--inner-title">
                                                <svg class="icon icon-arrow accordion__arrow ">
                                                    <use xlink:href="{{asset('front/images/sprite/sprite.svg#arrow')}}"></use>
                                                </svg>I have a gift card, how can I use it? Can I use it partly?
                                            </h5>
                                            <p class="accordion__text accordion__text--inner">Answer answer answer answer answer answer answer answer answer answer answer answer answer answer answer answer</p>
                                        </div>
                                        <div class="accordion__inner">
                                            <h5 class="accordion__descr accordion__descr--inner-title">
                                                <svg class="icon icon-arrow accordion__arrow ">
                                                    <use xlink:href="{{asset('front/images/sprite/sprite.svg#arrow')}}"></use>
                                                </svg>I have an UMICO loyal bonus card savings; can I use them during payment?
                                            </h5>
                                            <p class="accordion__text accordion__text--inner">Answer answer answer answer answer answer answer answer answer answer answer answer answer answer answer answer</p>
                                        </div>
                                        <div class="accordion__inner">
                                            <h5 class="accordion__descr accordion__descr--inner-title">
                                                <svg class="icon icon-arrow accordion__arrow ">
                                                    <use xlink:href="{{asset('front/images/sprite/sprite.svg#arrow')}}"></use>
                                                </svg>How do we earn UMİCO bonus card`s points during payment?
                                            </h5>
                                            <p class="accordion__text accordion__text--inner">Answer answer answer answer answer answer answer answer answer answer answer answer answer answer answer answer</p>
                                        </div>
                                        <div class="accordion__inner">
                                            <h5 class="accordion__descr accordion__descr--inner-title">
                                                <svg class="icon icon-arrow accordion__arrow ">
                                                    <use xlink:href="{{asset('front/images/sprite/sprite.svg#arrow')}}"></use>
                                                </svg>I have a Bravo loyal pension card; can I use it during payment?
                                            </h5>
                                            <p class="accordion__text accordion__text--inner">Answer answer answer answer answer answer answer answer answer answer answer answer answer answer answer answer</p>
                                        </div>
                                        <div class="accordion__inner">
                                            <h5 class="accordion__descr accordion__descr--inner-title">
                                                <svg class="icon icon-arrow accordion__arrow ">
                                                    <use xlink:href="{{asset('front/images/sprite/sprite.svg#arrow')}}"></use>
                                                </svg>I seem to have been charged twice the same transaction. What should I do?
                                            </h5>
                                            <p class="accordion__text accordion__text--inner">Answer answer answer answer answer answer answer answer answer answer answer answer answer answer answer answer</p>
                                        </div>
                                        <div class="accordion__inner">
                                            <h5 class="accordion__descr accordion__descr--inner-title">
                                                <svg class="icon icon-arrow accordion__arrow ">
                                                    <use xlink:href="{{asset('front/images/sprite/sprite.svg#arrow')}}"></use>
                                                </svg>What shall I do if I have been overcharged?
                                            </h5>
                                            <p class="accordion__text accordion__text--inner">Answer answer answer answer answer answer answer answer answer answer answer answer answer answer answer answer</p>
                                        </div>
                                        <div class="accordion__inner">
                                            <h5 class="accordion__descr accordion__descr--inner-title">
                                                <svg class="icon icon-arrow accordion__arrow ">
                                                    <use xlink:href="{{asset('front/images/sprite/sprite.svg#arrow')}}"></use>
                                                </svg>Can I use both Loyal Cards during payment?
                                            </h5>
                                            <p class="accordion__text accordion__text--inner">Answer answer answer answer answer answer answer answer answer answer answer answer answer answer answer answer</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="accordion__title">
                                    <h5 class="accordion__descr accordion__descr--top">
                                        <svg class="icon icon-arrow accordion__arrow accordion__arrow--top">
                                            <use xlink:href="{{asset('front/images/sprite/sprite.svg#arrow')}}"></use>
                                        </svg>Product information
                                    </h5>
                                    <div class="accordion__inner-wrap">
                                        <div class="accordion__inner">
                                            <h5 class="accordion__descr accordion__descr--inner-title">
                                                <svg class="icon icon-arrow accordion__arrow ">
                                                    <use xlink:href="{{asset('front/images/sprite/sprite.svg#arrow')}}"></use>
                                                </svg>Payment
                                            </h5>
                                            <p class="accordion__text accordion__text--inner">Answer answer answer answer answer answer answer answer answer answer answer answer answer answer answer answer</p>
                                        </div>
                                        <div class="accordion__inner">
                                            <h5 class="accordion__descr accordion__descr--inner-title">
                                                <svg class="icon icon-arrow accordion__arrow ">
                                                    <use xlink:href="{{asset('front/images/sprite/sprite.svg#arrow')}}"></use>
                                                </svg>What type and methods of payment can I use in-store?
                                            </h5>
                                            <p class="accordion__text accordion__text--inner">Answer answer answer answer answer answer answer answer answer answer answer answer answer answer answer answer</p>
                                        </div>
                                        <div class="accordion__inner">
                                            <h5 class="accordion__descr accordion__descr--inner-title">
                                                <svg class="icon icon-arrow accordion__arrow ">
                                                    <use xlink:href="{{asset('front/images/sprite/sprite.svg#arrow')}}"></use>
                                                </svg>I have a gift card, how can I use it? Can I use it partly?
                                            </h5>
                                            <p class="accordion__text accordion__text--inner">Answer answer answer answer answer answer answer answer answer answer answer answer answer answer answer answer</p>
                                        </div>
                                        <div class="accordion__inner">
                                            <h5 class="accordion__descr accordion__descr--inner-title">
                                                <svg class="icon icon-arrow accordion__arrow ">
                                                    <use xlink:href="{{asset('front/images/sprite/sprite.svg#arrow')}}"></use>
                                                </svg>I have an UMICO loyal bonus card savings; can I use them during payment?
                                            </h5>
                                            <p class="accordion__text accordion__text--inner">Answer answer answer answer answer answer answer answer answer answer answer answer answer answer answer answer</p>
                                        </div>
                                        <div class="accordion__inner">
                                            <h5 class="accordion__descr accordion__descr--inner-title">
                                                <svg class="icon icon-arrow accordion__arrow ">
                                                    <use xlink:href="{{asset('front/images/sprite/sprite.svg#arrow')}}"></use>
                                                </svg>How do we earn UMİCO bonus card`s points during payment?
                                            </h5>
                                            <p class="accordion__text accordion__text--inner">Answer answer answer answer answer answer answer answer answer answer answer answer answer answer answer answer</p>
                                        </div>
                                        <div class="accordion__inner">
                                            <h5 class="accordion__descr accordion__descr--inner-title">
                                                <svg class="icon icon-arrow accordion__arrow ">
                                                    <use xlink:href="{{asset('front/images/sprite/sprite.svg#arrow')}}"></use>
                                                </svg>I have a Bravo loyal pension card; can I use it during payment?
                                            </h5>
                                            <p class="accordion__text accordion__text--inner">Answer answer answer answer answer answer answer answer answer answer answer answer answer answer answer answer</p>
                                        </div>
                                        <div class="accordion__inner">
                                            <h5 class="accordion__descr accordion__descr--inner-title">
                                                <svg class="icon icon-arrow accordion__arrow ">
                                                    <use xlink:href="{{asset('front/images/sprite/sprite.svg#arrow')}}"></use>
                                                </svg>I seem to have been charged twice the same transaction. What should I do?
                                            </h5>
                                            <p class="accordion__text accordion__text--inner">Answer answer answer answer answer answer answer answer answer answer answer answer answer answer answer answer</p>
                                        </div>
                                        <div class="accordion__inner">
                                            <h5 class="accordion__descr accordion__descr--inner-title">
                                                <svg class="icon icon-arrow accordion__arrow ">
                                                    <use xlink:href="{{asset('front/images/sprite/sprite.svg#arrow')}}"></use>
                                                </svg>What shall I do if I have been overcharged?
                                            </h5>
                                            <p class="accordion__text accordion__text--inner">Answer answer answer answer answer answer answer answer answer answer answer answer answer answer answer answer</p>
                                        </div>
                                        <div class="accordion__inner">
                                            <h5 class="accordion__descr accordion__descr--inner-title">
                                                <svg class="icon icon-arrow accordion__arrow ">
                                                    <use xlink:href="{{asset('front/images/sprite/sprite.svg#arrow')}}"></use>
                                                </svg>Can I use both Loyal Cards during payment?
                                            </h5>
                                            <p class="accordion__text accordion__text--inner">Answer answer answer answer answer answer answer answer answer answer answer answer answer answer answer answer</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="accordion__title">
                                    <h5 class="accordion__descr accordion__descr--top">
                                        <svg class="icon icon-arrow accordion__arrow accordion__arrow--top">
                                            <use xlink:href="{{asset('front/images/sprite/sprite.svg#arrow')}}"></use>
                                        </svg>Refund and Return Policy - Our promise
                                    </h5>
                                    <div class="accordion__inner-wrap">
                                        <div class="accordion__inner">
                                            <h5 class="accordion__descr accordion__descr--inner-title">
                                                <svg class="icon icon-arrow accordion__arrow ">
                                                    <use xlink:href="{{asset('front/images/sprite/sprite.svg#arrow')}}"></use>
                                                </svg>Payment
                                            </h5>
                                            <p class="accordion__text accordion__text--inner">Answer answer answer answer answer answer answer answer answer answer answer answer answer answer answer answer</p>
                                        </div>
                                        <div class="accordion__inner">
                                            <h5 class="accordion__descr accordion__descr--inner-title">
                                                <svg class="icon icon-arrow accordion__arrow ">
                                                    <use xlink:href="{{asset('front/images/sprite/sprite.svg#arrow')}}"></use>
                                                </svg>What type and methods of payment can I use in-store?
                                            </h5>
                                            <p class="accordion__text accordion__text--inner">Answer answer answer answer answer answer answer answer answer answer answer answer answer answer answer answer</p>
                                        </div>
                                        <div class="accordion__inner">
                                            <h5 class="accordion__descr accordion__descr--inner-title">
                                                <svg class="icon icon-arrow accordion__arrow ">
                                                    <use xlink:href="{{asset('front/images/sprite/sprite.svg#arrow')}}"></use>
                                                </svg>I have a gift card, how can I use it? Can I use it partly?
                                            </h5>
                                            <p class="accordion__text accordion__text--inner">Answer answer answer answer answer answer answer answer answer answer answer answer answer answer answer answer</p>
                                        </div>
                                        <div class="accordion__inner">
                                            <h5 class="accordion__descr accordion__descr--inner-title">
                                                <svg class="icon icon-arrow accordion__arrow ">
                                                    <use xlink:href="{{asset('front/images/sprite/sprite.svg#arrow')}}"></use>
                                                </svg>I have an UMICO loyal bonus card savings; can I use them during payment?
                                            </h5>
                                            <p class="accordion__text accordion__text--inner">Answer answer answer answer answer answer answer answer answer answer answer answer answer answer answer answer</p>
                                        </div>
                                        <div class="accordion__inner">
                                            <h5 class="accordion__descr accordion__descr--inner-title">
                                                <svg class="icon icon-arrow accordion__arrow ">
                                                    <use xlink:href="{{asset('front/images/sprite/sprite.svg#arrow')}}"></use>
                                                </svg>How do we earn UMİCO bonus card`s points during payment?
                                            </h5>
                                            <p class="accordion__text accordion__text--inner">Answer answer answer answer answer answer answer answer answer answer answer answer answer answer answer answer</p>
                                        </div>
                                        <div class="accordion__inner">
                                            <h5 class="accordion__descr accordion__descr--inner-title">
                                                <svg class="icon icon-arrow accordion__arrow ">
                                                    <use xlink:href="{{asset('front/images/sprite/sprite.svg#arrow')}}"></use>
                                                </svg>I have a Bravo loyal pension card; can I use it during payment?
                                            </h5>
                                            <p class="accordion__text accordion__text--inner">Answer answer answer answer answer answer answer answer answer answer answer answer answer answer answer answer</p>
                                        </div>
                                        <div class="accordion__inner">
                                            <h5 class="accordion__descr accordion__descr--inner-title">
                                                <svg class="icon icon-arrow accordion__arrow ">
                                                    <use xlink:href="{{asset('front/images/sprite/sprite.svg#arrow')}}"></use>
                                                </svg>I seem to have been charged twice the same transaction. What should I do?
                                            </h5>
                                            <p class="accordion__text accordion__text--inner">Answer answer answer answer answer answer answer answer answer answer answer answer answer answer answer answer</p>
                                        </div>
                                        <div class="accordion__inner">
                                            <h5 class="accordion__descr accordion__descr--inner-title">
                                                <svg class="icon icon-arrow accordion__arrow ">
                                                    <use xlink:href="{{asset('front/images/sprite/sprite.svg#arrow')}}"></use>
                                                </svg>What shall I do if I have been overcharged?
                                            </h5>
                                            <p class="accordion__text accordion__text--inner">Answer answer answer answer answer answer answer answer answer answer answer answer answer answer answer answer</p>
                                        </div>
                                        <div class="accordion__inner">
                                            <h5 class="accordion__descr accordion__descr--inner-title">
                                                <svg class="icon icon-arrow accordion__arrow ">
                                                    <use xlink:href="{{asset('front/images/sprite/sprite.svg#arrow')}}"></use>
                                                </svg>Can I use both Loyal Cards during payment?
                                            </h5>
                                            <p class="accordion__text accordion__text--inner">Answer answer answer answer answer answer answer answer answer answer answer answer answer answer answer answer</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="accordion__title">
                                    <h5 class="accordion__descr accordion__descr--top">
                                        <svg class="icon icon-arrow accordion__arrow accordion__arrow--top">
                                            <use xlink:href="{{asset('front/images/sprite/sprite.svg#arrow')}}"></use>
                                        </svg>Technical advice and support
                                    </h5>
                                    <div class="accordion__inner-wrap">
                                        <div class="accordion__inner">
                                            <h5 class="accordion__descr accordion__descr--inner-title">
                                                <svg class="icon icon-arrow accordion__arrow ">
                                                    <use xlink:href="{{asset('front/images/sprite/sprite.svg#arrow')}}"></use>
                                                </svg>Payment
                                            </h5>
                                            <p class="accordion__text accordion__text--inner">Answer answer answer answer answer answer answer answer answer answer answer answer answer answer answer answer</p>
                                        </div>
                                        <div class="accordion__inner">
                                            <h5 class="accordion__descr accordion__descr--inner-title">
                                                <svg class="icon icon-arrow accordion__arrow ">
                                                    <use xlink:href="{{asset('front/images/sprite/sprite.svg#arrow')}}"></use>
                                                </svg>What type and methods of payment can I use in-store?
                                            </h5>
                                            <p class="accordion__text accordion__text--inner">Answer answer answer answer answer answer answer answer answer answer answer answer answer answer answer answer</p>
                                        </div>
                                        <div class="accordion__inner">
                                            <h5 class="accordion__descr accordion__descr--inner-title">
                                                <svg class="icon icon-arrow accordion__arrow ">
                                                    <use xlink:href="{{asset('front/images/sprite/sprite.svg#arrow')}}"></use>
                                                </svg>I have a gift card, how can I use it? Can I use it partly?
                                            </h5>
                                            <p class="accordion__text accordion__text--inner">Answer answer answer answer answer answer answer answer answer answer answer answer answer answer answer answer</p>
                                        </div>
                                        <div class="accordion__inner">
                                            <h5 class="accordion__descr accordion__descr--inner-title">
                                                <svg class="icon icon-arrow accordion__arrow ">
                                                    <use xlink:href="{{asset('front/images/sprite/sprite.svg#arrow')}}"></use>
                                                </svg>I have an UMICO loyal bonus card savings; can I use them during payment?
                                            </h5>
                                            <p class="accordion__text accordion__text--inner">Answer answer answer answer answer answer answer answer answer answer answer answer answer answer answer answer</p>
                                        </div>
                                        <div class="accordion__inner">
                                            <h5 class="accordion__descr accordion__descr--inner-title">
                                                <svg class="icon icon-arrow accordion__arrow ">
                                                    <use xlink:href="{{asset('front/images/sprite/sprite.svg#arrow')}}"></use>
                                                </svg>How do we earn UMİCO bonus card`s points during payment?
                                            </h5>
                                            <p class="accordion__text accordion__text--inner">Answer answer answer answer answer answer answer answer answer answer answer answer answer answer answer answer</p>
                                        </div>
                                        <div class="accordion__inner">
                                            <h5 class="accordion__descr accordion__descr--inner-title">
                                                <svg class="icon icon-arrow accordion__arrow ">
                                                    <use xlink:href="{{asset('front/images/sprite/sprite.svg#arrow')}}"></use>
                                                </svg>I have a Bravo loyal pension card; can I use it during payment?
                                            </h5>
                                            <p class="accordion__text accordion__text--inner">Answer answer answer answer answer answer answer answer answer answer answer answer answer answer answer answer</p>
                                        </div>
                                        <div class="accordion__inner">
                                            <h5 class="accordion__descr accordion__descr--inner-title">
                                                <svg class="icon icon-arrow accordion__arrow ">
                                                    <use xlink:href="{{asset('front/images/sprite/sprite.svg#arrow')}}"></use>
                                                </svg>I seem to have been charged twice the same transaction. What should I do?
                                            </h5>
                                            <p class="accordion__text accordion__text--inner">Answer answer answer answer answer answer answer answer answer answer answer answer answer answer answer answer</p>
                                        </div>
                                        <div class="accordion__inner">
                                            <h5 class="accordion__descr accordion__descr--inner-title">
                                                <svg class="icon icon-arrow accordion__arrow ">
                                                    <use xlink:href="{{asset('front/images/sprite/sprite.svg#arrow')}}"></use>
                                                </svg>What shall I do if I have been overcharged?
                                            </h5>
                                            <p class="accordion__text accordion__text--inner">Answer answer answer answer answer answer answer answer answer answer answer answer answer answer answer answer</p>
                                        </div>
                                        <div class="accordion__inner">
                                            <h5 class="accordion__descr accordion__descr--inner-title">
                                                <svg class="icon icon-arrow accordion__arrow ">
                                                    <use xlink:href="{{asset('front/images/sprite/sprite.svg#arrow')}}"></use>
                                                </svg>Can I use both Loyal Cards during payment?
                                            </h5>
                                            <p class="accordion__text accordion__text--inner">Answer answer answer answer answer answer answer answer answer answer answer answer answer answer answer answer</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="accordion__title">
                                    <h5 class="accordion__descr accordion__descr--top">
                                        <svg class="icon icon-arrow accordion__arrow accordion__arrow--top">
                                            <use xlink:href="{{asset('front/images/sprite/sprite.svg#arrow')}}"></use>
                                        </svg>General Information
                                    </h5>
                                    <div class="accordion__inner-wrap">
                                        <div class="accordion__inner">
                                            <h5 class="accordion__descr accordion__descr--inner-title">
                                                <svg class="icon icon-arrow accordion__arrow ">
                                                    <use xlink:href="{{asset('front/images/sprite/sprite.svg#arrow')}}"></use>
                                                </svg>Payment
                                            </h5>
                                            <p class="accordion__text accordion__text--inner">Answer answer answer answer answer answer answer answer answer answer answer answer answer answer answer answer</p>
                                        </div>
                                        <div class="accordion__inner">
                                            <h5 class="accordion__descr accordion__descr--inner-title">
                                                <svg class="icon icon-arrow accordion__arrow ">
                                                    <use xlink:href="{{asset('front/images/sprite/sprite.svg#arrow')}}"></use>
                                                </svg>What type and methods of payment can I use in-store?
                                            </h5>
                                            <p class="accordion__text accordion__text--inner">Answer answer answer answer answer answer answer answer answer answer answer answer answer answer answer answer</p>
                                        </div>
                                        <div class="accordion__inner">
                                            <h5 class="accordion__descr accordion__descr--inner-title">
                                                <svg class="icon icon-arrow accordion__arrow ">
                                                    <use xlink:href="{{asset('front/images/sprite/sprite.svg#arrow')}}"></use>
                                                </svg>I have a gift card, how can I use it? Can I use it partly?
                                            </h5>
                                            <p class="accordion__text accordion__text--inner">Answer answer answer answer answer answer answer answer answer answer answer answer answer answer answer answer</p>
                                        </div>
                                        <div class="accordion__inner">
                                            <h5 class="accordion__descr accordion__descr--inner-title">
                                                <svg class="icon icon-arrow accordion__arrow ">
                                                    <use xlink:href="{{asset('front/images/sprite/sprite.svg#arrow')}}"></use>
                                                </svg>I have an UMICO loyal bonus card savings; can I use them during payment?
                                            </h5>
                                            <p class="accordion__text accordion__text--inner">Answer answer answer answer answer answer answer answer answer answer answer answer answer answer answer answer</p>
                                        </div>
                                        <div class="accordion__inner">
                                            <h5 class="accordion__descr accordion__descr--inner-title">
                                                <svg class="icon icon-arrow accordion__arrow ">
                                                    <use xlink:href="{{asset('front/images/sprite/sprite.svg#arrow')}}"></use>
                                                </svg>How do we earn UMİCO bonus card`s points during payment?
                                            </h5>
                                            <p class="accordion__text accordion__text--inner">Answer answer answer answer answer answer answer answer answer answer answer answer answer answer answer answer</p>
                                        </div>
                                        <div class="accordion__inner">
                                            <h5 class="accordion__descr accordion__descr--inner-title">
                                                <svg class="icon icon-arrow accordion__arrow ">
                                                    <use xlink:href="{{asset('front/images/sprite/sprite.svg#arrow')}}"></use>
                                                </svg>I have a Bravo loyal pension card; can I use it during payment?
                                            </h5>
                                            <p class="accordion__text accordion__text--inner">Answer answer answer answer answer answer answer answer answer answer answer answer answer answer answer answer</p>
                                        </div>
                                        <div class="accordion__inner">
                                            <h5 class="accordion__descr accordion__descr--inner-title">
                                                <svg class="icon icon-arrow accordion__arrow ">
                                                    <use xlink:href="{{asset('front/images/sprite/sprite.svg#arrow')}}"></use>
                                                </svg>I seem to have been charged twice the same transaction. What should I do?
                                            </h5>
                                            <p class="accordion__text accordion__text--inner">Answer answer answer answer answer answer answer answer answer answer answer answer answer answer answer answer</p>
                                        </div>
                                        <div class="accordion__inner">
                                            <h5 class="accordion__descr accordion__descr--inner-title">
                                                <svg class="icon icon-arrow accordion__arrow ">
                                                    <use xlink:href="{{asset('front/images/sprite/sprite.svg#arrow')}}"></use>
                                                </svg>What shall I do if I have been overcharged?
                                            </h5>
                                            <p class="accordion__text accordion__text--inner">Answer answer answer answer answer answer answer answer answer answer answer answer answer answer answer answer</p>
                                        </div>
                                        <div class="accordion__inner">
                                            <h5 class="accordion__descr accordion__descr--inner-title">
                                                <svg class="icon icon-arrow accordion__arrow ">
                                                    <use xlink:href="{{asset('front/images/sprite/sprite.svg#arrow')}}"></use>
                                                </svg>Can I use both Loyal Cards during payment?
                                            </h5>
                                            <p class="accordion__text accordion__text--inner">Answer answer answer answer answer answer answer answer answer answer answer answer answer answer answer answer</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="accordion__title">
                                    <h5 class="accordion__descr accordion__descr--top">
                                        <svg class="icon icon-arrow accordion__arrow accordion__arrow--top">
                                            <use xlink:href="{{asset('front/images/sprite/sprite.svg#arrow')}}"></use>
                                        </svg>Marketing Mailing
                                    </h5>
                                    <div class="accordion__inner-wrap">
                                        <div class="accordion__inner">
                                            <h5 class="accordion__descr accordion__descr--inner-title">
                                                <svg class="icon icon-arrow accordion__arrow ">
                                                    <use xlink:href="{{asset('front/images/sprite/sprite.svg#arrow')}}"></use>
                                                </svg>Payment
                                            </h5>
                                            <p class="accordion__text accordion__text--inner">Answer answer answer answer answer answer answer answer answer answer answer answer answer answer answer answer</p>
                                        </div>
                                        <div class="accordion__inner">
                                            <h5 class="accordion__descr accordion__descr--inner-title">
                                                <svg class="icon icon-arrow accordion__arrow ">
                                                    <use xlink:href="{{asset('front/images/sprite/sprite.svg#arrow')}}"></use>
                                                </svg>What type and methods of payment can I use in-store?
                                            </h5>
                                            <p class="accordion__text accordion__text--inner">Answer answer answer answer answer answer answer answer answer answer answer answer answer answer answer answer</p>
                                        </div>
                                        <div class="accordion__inner">
                                            <h5 class="accordion__descr accordion__descr--inner-title">
                                                <svg class="icon icon-arrow accordion__arrow ">
                                                    <use xlink:href="{{asset('front/images/sprite/sprite.svg#arrow')}}"></use>
                                                </svg>I have a gift card, how can I use it? Can I use it partly?
                                            </h5>
                                            <p class="accordion__text accordion__text--inner">Answer answer answer answer answer answer answer answer answer answer answer answer answer answer answer answer</p>
                                        </div>
                                        <div class="accordion__inner">
                                            <h5 class="accordion__descr accordion__descr--inner-title">
                                                <svg class="icon icon-arrow accordion__arrow ">
                                                    <use xlink:href="{{asset('front/images/sprite/sprite.svg#arrow')}}"></use>
                                                </svg>I have an UMICO loyal bonus card savings; can I use them during payment?
                                            </h5>
                                            <p class="accordion__text accordion__text--inner">Answer answer answer answer answer answer answer answer answer answer answer answer answer answer answer answer</p>
                                        </div>
                                        <div class="accordion__inner">
                                            <h5 class="accordion__descr accordion__descr--inner-title">
                                                <svg class="icon icon-arrow accordion__arrow ">
                                                    <use xlink:href="{{asset('front/images/sprite/sprite.svg#arrow')}}"></use>
                                                </svg>How do we earn UMİCO bonus card`s points during payment?
                                            </h5>
                                            <p class="accordion__text accordion__text--inner">Answer answer answer answer answer answer answer answer answer answer answer answer answer answer answer answer</p>
                                        </div>
                                        <div class="accordion__inner">
                                            <h5 class="accordion__descr accordion__descr--inner-title">
                                                <svg class="icon icon-arrow accordion__arrow ">
                                                    <use xlink:href="{{asset('front/images/sprite/sprite.svg#arrow')}}"></use>
                                                </svg>I have a Bravo loyal pension card; can I use it during payment?
                                            </h5>
                                            <p class="accordion__text accordion__text--inner">Answer answer answer answer answer answer answer answer answer answer answer answer answer answer answer answer</p>
                                        </div>
                                        <div class="accordion__inner">
                                            <h5 class="accordion__descr accordion__descr--inner-title">
                                                <svg class="icon icon-arrow accordion__arrow ">
                                                    <use xlink:href="{{asset('front/images/sprite/sprite.svg#arrow')}}"></use>
                                                </svg>I seem to have been charged twice the same transaction. What should I do?
                                            </h5>
                                            <p class="accordion__text accordion__text--inner">Answer answer answer answer answer answer answer answer answer answer answer answer answer answer answer answer</p>
                                        </div>
                                        <div class="accordion__inner">
                                            <h5 class="accordion__descr accordion__descr--inner-title">
                                                <svg class="icon icon-arrow accordion__arrow ">
                                                    <use xlink:href="{{asset('front/images/sprite/sprite.svg#arrow')}}"></use>
                                                </svg>What shall I do if I have been overcharged?
                                            </h5>
                                            <p class="accordion__text accordion__text--inner">Answer answer answer answer answer answer answer answer answer answer answer answer answer answer answer answer</p>
                                        </div>
                                        <div class="accordion__inner">
                                            <h5 class="accordion__descr accordion__descr--inner-title">
                                                <svg class="icon icon-arrow accordion__arrow ">
                                                    <use xlink:href="{{asset('front/images/sprite/sprite.svg#arrow')}}"></use>
                                                </svg>Can I use both Loyal Cards during payment?
                                            </h5>
                                            <p class="accordion__text accordion__text--inner">Answer answer answer answer answer answer answer answer answer answer answer answer answer answer answer answer</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="accordion__title">
                                    <h5 class="accordion__descr accordion__descr--top">
                                        <svg class="icon icon-arrow accordion__arrow accordion__arrow--top">
                                            <use xlink:href="{{asset('front/images/sprite/sprite.svg#arrow')}}"></use>
                                        </svg>New Suppliers
                                    </h5>
                                    <div class="accordion__inner-wrap">
                                        <div class="accordion__inner">
                                            <h5 class="accordion__descr accordion__descr--inner-title">
                                                <svg class="icon icon-arrow accordion__arrow ">
                                                    <use xlink:href="{{asset('front/images/sprite/sprite.svg#arrow')}}"></use>
                                                </svg>Payment
                                            </h5>
                                            <p class="accordion__text accordion__text--inner">Answer answer answer answer answer answer answer answer answer answer answer answer answer answer answer answer</p>
                                        </div>
                                        <div class="accordion__inner">
                                            <h5 class="accordion__descr accordion__descr--inner-title">
                                                <svg class="icon icon-arrow accordion__arrow ">
                                                    <use xlink:href="{{asset('front/images/sprite/sprite.svg#arrow')}}"></use>
                                                </svg>What type and methods of payment can I use in-store?
                                            </h5>
                                            <p class="accordion__text accordion__text--inner">Answer answer answer answer answer answer answer answer answer answer answer answer answer answer answer answer</p>
                                        </div>
                                        <div class="accordion__inner">
                                            <h5 class="accordion__descr accordion__descr--inner-title">
                                                <svg class="icon icon-arrow accordion__arrow ">
                                                    <use xlink:href="{{asset('front/images/sprite/sprite.svg#arrow')}}"></use>
                                                </svg>I have a gift card, how can I use it? Can I use it partly?
                                            </h5>
                                            <p class="accordion__text accordion__text--inner">Answer answer answer answer answer answer answer answer answer answer answer answer answer answer answer answer</p>
                                        </div>
                                        <div class="accordion__inner">
                                            <h5 class="accordion__descr accordion__descr--inner-title">
                                                <svg class="icon icon-arrow accordion__arrow ">
                                                    <use xlink:href="{{asset('front/images/sprite/sprite.svg#arrow')}}"></use>
                                                </svg>I have an UMICO loyal bonus card savings; can I use them during payment?
                                            </h5>
                                            <p class="accordion__text accordion__text--inner">Answer answer answer answer answer answer answer answer answer answer answer answer answer answer answer answer</p>
                                        </div>
                                        <div class="accordion__inner">
                                            <h5 class="accordion__descr accordion__descr--inner-title">
                                                <svg class="icon icon-arrow accordion__arrow ">
                                                    <use xlink:href="{{asset('front/images/sprite/sprite.svg#arrow')}}"></use>
                                                </svg>How do we earn UMİCO bonus card`s points during payment?
                                            </h5>
                                            <p class="accordion__text accordion__text--inner">Answer answer answer answer answer answer answer answer answer answer answer answer answer answer answer answer</p>
                                        </div>
                                        <div class="accordion__inner">
                                            <h5 class="accordion__descr accordion__descr--inner-title">
                                                <svg class="icon icon-arrow accordion__arrow ">
                                                    <use xlink:href="{{asset('front/images/sprite/sprite.svg#arrow')}}"></use>
                                                </svg>I have a Bravo loyal pension card; can I use it during payment?
                                            </h5>
                                            <p class="accordion__text accordion__text--inner">Answer answer answer answer answer answer answer answer answer answer answer answer answer answer answer answer</p>
                                        </div>
                                        <div class="accordion__inner">
                                            <h5 class="accordion__descr accordion__descr--inner-title">
                                                <svg class="icon icon-arrow accordion__arrow ">
                                                    <use xlink:href="{{asset('front/images/sprite/sprite.svg#arrow')}}"></use>
                                                </svg>I seem to have been charged twice the same transaction. What should I do?
                                            </h5>
                                            <p class="accordion__text accordion__text--inner">Answer answer answer answer answer answer answer answer answer answer answer answer answer answer answer answer</p>
                                        </div>
                                        <div class="accordion__inner">
                                            <h5 class="accordion__descr accordion__descr--inner-title">
                                                <svg class="icon icon-arrow accordion__arrow ">
                                                    <use xlink:href="{{asset('front/images/sprite/sprite.svg#arrow')}}"></use>
                                                </svg>What shall I do if I have been overcharged?
                                            </h5>
                                            <p class="accordion__text accordion__text--inner">Answer answer answer answer answer answer answer answer answer answer answer answer answer answer answer answer</p>
                                        </div>
                                        <div class="accordion__inner">
                                            <h5 class="accordion__descr accordion__descr--inner-title">
                                                <svg class="icon icon-arrow accordion__arrow ">
                                                    <use xlink:href="{{asset('front/images/sprite/sprite.svg#arrow')}}"></use>
                                                </svg>Can I use both Loyal Cards during payment?
                                            </h5>
                                            <p class="accordion__text accordion__text--inner">Answer answer answer answer answer answer answer answer answer answer answer answer answer answer answer answer</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="accordion__title">
                                    <h5 class="accordion__descr accordion__descr--top">
                                        <svg class="icon icon-arrow accordion__arrow accordion__arrow--top">
                                            <use xlink:href="{{asset('front/images/sprite/sprite.svg#arrow')}}"></use>
                                        </svg>Bravo Gift and Loyal cards
                                    </h5>
                                    <div class="accordion__inner-wrap">
                                        <div class="accordion__inner">
                                            <h5 class="accordion__descr accordion__descr--inner-title">
                                                <svg class="icon icon-arrow accordion__arrow ">
                                                    <use xlink:href="{{asset('front/images/sprite/sprite.svg#arrow')}}"></use>
                                                </svg>Payment
                                            </h5>
                                            <p class="accordion__text accordion__text--inner">Answer answer answer answer answer answer answer answer answer answer answer answer answer answer answer answer</p>
                                        </div>
                                        <div class="accordion__inner">
                                            <h5 class="accordion__descr accordion__descr--inner-title">
                                                <svg class="icon icon-arrow accordion__arrow ">
                                                    <use xlink:href="{{asset('front/images/sprite/sprite.svg#arrow')}}"></use>
                                                </svg>What type and methods of payment can I use in-store?
                                            </h5>
                                            <p class="accordion__text accordion__text--inner">Answer answer answer answer answer answer answer answer answer answer answer answer answer answer answer answer</p>
                                        </div>
                                        <div class="accordion__inner">
                                            <h5 class="accordion__descr accordion__descr--inner-title">
                                                <svg class="icon icon-arrow accordion__arrow ">
                                                    <use xlink:href="{{asset('front/images/sprite/sprite.svg#arrow')}}"></use>
                                                </svg>I have a gift card, how can I use it? Can I use it partly?
                                            </h5>
                                            <p class="accordion__text accordion__text--inner">Answer answer answer answer answer answer answer answer answer answer answer answer answer answer answer answer</p>
                                        </div>
                                        <div class="accordion__inner">
                                            <h5 class="accordion__descr accordion__descr--inner-title">
                                                <svg class="icon icon-arrow accordion__arrow ">
                                                    <use xlink:href="{{asset('front/images/sprite/sprite.svg#arrow')}}"></use>
                                                </svg>I have an UMICO loyal bonus card savings; can I use them during payment?
                                            </h5>
                                            <p class="accordion__text accordion__text--inner">Answer answer answer answer answer answer answer answer answer answer answer answer answer answer answer answer</p>
                                        </div>
                                        <div class="accordion__inner">
                                            <h5 class="accordion__descr accordion__descr--inner-title">
                                                <svg class="icon icon-arrow accordion__arrow ">
                                                    <use xlink:href="{{asset('front/images/sprite/sprite.svg#arrow')}}"></use>
                                                </svg>How do we earn UMİCO bonus card`s points during payment?
                                            </h5>
                                            <p class="accordion__text accordion__text--inner">Answer answer answer answer answer answer answer answer answer answer answer answer answer answer answer answer</p>
                                        </div>
                                        <div class="accordion__inner">
                                            <h5 class="accordion__descr accordion__descr--inner-title">
                                                <svg class="icon icon-arrow accordion__arrow ">
                                                    <use xlink:href="{{asset('front/images/sprite/sprite.svg#arrow')}}"></use>
                                                </svg>I have a Bravo loyal pension card; can I use it during payment?
                                            </h5>
                                            <p class="accordion__text accordion__text--inner">Answer answer answer answer answer answer answer answer answer answer answer answer answer answer answer answer</p>
                                        </div>
                                        <div class="accordion__inner">
                                            <h5 class="accordion__descr accordion__descr--inner-title">
                                                <svg class="icon icon-arrow accordion__arrow ">
                                                    <use xlink:href="{{asset('front/images/sprite/sprite.svg#arrow')}}"></use>
                                                </svg>I seem to have been charged twice the same transaction. What should I do?
                                            </h5>
                                            <p class="accordion__text accordion__text--inner">Answer answer answer answer answer answer answer answer answer answer answer answer answer answer answer answer</p>
                                        </div>
                                        <div class="accordion__inner">
                                            <h5 class="accordion__descr accordion__descr--inner-title">
                                                <svg class="icon icon-arrow accordion__arrow ">
                                                    <use xlink:href="{{asset('front/images/sprite/sprite.svg#arrow')}}"></use>
                                                </svg>What shall I do if I have been overcharged?
                                            </h5>
                                            <p class="accordion__text accordion__text--inner">Answer answer answer answer answer answer answer answer answer answer answer answer answer answer answer answer</p>
                                        </div>
                                        <div class="accordion__inner">
                                            <h5 class="accordion__descr accordion__descr--inner-title">
                                                <svg class="icon icon-arrow accordion__arrow ">
                                                    <use xlink:href="{{asset('front/images/sprite/sprite.svg#arrow')}}"></use>
                                                </svg>Can I use both Loyal Cards during payment?
                                            </h5>
                                            <p class="accordion__text accordion__text--inner">Answer answer answer answer answer answer answer answer answer answer answer answer answer answer answer answer</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="accordion__title">
                                    <h5 class="accordion__descr accordion__descr--top">
                                        <svg class="icon icon-arrow accordion__arrow accordion__arrow--top">
                                            <use xlink:href="{{asset('front/images/sprite/sprite.svg#arrow')}}"></use>
                                        </svg>Delivery Service FAQs
                                    </h5>
                                    <div class="accordion__inner-wrap">
                                        <div class="accordion__inner">
                                            <h5 class="accordion__descr accordion__descr--inner-title">
                                                <svg class="icon icon-arrow accordion__arrow ">
                                                    <use xlink:href="{{asset('front/images/sprite/sprite.svg#arrow')}}"></use>
                                                </svg>Payment
                                            </h5>
                                            <p class="accordion__text accordion__text--inner">Answer answer answer answer answer answer answer answer answer answer answer answer answer answer answer answer</p>
                                        </div>
                                        <div class="accordion__inner">
                                            <h5 class="accordion__descr accordion__descr--inner-title">
                                                <svg class="icon icon-arrow accordion__arrow ">
                                                    <use xlink:href="{{asset('front/images/sprite/sprite.svg#arrow')}}"></use>
                                                </svg>What type and methods of payment can I use in-store?
                                            </h5>
                                            <p class="accordion__text accordion__text--inner">Answer answer answer answer answer answer answer answer answer answer answer answer answer answer answer answer</p>
                                        </div>
                                        <div class="accordion__inner">
                                            <h5 class="accordion__descr accordion__descr--inner-title">
                                                <svg class="icon icon-arrow accordion__arrow ">
                                                    <use xlink:href="{{asset('front/images/sprite/sprite.svg#arrow')}}"></use>
                                                </svg>I have a gift card, how can I use it? Can I use it partly?
                                            </h5>
                                            <p class="accordion__text accordion__text--inner">Answer answer answer answer answer answer answer answer answer answer answer answer answer answer answer answer</p>
                                        </div>
                                        <div class="accordion__inner">
                                            <h5 class="accordion__descr accordion__descr--inner-title">
                                                <svg class="icon icon-arrow accordion__arrow ">
                                                    <use xlink:href="{{asset('front/images/sprite/sprite.svg#arrow')}}"></use>
                                                </svg>I have an UMICO loyal bonus card savings; can I use them during payment?
                                            </h5>
                                            <p class="accordion__text accordion__text--inner">Answer answer answer answer answer answer answer answer answer answer answer answer answer answer answer answer</p>
                                        </div>
                                        <div class="accordion__inner">
                                            <h5 class="accordion__descr accordion__descr--inner-title">
                                                <svg class="icon icon-arrow accordion__arrow ">
                                                    <use xlink:href="{{asset('front/images/sprite/sprite.svg#arrow')}}"></use>
                                                </svg>How do we earn UMİCO bonus card`s points during payment?
                                            </h5>
                                            <p class="accordion__text accordion__text--inner">Answer answer answer answer answer answer answer answer answer answer answer answer answer answer answer answer</p>
                                        </div>
                                        <div class="accordion__inner">
                                            <h5 class="accordion__descr accordion__descr--inner-title">
                                                <svg class="icon icon-arrow accordion__arrow ">
                                                    <use xlink:href="{{asset('front/images/sprite/sprite.svg#arrow')}}"></use>
                                                </svg>I have a Bravo loyal pension card; can I use it during payment?
                                            </h5>
                                            <p class="accordion__text accordion__text--inner">Answer answer answer answer answer answer answer answer answer answer answer answer answer answer answer answer</p>
                                        </div>
                                        <div class="accordion__inner">
                                            <h5 class="accordion__descr accordion__descr--inner-title">
                                                <svg class="icon icon-arrow accordion__arrow ">
                                                    <use xlink:href="{{asset('front/images/sprite/sprite.svg#arrow')}}"></use>
                                                </svg>I seem to have been charged twice the same transaction. What should I do?
                                            </h5>
                                            <p class="accordion__text accordion__text--inner">Answer answer answer answer answer answer answer answer answer answer answer answer answer answer answer answer</p>
                                        </div>
                                        <div class="accordion__inner">
                                            <h5 class="accordion__descr accordion__descr--inner-title">
                                                <svg class="icon icon-arrow accordion__arrow ">
                                                    <use xlink:href="{{asset('front/images/sprite/sprite.svg#arrow')}}"></use>
                                                </svg>What shall I do if I have been overcharged?
                                            </h5>
                                            <p class="accordion__text accordion__text--inner">Answer answer answer answer answer answer answer answer answer answer answer answer answer answer answer answer</p>
                                        </div>
                                        <div class="accordion__inner">
                                            <h5 class="accordion__descr accordion__descr--inner-title">
                                                <svg class="icon icon-arrow accordion__arrow ">
                                                    <use xlink:href="{{asset('front/images/sprite/sprite.svg#arrow')}}"></use>
                                                </svg>Can I use both Loyal Cards during payment?
                                            </h5>
                                            <p class="accordion__text accordion__text--inner">Answer answer answer answer answer answer answer answer answer answer answer answer answer answer answer answer</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="about-post__col preloader preloader--v" data-simple-tab-content="about-tab-3">
                            <h2>Consumer Protection Act</h2>
                            <br>
                            <ol>
                                <li>Marketdə alınan məhsullar Müəssisənin siyasətinə və İstehlakçıların hüquqlarını müdafiəsi haqqında Azərbaycan Respublikasının qanununun 1-ci bəndinin 15-ci maddəsinə uyğun olaraq, satış əməliyyatından sonra 14 gün ərzində
                                    dəyişilə və ya geri qaytarıla bilər. </li>
                                <li>İstehlakçı tərəfindən əldə edilmiş lazımi keyfiyyətli mal istifadə olunmayıbsa və onun əmtəə görünüşü, istehlak xassələri, plombu, yarlığı, həmçinin mal və yaxud kassa qəbzi və ya ona mal ilə birlikdə verilmiş digər
                                    sənədləri saxlanılıbsa (zəmanət talonu və s), bu hallarda o dəyişdirilə/qaytarıla bilər</li>
                                <li>Məhsulu geri qaytarmaq və ya deyiştirmək üçün müştəri alış-veriş etdiyi marketə müraciət etməlidir. </li>
                                <li>Məhsulu dəyişdirmə anında satışda uyğun mal yoxdursa, istehlakçı dəyəri yenidən hesablamaqla istənilən başqa bir malı almaq və ya qaytarılan malın dəyəri məbləğində pulu geri götürmək.</li>
                            </ol>
                        </div>
                        <div class="about-post__col preloader preloader--v" data-simple-tab-content="about-tab-4">
                            <h2>Consumer Protection Act</h2>
                            <br>
                            <p>“İstehlakçıların hüquqlarının müdafiəsi haqqında” Azərbaycan Respublikası Qanununa əsasən Azərbaycan Respublikasının Nazirlər Kabineti 114 nömrəli qərarına əsasən dəyişdirilməli olmayan malların siyahısı:</p>
                            <ol>
                                <li>Qızıl və qızıl məlumatları.</li>
                                <li>Qiymətli və yarımqiymətli metallardan, daşlardan hazırlanmış məlumatlar.</li>
                                <li>İstehsal qüsurları istisna olmaqla bütün növ parçalar və ölçü ilə satılan bəzi mallar (lentlər, tesmalar, haşiyələr).</li>
                                <li>Parfümer-kosmetika malları.</li>
                                <li>Kişi, qadın və uşaq çimərlik paltarları.</li>
                                <li>Bağça yaşlı və yeni doğulan uşaqlar üçün alt paltarları.</li>
                                <li>Məişət-kimyası malları.</li>
                                <li>Şəxsi gigiyena əşyaları (diş fırçaları, daraqlar, biqudilər və s.).</li>
                                <li>Uşaq oyuncaqları.</li>
                                <li>İstehsal qüsurları istisna olmaqla istifadədə olmuş, üzərində yarlıklar olmayan kişi, qadın və uşaq corabları, alt paltarları.</li>
                                <li>Ərzaq üçün plastmasdan hazırlanmış məlumatlar.</li>
                                <li>Dəyəri ödənilmiş və mağazadan çıxarılmış (yararlılıq müddəti ərzində) ərzaq malları.</li>
                            </ol>
                        </div>
                    </article>
                </div>
            </div>
        </section>
    </main>
@endsection
