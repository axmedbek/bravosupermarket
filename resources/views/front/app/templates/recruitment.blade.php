@extends('front.app.careerLayout')
@section('main')
    <main class="main">
        <section class="breadcrumbs">
            <div class="container">
                <div class="row">
                    <div class="breadcrumbs_col">
                        <div class="breadcrumbs__nav">
                            <a class="breadcrumbs__item breadcrumbs__item--grey-color preloader-overlay preloader-overlay--vertical"
                               href="#">
                                <svg class="icon icon-home breadcrumbs__icon">
                                    <use xlink:href="./images/sprite/sprite.svg#home"></use>
                                </svg>
                            </a>
                            <a class="breadcrumbs__item breadcrumbs__item--grey-color preloader-overlay preloader-overlay--vertical"
                               href="#">
                                <svg class="icon icon-arrow breadcrumbs__icon breadcrumbs__icon--arrow">
                                    <use xlink:href="./images/sprite/sprite.svg#arrow"></use>
                                </svg>{{ app()->getLocale() == "az" ? "Karyera" : "Career" }}</a>
                            <span class="breadcrumbs__item breadcrumbs__item--grey-color
                            breadcrumbs__item--inactive preloader-overlay preloader-overlay--vertical">
                                <svg class="icon icon-arrow breadcrumbs__icon breadcrumbs__icon--arrow">
                                    <use xlink:href="./images/sprite/sprite.svg#arrow"></use>
                                </svg>{{ $page->title }}</span>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="inner-top-thumb inner-top-thumb--white-bg section--pb-inner">
            <div class="container container--full">
                <div class="row">
                    <div class="container-inner">
                        <div class="inner-top-thumb__box inner-top-thumb__box--overlay-none preloader preloader--v"
                             style="background: url({{$media[0]->url}}) no-repeat center/cover"></div>
                    </div>
                </div>
            </div>
        </section>
        <section class="careers-info section--pb">
            <div class="container">
                <div class="row">
                    <div class="careers-info__col">
                        {!! explode("separate from here",$page->content)[0] !!}
                    </div>
                </div>
            </div>
        </section>
        <section class="careers-news section--mb-last">
            @php($menus = \App\Menu::getTargetMenus('targetJobOpportunities'))
            <div class="container">
                <div class="row">
                    <div class="careers-news__col">
                        <div class="preloader preloader--v careers-news-box">
                            <div class="careers-news-box__inner careers-news-box__inner--width-full">
                                @if (isset($menus[0]))
                                    <a class="careers-news-box__tile careers-news-box__tile--horizontal" href="{{route('page',['slug' => $menus[0]->page['slug'],'id' => $menus[0]->page['id']])}}">
                                        <div class="careers-news-box__img careers-news-box__img--horizontal">
                                            <img class="blazy blazy--style" src="./"
                                                 data-src="{{ asset('front/images/main/careers/news1.jpg') }}"
                                                 alt="bravo market">
                                        </div>
                                        <div class="careers-news-box__descr"
                                             style="background: radial-gradient(50% 50% at 50% 50%, #5868FB 0%, #001AFF 100%)">
                                            <h4 class="careers-news-box__title">{{ $menus[0]->page['title'] }}</h4>
                                            <p class="careers-news-box__sub-title"></p>
                                            <div class="careers-news-box__arrow careers-news-box__arrow--left-bottom"
                                                 style="border-right-color: #001AFF"></div>
                                        </div>
                                    </a>
                                @endif
                                @if (isset($menus[2]))
                                    <a class="careers-news-box__tile careers-news-box__tile--horizontal" href="{{route('page',['slug' => $menus[2]->page['slug'],'id' => $menus[2]->page['id']])}}">
                                        <div class="careers-news-box__descr"
                                             style="background: radial-gradient(50% 50% at 50% 50%, #93E95E 0%, #5BBC1F 100%)">
                                            <h4 class="careers-news-box__title">{{ $menus[2]->page['title'] }}</h4>
                                            <p class="careers-news-box__sub-title"></p>
                                            <div class="careers-news-box__arrow careers-news-box__arrow--right-bottom"
                                                 style="border-left-color: #5BBC1F"></div>
                                        </div>
                                        <div class="careers-news-box__img careers-news-box__img--horizontal">
                                            <img class="blazy blazy--style" src="./"
                                                 data-src="{{ asset('front/images/main/careers/news3.jpg') }}"
                                                 alt="bravo market">
                                        </div>
                                    </a>
                                @endif
                            </div>
                            @if (isset($menus[1]))
                                <div class="careers-news-box__inner careers-news-box__inner--width-half">
                                    <a class="careers-news-box__tile careers-news-box__tile--vertical" href="{{route('page',['slug' => $menus[1]->page['slug'],'id' => $menus[1]->page['id']])}}">
                                        <div class="careers-news-box__img careers-news-box__img--vertical">
                                            <img class="blazy blazy--style" src="./"
                                                 data-src="{{ asset('front/images/main/careers/news2.jpg') }}" alt="bravo market">
                                        </div>
                                        <div class="careers-news-box__descr"
                                             style="background: radial-gradient(50% 50% at 50% 50%, #FAE794 0%, #FBD630 100%)">
                                            <h4 class="careers-news-box__title">{{ $menus[1]->page['title'] }}</h4>
                                            <p class="careers-news-box__sub-title"></p>
                                            <div class="careers-news-box__arrow careers-news-box__arrow--left-top"
                                                 style="border-bottom-color: #FBD630"></div>
                                        </div>
                                    </a>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>
@endsection
