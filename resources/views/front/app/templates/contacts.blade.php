@extends('front.app.layout')
@section('main')
    <main class="main">
        <section class="breadcrumbs">
            <div class="container">
                <div class="row">
                    <div class="breadcrumbs_col">
                        <div class="breadcrumbs__nav">
                            <a class="breadcrumbs__item preloader-overlay preloader-overlay--vertical" href="#">
                                <svg class="icon icon-home breadcrumbs__icon">
                                    <use xlink:href="{{asset('front/images/sprite/sprite.svg#home')}}"></use>
                                </svg>
                            </a>
                            <span class="breadcrumbs__item breadcrumbs__item--inactive preloader-overlay preloader-overlay--vertical">
                                    <svg class="icon icon-arrow breadcrumbs__icon breadcrumbs__icon--arrow">
                                        <use xlink:href="{{asset('front/images/sprite/sprite.svg#arrow')}}"></use>
                                    </svg>{{$page->title}}</span>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="inner-top-thumb section--mb-inner">
            <div class="container container--full">
                <div class="row">
                    <div class="container-inner">
                        <div class="inner-top-thumb__box preloader preloader--v" style="background: url({{$media[0]->url}}) no-repeat center/cover">
                            <h1 class="inner-top-thumb__title"> {{$page->title}}
                            </h1>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="about">
            <div class="container">
                <div class="row">
                    <aside class="about-aside">
                        <nav class="about-aside__nav">
                            <ul class="about-aside__menu">
                                <li class="about-aside__items preloader preloader--v">
                                    <span class="about-aside__link simple-tab__link--active" data-simple-tab-nav="contacts-tab-1">Potential Suppliers</span>
                                </li>
                                <li class="about-aside__items preloader preloader--v">
                                    <span class="about-aside__link" data-simple-tab-nav="contacts-tab-2">B2B</span>
                                </li>
                                <li class="about-aside__items preloader preloader--v">
                                    <span class="about-aside__link" data-simple-tab-nav="contacts-tab-3">Advertisers</span>
                                </li>
                                <li class="about-aside__items preloader preloader--v">
                                    <span class="about-aside__link" data-simple-tab-nav="contacts-tab-4">Media & Press</span>
                                </li>
                                <li class="about-aside__items preloader preloader--v">
                                    <span class="about-aside__link" data-simple-tab-nav="contacts-tab-5">Feedback</span>
                                </li>
                            </ul>
                        </nav>
                    </aside>
                    <article class="about-post">
                        <div class="about-post__col preloader preloader--v simple-tab__content--active" data-simple-tab-content="contacts-tab-1">
                            <h2>Supplier Registration Form</h2>
                            <p>Empty place for intro text</p>
                            <br>
                            <br>
                            <form class="contacts-form">
                                <div class="contacts-form__row">
                                    <div class="contacts-form__col contacts-form__col--half">
                                        <label class="contacts-form__label">Company name </label>
                                        <input class="contacts-form__input" type="text" name="contacts-page-company-name" placeholder="Paste your info" required>
                                    </div>
                                    <div class="contacts-form__col contacts-form__col--half">
                                        <label class="contacts-form__label">Presentation of Company</label>
                                        <input class="contacts-form__input" type="text" name="contacts-page-company-presentation" placeholder="Paste your info" required>
                                    </div>
                                </div>
                                <div class="contacts-form__row">
                                    <div class="contacts-form__col contacts-form__col--half">
                                        <label class="contacts-form__label">Tax ID / VAT number</label>
                                        <input class="contacts-form__input" type="text" name="contacts-page-tax-id" placeholder="Paste your info" required>
                                    </div>
                                    <div class="contacts-form__col contacts-form__col--half">
                                        <label class="contacts-form__label">Legal address (str.avn. apt.etc)</label>
                                        <input class="contacts-form__input" type="text" name="contacts-page-address" placeholder="Paste your info" required>
                                    </div>
                                </div>
                                <div class="contacts-form__row">
                                    <div class="contacts-form__col contacts-form__col--half">
                                        <label class="contacts-form__label">District</label>
                                        <input class="contacts-form__input" type="text" name="contacts-page-distict" placeholder="Paste your info" required>
                                    </div>
                                    <div class="contacts-form__col contacts-form__col--half">
                                        <label class="contacts-form__label">City</label>
                                        <input class="contacts-form__input" type="text" name="contacts-page-city" placeholder="Paste your info" required>
                                    </div>
                                </div>
                                <div class="contacts-form__row">
                                    <div class="contacts-form__col contacts-form__col--half">
                                        <label class="contacts-form__label">Country </label>
                                        <input class="contacts-form__input" type="text" name="contacts-page-country" placeholder="Paste your info" required>
                                    </div>
                                    <div class="contacts-form__col contacts-form__col--half">
                                        <label class="contacts-form__label">Post code</label>
                                        <input class="contacts-form__input" type="text" name="contacts-page-post-code" placeholder="Paste your info" required>
                                    </div>
                                </div>
                                <div class="contacts-form__row">
                                    <div class="contacts-form__col contacts-form__col--half">
                                        <label class="contacts-form__label">Phone </label>
                                        <input class="contacts-form__input" type="number" name="contacts-page-number" placeholder="Paste your info" required>
                                    </div>
                                    <div class="contacts-form__col contacts-form__col--half">
                                        <label class="contacts-form__label">Email address</label>
                                        <input class="contacts-form__input" type="email" name="contacts-page-email" placeholder="Paste your info" required>
                                    </div>
                                </div>
                                <div class="contacts-form__row">
                                    <div class="contacts-form__col contacts-form__col--half">
                                        <label class="contacts-form__label">Family</label>
                                        <div class="select-wrap select-wrap--form-page">
                                            <div class="select-wrap__text select-wrap__text--full" data-select-wrap="fsr-select-1" data-select-focus="false">
                                                <input class="select-wrap__input" type="hidden" name="contacts-page-family" value="Choose your info" data-select-input="fsr-select-1">
                                                <div class="select-wrap__selected select-wrap__selected--grey" data-selected-item="fsr-select-1" data-selected-focus="false">Choose your info </div>
                                                <svg class="icon icon-arrow select-wrap__icon-svg select-wrap__icon-svg--arrow">
                                                    <use xlink:href="{{asset('front/images/sprite/sprite.svg#arrow')}}"></use>
                                                </svg>
                                            </div>
                                            <div class="select-wrap__option select-wrap__option--form-page" data-select-option="fsr-select-1" data-select-option-opened="false" data-select-option-scroll="true">
                                                <span class="select-wrap__option-item select-wrap__option-item--form-page" data-select-option-items="Choose your info 1" data-select-option-input="fsr-select-1">Choose your info 1 </span>
                                                <span class="select-wrap__option-item select-wrap__option-item--form-page" data-select-option-items="Choose your info 2" data-select-option-input="fsr-select-1">Choose your info 2 </span>
                                                <span class="select-wrap__option-item select-wrap__option-item--form-page" data-select-option-items="Choose your info 3" data-select-option-input="fsr-select-1">Choose your info 3 </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="contacts-form__col contacts-form__col--half">
                                        <label class="contacts-form__label">Category</label>
                                        <div class="select-wrap select-wrap--form-page">
                                            <div class="select-wrap__text select-wrap__text--full" data-select-wrap="fsr-select-2" data-select-focus="false">
                                                <input class="select-wrap__input" type="hidden" name="contacts-page-family" value="Choose your info" data-select-input="fsr-select-2">
                                                <div class="select-wrap__selected select-wrap__selected--grey" data-selected-item="fsr-select-2" data-selected-focus="false">Choose your info </div>
                                                <svg class="icon icon-arrow select-wrap__icon-svg select-wrap__icon-svg--arrow">
                                                    <use xlink:href="{{asset('front/images/sprite/sprite.svg#arrow')}}"></use>
                                                </svg>
                                            </div>
                                            <div class="select-wrap__option select-wrap__option--form-page" data-select-option="fsr-select-2" data-select-option-opened="false" data-select-option-scroll="true">
                                                <span class="select-wrap__option-item select-wrap__option-item--form-page" data-select-option-items="Choose your info 1" data-select-option-input="fsr-select-2">Choose your info 1 </span>
                                                <span class="select-wrap__option-item select-wrap__option-item--form-page" data-select-option-items="Choose your info 2" data-select-option-input="fsr-select-2">Choose your info 2 </span>
                                                <span class="select-wrap__option-item select-wrap__option-item--form-page" data-select-option-items="Choose your info 3" data-select-option-input="fsr-select-2">Choose your info 3 </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="contacts-form__row">
                                    <div class="contacts-form__col contacts-form__col--half">
                                        <label class="contacts-form__label">Contact person </label>
                                        <input class="contacts-form__input" type="text" name="contacts-page-contact-person" placeholder="Paste your info" required>
                                    </div>
                                    <div class="contacts-form__col contacts-form__col--half">
                                        <label class="contacts-form__label">Category</label>
                                        <input class="contacts-form__input" type="text" name="contacts-page-category" placeholder="Paste your info" required>
                                    </div>
                                </div>
                                <div class="contacts-form__row">
                                    <div class="contacts-form__col contacts-form__col--half">
                                        <div class="contact-form-loaded">
                                            <input class="contact-form-loaded__input" type="file" name="contacts-page-presentation-file">
                                            <button class="contact-form-loaded__close">
                                                <svg class="icon icon-close contact-form-loaded__icon-svg">
                                                    <use xlink:href="{{asset('front/images/sprite/sprite.svg#close')}}"></use>
                                                </svg>
                                            </button>
                                            <div class="contact-form-loaded__inner">
                                                <div class="contact-form-loaded__img">
                                                    <img class="contact-form-loaded__icon blazy blazy--style" src="./" data-src="{{asset('front/images/main/icon/pdf_active.svg')}}" alt="bravo market">
                                                    <img class="contact-form-loaded__icon blazy blazy--style" src="./" data-src="{{asset('front/images/main/icon/pdf.svg')}}" alt="bravo market">
                                                </div>
                                                <div class="contact-form-loaded__content">
                                                    <h4 class="contact-form-loaded__title">Products price list</h4>
                                                    <p class="contact-form-loaded__descr">Formats: .pdf .png .jpeg .jpg </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="contacts-form__col contacts-form__col--half">
                                        <div class="contact-form-loaded">
                                            <input class="contact-form-loaded__input" type="file" name="contacts-page-price-file">
                                            <button class="contact-form-loaded__close">
                                                <svg class="icon icon-close contact-form-loaded__icon-svg">
                                                    <use xlink:href="{{asset('front/images/sprite/sprite.svg#close')}}"></use>
                                                </svg>
                                            </button>
                                            <div class="contact-form-loaded__inner">
                                                <div class="contact-form-loaded__img">
                                                    <img class="contact-form-loaded__icon blazy blazy--style" src="./" data-src="{{asset('front/images/main/icon/pdf_active.svg')}}" alt="bravo market">
                                                    <img class="contact-form-loaded__icon blazy blazy--style" src="./" data-src="{{asset('front/images/main/icon/pdf.svg')}}" alt="bravo market">
                                                </div>
                                                <div class="contact-form-loaded__content">
                                                    <h4 class="contact-form-loaded__title">Products price list</h4>
                                                    <p class="contact-form-loaded__descr">Formats: .pdf .png .jpeg .jpg </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="contacts-form__row">
                                    <div class="contacts-form__col contacts-form__col--half">
                                        <div class="contact-form-loaded">
                                            <input class="contact-form-loaded__input" type="file" name="contacts-page-cert-file-1">
                                            <button class="contact-form-loaded__close">
                                                <svg class="icon icon-close contact-form-loaded__icon-svg">
                                                    <use xlink:href="{{asset('front/images/sprite/sprite.svg#close')}}"></use>
                                                </svg>
                                            </button>
                                            <div class="contact-form-loaded__inner">
                                                <div class="contact-form-loaded__img">
                                                    <img class="contact-form-loaded__icon blazy blazy--style" src="./" data-src="{{asset('front/images/main/icon/pdf_active.svg')}}" alt="bravo market">
                                                    <img class="contact-form-loaded__icon blazy blazy--style" src="./" data-src="{{asset('front/images/main/icon/pdf.svg')}}" alt="bravo market">
                                                </div>
                                                <div class="contact-form-loaded__content">
                                                    <h4 class="contact-form-loaded__title">Company certificates</h4>
                                                    <p class="contact-form-loaded__descr">Formats: .pdf .png .jpeg .jpg </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="contacts-form__col contacts-form__col--half">
                                        <div class="contact-form-loaded">
                                            <input class="contact-form-loaded__input" type="file" name="contacts-page-cert-file-2">
                                            <button class="contact-form-loaded__close">
                                                <svg class="icon icon-close contact-form-loaded__icon-svg">
                                                    <use xlink:href="{{asset('front/images/sprite/sprite.svg#close')}}"></use>
                                                </svg>
                                            </button>
                                            <div class="contact-form-loaded__inner">
                                                <div class="contact-form-loaded__img">
                                                    <img class="contact-form-loaded__icon blazy blazy--style" src="./" data-src="{{asset('front/images/main/icon/pdf_active.svg')}}" alt="bravo market">
                                                    <img class="contact-form-loaded__icon blazy blazy--style" src="./" data-src="{{asset('front/images/main/icon/pdf.svg')}}" alt="bravo market">
                                                </div>
                                                <div class="contact-form-loaded__content">
                                                    <h4 class="contact-form-loaded__title">Company certificates</h4>
                                                    <p class="contact-form-loaded__descr">Formats: .pdf .png .jpeg .jpg </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="contacts-form__row contacts-form__row--send-btn">
                                    <div class="contacts-form__col contacts-form__col--half">
                                        <button class="contacts-form__send-btn primary-btn primary-btn--green">
                                            <svg class="icon icon-send contacts-form__send-icon">
                                                <use xlink:href="{{asset('front/images/sprite/sprite.svg#send')}}"></use>
                                            </svg>Send Form
                                        </button>
                                    </div>
                                </div>
                            </form>
                            <br>
                            <br>
                            <br>
                            <br>
                        </div>
                        <div class="about-post__col preloader preloader--v" data-simple-tab-content="contacts-tab-2">
                            <h2>B2B</h2>
                            <p>Biz təchizatçılarla əməkdaşlıq edirik və əgər siz də bizimlə əməkdaşlıq etməkdə maraqlısınızsa, aşağıdakı formanı doldurmağınız xahiş olunur.</p>
                            <br>
                            <br>
                            <form class="contacts-form">
                                <div class="contacts-form__row">
                                    <div class="contacts-form__col contacts-form__col--half">
                                        <label class="contacts-form__label">Company name </label>
                                        <input class="contacts-form__input" type="text" name="contacts-page-company-name" placeholder="Paste your info" required>
                                    </div>
                                    <div class="contacts-form__col contacts-form__col--half">
                                        <label class="contacts-form__label">Category</label>
                                        <div class="select-wrap select-wrap--form-page">
                                            <div class="select-wrap__text select-wrap__text--full" data-select-wrap="fsr-select-2.1" data-select-focus="false">
                                                <input class="select-wrap__input" type="hidden" name="contacts-page-family" value="Choose your info" data-select-input="fsr-select-2.1">
                                                <div class="select-wrap__selected select-wrap__selected--grey" data-selected-item="fsr-select-2.1" data-selected-focus="false">Choose your info </div>
                                                <svg class="icon icon-arrow select-wrap__icon-svg select-wrap__icon-svg--arrow">
                                                    <use xlink:href="{{asset('front/images/sprite/sprite.svg#arrow')}}"></use>
                                                </svg>
                                            </div>
                                            <div class="select-wrap__option select-wrap__option--form-page" data-select-option="fsr-select-2.1" data-select-option-opened="false" data-select-option-scroll="true">
                                                <span class="select-wrap__option-item select-wrap__option-item--form-page" data-select-option-items="Choose your info 1" data-select-option-input="fsr-select-2.1">Choose your info 1 </span>
                                                <span class="select-wrap__option-item select-wrap__option-item--form-page" data-select-option-items="Choose your info 2" data-select-option-input="fsr-select-2.1">Choose your info 2 </span>
                                                <span class="select-wrap__option-item select-wrap__option-item--form-page" data-select-option-items="Choose your info 3" data-select-option-input="fsr-select-2.1">Choose your info 3 </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="contacts-form__row">
                                    <div class="contacts-form__col contacts-form__col--half">
                                        <label class="contacts-form__label">Phone </label>
                                        <input class="contacts-form__input" type="number" name="contacts-page-number" placeholder="Paste your info" required>
                                    </div>
                                </div>
                                <div class="contacts-form__row">
                                    <div class="contacts-form__col contacts-form__col--full">
                                        <label class="contacts-form__label">Reviews </label>
                                        <textarea class="contacts-form__textarea" name="contacts-page-review" placeholder="Please help me contact with your company"></textarea>
                                    </div>
                                </div>
                                <div class="contacts-form__row">
                                    <div class="contacts-form__col contacts-form__col--half">
                                        <label class="contacts-form__label">Legal address (str.avn. apt.etc) </label>
                                        <input class="contacts-form__input" type="number" name="contacts-page-legal-address" placeholder="Paste your info" required>
                                    </div>
                                    <div class="contacts-form__col contacts-form__col--half">
                                        <label class="contacts-form__label">Email address</label>
                                        <input class="contacts-form__input" type="email" name="contacts-page-email" placeholder="Paste your info" required>
                                    </div>
                                </div>
                                <div class="contacts-form__row">
                                    <div class="contacts-form__col contacts-form__col--half">
                                        <div class="contact-form-loaded">
                                            <input class="contact-form-loaded__input" type="file" name="contacts-page-presentation-file">
                                            <button class="contact-form-loaded__close">
                                                <svg class="icon icon-close contact-form-loaded__icon-svg">
                                                    <use xlink:href="{{asset('front/images/sprite/sprite.svg#close')}}"></use>
                                                </svg>
                                            </button>
                                            <div class="contact-form-loaded__inner">
                                                <div class="contact-form-loaded__img">
                                                    <img class="contact-form-loaded__icon blazy blazy--style" src="./" data-src="{{asset("front/images/main/icon/pdf_active.svg")}}" alt="bravo market">
                                                    <img class="contact-form-loaded__icon blazy blazy--style" src="./" data-src="{{asset("front/images/main/icon/pdf.svg")}}" alt="bravo market">
                                                </div>
                                                <div class="contact-form-loaded__content">
                                                    <h4 class="contact-form-loaded__title">Products price list</h4>
                                                    <p class="contact-form-loaded__descr">Formats: .pdf .png .jpeg .jpg </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="contacts-form__col contacts-form__col--half">
                                        <div class="contact-form-loaded">
                                            <input class="contact-form-loaded__input" type="file" name="contacts-page-price-file">
                                            <button class="contact-form-loaded__close">
                                                <svg class="icon icon-close contact-form-loaded__icon-svg">
                                                    <use xlink:href="{{asset('front/images/sprite/sprite.svg#close')}}"></use>
                                                </svg>
                                            </button>
                                            <div class="contact-form-loaded__inner">
                                                <div class="contact-form-loaded__img">
                                                    <img class="contact-form-loaded__icon blazy blazy--style" src="./" data-src="{{asset("front/images/main/icon/pdf_active.svg")}}" alt="bravo market">
                                                    <img class="contact-form-loaded__icon blazy blazy--style" src="./" data-src="{{asset("front/images/main/icon/pdf.svg")}}" alt="bravo market">
                                                </div>
                                                <div class="contact-form-loaded__content">
                                                    <h4 class="contact-form-loaded__title">Products price list</h4>
                                                    <p class="contact-form-loaded__descr">Formats: .pdf .png .jpeg .jpg </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="contacts-form__row">
                                    <div class="contacts-form__col contacts-form__col--half">
                                        <div class="contact-form-loaded">
                                            <input class="contact-form-loaded__input" type="file" name="contacts-page-cert-file-1">
                                            <button class="contact-form-loaded__close">
                                                <svg class="icon icon-close contact-form-loaded__icon-svg">
                                                    <use xlink:href="{{asset('front/images/sprite/sprite.svg#close')}}"></use>
                                                </svg>
                                            </button>
                                            <div class="contact-form-loaded__inner">
                                                <div class="contact-form-loaded__img">
                                                    <img class="contact-form-loaded__icon blazy blazy--style" src="./" data-src="{{asset('front/images/main/icon/pdf_active.svg')}}" alt="bravo market">
                                                    <img class="contact-form-loaded__icon blazy blazy--style" src="./" data-src="{{asset("front/images/main/icon/pdf.svg")}}" alt="bravo market">
                                                </div>
                                                <div class="contact-form-loaded__content">
                                                    <h4 class="contact-form-loaded__title">Company certificates</h4>
                                                    <p class="contact-form-loaded__descr">Formats: .pdf .png .jpeg .jpg </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="contacts-form__col contacts-form__col--half">
                                        <div class="contact-form-loaded">
                                            <input class="contact-form-loaded__input" type="file" name="contacts-page-cert-file-2">
                                            <button class="contact-form-loaded__close">
                                                <svg class="icon icon-close contact-form-loaded__icon-svg">
                                                    <use xlink:href="{{asset('front/images/sprite/sprite.svg#close')}}"></use>
                                                </svg>
                                            </button>
                                            <div class="contact-form-loaded__inner">
                                                <div class="contact-form-loaded__img">
                                                    <img class="contact-form-loaded__icon blazy blazy--style" src="./" data-src="{{asset('front/images/main/icon/pdf_active.svg')}}" alt="bravo market">
                                                    <img class="contact-form-loaded__icon blazy blazy--style" src="./" data-src="{{asset("front/images/main/icon/pdf.svg")}}" alt="bravo market">
                                                </div>
                                                <div class="contact-form-loaded__content">
                                                    <h4 class="contact-form-loaded__title">Company certificates</h4>
                                                    <p class="contact-form-loaded__descr">Formats: .pdf .png .jpeg .jpg </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="contacts-form__row contacts-form__row--send-btn">
                                    <div class="contacts-form__col contacts-form__col--half">
                                        <button class="contacts-form__send-btn primary-btn primary-btn--green">
                                            <svg class="icon icon-send contacts-form__send-icon">
                                                <use xlink:href="{{asset('front/images/sprite/sprite.svg#send')}}"></use>
                                            </svg>Send Form
                                        </button>
                                    </div>
                                </div>
                            </form>
                            <br>
                            <br>
                            <br>
                            <br>
                        </div>
                        <div class="about-post__col preloader preloader--v" data-simple-tab-content="contacts-tab-3">
                            <h2>Advertisers</h2>
                            <div class="article-slider">
                                <div class="article-slider__main article-slider__main--slider-1 swiper-container preloader preloader--v">
                                    <div class="swiper-wrapper">
                                        <div class="swiper-slide">
                                            <div class="article-slider__slide">
                                                <img class="swiper-lazy" src="./" data-src="./images/main/adv/s1.jpg" alt="bravo posr slider img">
                                            </div>
                                        </div>
                                        <div class="swiper-slide">
                                            <div class="article-slider__slide">
                                                <img class="swiper-lazy" src="./" data-src="./images/main/adv/s1.jpg" alt="bravo posr slider img">
                                            </div>
                                        </div>
                                        <div class="swiper-slide">
                                            <div class="article-slider__slide">
                                                <img class="swiper-lazy" src="./" data-src="./images/main/adv/s1.jpg" alt="bravo posr slider img">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="article-slide-count article-slide-count--1"></div>
                                <div class="article-slider-navigation">
                                    <button class="article-slider-navigation__btn article-slider-navigation__btn--prev article-slider-navigation__btn--prev-js-1">
                                        <svg class="icon icon-left_arr pdf-booklet-controls__icon">
                                            <use xlink:href="{{asset('front/images/sprite/sprite.svg#left_arr')}}"></use>
                                        </svg>
                                    </button>
                                    <button class="article-slider-navigation__btn article-slider-navigation__btn--next article-slider-navigation__btn--next-js-1">
                                        <svg class="icon icon-right_arr pdf-booklet-controls__icon">
                                            <use xlink:href="{{asset('front/images/sprite/sprite.svg#right_arr')}}"></use>
                                        </svg>
                                    </button>
                                </div>
                            </div>
                            <p>Biz təchizatçılarla əməkdaşlıq edirik və əgər siz də bizimlə əməkdaşlıq etməkdə maraqlısınızsa, aşağıdakı formanı doldurmağınız xahiş olunur.</p>
                            <br>
                            <br>
                            <form class="contacts-form">
                                <div class="contacts-form__row">
                                    <div class="contacts-form__col contacts-form__col--half">
                                        <label class="contacts-form__label">Company name </label>
                                        <input class="contacts-form__input" type="text" name="contacts-page-company-name" placeholder="Paste your info" required>
                                    </div>
                                    <div class="contacts-form__col contacts-form__col--half">
                                        <label class="contacts-form__label">Relevant person</label>
                                        <input class="contacts-form__input" type="text" name="contacts-page-company-relevant-person" placeholder="Paste your info" required>
                                    </div>
                                </div>
                                <div class="contacts-form__row">
                                    <div class="contacts-form__col contacts-form__col--half">
                                        <label class="contacts-form__label">Email address</label>
                                        <input class="contacts-form__input" type="text" name="contacts-page-company-email" placeholder="Paste your info" required>
                                    </div>
                                    <div class="contacts-form__col contacts-form__col--half">
                                        <label class="contacts-form__label">Phone number </label>
                                        <input class="contacts-form__input" type="number" name="contacts-page-number" placeholder="Paste your info" required>
                                    </div>
                                </div>
                                <div class="contacts-form__row contacts-form__row--send-btn">
                                    <div class="contacts-form__col contacts-form__col--half">
                                        <button class="contacts-form__send-btn primary-btn primary-btn--green">
                                            <svg class="icon icon-send contacts-form__send-icon">
                                                <use xlink:href="{{asset('front/images/sprite/sprite.svg#send')}}"></use>
                                            </svg>Send Form
                                        </button>
                                    </div>
                                </div>
                            </form>
                            <br>
                            <br>
                            <br>
                            <br>
                            <div class="contacts-info-box">
                                <p class="contacts-info-box__title">Daha ətraflı məlumat üçün: </p>
                                <div class="contacts-info-box__link-box">
                                    <a class="contacts-info-box__link" href="tel:0512501094">
                                        <svg class="icon icon-phone contacts-info-box__icon">
                                            <use xlink:href="{{asset('front/images/sprite/sprite.svg#phone')}}"></use>
                                        </svg>(051) 250 10 94 </a>
                                    <a class="contacts-info-box__link" href="mailto: info@bravosupermarket.az">
                                        <svg class="icon icon-envelope contacts-info-box__icon">
                                            <use xlink:href="{{asset('front/images/sprite/sprite.svg#envelope')}}"></use>
                                        </svg>info@bravosupermarket.az</a>
                                </div>
                            </div>
                        </div>
                        <div class="about-post__col preloader preloader--v" data-simple-tab-content="contacts-tab-4">
                            <h2>Media & Press</h2>
                            <p>KİV ilə bağlı hər hansı sualınız olarsa, xahiş edirik, aşağıdakı formadan istifadə edin:</p>
                            <br>
                            <br>
                            <form class="contacts-form">
                                <div class="contacts-form__row">
                                    <div class="contacts-form__col contacts-form__col--half">
                                        <label class="contacts-form__label">Company name </label>
                                        <input class="contacts-form__input" type="text" name="contacts-page-company-name" placeholder="Paste your info" required>
                                    </div>
                                    <div class="contacts-form__col contacts-form__col--half">
                                        <label class="contacts-form__label">Relevant person</label>
                                        <input class="contacts-form__input" type="text" name="contacts-page-company-relevant-person" placeholder="Paste your info" required>
                                    </div>
                                </div>
                                <div class="contacts-form__row">
                                    <div class="contacts-form__col contacts-form__col--half">
                                        <label class="contacts-form__label">Email address</label>
                                        <input class="contacts-form__input" type="text" name="contacts-page-company-email" placeholder="Paste your info" required>
                                    </div>
                                    <div class="contacts-form__col contacts-form__col--half">
                                        <label class="contacts-form__label">Phone number </label>
                                        <input class="contacts-form__input" type="number" name="contacts-page-number" placeholder="Paste your info" required>
                                    </div>
                                </div>
                                <div class="contacts-form__row">
                                    <div class="contacts-form__col contacts-form__col--half">
                                        <label class="contacts-form__label">Subject</label>
                                        <input class="contacts-form__input" type="text" name="contacts-page-company-email" placeholder="Paste your info" required>
                                    </div>
                                </div>
                                <div class="contacts-form__row">
                                    <div class="contacts-form__col contacts-form__col--full">
                                        <label class="contacts-form__label">Message </label>
                                        <textarea class="contacts-form__textarea" name="contacts-page-message" placeholder="Please help me contact with your company"></textarea>
                                    </div>
                                </div>
                                <div class="contacts-form__row contacts-form__row--send-btn">
                                    <div class="contacts-form__col contacts-form__col--half">
                                        <button class="contacts-form__send-btn primary-btn primary-btn--green">
                                            <svg class="icon icon-send contacts-form__send-icon">
                                                <use xlink:href="{{asset('front/images/sprite/sprite.svg#send')}}"></use>
                                            </svg>Send Form
                                        </button>
                                    </div>
                                </div>
                            </form>
                            <br>
                            <br>
                            <br>
                            <br>
                        </div>
                        <div class="about-post__col preloader preloader--v" data-simple-tab-content="contacts-tab-5">
                            <h2>Feedback</h2>
                            <p>Sual və ya rəyiniz olduqda, xahiş edirik, aşağıdakı əlaqə formasını doldurun.</p>
                            <br>
                            <br>
                            <form class="contacts-form">
                                <div class="contacts-form__row">
                                    <div class="contacts-form__col contacts-form__col--half">
                                        <label class="contacts-form__label">First Name</label>
                                        <input class="contacts-form__input" type="text" name="contacts-page-feadback-first-name" placeholder="Paste your info" required>
                                    </div>
                                    <div class="contacts-form__col contacts-form__col--half">
                                        <label class="contacts-form__label">Last Name</label>
                                        <input class="contacts-form__input" type="text" name="contacts-page-feadback-last-name" placeholder="Paste your info" required>
                                    </div>
                                </div>
                                <div class="contacts-form__row">
                                    <div class="contacts-form__col contacts-form__col--half">
                                        <label class="contacts-form__label">Email address</label>
                                        <input class="contacts-form__input" type="text" name="contacts-page-feadback-company-email" placeholder="Paste your info" required>
                                    </div>
                                    <div class="contacts-form__col contacts-form__col--half">
                                        <label class="contacts-form__label">Phone number </label>
                                        <input class="contacts-form__input" type="number" name="contacts-page-feadback-number" placeholder="Paste your info" required>
                                    </div>
                                </div>
                                <div class="contacts-form__row">
                                    <div class="contacts-form__col contacts-form__col--half">
                                        <label class="contacts-form__label">Request type: </label>
                                        <div class="select-wrap__text select-wrap__text--full" data-select-wrap="fsr-select-4.1" data-select-focus="false">
                                            <input class="select-wrap__input" type="hidden" name="contacts-page-family" value="Choose your info" data-select-input="fsr-select-4.1">
                                            <div class="select-wrap__selected select-wrap__selected--grey" data-selected-item="fsr-select-4.1" data-selected-focus="false">Choose request type</div>
                                            <svg class="icon icon-arrow select-wrap__icon-svg select-wrap__icon-svg--arrow">
                                                <use xlink:href="{{asset('front/images/sprite/sprite.svg#arrow')}}"></use>
                                            </svg>
                                            <div class="select-wrap__option select-wrap__option--form-page" data-select-option="fsr-select-4.1" data-select-option-opened="false" data-select-option-scroll="true">
                                                <span class="select-wrap__option-item select-wrap__option-item--form-page" data-select-option-items="Choose request type 1" data-select-option-input="fsr-select-4.1">Choose request type 1 </span>
                                                <span class="select-wrap__option-item select-wrap__option-item--form-page" data-select-option-items="Choose request type 2" data-select-option-input="fsr-select-4.1">Choose request type 2 </span>
                                                <span class="select-wrap__option-item select-wrap__option-item--form-page" data-select-option-items="Choose request type 3" data-select-option-input="fsr-select-4.1">Choose request type 3 </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="contacts-form__row">
                                    <div class="contacts-form__col contacts-form__col--full">
                                        <label class="contacts-form__label">Message </label>
                                        <textarea class="contacts-form__textarea" name="contacts-page-feadback-message" placeholder="Please help me contact with your company"> </textarea>
                                    </div>
                                </div>
                                <div class="contacts-form__row contacts-form__row--send-btn">
                                    <div class="contacts-form__col contacts-form__col--half">
                                        <button class="contacts-form__send-btn primary-btn primary-btn--green">
                                            <svg class="icon icon-send contacts-form__send-icon">
                                                <use xlink:href="{{asset('front/images/sprite/sprite.svg#send')}}"></use>
                                            </svg>Send Form
                                        </button>
                                    </div>
                                </div>
                            </form>
                            <br>
                            <br>
                            <br>
                            <br>
                        </div>
                    </article>
                </div>
            </div>
        </section>
    </main>
@endsection
