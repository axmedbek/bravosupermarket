<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>Bravo Market</title>
    <meta content="" name="webskyus">
    <meta content="" name="this is description">
    <meta content="" name="website, landing">
    <meta content="width=device-width, initial-scale=1" name="viewport">
    <meta content="ie=edge" http-equiv="x-ua-compatible">
    <link rel="icon" href="./images/main/favicon/favicon.ico">
    <link rel="apple-touch-icon" href="./images/main/favicon/apple-icon-180x180.png">
    <link rel="stylesheet" href="style/main.min.css">
</head>

<body>
    <div class="body-wrapper">
        <header class="header">
            <div class="container">
                <div class="row">
                    <nav class="main-nav">
                        <a class="logo preloader preloader--v" href="#">
                            <img class="blazy blazy--style" src="./" data-src="./images/main/icon/l_vertical.svg" alt="bravo market logo">
                        </a>
                        <ul class="main-menu">
                            <li class="main-menu__list main-menu__list--active preloader preloader--v">
                                <a class="main-menu__link" href="#">About Us</a>
                            </li>
                            <li class="main-menu__list preloader preloader--v">
                                <a class="main-menu__link" href="#">Contact us</a>
                            </li>
                            <li class="main-menu__list preloader preloader--v">
                                <a class="main-menu__link" href="#">Careers</a>
                            </li>
                            <li class="main-menu__list preloader preloader--v">
                                <a class="main-menu__link" href="#">Customer Service</a>
                            </li>
                            <li class="main-menu__list preloader preloader--v">
                                <a class="main-menu__link" href="#">Leaflet</a>
                            </li>
                            <li class="main-menu__list preloader preloader--v">
                                <a class="main-menu__link" href="#">Cards</a>
                            </li>
                            <li class="main-menu__list preloader preloader--v">
                                <a class="main-menu__link" href="#">CSR Activities</a>
                            </li>
                        </ul>
                    </nav>
                    <div class="store-place">
                        <a class="store-search preloader preloader--v" href="#">
                            <span class="store-search__icon">
                                <svg class="icon icon-loc store-search__icon-svg">
                                    <use xlink:href="./images/sprite/sprite.svg#loc"></use>
                                </svg>
                            </span>
                            <span class="store-search__text store-search__text--mobile">
                                <span class="store-search__descr">Stores </span>
                            </span>
                            <span class="store-search__text">
                                <span class="store-search__descr">Find stores </span>
                                <span class="store-search__title">Baku </span>
                            </span>
                        </a>
                        <div class="lang preloader preloader--v">
                            <div class="select-wrap">
                                <div class="select-wrap__text" data-select-wrap="s1" data-select-focus="false">
                                    <input class="select-wrap__input" type="hidden" name="site-lang" value="az" data-select-input="s1">
                                    <svg class="icon icon-lang select-wrap__icon-svg">
                                        <use xlink:href="./images/sprite/sprite.svg#lang"></use>
                                    </svg>
                                    <div class="select-wrap__selected" data-selected-item="s1" data-selected-focus="false">az </div>
                                </div>
                                <div class="select-wrap__option" data-select-option="s1" data-select-option-opened="false">
                                    <span class="select-wrap__option-item" data-select-option-items="az" data-select-option-input="s1">az </span>
                                    <span class="select-wrap__option-item" data-select-option-items="en" data-select-option-input="s1">en </span>
                                    <span class="select-wrap__option-item" data-select-option-items="ru" data-select-option-input="s1">ru </span>
                                </div>
                            </div>
                        </div>
                        <button class="hamburger preloader preloader--v">
                            <span class="hamburger__item"></span>
                            <span class="hamburger__item"></span>
                            <span class="hamburger__item"></span>
                        </button>
                    </div>
                </div>
            </div>
        </header>
        <!-- main -->
        <main class="main">
            <section class="breadcrumbs">
                <div class="container">
                    <div class="row">
                        <div class="breadcrumbs_col">
                            <div class="breadcrumbs__nav">
                                <a class="breadcrumbs__item preloader-overlay preloader-overlay--vertical" href="#">
                                    <svg class="icon icon-home breadcrumbs__icon">
                                        <use xlink:href="./images/sprite/sprite.svg#home"></use>
                                    </svg>
                                </a>
                                <span class="breadcrumbs__item breadcrumbs__item--inactive preloader-overlay preloader-overlay--vertical">
                                    <svg class="icon icon-arrow breadcrumbs__icon breadcrumbs__icon--arrow">
                                        <use xlink:href="./images/sprite/sprite.svg#arrow"></use>
                                    </svg>Find Stores</span>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section class="inner-top-thumb section--mb-inner">
                <div class="container container--full">
                    <div class="row">
                        <div class="container-inner">
                            <div class="inner-top-thumb__box preloader preloader--v" style="background: url(./images/main/about/thumb.jpg) no-repeat center/cover">
                                <h1 class="inner-top-thumb__title"> Find Stores
                                </h1>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section class="store-find section--mb">
                <div class="container">
                    <div class="row row--align-center">
                        <div class="store-find__col-nav">
                            <div class="store-find-collection">
                                <div class="store-find-collection__row store-find-collection__row--fxd-row">
                                    <h2 class="preloader preloader--v store-find__caption">Find stores</h2>
                                    <button class="preloader preloader--v store-find__current-loc-search">Use current location</button>
                                </div>
                                <div class="store-find-collection__row store-find-collection__row--fxd-row">
                                    <form class="store-find-form">
                                        <img class="blazy blazy--style" src="./" data-src="./images/main/icon/find.svg" alt="bravo market">
                                        <input class="preloader preloader--v store-find-form__search" type="search" name="store-find-form-search" placeholder="find a stores by adress">
                                    </form>
                                </div>
                                <div class="store-find-collection__row">
                                    <h2 class="preloader preloader--v store-find-collection__title">Bravo Supermarket</h2>
                                    <div class="store-find-collection__wrap">
                                        <p class="preloader preloader--v store-find-collection__address">Near “KOROĞLU” m/s Baku, 1010, Azerbaijan</p>
                                        <a class="preloader preloader--v store-find-collection__route-btn" href="https://www.google.com/maps?saddr=Current+Location&amp;daddr=bravo+supermarket+near+Koroğlu+Parkı,+Mirali+Gashgai,+Baku"
                                            target="_blank">
                                            <svg class="icon icon-route store-find-collection__route-icon">
                                                <use xlink:href="./images/sprite/sprite.svg#route"></use>
                                            </svg>Route</a>
                                    </div>
                                    <div class="store-find-collection__status-box store-find-collection__status-box--fxd-column">
                                        <div class="store-find-collection__status-box-row store-find-collection__status-box-row--fxd-row">
                                            <p class="preloader preloader--v store-find-collection__shedule store-find-collection__shedule--bold">Today</p>
                                            <p class="preloader preloader--v store-find-collection__shedule">9:00 am –  11:00 pm</p>
                                            <p class="preloader preloader--v store-find-collection__info store-find-collection__info--work-info store-find-collection__info--closed">
                                                <svg class="icon icon-closed store-find-collection__info-svg">
                                                    <use xlink:href="./images/sprite/sprite.svg#closed"></use>
                                                </svg>Closed now
                                            </p>
                                        </div>
                                        <div class="store-find-collection__status-box-row store-find-collection__status-box-row--fxd-row">
                                            <p class="preloader preloader--v store-find-collection__shedule store-find-collection__shedule--bold">Weekly</p>
                                            <p class="preloader preloader--v store-find-collection__shedule">Monday - Sunday
                                                <br>9:00 am –  11:00 pm</p>
                                        </div>
                                        <div class="store-find-collection__status-box-row store-find-collection__status-box-row--fxd-row">
                                            <p class="preloader preloader--v store-find-collection__shedule store-find-collection__shedule--bold">Phone</p>
                                            <a class="preloader preloader--v store-find-collection__shedule store-find-collection__shedule--link" href="tel:4041378">404 13 78</a>
                                        </div>
                                        <div class="store-find-collection__status-box-row store-find-collection__status-box-row--fxd-row">
                                            <a class="preloader preloader--v store-find-collection__info store-find-collection__info--middle store-find-collection__info--link" href="#">
                                                <svg class="icon icon-info store-find-collection__info-svg">
                                                    <use xlink:href="./images/sprite/sprite.svg#info"></use>
                                                </svg>Need more information?</a>
                                        </div>
                                        <div class="store-find-collection__status-box-row store-find-collection__status-box-row--fxd-row">
                                            <a class="preloader preloader--v store-find-collection__back-btn" href="#">
                                                <svg class="icon icon-arrow store-find-collection__back-btn-icon">
                                                    <use xlink:href="./images/sprite/sprite.svg#arrow"></use>
                                                </svg>Back </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="store-find__col-map">
                            <div class="store-find__map" id="map1"></div>
                        </div>
                    </div>
                </div>
            </section>
        </main>
        <footer class="footer footer--white">
            <div class="container">
                <div class="row footer__row-nav">
                    <nav class="footer-nav">
                        <ul class="footer-nav__menu">
                            <li class="footer-nav__items footer-nav__items--title preloader preloader--v"> Quick links</li>
                            <li class="footer-nav__items preloader preloader--v">
                                <a class="footer-nav__link" href="#">About us</a>
                            </li>
                            <li class="footer-nav__items preloader preloader--v">
                                <a class="footer-nav__link" href="#">Promotions</a>
                            </li>
                            <li class="footer-nav__items preloader preloader--v">
                                <a class="footer-nav__link" href="#">Press center</a>
                            </li>
                            <li class="footer-nav__items preloader preloader--v">
                                <a class="footer-nav__link" href="#">Recipes</a>
                            </li>
                            <li class="footer-nav__items preloader preloader--v">
                                <a class="footer-nav__link" href="#">Our stores</a>
                            </li>
                            <li class="footer-nav__items preloader preloader--v">
                                <a class="footer-nav__link" href="#">Contact us </a>
                            </li>
                        </ul>
                    </nav>
                    <nav class="footer-nav">
                        <ul class="footer-nav__menu">
                            <li class="footer-nav__items footer-nav__items--title preloader preloader--v"> About Bravo</li>
                            <li class="footer-nav__items preloader preloader--v">
                                <a class="footer-nav__link" href="#">History</a>
                            </li>
                            <li class="footer-nav__items preloader preloader--v">
                                <a class="footer-nav__link" href="#">Mission & values</a>
                            </li>
                            <li class="footer-nav__items preloader preloader--v">
                                <a class="footer-nav__link" href="#">Local charity</a>
                            </li>
                            <li class="footer-nav__items preloader preloader--v">
                                <a class="footer-nav__link" href="#">Sustainability</a>
                            </li>
                            <li class="footer-nav__items preloader preloader--v">
                                <a class="footer-nav__link" href="#">Headquarters</a>
                            </li>
                            <li class="footer-nav__items preloader preloader--v">
                                <a class="footer-nav__link" href="#">Countries of operation</a>
                            </li>
                            <li class="footer-nav__items preloader preloader--v">
                                <a class="footer-nav__link" href="#">Compliance </a>
                            </li>
                        </ul>
                    </nav>
                    <nav class="footer-nav">
                        <ul class="footer-nav__menu">
                            <li class="footer-nav__items footer-nav__items--title preloader preloader--v"> Products & Services</li>
                            <li class="footer-nav__items preloader preloader--v">
                                <a class="footer-nav__link" href="#">Departments</a>
                            </li>
                            <li class="footer-nav__items preloader preloader--v">
                                <a class="footer-nav__link" href="#">Quality standards</a>
                            </li>
                            <li class="footer-nav__items preloader preloader--v">
                                <a class="footer-nav__link" href="#">Food safety</a>
                            </li>
                            <li class="footer-nav__items preloader preloader--v">
                                <a class="footer-nav__link" href="#">Product manuals</a>
                            </li>
                            <li class="footer-nav__items preloader preloader--v">
                                <a class="footer-nav__link" href="#">Product videos </a>
                            </li>
                        </ul>
                    </nav>
                    <nav class="footer-nav">
                        <ul class="footer-nav__menu">
                            <li class="footer-nav__items footer-nav__items--title preloader preloader--v"> Customer care</li>
                            <li class="footer-nav__items preloader preloader--v">
                                <a class="footer-nav__link" href="#">Potential suppliers</a>
                            </li>
                            <li class="footer-nav__items preloader preloader--v">
                                <a class="footer-nav__link" href="#">Real estate</a>
                            </li>
                            <li class="footer-nav__items preloader preloader--v">
                                <a class="footer-nav__link" href="#">FAQ</a>
                            </li>
                            <li class="footer-nav__items preloader preloader--v">
                                <a class="footer-nav__link" href="#">Product recalls </a>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
        </footer>
        <footer class="footer footer--green">
            <div class="container">
                <div class="row">
                    <div class="footer__col-logo preloader preloader--v">
                        <a class="footer__logo" href="#">
                            <img class="blazy blazy--style" src="./" data-src="./images/main/icon/l_vertical.svg" alt="bravo market logo">
                        </a>
                    </div>
                    <div class="footer__col-content">
                        <div class="footer-nav-about">
                            <nav class="footer-nav footer-nav--about preloader preloader--v">
                                <ul class="footer-nav__menu">
                                    <li class="footer-nav__items footer-nav__items--title footer-nav__items--title-white"> Get Bravo app</li>
                                    <li class="footer-nav__items">
                                        <a class="footer-nav__link footer-nav__link--white" href="#">Learn more</a>
                                    </li>
                                    <li class="footer-nav__items">
                                        <a class="footer-nav__link footer-nav__link--white" href="#">Google play</a>
                                    </li>
                                    <li class="footer-nav__items">
                                        <a class="footer-nav__link footer-nav__link--white" href="#">App store</a>
                                    </li>
                                </ul>
                            </nav>
                            <nav class="footer-nav footer-nav--about preloader preloader--v">
                                <ul class="footer-nav__menu">
                                    <li class="footer-nav__items footer-nav__items--title footer-nav__items--title-white">Legal</li>
                                    <li class="footer-nav__items">
                                        <a class="footer-nav__link footer-nav__link--white" href="#">Privacy policy</a>
                                    </li>
                                    <li class="footer-nav__items">
                                        <a class="footer-nav__link footer-nav__link--white" href="#">Terms of use</a>
                                    </li>
                                    <li class="footer-nav__items">
                                        <a class="footer-nav__link footer-nav__link--white" href="#">Contest rules and regulations</a>
                                    </li>
                                </ul>
                            </nav>
                            <p class="footer-nav-about__copyright preloader preloader--v">All rights reserved © Azerbaijan Supermarket MMC 2020 </p>
                        </div>
                    </div>
                    <div class="footer__col-social">
                        <div class="footer-social">
                            <h4 class="footer-social__title preloader preloader--v">Connect with us</h4>
                            <div class="footer-social__wrap">
                                <a class="footer-social__link preloader preloader--v" href="#">
                                    <svg class="icon icon-fb footer-social__icon">
                                        <use xlink:href="./images/sprite/sprite.svg#fb"></use>
                                    </svg>
                                </a>
                                <a class="footer-social__link preloader preloader--v" href="#">
                                    <svg class="icon icon-tw footer-social__icon">
                                        <use xlink:href="./images/sprite/sprite.svg#tw"></use>
                                    </svg>
                                </a>
                                <a class="footer-social__link preloader preloader--v" href="#">
                                    <svg class="icon icon-ins footer-social__icon">
                                        <use xlink:href="./images/sprite/sprite.svg#ins"></use>
                                    </svg>
                                </a>
                                <a class="footer-social__link preloader preloader--v" href="#">
                                    <svg class="icon icon-yt1 footer-social__icon">
                                        <use xlink:href="./images/sprite/sprite.svg#yt1"></use>
                                    </svg>
                                </a>
                                <a class="footer-social__link preloader preloader--v" href="#">
                                    <svg class="icon icon-lk footer-social__icon">
                                        <use xlink:href="./images/sprite/sprite.svg#lk"></use>
                                    </svg>
                                </a>
                                <a class="footer-social__link preloader preloader--v" href="#">
                                    <svg class="icon icon-pt footer-social__icon">
                                        <use xlink:href="./images/sprite/sprite.svg#pt"></use>
                                    </svg>
                                </a>
                                <a class="footer-social__link preloader preloader--v" href="#">
                                    <svg class="icon icon-gg footer-social__icon">
                                        <use xlink:href="./images/sprite/sprite.svg#gg"></use>
                                    </svg>
                                </a>
                                <a class="footer-social__link preloader preloader--v" href="#">
                                    <svg class="icon icon-tg footer-social__icon">
                                        <use xlink:href="./images/sprite/sprite.svg#tg"></use>
                                    </svg>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="footer__col-copyright">
                        <p class="footer-nav-about__copyright preloader preloader--v">All rights reserved © Azerbaijan Supermarket MMC 2020 </p>
                    </div>
                    <div class="footer__col-subs">
                        <form class="footer-subs">
                            <div class="footer-subs__row">
                                <div class="footer-subs__col">
                                    <h4 class="footer-subs__title preloader preloader--v">Sign up for our newsletter</h4>
                                </div>
                            </div>
                            <div class="footer-subs__row preloader preloader--v">
                                <div class="footer-subs__col">
                                    <input class="footer-subs__input" type="email" name="footer-subs-email" placeholder="Enter your email" required>
                                    <button class="footer-subs__btn" type="submit">
                                        <svg class="icon icon-arrow-r footer-subs__icon">
                                            <use xlink:href="./images/sprite/sprite.svg#arrow-r"></use>
                                        </svg>
                                    </button>
                                </div>
                            </div>
                            <div class="footer-subs__row">
                                <div class="footer-subs__col footer-subs__col--ai-center">
                                    <a class="footer-subs__dev-logo preloader preloader--v" href="#">
                                        <img class="blazy blazy--style" src="./" data-src="./images/main/icon/dev_logo.svg" alt="bravo market">
                                    </a>
                                    <p class="footer-subs__message footer-subs__message--success preloader preloader--v">Thanks for subscribing! </p>
                                    <!-- p.footer-subs__message.footer-subs__message--error Something went wrong	-->
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </footer>
    </div>
    <script src="js/main.min.js" defer></script>
    <script src="js/map/map.js" defer></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBD_b9mYVS9lB4KzR3_ptIU8gVfx_JhHQc" defer></script>
</body>

</html>