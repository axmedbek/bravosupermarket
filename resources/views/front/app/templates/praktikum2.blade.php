@extends('front.app.careerLayout')
@section('main')
    <main class="main">
        <section class="breadcrumbs">
            <div class="container">
                <div class="row">
                    <div class="breadcrumbs_col">
                        <div class="breadcrumbs__nav">
                            <a class="breadcrumbs__item breadcrumbs__item--grey-color preloader-overlay preloader-overlay--vertical" href="#">
                                <svg class="icon icon-home breadcrumbs__icon">
                                    <use xlink:href="./images/sprite/sprite.svg#home"></use>
                                </svg>
                            </a>
                            <a class="breadcrumbs__item breadcrumbs__item--grey-color preloader-overlay preloader-overlay--vertical" href="#">
                                <svg class="icon icon-arrow breadcrumbs__icon breadcrumbs__icon--arrow">
                                    <use xlink:href="./images/sprite/sprite.svg#arrow"></use>
                                </svg>PRAKTIKUM Summer Internship Program</a>
                            <span class="breadcrumbs__item breadcrumbs__item--grey-color breadcrumbs__item--inactive preloader-overlay preloader-overlay--vertical">
                                    <svg class="icon icon-arrow breadcrumbs__icon breadcrumbs__icon--arrow">
                                        <use xlink:href="./images/sprite/sprite.svg#arrow"></use>
                                    </svg>Apply</span>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="inner-top-thumb inner-top-thumb--white-bg section--pb-inner">
            <div class="container container--full">
                <div class="row">
                    <div class="container-inner">
                        <div class="inner-top-thumb__box inner-top-thumb__box--overlay-none preloader preloader--v" style="background: url(./images/main/careers/2.jpg) no-repeat center/cover"></div>
                    </div>
                </div>
            </div>
        </section>
        <section class="careers-info section--pb">
            <div class="container">
                <div class="row">
                    <div class="careers-info__col">
                        <h1 class="preloader preloader--v careers-info__title">Acceptance of application forms has been suspended.</h1>
                        <p class="preloader preloader--v careers-info__text">Follow the news on the acceptance of application forms on our official Social Media accounts:</p>
                        <br>
                        <div class="share-box">
                            <a class="share-box__link preloader preloader--v" href="#">
                                <img class="blazy blazy--style" src="./" data-src="./images/main/icon/fb.svg" alt="bravo market">
                            </a>
                            <a class="share-box__link preloader preloader--v" href="#">
                                <img class="blazy blazy--style" src="./" data-src="./images/main/icon/in.svg" alt="bravo market">
                            </a>
                            <a class="share-box__link preloader preloader--v" href="#">
                                <img class="blazy blazy--style" src="./" data-src="./images/main/icon/lk.svg" alt="bravo market">
                            </a>
                        </div>
                        <br>
                        <br>
                        <p class="preloader preloader--v careers-info__text">In case of questions, all inquiries can be forwarded to
                            <a class="preloader preloader--v careers-info__link" href="mailto: internship@azerbaijansupermarket.com">internship@azerbaijansupermarket.com</a>
                        </p>
                        <br>
                        <br>
                    </div>
                </div>
            </div>
        </section>
    </main>
@endsection
