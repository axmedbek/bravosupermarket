@extends('front.app.layout')
@section('main')
    <main class="main">
        <section class="breadcrumbs">
            <div class="container">
                <div class="row">
                    <div class="breadcrumbs_col">
                        <div class="breadcrumbs__nav">
                            <a class="breadcrumbs__item preloader-overlay preloader-overlay--vertical" href="#">
                                <svg class="icon icon-home breadcrumbs__icon">
                                    <use xlink:href="{{asset('front/images/sprite/sprite.svg#home')}}"></use>
                                </svg>
                            </a>
                            <span class="breadcrumbs__item breadcrumbs__item--inactive preloader-overlay preloader-overlay--vertical">
                                <svg class="icon icon-arrow breadcrumbs__icon breadcrumbs__icon--arrow">
                                    <use xlink:href="{{asset('front/images/sprite/sprite.svg#arrow')}}"></use>
                                </svg>{{$page->title}}</span>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="inner-top-thumb section--mb-inner">
            <div class="container container--full">
                <div class="row">
                    <div class="container-inner">
                        <div class="inner-top-thumb__box preloader preloader--v" style="background: url({{\App\Page::getMedia($page->page_id)[0]->url}}) no-repeat center/cover">
                            <h1 class="inner-top-thumb__title"> {{$page->title}}
                            </h1>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="cards-section section--mb">
            <div class="container">
                <div class="row">
                    @foreach($cards as $key=>$card)
                        <div class="cards-section__col">
                            <div class="row">
                                <div class="preloader preloader--v cards-section__img">
                                    <img class="blazy blazy--style" src="./" data-src="{{ $card->media->url }}" alt="bravo market">
                                </div>
                                <div class="cards-section__content">
                                    <h4 class="preloader preloader--v cards-section__title">{{$card->title}} </h4>
                                    <p class="preloader preloader--v cards-section__descr" data-full-text="c{{ $key }}">
                                        {{$card->content}}
{{--                                        editör istifade etsez aşağıdakını istifade edin--}}
{{--                                        {!! $card->content !!}--}}
                                    </p>
                                    <button class="preloader preloader--v cards-section__button" data-show-full-text="c{{ $key }}">
                                        <span class="cards-section__btn-text">{{ translate('common.read_more') }} </span>
                                        <span class="cards-section__btn-text">{{ translate('common.read_less') }} </span>
                                    </button>
                                </div>
                            </div>
                        </div>

                    @endforeach
                </div>
            </div>
        </section>
    </main>
@endsection
