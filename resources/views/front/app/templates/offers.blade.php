@extends('front.app.layout')

@section('main')
    <main class="main">
        <section class="breadcrumbs">
            <div class="container">
                <div class="row">
                    <div class="breadcrumbs_col">
                        <div class="breadcrumbs__nav">
                            <a class="breadcrumbs__item preloader-overlay preloader-overlay--vertical" href="#">
                                <svg class="icon icon-home breadcrumbs__icon">
                                    <use xlink:href="{{asset("front/images/sprite/sprite.svg#home")}}"></use>
                                </svg>
                            </a>
                            <span class="breadcrumbs__item breadcrumbs__item--inactive preloader-overlay preloader-overlay--vertical">
                                <svg class="icon icon-arrow breadcrumbs__icon breadcrumbs__icon--arrow">
                                    <use xlink:href="{{asset("front/images/sprite/sprite.svg#arrow")}}"></use>
                                </svg>{{$page->title}}</span>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="inner-top-thumb section--mb-inner">
            <div class="container container--full">
                <div class="row">
                    <div class="container-inner">
                        <div class="inner-top-thumb__box preloader preloader--v" style="background: url({{$media[0]->url}}) no-repeat center/cover">
                            <h1 class="inner-top-thumb__title"> {{$page->title}}
                            </h1>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="offers-tab section--mb-inner">
            <div class="container">
                <div class="row">
                    <div class="offers-tab__col">
                        <ul class="offers-tab__menu">
                            <li class="offers-tab__items simple-tab__link--active preloader preloader--v" data-simple-tab-nav="offers-tab-1">
                                <span class="offers-tab__link">{{ translate('special.offers') }}</span>
                            </li>
                            <li class="offers-tab__items preloader preloader--v" data-simple-tab-nav="offers-tab-2">
                                <span class="offers-tab__link">{{ translate('offer.upcoming') }}</span>
                            </li>
                        </ul>
                    </div>
                    <div class="offers-tab__col">
                        <div class="offers-tab__content preloader preloader--v simple-tab__content--active" data-simple-tab-content="offers-tab-1">
                            <div class="offers-tab-slider swiper-container">
                                <div class="swiper-wrapper" style="min-width: 640px;">
                                    @foreach($offers as $offer)
                                        <div class="swiper-slide" style="min-width: 160px;">
                                            <a class="offers-tab-slider__link" href="{{route('offers',['slug' => str_slug($offer->title),'offer_id' => $offer->offer_id])}}">
                                                <div class="offers-tab-slider__img">
                                                    <img class="blazy blazy--style" src="./" data-src="{{$offer->media->url}}" alt="bravo market goods">
                                                </div>
                                                <div class="offers-tab-slider__descr">
                                                    <time class="offers-tab-slider__time">From Thursday, 23.01</time>
                                                    <h5 class="offers-tab-slider__title">{{$offer->title}}</h5>
                                                </div>
                                            </a>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                        <div class="offers-tab__content preloader preloader--v" data-simple-tab-content="offers-tab-2">
                            <div class="offers-tab-content offers-tab-content--scrolled">
                                @foreach($upComingOffers as $upComingOffer)
                                    <a class="offers-tab-slider__link" href="{{route('offers',['slug' => str_slug($upComingOffer->title),'offer_id' => $upComingOffer->offer_id])}}">
                                        <div class="offers-tab-slider__img">
                                            <img class="blazy blazy--style" src="./" data-src="{{$upComingOffer->media->url}}" alt="bravo market goods">
                                        </div>
                                        <div class="offers-tab-slider__descr">
                                            <time class="offers-tab-slider__time">From Thursday, 23.01</time>
                                            <h5 class="offers-tab-slider__title">{{$upComingOffer->title}}</h5>
                                        </div>
                                    </a>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="shop-article offers-banner section--mb-last">
            <div class="container">
                <div class="shop-article-wrap">
                    <div class="row">
                        @foreach($offerProducts as $offerProduct)
                            <a class="shop-article__col offers-banner__col" href="#">
                                <div class="shop-article__thumb preloader preloader--v">
                                    <img style="width: 100%;" class="blazy blazy--style" src="./" data-src="{{$offerProduct->media->url}}" alt="{{$offerProduct->title}}">
                                </div>
                                <div class="shop-article__text">
                                    <h5 class="shop-article__title preloader preloader--v">{{$offerProduct->title}}</h5>
                                    <p class="shop-article__descr preloader preloader--v">{{$offerProduct->text}}</p>
                                </div>
                            </a>
                        @endforeach
                    </div>
                </div>
            </div>
        </section>
    </main>
@endsection
