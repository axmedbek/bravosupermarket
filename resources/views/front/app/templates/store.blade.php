@extends('front.app.layout')
@section('main')
    <main class="main">
        <section class="breadcrumbs">
            <div class="container">
                <div class="row">
                    <div class="breadcrumbs_col">
                        <div class="breadcrumbs__nav">
                            <a class="breadcrumbs__item preloader-overlay preloader-overlay--vertical" href="#">
                                <svg class="icon icon-home breadcrumbs__icon">
                                    <use xlink:href="/front/images/sprite/sprite.svg#home"></use>
                                </svg>
                            </a>
                            <span
                                class="breadcrumbs__item breadcrumbs__item--inactive preloader-overlay preloader-overlay--vertical">
                                    <svg class="icon icon-arrow breadcrumbs__icon breadcrumbs__icon--arrow">
                                        <use xlink:href="/front/images/sprite/sprite.svg#arrow"></use>
                                    </svg>{{ $page->title }} {{ translate('common.see_more') }} </span>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="inner-top-thumb section--mb-inner">
            <div class="container container--full">
                <div class="row">
                    <div class="container-inner">
                        <div class="inner-top-thumb__box preloader preloader--v"
                             style="background: url({{$media[0]->url}}) no-repeat center/cover">
                            <h1 class="inner-top-thumb__title">{{ $page->title }}
                            </h1>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="store-find section--mb">
            <div class="container">
                <div class="row row--align-center">
                    <div class="store-find__col-nav">
                        <div class="store-find-collection">
                            <div class="store-find-collection__row store-find-collection__row--fxd-row">
                                <h2 class="preloader preloader--v store-find__caption">{{ translate('map.find_stores') }}</h2>
                                <button class="preloader preloader--v store-find__current-loc-search">{{ translate('map.use_current_location') }}
                                </button>
                            </div>
                            <div class="store-find-collection__row store-find-collection__row--fxd-row">
                                <div class="store-find-form">
                                    <img class="blazy blazy--style" src="./" data-src="/front/images/main/icon/find.svg"
                                         alt="bravo market">
                                    <input class="preloader preloader--v store-find-form__search" type="search"
                                           name="store-find-form-search" placeholder="{{ translate('map.find_stores_by_address') }}">
                                </div>
                            </div>
                            <div id="side_location_list">

                            </div>
                        </div>
                    </div>
                    <div class="store-find__col-map">
                        <div class="store-find__map" id="map"></div>
                    </div>
                </div>
            </div>
        </section>
    </main>
    <p id="map_api_stores_url" style="display: none;">{{ route('api.stores') }}</p>
    <p id="map_api_store_url" style="display: none;">{{ route('api.store',':store_id') }}</p>
    <p id="map_api_search_url" style="display: none;">{{ route('api.store.search') }}</p>


    <p id="map_open_now" style="display: none;">{{ translate('map.open_now') }}</p>
    <p id="map_closed" style="display: none;">{{ translate('map.closed') }}</p>
    <p id="map_store_info" style="display: none;">{{ translate('map.store_info') }}</p>
    <p id="map_see_more" style="display: none;">{{ translate('common.see_more') }}</p>
    <p id="map_weekly" style="display: none;">{{ translate('map.weekly') }}</p>
    <p id="map_phone" style="display: none;">{{ translate('forms.phone') }}</p>
    <p id="map_need_more_info" style="display: none;">{{ translate('map.need_more_info') }}</p>
    <p id="map_route" style="display: none;">{{ translate('map.route') }}</p>
    <p id="map_go_back" style="display: none;">{{ translate('map.go_back') }}</p>

@endsection

@section('scripts')
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBD_b9mYVS9lB4KzR3_ptIU8gVfx_JhHQc&libraries=places" defer></script>
    <script src="{{asset('panel/js/plugins/ui/moment/moment.min.js?12345')}}"></script>
    <script src="{{ asset('front/js/map/map.js') }}" defer></script>
@endsection
