@extends('front.app.careerLayout')
@section('main')
    <main class="main">
        <section class="breadcrumbs">
            <div class="container">
                <div class="row">
                    <div class="breadcrumbs_col">
                        <div class="breadcrumbs__nav">
                            <a class="breadcrumbs__item breadcrumbs__item--grey-color preloader-overlay preloader-overlay--vertical"
                               href="#">
                                <svg class="icon icon-home breadcrumbs__icon">
                                    <use xlink:href="./images/sprite/sprite.svg#home"></use>
                                </svg>
                            </a>
                            <a class="breadcrumbs__item breadcrumbs__item--grey-color preloader-overlay preloader-overlay--vertical"
                               href="#">
                                <svg class="icon icon-arrow breadcrumbs__icon breadcrumbs__icon--arrow">
                                    <use xlink:href="./images/sprite/sprite.svg#arrow"></use>
                                </svg>{{ app()->getLocale() == "az" ? "Təcrübə Yay Təcrübə Proqramı" : "PRAKTIKUM Summer Internship Program" }}
                            </a>
                            <span class="breadcrumbs__item breadcrumbs__item--grey-color
                            breadcrumbs__item--inactive preloader-overlay preloader-overlay--vertical">
                                <svg class="icon icon-arrow breadcrumbs__icon breadcrumbs__icon--arrow">
                                    <use xlink:href="./images/sprite/sprite.svg#arrow"></use>
                                </svg>{{ app()->getLocale() == "az" ? "Təcrübə proqramı iştirakçıları" : "Praktikum intern stories" }}</span>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="inner-top-thumb inner-top-thumb--white-bg section--pb-inner">
            <div class="container container--full">
                <div class="row">
                    <div class="container-inner">
                        <div class="inner-top-thumb__box inner-top-thumb__box--overlay-none preloader preloader--v"
                             style="background: url(../../front/images/main/careers/2.jpg) no-repeat center/cover"></div>
                    </div>
                </div>
            </div>
        </section>
        <section class="careers-info section--pb">
            <div class="container">
                <div class="row">
                    <div class="careers-info__col careers-info__col--text-center">
                        <h1 class="preloader preloader--v careers-info__title careers-info__title--green">{{ $group_id }} {{ translate('praktikum.student_list_title') }}</h1>
                    </div>
                </div>
            </div>
        </section>
        <section class="careers-info section--pb">
            <div class="container">
                <div class="row">
                    <div class="careers-timeline">
                        <div class="careers-timeline__inner">
                            <div class="careers-timeline__content careers-timeline__content--left">
                                @if (isset($students[0]))
                                    @foreach($students[0] as $student)
                                        <div class="careers-timeline-user">
                                            <div class="preloader preloader--v careers-timeline-user__photo" style="width: auto">
                                                <img class="blazy blazy--style" src="./"
                                                     data-src="{{ \App\Student::getMedia($student['student_id'])[0]->url }}" alt="bravo market">
                                            </div>
                                            <div class="preloader preloader--v careers-timeline-user__descr">
                                                <div class="careers-timeline-user__name">{{ $student['name'] }}</div>
                                                <div class="careers-timeline-user__release-time">
                                                    {{ $student['title'] }}
                                                </div>
                                                <div class="careers-timeline-user__text">
                                                    {!! $student['content'] !!}
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                @endif
                            </div>
                            <div class="preloader preloader--h careers-timeline__line">
                                <div class="careers-timeline__line-inner"></div>
                                <a class="preloader preloader--v careers-timeline__load-btn careers-timeline__load-btn--desktop"
                                   href="{{ route('get.students.group',(int)$group_id - 1) }}">
                                    <span>Go to </span>
                                    <span>Previous</span>
                                    <span>year</span>
                                </a>
                            </div>
                            <div class="careers-timeline__content careers-timeline__content--right">
                                @if (isset($students[1]))
                                    @foreach($students[1] as $student)
                                        <div class="careers-timeline-user">
                                            <div class="preloader preloader--v careers-timeline-user__photo" style="width: auto">
                                                <img class="blazy blazy--style" src="./"
                                                     data-src="{{ \App\Student::getMedia($student['student_id'])[0]->url }}" alt="bravo market">
                                            </div>
                                            <div class="preloader preloader--v careers-timeline-user__descr">
                                                <div class="careers-timeline-user__name">{{ $student['name'] }}</div>
                                                <div class="careers-timeline-user__release-time">
                                                    {{ $student['title'] }}
                                                </div>
                                                <div class="careers-timeline-user__text">
                                                    {!! $student['content'] !!}
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="careers-timeline">
                        <a class="preloader preloader--v primary-btn primary-btn--green careers-timeline__load-btn careers-timeline__load-btn--mobile"
                           href="#">Go to Previous year</a>
                    </div>
                </div>
            </div>
        </section>
    </main>
@endsection
