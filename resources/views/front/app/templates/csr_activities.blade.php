@extends('front.app.layout')
@section('main')
    <main class="main">
        <section class="breadcrumbs">
            <div class="container">
                <div class="row">
                    <div class="breadcrumbs_col">
                        <div class="breadcrumbs__nav">
                            <a class="breadcrumbs__item preloader-overlay preloader-overlay--vertical" href="#">
                                <svg class="icon icon-home breadcrumbs__icon">
                                    <use xlink:href="{{asset('front/images/sprite/sprite.svg#home')}}"></use>
                                </svg>
                            </a>
                            <span class="breadcrumbs__item breadcrumbs__item--inactive preloader-overlay preloader-overlay--vertical">
                                    <svg class="icon icon-arrow breadcrumbs__icon breadcrumbs__icon--arrow">
                                        <use xlink:href="{{asset('front/images/sprite/sprite.svg#arrow')}}"></use>
                                    </svg>{{$page->title}}</span>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="inner-top-thumb section--mb-inner">
            <div class="container container--full">
                <div class="row">
                    <div class="container-inner">
                        <div class="inner-top-thumb__box preloader preloader--v" style="background: url({{$media[0]->url}}) no-repeat center/cover">
                            <h1 class="inner-top-thumb__title"> {{$page->title}}
                            </h1>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="about">
            <div class="container">
                <div class="row">
                    <aside class="about-aside">
                        <nav class="about-aside__nav">
                            <ul class="about-aside__menu">
                                <li class="about-aside__items preloader preloader--v">
                                    <span class="about-aside__link simple-tab__link--active" data-simple-tab-nav="about-tab-1">Reusable bags</span>
                                </li>
                                <li class="about-aside__items preloader preloader--v">
                                    <span class="about-aside__link" data-simple-tab-nav="about-tab-2">Donation boxes </span>
                                </li>
                                <li class="about-aside__items preloader preloader--v">
                                    <span class="about-aside__link" data-simple-tab-nav="about-tab-3">Clothing donation boxes by IDEA</span>
                                </li>
                                <li class="about-aside__items preloader preloader--v">
                                    <span class="about-aside__link" data-simple-tab-nav="about-tab-4">ASAN Mektub</span>
                                </li>
                                <li class="about-aside__items preloader preloader--v">
                                    <span class="about-aside__link" data-simple-tab-nav="about-tab-5">Sevinc Payi</span>
                                </li>
                                <li class="about-aside__items preloader preloader--v">
                                    <span class="about-aside__link" data-simple-tab-nav="about-tab-6">Ehtiyacı ola tənha yaşlılara ərzaq yardımı</span>
                                </li>
                            </ul>
                        </nav>
                    </aside>
                    <article class="about-post">
                        <div class="about-post__col preloader preloader--v simple-tab__content--active" data-simple-tab-content="about-tab-1">
                            <h2>Reusable bags</h2>
                            <img class="post-img post-img--full blazy blazy--style" src="./" data-src="{{asset('front/images/main/crs/1.jpg')}}" alt="bravo market logo">
                            <p>Let’s protect environment and nature with reusable BRAVO bags! We would give more benefit to nature by refusing the polietilen bags as contribution to protecting environment. For this reason, you can get shopping bags that
                                suit every taste at BRAVO! </p>
                            <br>
                            <br>
                            <br>
                            <br>
                        </div>
                        <div class="about-post__col preloader preloader--v" data-simple-tab-content="about-tab-2">
                            <h2>Donation boxes</h2>
                            <img class="post-img post-img--full blazy blazy--style" src="./" data-src="{{asset('front/images/main/crs/2.jpg')}}" alt="bravo market logo">
                            <p>You can support big charity campaigns by donating small amount to donation boxes by Azerbaijan Red Crescent Society at cash-points in our markets. The donations are spent by Azerbaijan Red Crescent Society for low income
                                families, lonely and chronically ill elderly, to those who apply for treatment, to the formation of warehouses for use in emergencies, as well as disasters at the local and international levels.</p>
                            <br>
                            <br>
                            <br>
                            <br>
                        </div>
                        <div class="about-post__col preloader preloader--v" data-simple-tab-content="about-tab-3">
                            <h2>Clothing donation boxes by IDEA</h2>
                            <img class="post-img post-img--full blazy blazy--style" src="./" data-src="{{asset('front/images/main/crs/3.jpg')}}" alt="bravo market logo">
                            <p>One of the easiest and pleasant ways to help someone is to give him the clothes you don’t need. You can find clothing donation containers in all BRAVO hypermarkets and superstore at Babek Avenue, prepared by IDEA Campaign
                                which is partnering with charity organizations and groups to distribute the collected items among those in need.Please consider donating your unneeded clothes and shoes, rather than sending them to landfill. By giving
                                a second life to your garments, you can help your local community without causing harm to the planet.</p>
                            <br>
                            <br>
                            <br>
                            <br>
                        </div>
                        <div class="about-post__col preloader preloader--v" data-simple-tab-content="about-tab-4">
                            <h2>ASAN Məktub</h2>
                            <img class="post-img post-img--full blazy blazy--style" src="./" data-src="{{asset('front/images/main/crs/4.jpg')}}" alt="bravo market logo">
                            <p>KSM layihəsi çərçivəsində BRAVO-nun uğurlu əməkdaşlıqlarından biri ASAN MƏKTUB-la reallaşıb. 2017-ci ilin dekabrından başlayan əməkdaşlıq “Bir qəlbi də sən isit”, “Bayram Payı” devizi ilə ümumilikdə bu günə qədər minlərlə
                                uşaqlara sevinc bəxş edib. </p>
                            <br>
                            <p>Belə ki, BRAVO hipermarketlərində Yeni İl və Novruz bayramı ərəfəsində xüsusi dekorativ ağacda yüzlərlə aztəminatlı, ehtiyacı olan uşaqların istəkləri yer alır. BRAVO müştəriləri bu ağacda yerləşən məktubları götürərək
                                onların reallaşmasında bilavasitə rol oynayır. </p>
                            <br>
                            <p>Bu günə qədər 1000-dən çox balaca fidanların arzuları gerçəkləşərək, onların sevincinə səbəb olub. </p>
                            <br>
                            <br>
                            <br>
                            <br>
                        </div>
                        <div class="about-post__col preloader preloader--v" data-simple-tab-content="about-tab-5">
                            <h2>Sevinc Payı</h2>
                            <div class="article-slider">
                                <div class="article-slider__main article-slider__main--slider-1 swiper-container preloader preloader--v">
                                    <div class="swiper-wrapper">
                                        <div class="swiper-slide">
                                            <div class="article-slider__slide">
                                                <img class="swiper-lazy" src="./" data-src="{{asset('front/images/main/crs/5.jpg')}}" alt="bravo posr slider img">
                                            </div>
                                        </div>
                                        <div class="swiper-slide">
                                            <div class="article-slider__slide">
                                                <img class="swiper-lazy" src="./" data-src="{{asset('front/images/main/crs/3.jpg')}}" alt="bravo posr slider img">
                                            </div>
                                        </div>
                                        <div class="swiper-slide">
                                            <div class="article-slider__slide">
                                                <img class="swiper-lazy" src="./" data-src="{{asset('front/images/main/crs/2.jpg')}}" alt="bravo posr slider img">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="article-slide-count article-slide-count--1"> </div>
                                <div class="article-slider-navigation">
                                    <button class="article-slider-navigation__btn article-slider-navigation__btn--prev article-slider-navigation__btn--prev-js-1">
                                        <svg class="icon icon-left_arr pdf-booklet-controls__icon">
                                            <use xlink:href="{{asset('front/images/sprite/sprite.svg#left_arr')}}"></use>
                                        </svg>
                                    </button>
                                    <button class="article-slider-navigation__btn article-slider-navigation__btn--next article-slider-navigation__btn--next-js-1">
                                        <svg class="icon icon-right_arr pdf-booklet-controls__icon">
                                            <use xlink:href="{{asset('front/images/sprite/sprite.svg#right_arr')}}"></use>
                                        </svg>
                                    </button>
                                </div>
                            </div>
                            <p>Bravo-nun keçirdiyi KSM layihələrdən biri də #PaylaşaraqSevindirək! şüarı altında satışa çıxarılan “Sevinc Payı” qutularıdır. Cəmi 4.99 AZN-a təklif olunan bu qutuların içində uşaqlar üçün şirniyyatlar, həmçinin hədiyyəli
                                kuponlar olur. Layihənin əsas məqsədi hər qutudan gələn 1 AZN gəlirin xeyriyyə məqsədilə istifadə edilməsidir. </p>
                            <br>
                            <p>Bravo bu layihəni artıq 3 dəfə reallaşdırıb: </p>
                            <br>
                            <p>Bu günə qədər 1000-dən çox balaca fidanların arzuları gerçəkləşərək, onların sevincinə səbəb olub. </p>
                            <br>
                            <ul>
                                <li>2019-cu ilin may-iyun aylarında 1 İyun Uşaqların Beynəlxalq Müdafiəsi Günü münasibətilə</li>
                                <li>2020-ci ilin fevral-aprel aylarında Novruz bayramı münasibətilə</li>
                                <li>2019-cu ilin noyabr-dekabr aylarında Yeni İl münasibətilə</li>
                            </ul>
                            <br>
                            <p>Bu günə qədər layihə vasitəsilə 14500 AZN həcmində vəsait qazanılmış və xeyriyyə məqsədilə istifadə olunmuşdur. Məbləğin ianə olunduğu qurumlar:</p>
                            <ul>
                                <li>Saray qəsəbəsində yerləşən 7 saylı fiziki və əqli qüsurlu uşaqlar üçün internat evi. Bu müəssisə Bravo tərəfindən 2 ədəd dərin dondurucu, tekstil məhsulları, şəxsi gigiyena vasitələri ilə təmin olunmuşdur</li>
                                <li>“Təmiz Dünya” Qadınlara Yardım İctimai Birliyi. Bu müəssisə Bravo tərəfindən 2 ədəd kondisioner, 1 ədəd paltaryuyan maşın, kiçik məişət texnikaları (elektrik çaydanı, ütü, ətçəkən), tekstil məhsulları, şəxsi qulluq
                                    və gigiyena vasitələri, uşaq qidası, meyvə və tərəvəz, eləcə də digər qida məhsulları ilə təmin olunmuşdur</li>
                                <li>Azərbaycan Uşaqlar Birliyi, uşaq sığınacağı – reinteqrasiya mərkəzi. Bu müəssisə Bravo tərəfindən şəxsi qulluq və gigiyena vasitələri, qida məhsulları, meyvə və tərəvəz ilə təmin olunmuşdur</li>
                                <li>Füzuli rayonu, Qayıdış qəsəbəsində yerləşən Qayıdış-9 saylı körpələr evi, uşaq bağçası. Bu müəssisə Bravo tərəfindən bağçanın eyvanının təmir olunması üçün tikinti materialları ilə təmin olunmuşdur.</li>
                            </ul>
                            <br>
                            <p>Layihənin bu günə qədər olan tərəfdaşları: OYNA uşaq meydançası, McDonald’s Azərbaycan, CinemaPlus kinoteatrı, Bumblebee, Amburan Kids, Megafun Əyləncə Mərkəzi. Tərəfdaşlar bu layihə üçün öz müəssisələrinə endirim və ya
                                hədiyyə kuponları təqdim edərək, bu KSM layihələrinə öz dəstək olmuşlar. </p>
                            <br>
                            <p>
                                Əgər siz də bu KSM layihədə tərəfdaş qismində iştirak etmək istəyirsinizsə, təklifinizi
                                <a href="#">Marketing_PrintMedia_Advertising@azerbaijansupermarket.com </a>elektron ünvanına göndərə bilərsiniz. </p>
                        </div>
                        <div class="about-post__col preloader preloader--v" data-simple-tab-content="about-tab-6">
                            <h2>Ehtiyacı ola tənha yaşlılara ərzaq yardımı</h2>
                            <div class="article-slider">
                                <div class="article-slider__main article-slider__main--slider-2 swiper-container preloader preloader--v">
                                    <div class="swiper-wrapper">
                                        <div class="swiper-slide">
                                            <div class="article-slider__slide">
                                                <img class="swiper-lazy" src="./" data-src="{{asset('front/images/main/crs/6.jpg')}}" alt="bravo posr slider img">
                                            </div>
                                        </div>
                                        <div class="swiper-slide">
                                            <div class="article-slider__slide">
                                                <img class="swiper-lazy" src="./" data-src="{{asset('front/images/main/crs/5.jpg')}}" alt="bravo posr slider img">
                                            </div>
                                        </div>
                                        <div class="swiper-slide">
                                            <div class="article-slider__slide">
                                                <img class="swiper-lazy" src="./" data-src="{{asset('front/images/main/crs/1.jpg')}}" alt="bravo posr slider img">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="article-slide-count article-slide-count--2"></div>
                                <div class="article-slider-navigation">
                                    <button class="article-slider-navigation__btn article-slider-navigation__btn--prev article-slider-navigation__btn--prev-js-2">
                                        <svg class="icon icon-left_arr pdf-booklet-controls__icon">
                                            <use xlink:href="{{asset('front/images/sprite/sprite.svg#left_arr')}}"></use>
                                        </svg>
                                    </button>
                                    <button class="article-slider-navigation__btn article-slider-navigation__btn--next article-slider-navigation__btn--next-js-2">
                                        <svg class="icon icon-right_arr pdf-booklet-controls__icon">
                                            <use xlink:href="{{asset('front/images/sprite/sprite.svg#right_arr')}}"></use>
                                        </svg>
                                    </button>
                                </div>
                            </div>
                            <p>Lacus, nibh diam, vulputate ridiculus. A, aliquam auctor hac nisi aliquam nunc aliquam nulla. Quis in ipsum neque nunc, urna. At volutpat enim maecenas nisi maecenas facilisis elit, purus. Vel eu elementum id pharetra nibh
                                sapien consequat posuere erat. </p>
                            <br>
                            <p>Semper tincidunt vel varius mauris. Rhoncus feugiat viverra sodales quis eu nulla varius nulla eget. Cursus neque luctus id proin. Lectus tristique sed nunc, vulputate ac lorem in commodo non. Tellus sem sed quam semper.
                                Et vel nec, venenatis risus augue. Id egestas arcu urna, volutpat volutpat pretium risus. Leo nec tempor urna at id sit in neque. Aliquam nunc netus tempor vitae, tortor, sed duis. Mi.</p>
                        </div>
                    </article>
                </div>
            </div>
        </section>
    </main>
@endsection
