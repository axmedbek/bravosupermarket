<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>Bravo Market</title>
    <meta content="" name="webskyus">
    <meta content="" name="this is description">
    <meta content="" name="website, landing">
    <meta content="width=device-width, initial-scale=1" name="viewport">
    <meta content="ie=edge" http-equiv="x-ua-compatible">
    <link rel="icon" href="./images/main/favicon/favicon.ico">
    <link rel="apple-touch-icon" href="./images/main/favicon/apple-icon-180x180.png">
    <link rel="stylesheet" href="style/main.min.css">
</head>

<body>
    <div class="body-wrapper">
        <header class="header header--no-border">
            <div class="container">
                <div class="row">
                    <div class="header-back-link preloader preloader--v">
                        <a class="header-back-link__main" href="#">
                            <svg class="icon icon-home header-back-link__icon">
                                <use xlink:href="./images/sprite/sprite.svg#home"></use>
                            </svg>bravosupermarket.az</a>
                    </div>
                </div>
                <div class="row">
                    <nav class="main-nav main-nav--careers">
                        <a class="logo preloader preloader--v" href="#">
                            <img class="blazy blazy--style" src="./" data-src="./images/main/icon/l_vertical.svg" alt="bravo market logo">
                        </a>
                        <ul class="main-menu">
                            <li class="main-menu__list main-menu__list--active preloader preloader--v">
                                <a class="main-menu__link" href="#">Job Opportunities</a>
                            </li>
                            <li class="main-menu__list preloader preloader--v">
                                <a class="main-menu__link" href="#">PRAKTIKUM Summer Internship Program</a>
                            </li>
                            <li class="main-menu__list preloader preloader--v">
                                <a class="main-menu__link" href="#">Other projects</a>
                            </li>
                        </ul>
                    </nav>
                    <div class="store-place store-place--align-center">
                        <a class="preloader preloader--v store-place__job-link primary-btn primary-btn--green preloader--v" href="#">Job search</a>
                        <div class="lang lang--align-center preloader preloader--v">
                            <div class="select-wrap">
                                <div class="select-wrap__text" data-select-wrap="s1" data-select-focus="false">
                                    <input class="select-wrap__input" type="hidden" name="site-lang" value="az" data-select-input="s1">
                                    <svg class="icon icon-lang select-wrap__icon-svg">
                                        <use xlink:href="./images/sprite/sprite.svg#lang"></use>
                                    </svg>
                                    <div class="select-wrap__selected" data-selected-item="s1" data-selected-focus="false">az </div>
                                </div>
                                <div class="select-wrap__option" data-select-option="s1" data-select-option-opened="false">
                                    <span class="select-wrap__option-item" data-select-option-items="az" data-select-option-input="s1">az </span>
                                    <span class="select-wrap__option-item" data-select-option-items="en" data-select-option-input="s1">en </span>
                                    <span class="select-wrap__option-item" data-select-option-items="ru" data-select-option-input="s1">ru </span>
                                </div>
                            </div>
                        </div>
                        <button class="hamburger preloader preloader--v">
                            <span class="hamburger__item"></span>
                            <span class="hamburger__item"></span>
                            <span class="hamburger__item"></span>
                        </button>
                    </div>
                </div>
            </div>
        </header>
        <!-- main -->
        <main class="main">
            <section class="breadcrumbs">
                <div class="container">
                    <div class="row">
                        <div class="breadcrumbs_col">
                            <div class="breadcrumbs__nav">
                                <a class="breadcrumbs__item preloader-overlay preloader-overlay--vertical" href="#">
                                    <svg class="icon icon-home breadcrumbs__icon">
                                        <use xlink:href="./images/sprite/sprite.svg#home"></use>
                                    </svg>
                                </a>
                                <a class="breadcrumbs__item preloader-overlay preloader-overlay--vertical" href="#">
                                    <svg class="icon icon-arrow breadcrumbs__icon breadcrumbs__icon--arrow">
                                        <use xlink:href="./images/sprite/sprite.svg#arrow"></use>
                                    </svg>Job Opportunities</a>
                                <a class="breadcrumbs__item preloader-overlay preloader-overlay--vertical" href="#">
                                    <svg class="icon icon-arrow breadcrumbs__icon breadcrumbs__icon--arrow">
                                        <use xlink:href="./images/sprite/sprite.svg#arrow"></use>
                                    </svg>Mass Recruitment</a>
                                <a class="breadcrumbs__item preloader-overlay preloader-overlay--vertical" href="#">
                                    <svg class="icon icon-arrow breadcrumbs__icon breadcrumbs__icon--arrow">
                                        <use xlink:href="./images/sprite/sprite.svg#arrow"></use>
                                    </svg>Mağaza heyəti</a>
                                <span class="breadcrumbs__item breadcrumbs__item--inactive preloader-overlay preloader-overlay--vertical">
                                    <svg class="icon icon-arrow breadcrumbs__icon breadcrumbs__icon--arrow">
                                        <use xlink:href="./images/sprite/sprite.svg#arrow"></use>
                                    </svg>Baş satıcı</span>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section class="inner-top-thumb inner-top-thumb--white-bg section--pb-inner">
                <div class="container container--full">
                    <div class="row">
                        <div class="container-inner">
                            <div class="inner-top-thumb__box preloader preloader--v" style="background: url(./images/main/careers/1.jpg) no-repeat center/cover"></div>
                        </div>
                    </div>
                </div>
            </section>
            <section class="careers-info section--pb">
                <div class="container">
                    <div class="row">
                        <div class="careers-info__col-content">
                            <h1 class="preloader preloader--v careers-info__title">Some title for page </h1>
                            <p class="preloader preloader--v careers-info__bold-text">£41,000 up to £58,300 per annum (pro rata) - This isn’t looking after your local store. This is building a global business.</p>
                            <br>
                            <p class="preloader preloader--v careers-info__text">Store Managers spend a lot of time on the shop floor, working alongside the team, keeping our customers happy and ensuring the store is hitting its targets. They take pride in their team, developing them and leading from the
                                front, as if running their own business.</p>
                            <br>
                            <p class="preloader preloader--v careers-info__text">This is your opportunity to build a career in an international company that’s growing year on year. Taking full ownership of a Lidl store, you’ll experience the thrill of hitting targets, solving problems and reaching your
                                potential.</p>
                            <br>
                            <p class="preloader preloader--v careers-info__text">When you join us, we’ll support you to get started, showing you our best practice and optimal processes, then it’s up to you to make your store a success. We can’t promise it will be easy, and can be challenging, but we know
                                you’ll love leading our teams, providing excellent service to our customers and our unique culture. Find out more and apply for a career a Lidl less ordinary.</p>
                            <p class="preloader preloader--v careers-info__text">Please note that your application will include three online exercises, designed to provide us with a more in-depth understanding of you as a potential future leader for Lidl. To find out more and even conduct a practice exercise,
                                visit:
                                <a class="preloader preloader--v careers-info__link" href="#">getstarted.cut-e.com</a>
                            </p>
                            <br>
                            <p class="preloader preloader--v careers-info__bold-text">The process will also include a telephone interview and a face-to-face selection event. We look forward to receiving your application.</p>
                            <br>
                            <br>
                            <h2 class="preloader preloader--v careers-info__sub-title careers-info__sub-title--green careers-info__sub-title--green-big">What you'll do</h2>
                            <ul class="careers-info-list preloader preloader--v">
                                <li class="careers-info-list__items">Lead and motivate your team, in accordance with our Leadership & Company Principles, to work to their full potential every day and provide an environment where colleagues can produce their best work</li>
                                <li class="careers-info-list__items">Create an environment where every member of your team can do their best work</li>
                                <li class="careers-info-list__items">Take full responsibility for the performance and day-to-day operations of your store</li>
                                <li class="careers-info-list__items">Effectively delegate workload and motivate your team to achieve your store’s key performance indicators</li>
                                <li class="careers-info-list__items">Optimisation of store process through regular process analysis and implementation Solve problems swiftly to enable your team to focus on their tasks</li>
                                <li class="careers-info-list__items">Identify employee potential and apply specific measures to develop employees</li>
                                <li class="careers-info-list__items">Recruit, train and develop your team</li>
                                <li class="careers-info-list__items">Ensure and provide excellent Customer Service throughout your store</li>
                                <li class="careers-info-list__items">Monitor, manage and improve key performance indicators throughout your store</li>
                            </ul>
                        </div>
                        <div class="careers-info__col-aside">
                            <div class="careers-info-aside">
                                <p class="preloader preloader--v careers-info-aside__title">Vakansiyalar</p>
                                <p class="preloader preloader--v careers-info-aside__descr">Baş satıcı</p>
                                <div class="careers-info-aside__share-box">
                                    <a class="careers-info-aside__button" href="#">
                                        <svg class="icon icon-repost careers-info-aside__icon">
                                            <use xlink:href="./images/sprite/sprite.svg#repost"></use>
                                        </svg>Repost </a>
                                    <a class="careers-info-aside__button careers-info-aside__button--print" href="javascript:window.print()">
                                        <svg class="icon icon-print careers-info-aside__icon">
                                            <use xlink:href="./images/sprite/sprite.svg#print"></use>
                                        </svg>Print</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section class="benefits-section section--white-bg section--pb">
                <div class="container">
                    <div class="row">
                        <div class="careers-info__col">
                            <h2 class="career-news-title__name career-news-title__name--mb0">Your Benefits</h2>
                        </div>
                    </div>
                    <div class="row">
                        <div class="careers-info__col-full careers-info__col-full--line">
                            <div class="benefits-slider swiper-container">
                                <div class="swiper-wrapper">
                                    <div class="swiper-slide">
                                        <div class="benefits-slider__items">
                                            <div class="benefits-slider__overlay">
                                                <div class="benefits-slider__overlay-wrap">
                                                    <div class="benefits-slider__overlay-inner">
                                                        <h2 class="benefits-slider__discount-title">Discount Card</h2>
                                                        <p class="benefits-slider__discount-text">10% off in stores</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="preloader preloader--v benefits-slider__items-inner">
                                                <img class="blazy blazy--style" src="./" data-src="./images/main/icon/b1.svg" alt="bravo market">
                                                <img class="blazy blazy--style" src="./" data-src="./images/main/icon/b1_white.svg" alt="bravo market">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="swiper-slide">
                                        <div class="benefits-slider__items">
                                            <div class="benefits-slider__overlay">
                                                <div class="benefits-slider__overlay-wrap">
                                                    <div class="benefits-slider__overlay-inner">
                                                        <h2 class="benefits-slider__discount-title">Discount Card</h2>
                                                        <p class="benefits-slider__discount-text">10% off in stores 10% off in stores 10% off in stores </p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="preloader preloader--v benefits-slider__items-inner">
                                                <img class="blazy blazy--style" src="./" data-src="./images/main/icon/b2.svg" alt="bravo market">
                                                <img class="blazy blazy--style" src="./" data-src="./images/main/icon/b2_white.svg" alt="bravo market">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="swiper-slide">
                                        <div class="benefits-slider__items">
                                            <div class="benefits-slider__overlay">
                                                <div class="benefits-slider__overlay-wrap">
                                                    <div class="benefits-slider__overlay-inner">
                                                        <h2 class="benefits-slider__discount-title">Discount Card</h2>
                                                        <p class="benefits-slider__discount-text">10% off in stores 10% off in stores 10% off in stores </p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="preloader preloader--v benefits-slider__items-inner">
                                                <img class="blazy blazy--style" src="./" data-src="./images/main/icon/b3.svg" alt="bravo market">
                                                <img class="blazy blazy--style" src="./" data-src="./images/main/icon/b3_white.svg" alt="bravo market">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="swiper-slide">
                                        <div class="benefits-slider__items">
                                            <div class="benefits-slider__overlay">
                                                <div class="benefits-slider__overlay-wrap">
                                                    <div class="benefits-slider__overlay-inner">
                                                        <h2 class="benefits-slider__discount-title">Discount Card</h2>
                                                        <p class="benefits-slider__discount-text">10% off in stores 10% off in stores 10% off in stores </p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="preloader preloader--v benefits-slider__items-inner">
                                                <img class="blazy blazy--style" src="./" data-src="./images/main/icon/b4.svg" alt="bravo market">
                                                <img class="blazy blazy--style" src="./" data-src="./images/main/icon/b4_white.svg" alt="bravo market">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="swiper-slide">
                                        <div class="benefits-slider__items">
                                            <div class="benefits-slider__overlay">
                                                <div class="benefits-slider__overlay-wrap">
                                                    <div class="benefits-slider__overlay-inner">
                                                        <h2 class="benefits-slider__discount-title">Discount Card</h2>
                                                        <p class="benefits-slider__discount-text">10% off in stores 10% off in stores 10% off in stores </p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="preloader preloader--v benefits-slider__items-inner">
                                                <img class="blazy blazy--style" src="./" data-src="./images/main/icon/b5.svg" alt="bravo market">
                                                <img class="blazy blazy--style" src="./" data-src="./images/main/icon/b5_white.svg" alt="bravo market">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="benefits-slider-pagination">
                                <div class="benefits-slider-pagination__btn benefits-slider-pagination__btn--prev">
                                    <svg class="icon icon-arrow1 benefits-slider-pagination__icon">
                                        <use xlink:href="./images/sprite/sprite.svg#arrow1"></use>
                                    </svg>
                                </div>
                                <div class="benefits-slider-pagination__btn benefits-slider-pagination__btn--next">
                                    <svg class="icon icon-arrow1 benefits-slider-pagination__icon">
                                        <use xlink:href="./images/sprite/sprite.svg#arrow1"></use>
                                    </svg>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section class="careers-update section--pb section--white-bg">
                <div class="container">
                    <div class="row">
                        <div class="careers-update__col" style="background: url(./images/main/careers/job.jpg) no-repeat center/cover">
                            <div class="careers-update__info-box">
                                <h5 class="careers-update__title preloader preloader--v">Update</h5>
                                <p class="careers-update__text preloader preloader--v">We’re very pleased that you’d like to become a member of team Lidl GB. Due to the current situation (COVID-19) there may be delays or changes to the application process. We apologise for this and thank you for your understanding.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section class="careers-about-content section--mb-last">
                <div class="container">
                    <div class="row">
                        <div class="careers-about-content__col">
                            <div class="careers-about-content-video shop-article__thumb preloader preloader--v">
                                <img class="blazy blazy--style" src="./" data-src="./images/main/careers/job1.jpg" alt="shop article thumb">
                                <div class="shop-article__video">
                                    <div class="shop-article__video-wrap shop-article__video-wrap--first shop-article__video-wrap--active">
                                        <svg class="icon icon-yt shop-article__video-icon shop-article__video-icon--white">
                                            <use xlink:href="./images/sprite/sprite.svg#yt"></use>
                                        </svg>
                                    </div>
                                    <div class="shop-article__video-wrap">
                                        <iframe class="yt-player" src="https://www.youtube.com/embed/TBikbn5XJhg?enablejsapi=1"></iframe>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </main>
        <footer class="footer footer--white">
            <div class="container">
                <div class="row footer__row-nav">
                    <nav class="footer-nav">
                        <ul class="footer-nav__menu">
                            <li class="footer-nav__items footer-nav__items--title preloader preloader--v"> Quick links</li>
                            <li class="footer-nav__items preloader preloader--v">
                                <a class="footer-nav__link" href="#">About us</a>
                            </li>
                            <li class="footer-nav__items preloader preloader--v">
                                <a class="footer-nav__link" href="#">Promotions</a>
                            </li>
                            <li class="footer-nav__items preloader preloader--v">
                                <a class="footer-nav__link" href="#">Press center</a>
                            </li>
                            <li class="footer-nav__items preloader preloader--v">
                                <a class="footer-nav__link" href="#">Recipes</a>
                            </li>
                            <li class="footer-nav__items preloader preloader--v">
                                <a class="footer-nav__link" href="#">Our stores</a>
                            </li>
                            <li class="footer-nav__items preloader preloader--v">
                                <a class="footer-nav__link" href="#">Contact us </a>
                            </li>
                        </ul>
                    </nav>
                    <nav class="footer-nav">
                        <ul class="footer-nav__menu">
                            <li class="footer-nav__items footer-nav__items--title preloader preloader--v"> About Bravo</li>
                            <li class="footer-nav__items preloader preloader--v">
                                <a class="footer-nav__link" href="#">History</a>
                            </li>
                            <li class="footer-nav__items preloader preloader--v">
                                <a class="footer-nav__link" href="#">Mission & values</a>
                            </li>
                            <li class="footer-nav__items preloader preloader--v">
                                <a class="footer-nav__link" href="#">Local charity</a>
                            </li>
                            <li class="footer-nav__items preloader preloader--v">
                                <a class="footer-nav__link" href="#">Sustainability</a>
                            </li>
                            <li class="footer-nav__items preloader preloader--v">
                                <a class="footer-nav__link" href="#">Headquarters</a>
                            </li>
                            <li class="footer-nav__items preloader preloader--v">
                                <a class="footer-nav__link" href="#">Countries of operation</a>
                            </li>
                            <li class="footer-nav__items preloader preloader--v">
                                <a class="footer-nav__link" href="#">Compliance </a>
                            </li>
                        </ul>
                    </nav>
                    <nav class="footer-nav">
                        <ul class="footer-nav__menu">
                            <li class="footer-nav__items footer-nav__items--title preloader preloader--v"> Products & Services</li>
                            <li class="footer-nav__items preloader preloader--v">
                                <a class="footer-nav__link" href="#">Departments</a>
                            </li>
                            <li class="footer-nav__items preloader preloader--v">
                                <a class="footer-nav__link" href="#">Quality standards</a>
                            </li>
                            <li class="footer-nav__items preloader preloader--v">
                                <a class="footer-nav__link" href="#">Food safety</a>
                            </li>
                            <li class="footer-nav__items preloader preloader--v">
                                <a class="footer-nav__link" href="#">Product manuals</a>
                            </li>
                            <li class="footer-nav__items preloader preloader--v">
                                <a class="footer-nav__link" href="#">Product videos </a>
                            </li>
                        </ul>
                    </nav>
                    <nav class="footer-nav">
                        <ul class="footer-nav__menu">
                            <li class="footer-nav__items footer-nav__items--title preloader preloader--v"> Customer care</li>
                            <li class="footer-nav__items preloader preloader--v">
                                <a class="footer-nav__link" href="#">Potential suppliers</a>
                            </li>
                            <li class="footer-nav__items preloader preloader--v">
                                <a class="footer-nav__link" href="#">Real estate</a>
                            </li>
                            <li class="footer-nav__items preloader preloader--v">
                                <a class="footer-nav__link" href="#">FAQ</a>
                            </li>
                            <li class="footer-nav__items preloader preloader--v">
                                <a class="footer-nav__link" href="#">Product recalls </a>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
        </footer>
        <footer class="footer footer--green">
            <div class="container">
                <div class="row">
                    <div class="footer__col-logo preloader preloader--v">
                        <a class="footer__logo" href="#">
                            <img class="blazy blazy--style" src="./" data-src="./images/main/icon/l_vertical.svg" alt="bravo market logo">
                        </a>
                    </div>
                    <div class="footer__col-content">
                        <div class="footer-nav-about">
                            <nav class="footer-nav footer-nav--about preloader preloader--v">
                                <ul class="footer-nav__menu">
                                    <li class="footer-nav__items footer-nav__items--title footer-nav__items--title-white"> Get Bravo app</li>
                                    <li class="footer-nav__items">
                                        <a class="footer-nav__link footer-nav__link--white" href="#">Learn more</a>
                                    </li>
                                    <li class="footer-nav__items">
                                        <a class="footer-nav__link footer-nav__link--white" href="#">Google play</a>
                                    </li>
                                    <li class="footer-nav__items">
                                        <a class="footer-nav__link footer-nav__link--white" href="#">App store</a>
                                    </li>
                                </ul>
                            </nav>
                            <nav class="footer-nav footer-nav--about preloader preloader--v">
                                <ul class="footer-nav__menu">
                                    <li class="footer-nav__items footer-nav__items--title footer-nav__items--title-white">Legal</li>
                                    <li class="footer-nav__items">
                                        <a class="footer-nav__link footer-nav__link--white" href="#">Privacy policy</a>
                                    </li>
                                    <li class="footer-nav__items">
                                        <a class="footer-nav__link footer-nav__link--white" href="#">Terms of use</a>
                                    </li>
                                    <li class="footer-nav__items">
                                        <a class="footer-nav__link footer-nav__link--white" href="#">Contest rules and regulations</a>
                                    </li>
                                </ul>
                            </nav>
                            <p class="footer-nav-about__copyright preloader preloader--v">All rights reserved © Azerbaijan Supermarket MMC 2020 </p>
                        </div>
                    </div>
                    <div class="footer__col-social">
                        <div class="footer-social">
                            <h4 class="footer-social__title preloader preloader--v">Connect with us</h4>
                            <div class="footer-social__wrap">
                                <a class="footer-social__link preloader preloader--v" href="#">
                                    <svg class="icon icon-fb footer-social__icon">
                                        <use xlink:href="./images/sprite/sprite.svg#fb"></use>
                                    </svg>
                                </a>
                                <a class="footer-social__link preloader preloader--v" href="#">
                                    <svg class="icon icon-tw footer-social__icon">
                                        <use xlink:href="./images/sprite/sprite.svg#tw"></use>
                                    </svg>
                                </a>
                                <a class="footer-social__link preloader preloader--v" href="#">
                                    <svg class="icon icon-ins footer-social__icon">
                                        <use xlink:href="./images/sprite/sprite.svg#ins"></use>
                                    </svg>
                                </a>
                                <a class="footer-social__link preloader preloader--v" href="#">
                                    <svg class="icon icon-yt1 footer-social__icon">
                                        <use xlink:href="./images/sprite/sprite.svg#yt1"></use>
                                    </svg>
                                </a>
                                <a class="footer-social__link preloader preloader--v" href="#">
                                    <svg class="icon icon-lk footer-social__icon">
                                        <use xlink:href="./images/sprite/sprite.svg#lk"></use>
                                    </svg>
                                </a>
                                <a class="footer-social__link preloader preloader--v" href="#">
                                    <svg class="icon icon-pt footer-social__icon">
                                        <use xlink:href="./images/sprite/sprite.svg#pt"></use>
                                    </svg>
                                </a>
                                <a class="footer-social__link preloader preloader--v" href="#">
                                    <svg class="icon icon-gg footer-social__icon">
                                        <use xlink:href="./images/sprite/sprite.svg#gg"></use>
                                    </svg>
                                </a>
                                <a class="footer-social__link preloader preloader--v" href="#">
                                    <svg class="icon icon-tg footer-social__icon">
                                        <use xlink:href="./images/sprite/sprite.svg#tg"></use>
                                    </svg>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="footer__col-copyright">
                        <p class="footer-nav-about__copyright preloader preloader--v">All rights reserved © Azerbaijan Supermarket MMC 2020 </p>
                    </div>
                    <div class="footer__col-subs">
                        <form class="footer-subs">
                            <div class="footer-subs__row">
                                <div class="footer-subs__col">
                                    <h4 class="footer-subs__title preloader preloader--v">Sign up for our newsletter</h4>
                                </div>
                            </div>
                            <div class="footer-subs__row preloader preloader--v">
                                <div class="footer-subs__col">
                                    <input class="footer-subs__input" type="email" name="footer-subs-email" placeholder="Enter your email" required>
                                    <button class="footer-subs__btn" type="submit">
                                        <svg class="icon icon-arrow-r footer-subs__icon">
                                            <use xlink:href="./images/sprite/sprite.svg#arrow-r"></use>
                                        </svg>
                                    </button>
                                </div>
                            </div>
                            <div class="footer-subs__row">
                                <div class="footer-subs__col footer-subs__col--ai-center">
                                    <a class="footer-subs__dev-logo preloader preloader--v" href="#">
                                        <img class="blazy blazy--style" src="./" data-src="./images/main/icon/dev_logo.svg" alt="bravo market">
                                    </a>
                                    <p class="footer-subs__message footer-subs__message--success preloader preloader--v">Thanks for subscribing! </p>
                                    <!-- p.footer-subs__message.footer-subs__message--error Something went wrong	-->
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </footer>
    </div>
    <script src="js/main.min.js" defer></script>
</body>

</html>