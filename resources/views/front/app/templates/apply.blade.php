@extends('front.app.careerLayout')
@section('styles')
    <style>
        .success{
            width: 97.5%;
            background: #caebca;
            padding: 11px;
            color: green;
            box-shadow: 0 0 20px rgba(0,0,0,.05);
            -webkit-appearance: none;
            border: 1px solid #ededed;
            margin-bottom: 15px;
        }
    </style>
@stop
@section('main')
    <main class="main">
        <section class="breadcrumbs">
            <div class="container">
                <div class="row">
                    <div class="breadcrumbs_col">
                        <div class="breadcrumbs__nav">
                            <a class="breadcrumbs__item preloader-overlay preloader-overlay--vertical" href="#">
                                <svg class="icon icon-home breadcrumbs__icon">
                                    <use xlink:href="{{ asset('front/images/sprite/sprite.svg#home') }}"></use>
                                </svg>
                            </a>
                            <a class="breadcrumbs__item preloader-overlay preloader-overlay--vertical" href="#">
                                <svg class="icon icon-arrow breadcrumbs__icon breadcrumbs__icon--arrow">
                                    <use xlink:href="{{ asset('front/images/sprite/sprite.svg#arrow') }}"></use>
                                </svg>Job Opportunities
                            </a>
                            <a class="breadcrumbs__item preloader-overlay preloader-overlay--vertical" href="#">
                                <svg class="icon icon-arrow breadcrumbs__icon breadcrumbs__icon--arrow">
                                    <use xlink:href="{{ asset('front/images/sprite/sprite.svg#arrow') }}"></use>
                                </svg>Mass Recruitment
                            </a>
                            <span class="breadcrumbs__item breadcrumbs__item--inactive preloader-overlay preloader-overlay--vertical">
                                    <svg class="icon icon-arrow breadcrumbs__icon breadcrumbs__icon--arrow">
                                        <use xlink:href="{{ asset('front/images/sprite/sprite.svg#arrow') }}"></use>
                                    </svg>{{ translate('career.apply') }}
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="inner-top-thumb inner-top-thumb--white-bg section--pb-inner">
            <div class="container container--full">
                <div class="row">
                    <div class="container-inner">
                        <div class="inner-top-thumb__box preloader preloader--v" style="background: url('{{ asset('front/images/main/careers/1.jpg') }}') no-repeat center/cover"></div>
                    </div>
                </div>
            </div>
        </section>
        <section class="careers-info section--pb-last">
            <div class="container">
                <div class="row">


                    <div class="careers-info__col">
                        <h1 class="preloader preloader--v careers-info__title">{{ translate('career.apply') }}</h1>
                        @if ($errors->any())
                            <ul style="margin-bottom: 16px; margin: 17px; color: red;">
                                @foreach ($errors->all() as $error)
                                    <li style="list-style: outside;">{!! $error !!}</li>
                                @endforeach
                            </ul>
                        @endif

                        @if(session('message'))
                            <div class="success">
                                {{ session('message') }}
                            </div>
                        @endif
                        <form class="contacts-form contacts-form--shadow" method="post" enctype="multipart/form-data">
                            @csrf


                            <div class="contacts-form__row">
                                <div class="contacts-form__col contacts-form__col--full">
                                    <label class="preloader preloader--v contacts-form__label">{{ translate('career.vacancy') }}</label>
                                    <div class="preloader preloader--v select-wrap__text select-wrap__text--full" data-select-wrap="apply-select-1.1" data-select-focus="false">
                                        <input class="select-wrap__input" type="hidden" name="vacancy_id" value="{{ request('v_id',old('vacancy_id')) }}"  data-select-input="apply-select-1.1">
                                        <div class="select-wrap__selected select-wrap__selected--grey" data-selected-item="apply-select-1.1" data-selected-focus="false">{{ request('v_id',old('vacancy_id',translate('career.select'))) }}</div>
                                        <svg class="icon icon-arrow select-wrap__icon-svg select-wrap__icon-svg--arrow">
                                            <use xlink:href="{{ asset('front/images/sprite/sprite.svg#arrow') }}"></use>
                                        </svg>
                                        <div class="select-wrap__option select-wrap__option--form-page" data-select-option="apply-select-1.1" data-select-option-opened="false" data-select-option-scroll="true">
                                            @foreach($vacancies as $v)
                                                <span class="select-wrap__option-item select-wrap__option-item--form-page" data-select-option-items="{{ $v->id }}" data-select-option-input="apply-select-1.1">{{ $v->title }} </span>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="contacts-form__row">
                                <div class="contacts-form__col contacts-form__col--full contacts-form__col--mt">
                                    <label class="preloader preloader--v contacts-form__label contacts-form__label--title">{{ translate('career.eneralquestions') }}</label>
                                </div>
                            </div>
                            <div class="contacts-form__row">
                                <div class="preloader preloader--v contacts-form__col contacts-form__col--half">
                                    <label class="contacts-form__label contacts-form__label--black">{{ translate('career.fullname') }}</label>
                                    <input class="contacts-form__input" type="text" name="name" value="{{ old('name') }}" placeholder="{{ translate('career.paste_your_info') }}" required>
                                </div>
                                <div class="preloader preloader--v contacts-form__col contacts-form__col--half">
                                    <label class="contacts-form__label contacts-form__label--black">{{ translate('career.phone') }}</label>
                                    <input class="contacts-form__input" type="number" name="phone" value="{{ old('phone') }}" placeholder="{{ translate('career.paste_your_info') }}" required>
                                </div>
                            </div>
                            <div class="contacts-form__row">
                                <div class="contacts-form__col contacts-form__col--full contacts-form__col--mt">
                                    <label class="preloader preloader--v contacts-form__label contacts-form__label--title">{{translate('career.experience')}}</label>
                                </div>
                            </div>
                            <div class="contacts-form__row">
                                <div class="contacts-form__col contacts-form__col--full">
                                    <label class="preloader preloader--v contacts-form__label contacts-form__label--black"> {{ translate('career.experience_store_management') }}:</label>
                                    <div class="contacts-form__sub-col">
                                        @foreach(experience_store_management() as $key => $value)
                                            <div class="preloader preloader--v contacts-form__checkbox-wrap">
                                                <input class="contacts-form__checkbox" id="contact-form__checkbox{{ $key }}" {{ old('experience_store_management') == $value ? 'checked' : '' }}  type="radio" value="{{ $value }}" name="experience_store_management">
                                                <label class="contacts-form__checkbox-label" for="contact-form__checkbox{{ $key }}">{{ translate('career.'.$value) }}</label>
                                            </div>
                                        @endforeach

                                    </div>
                                </div>
                            </div>
                            <div class="contacts-form__row">
                                <div class="contacts-form__col contacts-form__col--full">
                                    <label class="preloader preloader--v contacts-form__label contacts-form__label--black">{{ translate('career.working_now?') }}</label>
                                    <div class="contacts-form__sub-col">
                                        <div class="preloader preloader--v contacts-form__checkbox-wrap">
                                            <input class="contacts-form__checkbox" id="contact_work-form__checkbox51" {{ old('working_now') == 'yes' ? 'checked' : '' }} type="radio" value="yes" name="working_now">
                                            <label class="contacts-form__checkbox-label" for="contact_work-form__checkbox51">{{ translate('career.yes') }}</label>
                                        </div>
                                        <div class="preloader preloader--v contacts-form__checkbox-wrap">
                                            <input class="contacts-form__checkbox" id="contact_work-form__checkbox6"  {{ old('working_now') == 'no' ? 'checked' : '' }} type="radio" value="no" name="working_now">
                                            <label class="contacts-form__checkbox-label" for="contact_work-form__checkbox6">{{ translate('career.no') }}</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="contacts-form__row">
                                <div class="preloader preloader--v contacts-form__col contacts-form__col--full">
                                    <label class="contacts-form__label contacts-form__label--black"> {{ translate('career.the_position_you_are_currently_working_on_or_most_recently') }} </label>
                                    <textarea class="contacts-form__textarea" name="description"  placeholder="Please help me contact with your company">{{ old('description') }}</textarea>
                                </div>
                            </div>

                            <div class="contacts-form__row">
                                <div class="contacts-form__col contacts-form__col--full">
                                    <label class="preloader preloader--v contacts-form__label contacts-form__label--black">{{ translate('career.who_from') }}</label>
                                    <div class="contacts-form__sub-col">
                                        @foreach(from_who() as $key=>$value)
                                            <div class="preloader preloader--v contacts-form__checkbox-wrap">
                                                <input class="contacts-form__checkbox" id="contact_who-form__checkbox{{ $key }}" {{ old('from_who') == $value ? 'checked' : '' }}  value="{{ $value }}" type="radio" name="from_who">
                                                <label class="contacts-form__checkbox-label" for="contact_who-form__checkbox{{ $key }}">{{ $value }}</label>
                                            </div>
                                        @endforeach

                                    </div>
                                </div>
                            </div>
                            <div class="contacts-form__row">
                                <div class="contacts-form__col contacts-form__col--full">
                                    <label class="preloader preloader--v contacts-form__label contacts-form__label--black">{{ translate('career.attach_the_document') }}</label>
                                    <div class="row">
                                        <div class="contacts-form__col contacts-form__col--half">
                                            <div class="preloader preloader--v contact-form-loaded">
                                                <input class="contact-form-loaded__input" type="file" name="file_first">
                                                <button class="contact-form-loaded__close">
                                                    <svg class="icon icon-close contact-form-loaded__icon-svg">
                                                        <use xlink:href="{{ asset('front/images/sprite/sprite.svg#close') }}"></use>
                                                    </svg>
                                                </button>
                                                <div class="contact-form-loaded__inner">
                                                    <div class="contact-form-loaded__img">
                                                        <img class="contact-form-loaded__icon blazy blazy--style" src="./" data-src="{{ asset('front/images/main/icon/pdf_active.svg') }}" alt="bravo market">
                                                        <img class="contact-form-loaded__icon blazy blazy--style" src="./" data-src="{{ asset('front/images/main/icon/pdf.svg') }}" alt="bravo market">
                                                    </div>
                                                    <div class="contact-form-loaded__content">
                                                        <h4 class="contact-form-loaded__title">{{ translate('career.extra') }} №1</h4>
                                                        <p class="contact-form-loaded__descr">{{ translate('career.formats') }}: .pdf .png .jpeg .jpg </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="contacts-form__col contacts-form__col--half">
                                            <div class="preloader preloader--v contact-form-loaded">
                                                <input class="contact-form-loaded__input" type="file" name="file_second">
                                                <button class="contact-form-loaded__close">
                                                    <svg class="icon icon-close contact-form-loaded__icon-svg">
                                                        <use xlink:href="{{ asset('front/images/sprite/sprite.svg#close') }}"></use>
                                                    </svg>
                                                </button>
                                                <div class="contact-form-loaded__inner">
                                                    <div class="contact-form-loaded__img">
                                                        <img class="contact-form-loaded__icon blazy blazy--style" src="./" data-src="{{ asset('front/images/main/icon/pdf_active.svg') }}" alt="bravo market">
                                                        <img class="contact-form-loaded__icon blazy blazy--style" src="./" data-src="{{ asset('front/images/main/icon/pdf.svg') }}" alt="bravo market">
                                                    </div>
                                                    <div class="contact-form-loaded__content">
                                                        <h4 class="contact-form-loaded__title">{{ translate('career.extra') }} №2</h4>
                                                        <p class="contact-form-loaded__descr">{{ translate('career.formats') }}: .pdf .png .jpeg .jpg </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="contacts-form__row contacts-form__row--send-btn">
                                <div class="contacts-form__col contacts-form__col--half">
                                    <button class="preloader preloader--v contacts-form__send-btn primary-btn primary-btn--green">
                                        <svg class="icon icon-send contacts-form__send-icon">
                                            <use xlink:href="{{ asset('front/images/sprite/sprite.svg#send') }}"></use>
                                        </svg>{{ translate('career.send_form') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>
    </main>
@stop
