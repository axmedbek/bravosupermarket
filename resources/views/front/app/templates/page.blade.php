@extends('front.app.layout')
@section('styles')
    <style>
        .about-post-custom {
            width: calc(66.66667% - 20px);
            margin-bottom: 70px;
            box-sizing: border-box;
            margin-left: 10px;
            margin-right: 10px;
        }

        .about-post-custom ul li {
            list-style: unset !important;
        }

        .about-post-custom table {
            max-width: 700px !important;
        }
    </style>
@endsection
@section('main')
    <main class="main">
        <section class="breadcrumbs">
            <div class="container">
                <div class="row">
                    <div class="breadcrumbs_col">
                        <div class="breadcrumbs__nav">
                            <a class="breadcrumbs__item preloader-overlay preloader-overlay--vertical" href="#">
                                <svg class="icon icon-home breadcrumbs__icon">
                                    <use xlink:href="./images/sprite/sprite.svg#home"></use>
                                </svg>
                            </a>
                            <span
                                class="breadcrumbs__item breadcrumbs__item--inactive preloader-overlay preloader-overlay--vertical">
                            <svg class="icon icon-arrow breadcrumbs__icon breadcrumbs__icon--arrow">
                                <use xlink:href="./images/sprite/sprite.svg#arrow"></use>
                            </svg>{{$page->title}}</span>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="inner-top-thumb section--mb-inner">
            <div class="container container--full">
                <div class="row">
                    <div class="container-inner">
                        <div class="inner-top-thumb__box preloader preloader--v"
                             style="background: url({{$media[0]->url}}) no-repeat center/cover">
                            <h1 class="inner-top-thumb__title"> {{$page->title}}
                            </h1>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="about">
            <div class="container">
                <div class="row">
                    @if (count(\App\Page::getPageTabs($page->page_id)) < 1)
                        <div class="careers-info__col">
                            {!! $page->content !!}
                            <br>
                            <br>
                        </div>
                    @else
                        <aside class="about-aside">
                            <nav class="about-aside__nav">
                                <ul class="about-aside__menu">
                                    @foreach(\App\Page::getPageTabs($page->page_id) as $pageTab)
                                        @if (count(\App\Page::getSubTabs($pageTab->tab_id)) < 1)
                                            <li class="about-aside__items preloader preloader--v">
                                            <span
                                                class="about-aside__link {{$loop->iteration===1?'simple-tab__link--active':''}}"
                                                data-simple-tab-nav="{{$pageTab->tab_id}}">{{$pageTab->title}}</span>
                                            </li>
                                        @else
                                            <li class="about-aside__items">
                                            <span class="about-aside__link"
                                                  data-simple-tab-nav="{{ \App\Page::getSubTabs($pageTab->tab_id)[0]->tab_id }}"
                                                  data-simple-tab-submenu="true">{{ $pageTab->title }}</span>
                                                <ul class="about-aside__menu-inner">
                                                    @foreach(\App\Page::getSubTabs($pageTab->tab_id) as $subTab)
                                                        <li class="about-aside__items">
                                                            <span
                                                                class="about-aside__link simple-tab__link--active-inner"
                                                                data-simple-tab-inner-nav="{{ $subTab->tab_id }}">{{ $subTab->title }}</span>
                                                        </li>
                                                    @endforeach
                                                </ul>
                                            </li>
                                        @endif
                                    @endforeach
                                </ul>
                            </nav>
                        </aside>
                        <article class="about-post-custom">
                            @if (session()->has('success'))
                                <h3 class="alert alert-success" style="color: white;
                                    background-color: #77bc1f;
                                    text-align: center;">{{ session()->get('success') }}</h3>
                            @elseif(session()->has('error'))
                                <h3 class="alert alert-success" style="color: white;
                                    background-color: #fb1818;
                                    text-align: center;">{{ session()->get('error') }}</h3>
                            @endif
                            @foreach(\App\Page::getPageTabs($page->page_id) as $pageTab)
                                @if (count(\App\Page::getSubTabs($pageTab->tab_id)) < 1)
                                    <div
                                        class="about-post__col preloader preloader--v {{$loop->iteration===1?'simple-tab__content--active':''}}"
                                        data-simple-tab-content="{{$pageTab->tab_id}}">
                                        {!! $pageTab->content !!}
                                        @if (!is_null($pageTab->form_template) && $pageTab->form_template !== 'page')
                                            @include('front.app.forms.'.$pageTab->form_template)
                                        @endif
                                    </div>
                                @else
                                    @foreach(\App\Page::getSubTabs($pageTab->tab_id) as $subTab)
                                        <div
                                            class="about-post__col preloader preloader--v {{$loop->iteration===1?'simple-tab__content--active':''}}"
                                            data-simple-tab-content="{{$subTab->tab_id}}">
                                            {!! $subTab->content !!}

                                            @if (!is_null($subTab->form_template) && $subTab->form_template !== 'page')
                                                @include('front.app.forms.'.$subTab->form_template)
                                            @endif
                                        </div>
                                    @endforeach
                                @endif
                            @endforeach
                        </article>
                    @endif
                </div>
            </div>
        </section>
    </main>
@endsection

@section('scripts')
    <script>

        var simulateClick = function (elem) {
            // Create our event (with options)
            var evt = new MouseEvent('click', {
                bubbles: true,
                cancelable: true,
                view: window
            });
            // If cancelled, don't dispatch our event
            var canceled = !elem.dispatchEvent(evt);
        };

        window.addEventListener("load", function () {
            let element = document.querySelector('.about-aside__menu > li span');
            simulateClick(element)
        });
    </script>
@endsection
