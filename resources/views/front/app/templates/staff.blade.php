@extends('front.app.careerLayout')
@section('styles')
    <style>
        .careers-info__col ul {
            counter-reset: numbers 0;
        }

        .careers-info__col li {
            list-style: none;
            font: 400 16px/1.5 helvetica-r,Arial,sans-serif;
            position: relative;
            margin-bottom: 35px;
            margin-left: 20px;
            letter-spacing: .03em;
            text-transform: uppercase;
            color: #0a0a0a;
        }

        .careers-info__col ul li::before {
            font: 400 16px/1.5 helvetica-b,Arial,sans-serif;
            content: counter(numbers) "";
            counter-increment: numbers;
            position: absolute;
            top: 0;
            left: -58px;
            -webkit-box-align: center;
            -webkit-align-items: center;
            -ms-flex-align: center;
            align-items: center;
            width: 38px;
            height: 38px;
            letter-spacing: .03em;
            background: url(../front/images/main/icon/count_bg.svg) center/contain no-repeat;
            color: #fff;
            -webkit-box-pack: center;
            -webkit-justify-content: center;
            -ms-flex-pack: center;
            justify-content: center;
            margin-top: -14px;
            text-align: center;
            padding-top: 14px;
        }
    </style>
@stop
@section('main')
    <!-- main -->
    <main class="main">
        <section class="breadcrumbs">
            <div class="container">
                <div class="row">
                    <div class="breadcrumbs_col">
                        <div class="breadcrumbs__nav">
                            <a class="breadcrumbs__item preloader-overlay preloader-overlay--vertical" href="#">
                                <svg class="icon icon-home breadcrumbs__icon">
                                    <use xlink:href="{{asset('front/images/sprite/sprite.svg#home')}}"></use>
                                </svg>
                            </a>
                            <span class="breadcrumbs__item breadcrumbs__item--inactive preloader-overlay preloader-overlay--vertical">
                            <svg class="icon icon-arrow breadcrumbs__icon breadcrumbs__icon--arrow">
                                <use xlink:href="{{asset('front/images/sprite/sprite.svg#arrow')}}"></use>
                            </svg>{{$page->title}}</span>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="inner-top-thumb inner-top-thumb--white-bg section--pb-inner">
            <div class="container container--full">
                <div class="row">
                    <div class="container-inner">
                        <div class="inner-top-thumb__box preloader preloader--v" style="background: url({{$media[0]->url}}) no-repeat center/cover"></div>
                    </div>
                </div>
            </div>
        </section>
        <section class="careers-info section--pb-last">
            <div class="container">
                <div class="row">
                    <div class="careers-info__col">
                        {!! explode("separate from here",$page->content)[0] !!}
                        <br>
                        <h2 class="preloader preloader--v careers-info__sub-title careers-info__sub-title--green">{{ translate('careers.muraciet') }}</h2>
                        <br>
                        {!! count(explode("separate from here",$page->content)) > 1 ? explode("separate from here",$page->content)[1] : '' !!}
                        <br>
                        <br>
                        <h2 class="preloader preloader--v careers-info__sub-title careers-info__sub-title--green">{{ translate('careers.vakansiyalar') }} </h2>
                        <form action="{{ route('career.applyGet') }}">
                            <ul class="careers-info-vacancies">
                                @php
                                    use App\Vacancy;
                                    $vacancies = Vacancy::where('locale', getLocale())->where('status', 1)->get();
                                @endphp
                                @foreach($vacancies as $key=>$value)
                                    <li class="preloader preloader--v careers-info-vacancies__items">
                                    <span class="preloader preloader--v careers-info-vacancies__link">
                                        <input id="vacancy-page-checkbox{{ $key }}" value="{{ $value->id }}" type="radio" name="v_id" {{ !$key ? 'checked' : '' }}>
                                        <label class="preloader preloader--v careers-info-vacancies__type" for="vacancy-page-checkbox{{ $key }}">{{ $value->title }}</label>
                                        <span class="preloader preloader--v careers-info-vacancies__descr" data-full-text="v{{ $key }}">
                                            {!! $value->content !!}</span>
                                        <span class="preloader preloader--v careers-info-vacancies__read"
                                              data-show-full-text="v{{ $key }}">
                                            <span>{{ translate('career.read_more') }}</span>
                                            <span>{{ translate('career.read_less') }}</span>
                                        </span>
                                    </span>
                                    </li>

                                @endforeach

                            </ul>
                            <button type="submit" class="preloader preloader--v primary-btn primary-btn--outline-green careers-info-vacancies-btn" href="#">apply Now</button>
                        </form>

                    </div>
                </div>
            </div>
        </section>
    </main>
@stop
