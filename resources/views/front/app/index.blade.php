@extends('front.app.layout')
@section('styles')
    <style>
        .banner-top-slider-thumb {
            margin-top: 8px;
        }

        .banner-top-slider-thumb .banner-top-slider-thumb__title {
            color: #273a4b;
            border-color: #273a4b;
        }

        .banner-top-slider-thumb .swiper-slide-thumb-active .banner-top-slider-thumb__title {
            background-color: #77bc1f;
            color: #ffffff;
            padding-left: 10px;
            padding-right: 10px;
            padding-top: 6px;
            border-color: #272525;
            border-radius: 4px;
        }
    </style>
@endsection
@section('main')
    <main class="main">
        <section class="banner-slider section--mb">
            <div class="container container--full">
                <div class="row">
                    <div class="container-inner">
                        <div class="banner-top-slider swiper-container preloader preloader--v">
                            <div class="swiper-wrapper">
                                @foreach($sliders as $slider)
                                    <div class="swiper-slide">
                                        <a class="banner-top-slider__slide" href="#">
                                            <img class="swiper-lazy" src="./" data-src="{{$slider->media->url}}"
                                                 alt="main top slider, bravo market offers">
                                        </a>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="banner-top-slider-thumb swiper-container">
                        <div class="swiper-wrapper" style="justify-content: center;">
                            @foreach($sliders as $slider)
                                <div class="swiper-slide">
                                    <h4 class="banner-top-slider-thumb__title"> {{$slider->title}}</h4>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="special-offer section--mb">
            <div class="container container--full">
                <div class="row">
                    <div class="container-inner section--green">
                        <div class="container">
                            <div class="row row--inner">
                                <div class="special-offer__col-title">
                                    <h3 class="special-offer__title preloader preloader--v">{{ translate('special.offers') }}</h3>
                                    <p class="special-offer__descr preloader preloader--v">{{ translate('special.offers_desc') }}</p>
                                </div>
                                <div class="special-offer__col-content">
                                    <div class="row">
                                        @foreach($offers as $key => $offer)
                                            <a class="special-offer-box preloader preloader--v"
                                               href="{{route('offers',['slug' => \Illuminate\Support\Str::slug($offer->title),'offer_id' => $offer->offer_id])}}">
                                                <h5 class="special-offer-box__title">{{$offer->title}}</h5>
                                                <div class="special-offer-calendar"
                                                     style="width: 120px;min-height: 120px;background-color: white;">
                                                    <h4 class="special-offer-calendar__title">{{ $key === 0 ? translate('common.weekin') : translate('common.weekend') }}</h4>
                                                    <div class="special-offer-calendar__content">
                                                        @if ($key === 0)
                                                            @if (getLocale() !== "az")
                                                                <p style="margin: 0;
                                                                        font-size: 10px;
                                                                       width: 100px;
                                                                       text-align: center;"
                                                                   class="special-offer-calendar__month">{{ translate('common.starting_from') }}</p>
                                                            @endif
                                                        @endif
                                                        @if ($key !== 0)
                                                           <div style="display: flex">
                                                               <div>
                                                                   <p style="margin: 0"
                                                                      class="special-offer-calendar__number">{{humanDate($offer->date,'Y-m-d H:i:s','d')}}</p>
                                                                   <p style="margin-left: 5px;"
                                                                      class="special-offer-calendar__month">{{humanDate($offer->date,'Y-m-d H:i:s','F')}}</p>
                                                               </div>
                                                               <div style="margin-top: 12px;
                                                                margin-left: 2px;
                                                                margin-right: 6px;
                                                                font-size: 30px;
                                                                color: #0a0a0a;
                                                                font-weight: bold;">-</div>
                                                               <div>
                                                                   <p style="margin: 0"
                                                                      class="special-offer-calendar__number">{{humanDate(\Carbon\Carbon::parse($offer->date)->addDays(1),'Y-m-d H:i:s','d')}}</p>
                                                                   <p style="margin-left: 5px;"
                                                                      class="special-offer-calendar__month">{{humanDate(\Carbon\Carbon::parse($offer->date)->addDays(1),'Y-m-d H:i:s','F')}}</p>
                                                               </div>
                                                           </div>
                                                        @else
                                                            <p style="margin: 0"
                                                               class="special-offer-calendar__number">{{humanDate($offer->date,'Y-m-d H:i:s','d')}}</p>
                                                            <p style="margin: 0"
                                                               class="special-offer-calendar__month">{{humanDate($offer->date,'Y-m-d H:i:s','F')}}{{ getLocale() === 'az' ? (count(explode(" ",trim(translate('common.starting_from')))) > 0 ? explode(" ",trim(translate('common.starting_from')))[0] : '') : '' }}</p>
                                                            @if ($key === 0)
                                                                @if (getLocale() === "az")
                                                                    <p style="margin: 0;
                                                                        font-size: 10px;
                                                                       width: 100px;
                                                                       text-align: center;"
                                                                       class="special-offer-calendar__month">{{ count(explode(" ",trim(translate('common.starting_from')))) > 1 ? explode(" ",trim(translate('common.starting_from')))[1] : '' }}</p>
                                                                @endif
                                                            @endif
                                                        @endif
                                                    </div>
                                                </div>
                                            </a>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="shop-article section--mb">
            <div class="container">
                <div class="row">
                    <div class="section">
                        <h2 class="section__title preloader preloader--v">{{ translate('home.bravo_shop') }}</h2>
                        <p class="section__descr preloader preloader--v">{{ translate('home.bravo_shop_desc') }}</p>
                    </div>
                </div>
                <div class="shop-article-wrap">
                    <div class="row">
                        @foreach($sabBanners as $sabBanner)
                            @if($loop->iteration<=3)
                                <a class="shop-article__col" href="{{$sabBanner->url}}">
                                    <div class="shop-article__thumb preloader preloader--v">
                                        <img style="width: 100%" class="blazy blazy--style"
                                             src="{{$sabBanner->media->url}}" data-src="{{$sabBanner->media->url}}"
                                             alt="shop article thumb">
                                    </div>
                                    <div class="shop-article__text">
                                        <h5 class="shop-article__title preloader preloader--v">{{$sabBanner->title}}</h5>
                                        <p class="shop-article__descr preloader preloader--v">{{$sabBanner->text}}</p>
                                    </div>
                                </a>
                            @else
                                <a class="shop-article__col more-data" style="display:none;" href="{{$sabBanner->url}}">
                                    <div class="shop-article__thumb preloader preloader--v">
                                        <img style="width: 100%" class="blazy blazy--style"
                                             src="{{$sabBanner->media->url}}" data-src="{{$sabBanner->media->url}}"
                                             alt="shop article thumb">
                                    </div>
                                    <div class="shop-article__text">
                                        <h5 class="shop-article__title preloader preloader--v">{{$sabBanner->title}}</h5>
                                        <p class="shop-article__descr preloader preloader--v">{{$sabBanner->text}}</p>
                                    </div>
                                </a>
                            @endif
                        @endforeach
                    </div>
                </div>
                <div class="row">
                    <div class="shop-article__full-col">
                        <a class="primary-btn primary-btn--green shop-article__btn preloader preloader--v load-more-sab"
                           href="#">{{ translate('common.see_more') }}</a>
                    </div>
                </div>
            </div>
        </section>
        <section class="careers section--mb">
            <div class="container container--full">
                <div class="row">
                    <div class="container-inner section--green">
                        <div class="container">
                            @foreach($abbBanners as $abbBanner)
                                <div class="row row--inner">
                                    <div class="careers__col-content">
                                        <h3 class="careers__title preloader preloader--v">{{$abbBanner->title??''}}</h3>
                                        <p class="careers__descr preloader preloader--v">{{$abbBanner->text??''}}</p>
                                        <a class="primary-btn primary-btn--white careers__btn preloader preloader--v"
                                           href="{{$abbBanner->url}}">{{ translate('common.read_more') }}</a>
                                    </div>
                                    <div class="careers__col-img preloader preloader--v">
                                        <img class="careers__img blazy blazy--style" src="./"
                                             data-src="{{$abbBanner->media->url??''}}" alt="bravo market">
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="bravo-banner section--mb preloader preloader--v">
            <div class="container container--full">
                @foreach($bbBanners as $bbBanner)
                    <div class="row">
                        <div class="container-inner section--green">
                            <a class="bravo-banner__link" href="{{$bbBanner->url??''}}">
                                <img class="bravo-banner__img blazy blazy--style" src="{{$bbBanner->media->url??''}}"
                                     data-src="{{$bbBanner->media->url??''}}" alt="{{$bbBanner->title??''}}">
                            </a>
                        </div>
                    </div>
                @endforeach
            </div>
        </section>
        <section class="careers section--mb">
            <div class="container container--full">
                <div class="row">
                    <div class="container-inner section--green">
                        <div class="container">
                            @foreach($bbbBanners as $bbbBanner)
                                <div class="row row--inner">
                                    <div class="careers__col-img preloader preloader--v">
                                        <img class="careers__img careers__img--right blazy blazy--style"
                                             src="{{$bbbBanner->media->url??''}}"
                                             data-src="{{$bbbBanner->media->url??''}}" alt="bravo market">
                                    </div>
                                    <div class="careers__col-content careers__col-content--right">
                                        <h3 class="careers__title preloader preloader--v">{{$bbbBanner->title??''}}</h3>
                                        <p class="careers__descr preloader preloader--v">{{$bbbBanner->text??''}}</p>
                                        <a class="primary-btn primary-btn--white careers__btn preloader preloader--v"
                                           href="{{$bbbBanner->url??''}}">{{ translate('common.read_more') }}</a>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="shop-article section--mb">
            <div class="container">
                <div class="row">
                    <div class="section">
                        <h2 class="section__title preloader preloader--v">{{ translate('home.media') }}</h2>
                        <p class="section__descr preloader preloader--v">{{ translate('home.media_desc') }}</p>
                    </div>
                </div>
                <div class="shop-article-wrap">
                    <div class="youtube_media_videos row" style="margin-bottom: 10px;">
                        <div class="shop-article__col">
                            <div class="shop-article__thumb">
                                <div class="shop-article__video">
                                    <div class="shop-article__video-wrap shop-article__video-wrap--first">
                                        <svg class="icon icon-yt shop-article__video-icon">
                                            <use xlink:href="front/images/sprite/sprite.svg#yt"></use>
                                        </svg>
                                    </div>
                                    <div class="shop-article__video-wrap shop-article__video-wrap--active">
                                        <iframe class="yt-player"
                                                src="https://www.youtube.com/embed/9GhqTa35VQw?enablejsapi=1"></iframe>
                                    </div>
                                </div>
                            </div>
                            <a class="shop-article__text" href="">
                                <p class="shop-article__descr">BRAVO - HƏR GÜN DAHA YAXŞISI ÜÇÜN
                                </p>
                            </a>
                        </div>
                    </div>
                    <div style="text-align: center;">
                        <a class="primary-btn primary-btn--green careers__btn preloader preloader--v" target="_blank"
                           href="https://www.youtube.com/channel/UCUDPswYmu2SCJQCr7qDdtwA/videos">{{ translate('common.see_more') }}</a>
                    </div>
                </div>
            </div>
        </section>
    </main>
@endsection

@section('scripts')
    <script>
        $(document).on('click', '.load-more-sab', function (e) {
            e.preventDefault();
            $('.more-data').show();
        });

        $.get('https://www.googleapis.com/youtube/v3/search?key=AIzaSyAL5eKtxDyc3iy5Mlwnzkx8h-KRIf8fU4I&channelId=UCUDPswYmu2SCJQCr7qDdtwA&part=snippet,id&order=date&maxResults=2', function (data) {
            console.log(data);
            data.items.map(item => (
                $('.youtube_media_videos').append('<div class="shop-article__col">\n' +
                    '                                <div class="shop-article__thumb">\n' +
                    '                                    <div class="shop-article__video">\n' +
                    '                                        <div class="shop-article__video-wrap shop-article__video-wrap--first">\n' +
                    '                                            <svg class="icon icon-yt shop-article__video-icon">\n' +
                    '                                                <use xlink:href="front/images/sprite/sprite.svg#yt"></use>\n' +
                    '                                            </svg>\n' +
                    '                                        </div>\n' +
                    '                                        <div class="shop-article__video-wrap shop-article__video-wrap--active">\n' +
                    '                                            <iframe class="yt-player" src="https://www.youtube.com/embed/' + item.id.videoId + '?enablejsapi=1"></iframe>\n' +
                    '                                        </div>\n' +
                    '                                    </div>\n' +
                    '                                </div>\n' +
                    '                                <a class="shop-article__text" href="">\n' +
                    '                                    <p class="shop-article__descr">' + item.snippet.title + '</p>\n' +
                    '                                </a>\n' +
                    '                            </div>')
            ))
        })

    </script>
@endsection
