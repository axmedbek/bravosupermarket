<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>Bravo Market</title>
    <meta content="" name="webskyus">
    <meta content="" name="this is description">
    <meta content="" name="website, landing">
    <meta content="width=device-width, initial-scale=1" name="viewport">
    <meta content="ie=edge" http-equiv="x-ua-compatible">
    <link rel="icon" href="{{asset('front/images/main/favicon/favicon.ico')}}">
    <link rel="apple-touch-icon" href="{{asset('front/images/main/favicon/apple-icon-180x180.png')}}">
    <link rel="stylesheet" href="{{asset('front/style/main.min.css')}}">
    @yield('styles')
</head>

<body>
<div class="body-wrapper">
    <header class="header">
        <div class="container">
            <div class="row">
                <nav class="main-nav">
                    <a class="logo preloader preloader--v" href="#">
                        <img class="blazy blazy--style" src="./" data-src="{{asset('front/images/main/icon/l_vertical.svg')}}" alt="bravo market logo')}}">
                    </a>
                    <ul class="main-menu">
                        <li class="main-menu__list preloader preloader--v">
                            <a class="main-menu__link" href="#">About Us</a>
                        </li>
                        <li class="main-menu__list preloader preloader--v">
                            <a class="main-menu__link" href="#">Contact us</a>
                        </li>
                        <li class="main-menu__list preloader preloader--v">
                            <a class="main-menu__link" href="#">Careers</a>
                        </li>
                        <li class="main-menu__list preloader preloader--v">
                            <a class="main-menu__link" href="#">Customer Service</a>
                        </li>
                        <li class="main-menu__list preloader preloader--v">
                            <a class="main-menu__link" href="#">Leaflet</a>
                        </li>
                        <li class="main-menu__list preloader preloader--v">
                            <a class="main-menu__link" href="#">Cards</a>
                        </li>
                        <li class="main-menu__list preloader preloader--v">
                            <a class="main-menu__link" href="#">CSR Activities</a>
                        </li>
                    </ul>
                </nav>
                <div class="store-place">
                    <a class="store-search preloader preloader--v" href="#">
                            <span class="store-search__icon">
                                <svg class="icon icon-loc store-search__icon-svg">
                                    <use xlink:href="{{asset('front/images/sprite/sprite.svg#loc')}}"></use>
                                </svg>
                            </span>
                        <span class="store-search__text store-search__text--mobile">
                                <span class="store-search__descr">Stores </span>
                            </span>
                        <span class="store-search__text">
                                <span class="store-search__descr">Find stores </span>
                                <span class="store-search__title">Baku </span>
                            </span>
                    </a>
                    <div class="lang preloader preloader--v">
                        <div class="select-wrap">
                            <div class="select-wrap__text" data-select-wrap="s1" data-select-focus="false">
                                <input class="select-wrap__input" type="hidden" name="site-lang" value="az" data-select-input="s1">
                                <svg class="icon icon-lang select-wrap__icon-svg">
                                    <use xlink:href="{{asset('front/images/sprite/sprite.svg#lang')}}"></use>
                                </svg>
                                <div class="select-wrap__selected" data-selected-item="s1" data-selected-focus="false">az </div>
                            </div>
                            <div class="select-wrap__option" data-select-option="s1" data-select-option-opened="false">
                                <span class="select-wrap__option-item" data-select-option-items="az" data-select-option-input="s1">az </span>
                                <span class="select-wrap__option-item" data-select-option-items="en" data-select-option-input="s1">en </span>
                                <span class="select-wrap__option-item" data-select-option-items="ru" data-select-option-input="s1">ru </span>
                            </div>
                        </div>
                    </div>
                    <button class="hamburger preloader preloader--v">
                        <span class="hamburger__item"></span>
                        <span class="hamburger__item"></span>
                        <span class="hamburger__item"></span>
                    </button>
                </div>
            </div>
        </div>
    </header>
    <!-- hidden h1 title -->
    <h1 class="hidden">Bravo market </h1>
    <!-- main -->
    @yield('main')
    <footer class="footer footer--white">
        <div class="container">
            <div class="row footer__row-nav">
                <nav class="footer-nav">
                    <ul class="footer-nav__menu">
                        <li class="footer-nav__items footer-nav__items--title preloader preloader--v"> Quick links</li>
                        <li class="footer-nav__items preloader preloader--v">
                            <a class="footer-nav__link" href="#">About us</a>
                        </li>
                        <li class="footer-nav__items preloader preloader--v">
                            <a class="footer-nav__link" href="#">Promotions</a>
                        </li>
                        <li class="footer-nav__items preloader preloader--v">
                            <a class="footer-nav__link" href="#">Press center</a>
                        </li>
                        <li class="footer-nav__items preloader preloader--v">
                            <a class="footer-nav__link" href="#">Recipes</a>
                        </li>
                        <li class="footer-nav__items preloader preloader--v">
                            <a class="footer-nav__link" href="#">Our stores</a>
                        </li>
                        <li class="footer-nav__items preloader preloader--v">
                            <a class="footer-nav__link" href="#">Contact us </a>
                        </li>
                    </ul>
                </nav>
                <nav class="footer-nav">
                    <ul class="footer-nav__menu">
                        <li class="footer-nav__items footer-nav__items--title preloader preloader--v"> About Bravo</li>
                        <li class="footer-nav__items preloader preloader--v">
                            <a class="footer-nav__link" href="#">History</a>
                        </li>
                        <li class="footer-nav__items preloader preloader--v">
                            <a class="footer-nav__link" href="#">Mission & values</a>
                        </li>
                        <li class="footer-nav__items preloader preloader--v">
                            <a class="footer-nav__link" href="#">Local charity</a>
                        </li>
                        <li class="footer-nav__items preloader preloader--v">
                            <a class="footer-nav__link" href="#">Sustainability</a>
                        </li>
                        <li class="footer-nav__items preloader preloader--v">
                            <a class="footer-nav__link" href="#">Headquarters</a>
                        </li>
                        <li class="footer-nav__items preloader preloader--v">
                            <a class="footer-nav__link" href="#">Countries of operation</a>
                        </li>
                        <li class="footer-nav__items preloader preloader--v">
                            <a class="footer-nav__link" href="#">Compliance </a>
                        </li>
                    </ul>
                </nav>
                <nav class="footer-nav">
                    <ul class="footer-nav__menu">
                        <li class="footer-nav__items footer-nav__items--title preloader preloader--v"> Products & Services</li>
                        <li class="footer-nav__items preloader preloader--v">
                            <a class="footer-nav__link" href="#">Departments</a>
                        </li>
                        <li class="footer-nav__items preloader preloader--v">
                            <a class="footer-nav__link" href="#">Quality standards</a>
                        </li>
                        <li class="footer-nav__items preloader preloader--v">
                            <a class="footer-nav__link" href="#">Food safety</a>
                        </li>
                        <li class="footer-nav__items preloader preloader--v">
                            <a class="footer-nav__link" href="#">Product manuals</a>
                        </li>
                        <li class="footer-nav__items preloader preloader--v">
                            <a class="footer-nav__link" href="#">Product videos </a>
                        </li>
                    </ul>
                </nav>
                <nav class="footer-nav">
                    <ul class="footer-nav__menu">
                        <li class="footer-nav__items footer-nav__items--title preloader preloader--v"> Customer care</li>
                        <li class="footer-nav__items preloader preloader--v">
                            <a class="footer-nav__link" href="#">Potential suppliers</a>
                        </li>
                        <li class="footer-nav__items preloader preloader--v">
                            <a class="footer-nav__link" href="#">Real estate</a>
                        </li>
                        <li class="footer-nav__items preloader preloader--v">
                            <a class="footer-nav__link" href="#">FAQ</a>
                        </li>
                        <li class="footer-nav__items preloader preloader--v">
                            <a class="footer-nav__link" href="#">Product recalls </a>
                        </li>
                    </ul>
                </nav>
            </div>
        </div>
    </footer>
    <footer class="footer footer--green">
        <div class="container">
            <div class="row">
                <div class="footer__col-logo preloader preloader--v">
                    <a class="footer__logo" href="#">
                        <img class="blazy blazy--style" src="./" data-src="{{asset('front/images/main/icon/l_vertical.svg')}}" alt="bravo market logo">
                    </a>
                </div>
                <div class="footer__col-content">
                    <div class="footer-nav-about">
                        <nav class="footer-nav footer-nav--about preloader preloader--v">
                            <ul class="footer-nav__menu">
                                <li class="footer-nav__items footer-nav__items--title footer-nav__items--title-white"> Get Bravo app</li>
                                <li class="footer-nav__items">
                                    <a class="footer-nav__link footer-nav__link--white" href="#">Learn more</a>
                                </li>
                                <li class="footer-nav__items">
                                    <a class="footer-nav__link footer-nav__link--white" href="#">Google play</a>
                                </li>
                                <li class="footer-nav__items">
                                    <a class="footer-nav__link footer-nav__link--white" href="#">App store</a>
                                </li>
                            </ul>
                        </nav>
                        <nav class="footer-nav footer-nav--about preloader preloader--v">
                            <ul class="footer-nav__menu">
                                <li class="footer-nav__items footer-nav__items--title footer-nav__items--title-white">Legal</li>
                                <li class="footer-nav__items">
                                    <a class="footer-nav__link footer-nav__link--white" href="#">Privacy policy</a>
                                </li>
                                <li class="footer-nav__items">
                                    <a class="footer-nav__link footer-nav__link--white" href="#">Terms of use</a>
                                </li>
                                <li class="footer-nav__items">
                                    <a class="footer-nav__link footer-nav__link--white" href="#">Contest rules and regulations</a>
                                </li>
                            </ul>
                        </nav>
                        <p class="footer-nav-about__copyright preloader preloader--v">All rights reserved © Azerbaijan Supermarket MMC 2020 </p>
                    </div>
                </div>
                <div class="footer__col-social">
                    <div class="footer-social">
                        <h4 class="footer-social__title preloader preloader--v">Connect with us</h4>
                        <div class="footer-social__wrap">
                            <a class="footer-social__link preloader preloader--v" href="#">
                                <svg class="icon icon-fb footer-social__icon">
                                    <use xlink:href="{{asset('front/images/sprite/sprite.svg#fb')}}"></use>
                                </svg>
                            </a>
                            <a class="footer-social__link preloader preloader--v" href="#">
                                <svg class="icon icon-tw footer-social__icon">
                                    <use xlink:href="{{asset('front/images/sprite/sprite.svg#tw')}}"></use>
                                </svg>
                            </a>
                            <a class="footer-social__link preloader preloader--v" href="#">
                                <svg class="icon icon-ins footer-social__icon">
                                    <use xlink:href="{{asset('front/images/sprite/sprite.svg#ins')}}"></use>
                                </svg>
                            </a>
                            <a class="footer-social__link preloader preloader--v" href="#">
                                <svg class="icon icon-yt1 footer-social__icon">
                                    <use xlink:href="{{asset('front/images/sprite/sprite.svg#yt1')}}"></use>
                                </svg>
                            </a>
                            <a class="footer-social__link preloader preloader--v" href="#">
                                <svg class="icon icon-lk footer-social__icon">
                                    <use xlink:href="{{asset('front/images/sprite/sprite.svg#lk')}}"></use>
                                </svg>
                            </a>
                            <a class="footer-social__link preloader preloader--v" href="#">
                                <svg class="icon icon-pt footer-social__icon">
                                    <use xlink:href="{{asset('front/images/sprite/sprite.svg#pt')}}"></use>
                                </svg>
                            </a>
                            <a class="footer-social__link preloader preloader--v" href="#">
                                <svg class="icon icon-gg footer-social__icon">
                                    <use xlink:href="{{asset('front/images/sprite/sprite.svg#gg')}}"></use>
                                </svg>
                            </a>
                            <a class="footer-social__link preloader preloader--v" href="#">
                                <svg class="icon icon-tg footer-social__icon">
                                    <use xlink:href="{{asset('front/images/sprite/sprite.svg#tg')}}"></use>
                                </svg>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="footer__col-copyright">
                    <p class="footer-nav-about__copyright preloader preloader--v">All rights reserved © Azerbaijan Supermarket MMC 2020 </p>
                </div>
                <div class="footer__col-subs">
                    <form class="footer-subs">
                        <div class="footer-subs__row">
                            <div class="footer-subs__col">
                                <h4 class="footer-subs__title preloader preloader--v">Sign up for our newsletter</h4>
                            </div>
                        </div>
                        <div class="footer-subs__row preloader preloader--v">
                            <div class="footer-subs__col">
                                <input class="footer-subs__input" type="email" name="footer-subs-email" placeholder="Enter your email" required>
                                <button class="footer-subs__btn" type="submit">
                                    <svg class="icon icon-arrow-r footer-subs__icon">
                                        <use xlink:href="{{asset('front/images/sprite/sprite.svg#arrow-r')}}"></use>
                                    </svg>
                                </button>
                            </div>
                        </div>
                        <div class="footer-subs__row">
                            <div class="footer-subs__col footer-subs__col--ai-center">
                                <a class="footer-subs__dev-logo preloader preloader--v" href="#">
                                    <img class="blazy blazy--style" src="./" data-src="{{asset('front/images/main/icon/dev_logo.svg')}}" alt="bravo market">
                                </a>
                                <p class="footer-subs__message footer-subs__message--success preloader preloader--v">Thanks for subscribing! </p>
                                <!-- p.footer-subs__message.footer-subs__message--error Something went wrong	-->
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </footer>
</div>
<script src="{{asset('front/js/main.min.js')}}" defer></script>
@yield('scripts')
</body>

</html>
