<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>Bravo Market</title>
    <meta content="" name="webskyus">
    <meta content="" name="this is description">
    <meta content="" name="website, landing">
    <meta content="width=device-width, initial-scale=1" name="viewport">
    <meta content="ie=edge" http-equiv="x-ua-compatible">
    <link rel="icon" href="{{asset('front/images/main/favicon/favicon.ico')}}">
    <link rel="apple-touch-icon" href="{{asset('front/images/main/favicon/apple-icon-180x180.png')}}">
    <link rel="stylesheet" href="{{asset('front/style/main.min.css')}}">
    <script src="{{asset('panel/js/main/jquery.min.js?12345')}}"></script>

    @yield('styles')
</head>

<body>
<div class="body-wrapper">
    <header class="header">
        <div class="container">
            <div class="row">
                <nav class="main-nav">
                    <a class="logo preloader preloader--v" href="{{route('home')}}">
                        <img class="blazy blazy--style" src="./" data-src="{{asset('front/images/main/icon/l_vertical.svg')}}" alt="bravo market logo')}}">
                    </a>
                    <ul class="main-menu">
                        @php($menus = \App\Menu::getTargetMenus('targetCareersTop'))
                        @foreach($menus as $menu)
                            <li class="main-menu__list preloader preloader--v">
                                <a class="main-menu__link" href="@if($menu->page['id']){{route('page',['slug' => $menu->page['slug'],'id' => $menu->page['id']])}} @endif">{{$menu->name}}</a>
                            </li>
                        @endforeach
                    </ul>
                </nav>
                <div class="store-place">
                    @php($locationMenu = \App\Menu::getTargetLocationMenu())
                    @if (!is_null($locationMenu))
                        <a class="store-search preloader preloader--v" href="{{ route('page',['slug' => $locationMenu->page['slug'],'id' => $locationMenu->page['id']]) }}">
                            <span class="store-search__icon">
                                <svg class="icon icon-loc store-search__icon-svg">
                                    <use xlink:href="{{asset('front/images/sprite/sprite.svg#loc')}}"></use>
                                </svg>
                            </span>
                            <span class="store-search__text store-search__text--mobile">
                                <span class="store-search__descr">{{ $locationMenu->name }}</span>
                            </span>
                            <span class="store-search__text">
                                <span class="store-search__descr">{{ $locationMenu->name }}</span>
                                <span class="store-search__title">{{ $locationMenu->description }} </span>
                            </span>
                        </a>
                    @endif
                    <div class="lang preloader preloader--v">
                        <div class="select-wrap">
                            <div class="select-wrap__text" data-select-wrap="s1" data-select-focus="false">
                                <input class="select-wrap__input" type="hidden" name="site-lang" value="az" data-select-input="s1">
                                <svg class="icon icon-lang select-wrap__icon-svg">
                                    <use xlink:href="{{asset('front/images/sprite/sprite.svg#lang')}}"></use>
                                </svg>
                                <div class="select-wrap__selected" data-selected-item="s1" data-selected-focus="false">{{getLanguage()->key == "az" ? "AZE" : (getLanguage()->key == "en" ? "ENG" : getLanguage()->key)}} </div>
                            </div>
                            <div class="select-wrap__option" data-select-option="s1" data-select-option-opened="false">
                                @foreach(getLanguages() as $language)
                                    <a href="{{route('setLocale',['language' => $language->key])}}" style="color:#333">
                                        <span class="select-wrap__option-item {{ getLocale() === $language->key ? 'select-wrap__option-item_active' : '' }}" data-select-option-items="{{$language->key}}" data-select-option-input="s1">{{$language->key == "az" ? "AZE" : ($language->key == "en" ? "ENG" : $language->key)}} </span>
                                    </a>
                                @endforeach
                            </div>
                        </div>
                    </div>
                    <button class="hamburger preloader preloader--v">
                        <span class="hamburger__item"></span>
                        <span class="hamburger__item"></span>
                        <span class="hamburger__item"></span>
                    </button>
                </div>
            </div>
        </div>
    </header>
    <!-- hidden h1 title -->
    <h1 class="hidden">Bravo market </h1>
    <!-- main -->
    @yield('main')
    <footer class="footer footer--white">
        <div class="container">
            <div class="row footer__row-nav">
                @php($bottomMenus = \App\Menu::where('parent_id',null)->where('locale',getLocale())->where('target','LIKE','%targetBottom%')->get())
                @foreach($bottomMenus as $bottomMenu)
                    <nav class="footer-nav">
                        @php($bottomSubMenus = \App\Menu::where('parent_id',$bottomMenu->menu_id)->where('locale',getLocale())->where('target','LIKE','%targetBottom%')->get())
                        <ul class="footer-nav__menu">
                            <li class="footer-nav__items footer-nav__items--title preloader preloader--v"> {{$bottomMenu->name}}</li>

                            @foreach($bottomSubMenus as $bottomSubMenu)
                                <li class="footer-nav__items preloader preloader--v">
                                    <a class="footer-nav__link" href="{{$bottomSubMenu->url}}">{{$bottomSubMenu->name}}</a>
                                </li>
                            @endforeach
                        </ul>
                    </nav>
                @endforeach
            </div>
        </div>
    </footer>
    <footer class="footer footer--green">
        <div class="container">
            <div class="row">
                <div class="footer__col-logo preloader preloader--v">
                    <a class="footer__logo" href="{{route('home')}}">
                        <img class="blazy blazy--style" src="./" data-src="{{asset('front/images/main/icon/l_vertical.svg')}}" alt="bravo market logo">
                    </a>
                </div>
                <div class="footer__col-content">
                    <div class="footer-nav-about">



                        @php($footerMenus = \App\Menu::where('parent_id',null)->where('target','LIKE','%targetFooter%')->orderBy('order', 'asc')->where('locale',getLocale())->get())
                        @foreach($footerMenus as $footerMenu)
                            <nav class="footer-nav footer-nav--about preloader preloader--v">
                                @php($footerSubMenus = \App\Menu::where('parent_id',$footerMenu->menu_id)->orderBy('order', 'asc')->where('target','LIKE','%targetFooter%')->where('locale',getLocale())->get())
                                <ul class="footer-nav__menu">
                                    <li class="footer-nav__items footer-nav__items--title footer-nav__items--title-white"> {{$footerMenu->name}}</li>
                                    @foreach($footerSubMenus as $footerSubMenu)
                                        <li class="footer-nav__items">
                                            <a class="footer-nav__link footer-nav__link--white" href="@if($footerSubMenu->page['id']){{route('page',['slug' => $footerSubMenu->page['slug'],'id' => $footerSubMenu->page['id']])}} @else {{$footerSubMenu->url??'#'}} @endif">{{$footerSubMenu->name}}</a>
                                        </li>
                                    @endforeach
                                </ul>
                            </nav>

                        @endforeach

                        <p class="footer-nav-about__copyright preloader preloader--v">{{ translate('common.copyright') }}</p>
                    </div>
                </div>
                <div class="footer__col-social">
                    <div class="footer-social">
                        <h4 class="footer-social__title preloader preloader--v">{{ translate('common.connect_us') }}</h4>
                        <div class="footer-social__wrap">
                            <a class="footer-social__link preloader preloader--v" href="{{config('settings.socials.facebook')}}" target="_blank">
                                <svg class="icon icon-fb footer-social__icon">
                                    <use xlink:href="{{asset('front/images/sprite/sprite.svg#fb')}}"></use>
                                </svg>
                            </a>
                            {{--                            <a class="footer-social__link preloader preloader--v" href="#">--}}
                            {{--                                <svg class="icon icon-tw footer-social__icon">--}}
                            {{--                                    <use xlink:href="{{asset('front/images/sprite/sprite.svg#tw')}}"></use>--}}
                            {{--                                </svg>--}}
                            {{--                            </a>--}}
                            <a class="footer-social__link preloader preloader--v" href="{{config('settings.socials.instagram')}}" target="_blank">
                                <svg class="icon icon-ins footer-social__icon">
                                    <use xlink:href="{{asset('front/images/sprite/sprite.svg#ins')}}"></use>
                                </svg>
                            </a>
                            <a class="footer-social__link preloader preloader--v" href="{{config('settings.socials.youtube')}}" target="_blank">
                                <svg class="icon icon-yt1 footer-social__icon">
                                    <use xlink:href="{{asset('front/images/sprite/sprite.svg#yt1')}}"></use>
                                </svg>
                            </a>
                            <a class="footer-social__link preloader preloader--v" href="{{config('settings.socials.linkedin')}}" target="_blank">
                                <svg class="icon icon-lk footer-social__icon">
                                    <use xlink:href="{{asset('front/images/sprite/sprite.svg#lk')}}"></use>
                                </svg>
                            </a>
                            {{--                            <a class="footer-social__link preloader preloader--v" href="{{config('settings.socials.pinterest')}}">--}}
                            {{--                                <svg class="icon icon-pt footer-social__icon">--}}
                            {{--                                    <use xlink:href="{{asset('front/images/sprite/sprite.svg#pt')}}"></use>--}}
                            {{--                                </svg>--}}
                            {{--                            </a>--}}
                            {{--                            <a class="footer-social__link preloader preloader--v" href="{{config('settings.socials.googleplus')}}">--}}
                            {{--                                <svg class="icon icon-gg footer-social__icon">--}}
                            {{--                                    <use xlink:href="{{asset('front/images/sprite/sprite.svg#gg')}}"></use>--}}
                            {{--                                </svg>--}}
                            {{--                            </a>--}}
                            {{--                            <a class="footer-social__link preloader preloader--v" href="{{config('settings.socials.telegram')}}">--}}
                            {{--                                <svg class="icon icon-tg footer-social__icon">--}}
                            {{--                                    <use xlink:href="{{asset('front/images/sprite/sprite.svg#tg')}}"></use>--}}
                            {{--                                </svg>--}}
                            {{--                            </a>--}}
                        </div>
                    </div>
                </div>
                <div class="footer__col-copyright">
                    <p class="footer-nav-about__copyright preloader preloader--v">{{ translate('common.copyright') }}</p>
                </div>
                <div class="footer__col-subs">
                    <form class="footer-subs">
                        <div class="footer-subs__row">
                            <div class="footer-subs__col">
                                <h4 class="footer-subs__title preloader preloader--v">{{ translate('common.newsletter') }}</h4>
                            </div>
                        </div>
                        <div class="footer-subs__row preloader preloader--v">
                            <div class="footer-subs__col">
                                <input class="footer-subs__input" type="email" name="footer-subs-email" placeholder="{{ translate('forms.enter_email') }}" required>
                                <button class="footer-subs__btn" type="submit">
                                    <svg class="icon icon-arrow-r footer-subs__icon">
                                        <use xlink:href="{{asset('front/images/sprite/sprite.svg#arrow-r')}}"></use>
                                    </svg>
                                </button>
                            </div>
                        </div>
                        <div class="footer-subs__row">
                            <div class="footer-subs__col footer-subs__col--ai-center">
                                <a class="footer-subs__dev-logo preloader preloader--v" href="#">
                                    <img class="blazy blazy--style" src="./" data-src="{{asset('front/images/main/icon/dev_logo.svg')}}" alt="bravo market">
                                </a>
                                @if (!is_null(getParamFromURL('footer-subs-email')))
                                    <p class="footer-subs__message footer-subs__message--success preloader preloader--v">
                                        {{ translate('common.subscribe_thanks') }}
                                    </p>
                            @endif
                            <!-- p.footer-subs__message.footer-subs__message--error Something went wrong	-->
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </footer>
</div>
<script src="{{asset('front/js/main.min.js')}}" defer></script>
@yield('scripts')
</body>

</html>
