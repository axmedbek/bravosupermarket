<form class="contacts-form" action="{{ route('form.customer_feedback') }}" method="post" enctype="multipart/form-data">
    {{ csrf_field() }}
    <div class="contacts-form__row">
        <div class="contacts-form__col contacts-form__col--half">
            <label class="contacts-form__label">{{ translate('forms.request_type') }} : </label>
            <div class="select-wrap__text select-wrap__text--full" data-select-wrap="fsr-select-4.3"
                 data-select-focus="false">
                <input class="select-wrap__input" type="hidden" name="request_type" value="Choose your info"
                       data-select-input="fsr-select-4.3">
                <div class="select-wrap__selected select-wrap__selected--grey" data-selected-item="fsr-select-4.3"
                     data-selected-focus="false">{{ translate('forms.choose_request_type') }}</div>
                <svg class="icon icon-arrow select-wrap__icon-svg select-wrap__icon-svg--arrow">
                    <use xlink:href="{{asset('front/images/sprite/sprite.svg#arrow')}}"></use>
                </svg>
                <div class="select-wrap__option select-wrap__option--form-page" data-select-option="fsr-select-4.3"
                     data-select-option-opened="false" data-select-option-scroll="true">
                    <span class="select-wrap__option-item select-wrap__option-item--form-page"
                          data-select-option-items="{{ translate('forms.sual') }}"
                          data-select-option-input="fsr-select-4.3">{{ translate('forms.sual') }}</span>
                    <span class="select-wrap__option-item select-wrap__option-item--form-page"
                          data-select-option-items="{{ translate('forms.teklif') }}"
                          data-select-option-input="fsr-select-4.3">{{ translate('forms.teklif') }}</span>
                    <span class="select-wrap__option-item select-wrap__option-item--form-page"
                          data-select-option-items="{{ translate('forms.irad') }}"
                          data-select-option-input="fsr-select-4.3">{{ translate('forms.irad') }}</span>
                    <span class="select-wrap__option-item select-wrap__option-item--form-page"
                          data-select-option-items="{{ translate('forms.tesekkur') }}"
                          data-select-option-input="fsr-select-4.3">{{ translate('forms.tesekkur') }}</span>
                </div>
            </div>
        </div>
        <div class="contacts-form__col contacts-form__col--half">
            <label class="contacts-form__label">{{ translate('forms.request_subject') }} : </label>
            <div class="select-wrap__text select-wrap__text--full" data-select-wrap="fsr-select-4.4"
                 data-select-focus="false">
                <input class="select-wrap__input" type="hidden" name="request_subject" value="Choose your info"
                       data-select-input="fsr-select-4.4">
                <div class="select-wrap__selected select-wrap__selected--grey" data-selected-item="fsr-select-4.4"
                     data-selected-focus="false">{{ translate('forms.choose_request_subject') }}</div>
                <svg class="icon icon-arrow select-wrap__icon-svg select-wrap__icon-svg--arrow">
                    <use xlink:href="{{asset('front/images/sprite/sprite.svg#arrow')}}"></use>
                </svg>
                <div class="select-wrap__option select-wrap__option--form-page" data-select-option="fsr-select-4.4"
                     data-select-option-opened="false" data-select-option-scroll="true">
                    <span class="select-wrap__option-item select-wrap__option-item--form-page"
                              data-select-option-items="{{ translate('forms.mehsul_cesidi') }}"
                              data-select-option-input="fsr-select-4.4">{{ translate('forms.mehsul_cesidi') }}</span>
                    <span class="select-wrap__option-item select-wrap__option-item--form-page"
                          data-select-option-items="{{ translate('forms.mehsul_keyfiyyet') }}"
                          data-select-option-input="fsr-select-4.4">{{ translate('forms.mehsul_keyfiyyet') }}</span>
                    <span class="select-wrap__option-item select-wrap__option-item--form-page"
                          data-select-option-items="{{ translate('forms.qiymet') }}"
                          data-select-option-input="fsr-select-4.4">{{ translate('forms.qiymet') }}</span>
                    <span class="select-wrap__option-item select-wrap__option-item--form-page"
                          data-select-option-items="{{ translate('forms.kassa_xidmeti') }}"
                          data-select-option-input="fsr-select-4.4">{{ translate('forms.kassa_xidmeti') }}</span>
                    <span class="select-wrap__option-item select-wrap__option-item--form-page"
                          data-select-option-items="{{ translate('forms.bonus_endirim') }}"
                          data-select-option-input="fsr-select-4.4">{{ translate('forms.bonus_endirim') }}</span>
                    <span class="select-wrap__option-item select-wrap__option-item--form-page"
                          data-select-option-items="{{ translate('forms.diger') }}"
                          data-select-option-input="fsr-select-4.4">{{ translate('forms.diger') }}</span>
                </div>
            </div>
        </div>
    </div>
    <div class="contacts-form__row">
        <div class="contacts-form__col contacts-form__col--half">
            <label class="contacts-form__label">{{ translate('forms.firstname') }}</label>
            <input class="contacts-form__input" type="text" name="firstname"
                   placeholder="{{ translate('forms.firstname') }}" required>
        </div>
        <div class="contacts-form__col contacts-form__col--half">
            <label class="contacts-form__label">{{ translate('forms.lastname') }}</label>
            <input class="contacts-form__input" type="text" name="lastname"
                   placeholder="{{ translate('forms.lastname') }}" required>
        </div>
    </div>
    <div class="contacts-form__row">
        <div class="contacts-form__col contacts-form__col--half">
            <label class="contacts-form__label">{{ translate('forms.email') }}</label>
            <input class="contacts-form__input" type="text" name="email"
                   placeholder="{{ translate('forms.email') }}" required>
        </div>
        <div class="contacts-form__col contacts-form__col--half">
            <label class="contacts-form__label">{{ translate('forms.phone') }}</label>
            <input class="contacts-form__input" type="number" name="phone"
                   placeholder="{{ translate('forms.phone') }}" required>
        </div>
    </div>
    <div class="contacts-form__col contacts-form__col--half">
        <div class="contact-form-loaded">
            <input class="contact-form-loaded__input" type="file" name="images[]" multiple>
            <button class="contact-form-loaded__close">
                <svg class="icon icon-close contact-form-loaded__icon-svg">
                    <use xlink:href="{{asset('front/images/sprite/sprite.svg#close')}}"></use>
                </svg>
            </button>
            <div class="contact-form-loaded__inner">
                <div class="contact-form-loaded__img">
                    <img class="contact-form-loaded__icon blazy blazy--style" src="./" data-src="{{asset('front/images/main/icon/pdf_active.svg')}}" alt="bravo market">
                    <img class="contact-form-loaded__icon blazy blazy--style" src="./" data-src="{{asset('front/images/main/icon/pdf.svg')}}" alt="bravo market">
                </div>
                <div class="contact-form-loaded__content">
                    <h4 class="contact-form-loaded__title">{{ translate('forms.muraciet_images') }}</h4>
                    <p class="contact-form-loaded__descr">{{ translate('forms.formats') }}: .pdf .png .jpeg .jpg .xlxs</p>
                </div>
            </div>
        </div>
    </div>
    <div class="contacts-form__row">
        <div class="contacts-form__col contacts-form__col--full">
            <label class="contacts-form__label">{{ translate('forms.muraciet_tesviri') }} </label>
            <textarea class="contacts-form__textarea" name="message"
                      placeholder="{{ translate('forms.muraciet_tesviri') }}"> </textarea>
        </div>
    </div>
    <div class="contacts-form__row contacts-form__row--send-btn">
        <div class="contacts-form__col contacts-form__col--half">
            <button class="contacts-form__send-btn primary-btn primary-btn--green">
                <svg class="icon icon-send contacts-form__send-icon">
                    <use xlink:href="{{asset('front/images/sprite/sprite.svg#send')}}"></use>
                </svg>{{ translate('forms.send') }}
            </button>
        </div>
    </div>
</form>
