<form class="contacts-form" action="{{ route('form.contact_feedback') }}" method="post">
    {{ csrf_field() }}
    <div class="contacts-form__row">
        <div class="contacts-form__col contacts-form__col--half">
            <label class="contacts-form__label">{{ translate('forms.firstname') }}</label>
            <input class="contacts-form__input" type="text" name="firstname"
                   placeholder="{{ translate('forms.firstname') }}" required>
        </div>
        <div class="contacts-form__col contacts-form__col--half">
            <label class="contacts-form__label">{{ translate('forms.lastname') }}</label>
            <input class="contacts-form__input" type="text" name="lastname"
                   placeholder="{{ translate('forms.lastname') }}" required>
        </div>
    </div>
    <div class="contacts-form__row">
        <div class="contacts-form__col contacts-form__col--half">
            <label class="contacts-form__label">{{ translate('forms.email') }}</label>
            <input class="contacts-form__input" type="text" name="email"
                   placeholder="{{ translate('forms.email') }}" required>
        </div>
        <div class="contacts-form__col contacts-form__col--half">
            <label class="contacts-form__label">{{ translate('forms.phone') }}</label>
            <input class="contacts-form__input" type="number" name="phone"
                   placeholder="{{ translate('forms.phone') }}" required>
        </div>
    </div>
    <div class="contacts-form__row">
        <div class="contacts-form__col contacts-form__col--half">
            <label class="contacts-form__label">{{ translate('forms.date') }}</label>
            <input class="contacts-form__input" type="date" name="date"
                   placeholder="{{ translate('forms.choose_date') }}" required>
        </div>
    </div>
    <div class="contacts-form__row">
        <div class="contacts-form__col contacts-form__col--half">
            <label class="contacts-form__label">{{ translate('forms.request_type') }} : </label>
            <div class="select-wrap__text select-wrap__text--full" data-select-wrap="fsr-select-4.1"
                 data-select-focus="false">
                <input class="select-wrap__input" type="hidden" name="request_type" value="Choose your info"
                       data-select-input="fsr-select-4.1">
                <div class="select-wrap__selected select-wrap__selected--grey" data-selected-item="fsr-select-4.1"
                     data-selected-focus="false">{{ translate('forms.choose_request_type') }}</div>
                <svg class="icon icon-arrow select-wrap__icon-svg select-wrap__icon-svg--arrow">
                    <use xlink:href="{{asset('front/images/sprite/sprite.svg#arrow')}}"></use>
                </svg>
                <div class="select-wrap__option select-wrap__option--form-page" data-select-option="fsr-select-4.1"
                     data-select-option-opened="false" data-select-option-scroll="true">
                    <span class="select-wrap__option-item select-wrap__option-item--form-page"
                          data-select-option-items="{{ translate('forms.suallar') }}"
                          data-select-option-input="fsr-select-4.1">{{ translate('forms.suallar') }}</span>
                    <span class="select-wrap__option-item select-wrap__option-item--form-page"
                          data-select-option-items="{{ translate('forms.comments') }}"
                          data-select-option-input="fsr-select-4.1">{{ translate('forms.comments') }}</span>
                </div>
            </div>
        </div>
        <div class="contacts-form__col contacts-form__col--half">
            <label class="contacts-form__label">{{ translate('forms.stores') }} : </label>
            <div class="select-wrap__text select-wrap__text--full" data-select-wrap="fsr-select-4.2"
                 data-select-focus="false">
                <input class="select-wrap__input" type="hidden" name="store" value="Choose your info"
                       data-select-input="fsr-select-4.2">
                <div class="select-wrap__selected select-wrap__selected--grey" data-selected-item="fsr-select-4.2"
                     data-selected-focus="false">{{ translate('forms.choose_store') }}</div>
                <svg class="icon icon-arrow select-wrap__icon-svg select-wrap__icon-svg--arrow">
                    <use xlink:href="{{asset('front/images/sprite/sprite.svg#arrow')}}"></use>
                </svg>
                <div class="select-wrap__option select-wrap__option--form-page" data-select-option="fsr-select-4.2"
                     data-select-option-opened="false" data-select-option-scroll="true">
                    @foreach(\App\Store::where('locale',getLocale())->get() as $store)
                        <span class="select-wrap__option-item select-wrap__option-item--form-page"
                              data-select-option-items="{{ $store->name }}"
                              data-select-option-input="fsr-select-4.2">{{ $store->name }}</span>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
    <div class="contacts-form__row">
        <div class="contacts-form__col contacts-form__col--full">
            <label class="contacts-form__label">{{ translate('forms.message') }} </label>
            <textarea class="contacts-form__textarea" name="message"
                      placeholder="{{ translate('forms.message') }}"> </textarea>
        </div>
    </div>
    <div class="contacts-form__row contacts-form__row--send-btn">
        <div class="contacts-form__col contacts-form__col--half">
            <button class="contacts-form__send-btn primary-btn primary-btn--green">
                <svg class="icon icon-send contacts-form__send-icon">
                    <use xlink:href="{{asset('front/images/sprite/sprite.svg#send')}}"></use>
                </svg>{{ translate('forms.send') }}
            </button>
        </div>
    </div>
</form>
