<form class="contacts-form" action="{{ route('form.media') }}" method="post">
    {{ csrf_field() }}
    <div class="contacts-form__row">
        <div class="contacts-form__col contacts-form__col--half">
            <label class="contacts-form__label">{{ translate('forms.company_name') }}</label>
            <input class="contacts-form__input" type="text" name="company_name" placeholder="{{ translate('forms.company_name') }}" required>
        </div>
        <div class="contacts-form__col contacts-form__col--half">
            <label class="contacts-form__label">{{ translate('forms.relevant_person') }}</label>
            <input class="contacts-form__input" type="text" name="relevant_person" placeholder="{{ translate('forms.relevant_person') }}" required>
        </div>
    </div>
    <div class="contacts-form__row">
        <div class="contacts-form__col contacts-form__col--half">
            <label class="contacts-form__label">{{ translate('forms.email') }}</label>
            <input class="contacts-form__input" type="text" name="email" placeholder="{{ translate('forms.email') }}" required>
        </div>
        <div class="contacts-form__col contacts-form__col--half">
            <label class="contacts-form__label">{{ translate('forms.phone') }} </label>
            <input class="contacts-form__input" type="number" name="phone" placeholder="{{ translate('forms.phone') }}" required>
        </div>
    </div>
    <div class="contacts-form__row">
        <div class="contacts-form__col contacts-form__col--half">
            <label class="contacts-form__label">{{ translate('forms.address') }}</label>
            <input class="contacts-form__input" type="text" name="address" placeholder="{{ translate('forms.address') }}" required>
        </div>
        <div class="contacts-form__col contacts-form__col--half">
            <label class="contacts-form__label">{{ translate('forms.subject') }}</label>
            <input class="contacts-form__input" type="text" name="subject" placeholder="{{ translate('forms.subject') }}" required>
        </div>
    </div>
    <div class="contacts-form__row">
        <div class="contacts-form__col contacts-form__col--full">
            <label class="contacts-form__label">{{ translate('forms.message') }} </label>
            <textarea class="contacts-form__textarea" name="message" placeholder="{{ translate('forms.message') }}"></textarea>
        </div>
    </div>
    <div class="contacts-form__row contacts-form__row--send-btn">
        <div class="contacts-form__col contacts-form__col--half">
            <button class="contacts-form__send-btn primary-btn primary-btn--green">
                <svg class="icon icon-send contacts-form__send-icon">
                    <use xlink:href="{{asset('front/images/sprite/sprite.svg#send')}}"></use>
                </svg>{{ translate('forms.send') }}
            </button>
        </div>
    </div>
</form>
