<div class="article-slider">
    <div class="article-slider__main article-slider__main--slider-1 swiper-container swiper-container-initialized swiper-container-horizontal">
        <div class="swiper-wrapper" style="transition-duration: 0ms; transform: translate3d(0px, 0px, 0px);">
            <div class="swiper-slide swiper-slide-active" style="width: 740px;">
                <div class="article-slider__slide">
                    <img class="swiper-lazy swiper-lazy-loaded" src="{{ asset('front/images/main/adv/ad1.png') }}" alt="bravo posr slider img">
                </div>
            </div>
            <div class="swiper-slide swiper-slide-next" style="width: 740px;">
                <div class="article-slider__slide">
                    <img class="swiper-lazy swiper-lazy-loaded" src="{{ asset('front/images/main/adv/ad2.jpg') }}" alt="bravo posr slider img">
                </div>
            </div>
            <div class="swiper-slide" style="width: 740px;">
                <div class="article-slider__slide">
                    <img class="swiper-lazy swiper-lazy-loaded" src="{{ asset('front/images/main/adv/ad3.jpg') }}" alt="bravo posr slider img">
                </div>
            </div>
        </div>
        <span class="swiper-notification" aria-live="assertive" aria-atomic="true"></span></div>
    <div class="article-slide-count article-slide-count--1 swiper-pagination-fraction"><span class="swiper-pagination-current">1</span> / <span class="swiper-pagination-total">3</span></div>
    <div class="article-slider-navigation">
        <button class="article-slider-navigation__btn article-slider-navigation__btn--prev article-slider-navigation__btn--prev-js-1 swiper-button-disabled" tabindex="0" role="button" aria-label="Previous slide" aria-disabled="true">
            <svg class="icon icon-left_arr pdf-booklet-controls__icon">
                <use xlink:href="{{ asset('front/images/sprite/sprite.svg#left_arr') }}"></use>
            </svg>
        </button>
        <button class="article-slider-navigation__btn article-slider-navigation__btn--next article-slider-navigation__btn--next-js-1" tabindex="0" role="button" aria-label="Next slide" aria-disabled="false">
            <svg class="icon icon-right_arr pdf-booklet-controls__icon">
                <use xlink:href="{{ asset('front/images/sprite/sprite.svg#right_arr') }}"></use>
            </svg>
        </button>
    </div>
</div>
<form class="contacts-form" action="{{ route('form.ad') }}" method="post">
    {{ csrf_field() }}
    <div class="contacts-form__row">
        <div class="contacts-form__col contacts-form__col--half">
            <label class="contacts-form__label">{{ translate('forms.company_name') }}</label>
            <input class="contacts-form__input" type="text" name="company_name" placeholder="{{ translate('forms.company_name') }}" required>
        </div>
        <div class="contacts-form__col contacts-form__col--half">
            <label class="contacts-form__label">{{ translate('forms.relevant_person') }}</label>
            <input class="contacts-form__input" type="text" name="relevant_person" placeholder="{{ translate('forms.relevant_person') }}" required>
        </div>
    </div>
    <div class="contacts-form__row">
        <div class="contacts-form__col contacts-form__col--half">
            <label class="contacts-form__label">{{ translate('forms.email') }}</label>
            <input class="contacts-form__input" type="text" name="email" placeholder="{{ translate('forms.email') }}" required>
        </div>
        <div class="contacts-form__col contacts-form__col--half">
            <label class="contacts-form__label">{{ translate('forms.phone') }}</label>
            <input class="contacts-form__input" type="number" name="phone" placeholder="{{ translate('forms.phone') }}" required>
        </div>
    </div>
    <div class="contacts-form__row contacts-form__row--send-btn">
        <div class="contacts-form__col contacts-form__col--half">
            <button class="contacts-form__send-btn primary-btn primary-btn--green">
                <svg class="icon icon-send contacts-form__send-icon">
                    <use xlink:href="{{asset('front/images/sprite/sprite.svg#send')}}"></use>
                </svg>{{ translate('forms.send') }}
            </button>
        </div>
    </div>
</form>
