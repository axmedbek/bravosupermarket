<form class="contacts-form" action="{{ route('form.b2b') }}" method="post">
    {{ csrf_field() }}
    <div class="contacts-form__row">
        <div class="contacts-form__col contacts-form__col--half">
            <label class="contacts-form__label">{{ translate('forms.company_name') }}</label>
            <input class="contacts-form__input" type="text" name="company_name" placeholder="{{ translate('forms.company_name') }}" required>
        </div>
        <div class="contacts-form__col contacts-form__col--half">
            <label class="contacts-form__label">{{ translate('forms.presentation_company') }}</label>
            <input class="contacts-form__input" type="text" name="company_desc" placeholder="{{ translate('forms.presentation_company') }}" required>
        </div>
    </div>
    <div class="contacts-form__row">
        {{--        <div class="contacts-form__col contacts-form__col--half">--}}
        {{--            <label class="contacts-form__label">{{ translate('forms.tax_id') }}</label>--}}
        {{--            <input class="contacts-form__input" type="text" name="contacts-page-tax-id" placeholder="{{ translate('forms.tax_id') }}" required>--}}
        {{--        </div>--}}
        <div class="contacts-form__col contacts-form__col--half">
            <label class="contacts-form__label">{{ translate('forms.contact_person') }} </label>
            <input class="contacts-form__input" type="text" name="relevant_person" placeholder="{{ translate('forms.contact_person') }}" required>
        </div>
        <div class="contacts-form__col contacts-form__col--half">
            <label class="contacts-form__label">{{ translate('forms.address') }}</label>
            <input class="contacts-form__input" type="text" name="address" placeholder="{{ translate('forms.address') }}" required>
        </div>
    </div>
    <div class="contacts-form__row">
        <div class="contacts-form__col contacts-form__col--half">
            <label class="contacts-form__label">{{ translate('forms.district') }}</label>
            <input class="contacts-form__input" type="text" name="district" placeholder="{{ translate('forms.district') }}" required>
        </div>
        <div class="contacts-form__col contacts-form__col--half">
            <label class="contacts-form__label">{{ translate('forms.city') }}</label>
            <input class="contacts-form__input" type="text" name="city" placeholder="{{ translate('forms.city') }}" required>
        </div>
    </div>
    <div class="contacts-form__row">
        <div class="contacts-form__col contacts-form__col--half">
            <label class="contacts-form__label">{{ translate('forms.country') }} </label>
            <input class="contacts-form__input" type="text" name="country" placeholder="{{ translate('forms.country') }}" required>
        </div>
        <div class="contacts-form__col contacts-form__col--half">
            <label class="contacts-form__label">{{ translate('forms.post_code') }}</label>
            <input class="contacts-form__input" type="text" name="zip_code" placeholder="{{ translate('forms.post_code') }}" required>
        </div>
    </div>
    <div class="contacts-form__row">
        <div class="contacts-form__col contacts-form__col--half">
            <label class="contacts-form__label">{{ translate('forms.phone') }} </label>
            <input class="contacts-form__input" type="number" name="phone" placeholder="{{ translate('forms.phone') }}" required>
        </div>
        <div class="contacts-form__col contacts-form__col--half">
            <label class="contacts-form__label">{{ translate('forms.email') }}</label>
            <input class="contacts-form__input" type="email" name="email" placeholder="{{ translate('forms.email') }}" required>
        </div>
    </div>
    <div class="contacts-form__row">
        <div class="contacts-form__col contacts-form__col--half">
            <label class="contacts-form__label">{{ translate('forms.category') }}</label>
            <div class="select-wrap select-wrap--form-page">
                <div class="select-wrap__text select-wrap__text--full" data-select-wrap="fsr-select-1" data-select-focus="false">
                    <input class="select-wrap__input" type="hidden" name="category" value="Choose your info" data-select-input="fsr-select-1">
                    <div class="select-wrap__selected select-wrap__selected--grey" data-selected-item="fsr-select-1" data-selected-focus="false">{{ translate('forms.choose_category') }}</div>
                    <svg class="icon icon-arrow select-wrap__icon-svg select-wrap__icon-svg--arrow">
                        <use xlink:href="{{asset('front/images/sprite/sprite.svg#arrow')}}"></use>
                    </svg>
                </div>
                <div class="select-wrap__option select-wrap__option--form-page" data-select-option="fsr-select-1" data-select-option-opened="false" data-select-option-scroll="true">
                    <span class="select-wrap__option-item select-wrap__option-item--form-page" data-select-option-items="{{ translate('forms.qida') }}" data-select-option-input="fsr-select-1">{{ translate('forms.qida') }}</span>
                    <span class="select-wrap__option-item select-wrap__option-item--form-page" data-select-option-items="{{ translate('forms.qeyri_qida') }}" data-select-option-input="fsr-select-1">{{ translate('forms.qeyri_qida') }}</span>
                </div>
            </div>
        </div>
        <div class="contacts-form__col contacts-form__col--half">
            <label class="contacts-form__label">{{ translate('forms.family') }}</label>
            <div class="select-wrap select-wrap--form-page">
                <div class="select-wrap__text select-wrap__text--full" data-select-wrap="fsr-select-2" data-select-focus="false">
                    <input class="select-wrap__input" type="hidden" name="family" value="Choose category" data-select-input="fsr-select-2">
                    <div class="select-wrap__selected select-wrap__selected--grey" data-selected-item="fsr-select-2" data-selected-focus="false">{{ translate('forms.choose_family') }}</div>
                    <svg class="icon icon-arrow select-wrap__icon-svg select-wrap__icon-svg--arrow">
                        <use xlink:href="{{asset('front/images/sprite/sprite.svg#arrow')}}"></use>
                    </svg>
                </div>
                <div class="select-wrap__option select-wrap__option--form-page" data-select-option="fsr-select-2" data-select-option-opened="false" data-select-option-scroll="true">
                    <span class="select-wrap__option-item select-wrap__option-item--form-page" data-select-option-items="{{ translate('forms.serinledici') }}" data-select-option-input="fsr-select-2">{{ translate('forms.serinledici') }}</span>
                    <span class="select-wrap__option-item select-wrap__option-item--form-page" data-select-option-items="{{ translate('forms.spirtli') }}" data-select-option-input="fsr-select-2">{{ translate('forms.spirtli') }}</span>
                    <span class="select-wrap__option-item select-wrap__option-item--form-page" data-select-option-items="{{ translate('forms.quru_qida') }}" data-select-option-input="fsr-select-2">{{ translate('forms.quru_qida') }}</span>
                    <span class="select-wrap__option-item select-wrap__option-item--form-page" data-select-option-items="{{ translate('forms.sirniyyat') }}" data-select-option-input="fsr-select-2">{{ translate('forms.sirniyyat') }}</span>
                </div>
            </div>
        </div>
    </div>
    {{--    <div class="contacts-form__row">--}}
    {{--        <div class="contacts-form__col contacts-form__col--half">--}}
    {{--            <label class="contacts-form__label">{{ translate('forms.contact_person') }} </label>--}}
    {{--            <input class="contacts-form__input" type="text" name="contacts-page-contact-person" placeholder="{{ translate('forms.contact_person') }}" required>--}}
    {{--        </div>--}}
    {{--        <div class="contacts-form__col contacts-form__col--half">--}}
    {{--            <label class="contacts-form__label">{{ translate('forms.category') }} </label>--}}
    {{--            <input class="contacts-form__input" type="text" name="contacts-page-category" placeholder="{{ translate('forms.category') }}" required>--}}
    {{--        </div>--}}
    {{--    </div>--}}
    {{--    <div class="contacts-form__row">--}}
    {{--        <div class="contacts-form__col contacts-form__col--half">--}}
    {{--            <div class="contact-form-loaded">--}}
    {{--                <input class="contact-form-loaded__input" type="file" name="contacts-page-presentation-file">--}}
    {{--                <button class="contact-form-loaded__close">--}}
    {{--                    <svg class="icon icon-close contact-form-loaded__icon-svg">--}}
    {{--                        <use xlink:href="{{asset('front/images/sprite/sprite.svg#close')}}"></use>--}}
    {{--                    </svg>--}}
    {{--                </button>--}}
    {{--                <div class="contact-form-loaded__inner">--}}
    {{--                    <div class="contact-form-loaded__img">--}}
    {{--                        <img class="contact-form-loaded__icon blazy blazy--style" src="./" data-src="{{asset('front/images/main/icon/pdf_active.svg')}}" alt="bravo market">--}}
    {{--                        <img class="contact-form-loaded__icon blazy blazy--style" src="./" data-src="{{asset('front/images/main/icon/pdf.svg')}}" alt="bravo market">--}}
    {{--                    </div>--}}
    {{--                    <div class="contact-form-loaded__content">--}}
    {{--                        <h4 class="contact-form-loaded__title">{{ translate('forms.product_price_list') }} </h4>--}}
    {{--                        <p class="contact-form-loaded__descr">{{ translate('forms.formats') }} : .pdf .png .jpeg .jpg </p>--}}
    {{--                    </div>--}}
    {{--                </div>--}}
    {{--            </div>--}}
    {{--        </div>--}}
    {{--        <div class="contacts-form__col contacts-form__col--half">--}}
    {{--            <div class="contact-form-loaded">--}}
    {{--                <input class="contact-form-loaded__input" type="file" name="contacts-page-price-file">--}}
    {{--                <button class="contact-form-loaded__close">--}}
    {{--                    <svg class="icon icon-close contact-form-loaded__icon-svg">--}}
    {{--                        <use xlink:href="{{asset('front/images/sprite/sprite.svg#close')}}"></use>--}}
    {{--                    </svg>--}}
    {{--                </button>--}}
    {{--                <div class="contact-form-loaded__inner">--}}
    {{--                    <div class="contact-form-loaded__img">--}}
    {{--                        <img class="contact-form-loaded__icon blazy blazy--style" src="./" data-src="{{asset('front/images/main/icon/pdf_active.svg')}}" alt="bravo market">--}}
    {{--                        <img class="contact-form-loaded__icon blazy blazy--style" src="./" data-src="{{asset('front/images/main/icon/pdf.svg')}}" alt="bravo market">--}}
    {{--                    </div>--}}
    {{--                    <div class="contact-form-loaded__content">--}}
    {{--                        <h4 class="contact-form-loaded__title">{{ translate('forms.product_price_list') }} </h4>--}}
    {{--                        <p class="contact-form-loaded__descr">{{ translate('forms.formats') }} : .pdf .png .jpeg .jpg </p>--}}
    {{--                    </div>--}}
    {{--                </div>--}}
    {{--            </div>--}}
    {{--        </div>--}}
    {{--    </div>--}}
    {{--    <div class="contacts-form__row">--}}
    {{--        <div class="contacts-form__col contacts-form__col--half">--}}
    {{--            <div class="contact-form-loaded">--}}
    {{--                <input class="contact-form-loaded__input" type="file" name="contacts-page-cert-file-1">--}}
    {{--                <button class="contact-form-loaded__close">--}}
    {{--                    <svg class="icon icon-close contact-form-loaded__icon-svg">--}}
    {{--                        <use xlink:href="{{asset('front/images/sprite/sprite.svg#close')}}"></use>--}}
    {{--                    </svg>--}}
    {{--                </button>--}}
    {{--                <div class="contact-form-loaded__inner">--}}
    {{--                    <div class="contact-form-loaded__img">--}}
    {{--                        <img class="contact-form-loaded__icon blazy blazy--style" src="./" data-src="{{asset('front/images/main/icon/pdf_active.svg')}}" alt="bravo market">--}}
    {{--                        <img class="contact-form-loaded__icon blazy blazy--style" src="./" data-src="{{asset('front/images/main/icon/pdf.svg')}}" alt="bravo market">--}}
    {{--                    </div>--}}
    {{--                    <div class="contact-form-loaded__content">--}}
    {{--                        <h4 class="contact-form-loaded__title">{{ translate('forms.company_certificates') }}</h4>--}}
    {{--                        <p class="contact-form-loaded__descr">{{ translate('forms.formats') }}: .pdf .png .jpeg .jpg </p>--}}
    {{--                    </div>--}}
    {{--                </div>--}}
    {{--            </div>--}}
    {{--        </div>--}}
    {{--        <div class="contacts-form__col contacts-form__col--half">--}}
    {{--            <div class="contact-form-loaded">--}}
    {{--                <input class="contact-form-loaded__input" type="file" name="contacts-page-cert-file-2">--}}
    {{--                <button class="contact-form-loaded__close">--}}
    {{--                    <svg class="icon icon-close contact-form-loaded__icon-svg">--}}
    {{--                        <use xlink:href="{{asset('front/images/sprite/sprite.svg#close')}}"></use>--}}
    {{--                    </svg>--}}
    {{--                </button>--}}
    {{--                <div class="contact-form-loaded__inner">--}}
    {{--                    <div class="contact-form-loaded__img">--}}
    {{--                        <img class="contact-form-loaded__icon blazy blazy--style" src="./" data-src="{{asset('front/images/main/icon/pdf_active.svg')}}" alt="bravo market">--}}
    {{--                        <img class="contact-form-loaded__icon blazy blazy--style" src="./" data-src="{{asset('front/images/main/icon/pdf.svg')}}" alt="bravo market">--}}
    {{--                    </div>--}}
    {{--                    <div class="contact-form-loaded__content">--}}
    {{--                        <h4 class="contact-form-loaded__title">{{ translate('forms.company_certificates') }}</h4>--}}
    {{--                        <p class="contact-form-loaded__descr">{{ translate('forms.formats') }}: .pdf .png .jpeg .jpg </p>--}}
    {{--                    </div>--}}
    {{--                </div>--}}
    {{--            </div>--}}
    {{--        </div>--}}
    {{--    </div>--}}
    <div class="contacts-form__row contacts-form__row--send-btn">
        <div class="contacts-form__col contacts-form__col--half">
            <button class="contacts-form__send-btn primary-btn primary-btn--green">
                <svg class="icon icon-send contacts-form__send-icon">
                    <use xlink:href="{{asset('front/images/sprite/sprite.svg#send')}}"></use>
                </svg>{{ translate('forms.send') }}
            </button>
        </div>
    </div>
</form>
