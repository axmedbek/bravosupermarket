<div class="accordion">
    @foreach(\App\Faq::getFaqs() as $parentFaq)
        <div class="accordion__title">
            <h5 class="accordion__descr accordion__descr--top accordion__descr--active">
                <svg class="icon icon-arrow accordion__arrow accordion__arrow--top">
                    <use xlink:href="{{asset('front/images/sprite/sprite.svg#arrow')}}"></use>
                </svg>
                {{ $parentFaq['question'] }}
            </h5>

            <div class="accordion__inner-wrap accordion-visible">
                @if($subfaqs = $parentFaq->subFaqs)
                    @foreach($subfaqs as $subFaq)
                        <div class="accordion__inner">
                            <h5 class="accordion__descr accordion__descr--inner-title">
                                <svg class="icon icon-arrow accordion__arrow ">
                                    <use xlink:href="{{asset('front/images/sprite/sprite.svg#arrow')}}"></use>
                                </svg>
                                {{ $subFaq['question'] }}
                            </h5>
                            <p class="accordion__text accordion__text--inner">{{ $subFaq['answer'] }}</p>
                        </div>
                    @endforeach
                @endif
            </div>
        </div>
    @endforeach
</div>
