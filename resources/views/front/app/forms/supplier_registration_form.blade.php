<form class="contacts-form" action="{{ route('form.supplier') }}" method="post" enctype="multipart/form-data">
    {{ csrf_field() }}
    <div class="contacts-form__row">
        <div class="contacts-form__col contacts-form__col--half">
            <label class="contacts-form__label">{{ translate('forms.company_name') }}</label>
            <input class="contacts-form__input" type="text" name="company_name" placeholder="{{ translate('forms.company_name') }}" required>
        </div>
        <div class="contacts-form__col contacts-form__col--half">
            <label class="contacts-form__label">{{ translate('forms.presentation_company') }}</label>
            <input class="contacts-form__input" type="text" name="company_desc" placeholder="{{ translate('forms.presentation_company') }}" required>
        </div>
    </div>
    <div class="contacts-form__row">
        <div class="contacts-form__col contacts-form__col--half">
            <label class="contacts-form__label">{{ translate('forms.tax_id') }}</label>
            <input class="contacts-form__input" type="text" name="voen" placeholder="{{ translate('forms.tax_id') }}" required>
        </div>
        <div class="contacts-form__col contacts-form__col--half">
            <label class="contacts-form__label">{{ translate('forms.address') }}</label>
            <input class="contacts-form__input" type="text" name="address" placeholder="{{ translate('forms.address') }}" required>
        </div>
    </div>
    <div class="contacts-form__row">
        <div class="contacts-form__col contacts-form__col--half">
            <label class="contacts-form__label">{{ translate('forms.district') }}</label>
            <input class="contacts-form__input" type="text" name="district" placeholder="{{ translate('forms.district') }}" required>
        </div>
        <div class="contacts-form__col contacts-form__col--half">
            <label class="contacts-form__label">{{ translate('forms.city') }}</label>
            <input class="contacts-form__input" type="text" name="city" placeholder="{{ translate('forms.city') }}" required>
        </div>
    </div>
    <div class="contacts-form__row">
        <div class="contacts-form__col contacts-form__col--half">
            <label class="contacts-form__label">{{ translate('forms.country') }} </label>
            <input class="contacts-form__input" type="text" name="country" placeholder="{{ translate('forms.country') }}" required>
        </div>
        <div class="contacts-form__col contacts-form__col--half">
            <label class="contacts-form__label">{{ translate('forms.post_code') }}</label>
            <input class="contacts-form__input" type="text" name="zip_code" placeholder="{{ translate('forms.post_code') }}" required>
        </div>
    </div>
    <div class="contacts-form__row">
        <div class="contacts-form__col contacts-form__col--half">
            <label class="contacts-form__label">{{ translate('forms.phone') }} </label>
            <input class="contacts-form__input" type="number" name="phone" placeholder="{{ translate('forms.phone') }}" required>
        </div>
        <div class="contacts-form__col contacts-form__col--half">
            <label class="contacts-form__label">{{ translate('forms.email') }}</label>
            <input class="contacts-form__input" type="email" name="email" placeholder="{{ translate('forms.email') }}" required>
        </div>
    </div>
    <div class="contacts-form__row">
        <div class="contacts-form__col contacts-form__col--half">
            <label class="contacts-form__label">{{ translate('forms.family') }}</label>
            <div class="select-wrap select-wrap--form-page">
                <div class="select-wrap__text select-wrap__text--full" data-select-wrap="fsr-select-3" data-select-focus="false">
                    <input class="select-wrap__input" type="hidden" name="family" value="Choose your info" data-select-input="fsr-select-3">
                    <div class="select-wrap__selected select-wrap__selected--grey" data-selected-item="fsr-select-3" data-selected-focus="false">{{ translate('forms.choose_family') }}</div>
                    <svg class="icon icon-arrow select-wrap__icon-svg select-wrap__icon-svg--arrow">
                        <use xlink:href="{{asset('front/images/sprite/sprite.svg#arrow')}}"></use>
                    </svg>
                </div>
                <div class="select-wrap__option select-wrap__option--form-page" data-select-option="fsr-select-3" data-select-option-opened="false" data-select-option-scroll="true">
                    <span class="select-wrap__option-item select-wrap__option-item--form-page" data-select-option-items="{{ translate('forms.meat') }}" data-select-option-input="fsr-select-3">{{ translate('forms.meat') }}</span>
                    <span class="select-wrap__option-item select-wrap__option-item--form-page" data-select-option-items="{{ translate('forms.fresh') }}" data-select-option-input="fsr-select-3">{{ translate('forms.fresh') }}</span>
                    <span class="select-wrap__option-item select-wrap__option-item--form-page" data-select-option-items="{{ translate('forms.fruit') }}" data-select-option-input="fsr-select-3">{{ translate('forms.fruit') }}</span>
                    <span class="select-wrap__option-item select-wrap__option-item--form-page" data-select-option-items="{{ translate('forms.bakery') }}" data-select-option-input="fsr-select-3">{{ translate('forms.bakery') }}</span>
                    <span class="select-wrap__option-item select-wrap__option-item--form-page" data-select-option-items="{{ translate('forms.delicates') }}" data-select-option-input="fsr-select-3">{{ translate('forms.delicates') }}</span>
                    <span class="select-wrap__option-item select-wrap__option-item--form-page" data-select-option-items="{{ translate('forms.eggs') }}" data-select-option-input="fsr-select-3">{{ translate('forms.eggs') }}</span>
                    <span class="select-wrap__option-item select-wrap__option-item--form-page" data-select-option-items="{{ translate('forms.frozen') }}" data-select-option-input="fsr-select-3">{{ translate('forms.frozen') }}</span>
                    <span class="select-wrap__option-item select-wrap__option-item--form-page" data-select-option-items="{{ translate('forms.grocery') }}" data-select-option-input="fsr-select-3">{{ translate('forms.grocery') }}</span>
                    <span class="select-wrap__option-item select-wrap__option-item--form-page" data-select-option-items="{{ translate('forms.beer') }}" data-select-option-input="fsr-select-3">{{ translate('forms.beer') }}</span>
                    <span class="select-wrap__option-item select-wrap__option-item--form-page" data-select-option-items="{{ translate('forms.drinks') }}" data-select-option-input="fsr-select-3">{{ translate('forms.drinks') }}</span>
                    <span class="select-wrap__option-item select-wrap__option-item--form-page" data-select-option-items="{{ translate('forms.siqaretler') }}" data-select-option-input="fsr-select-3">{{ translate('forms.siqaretler') }}</span>
                    <span class="select-wrap__option-item select-wrap__option-item--form-page" data-select-option-items="{{ translate('forms.hpc') }}" data-select-option-input="fsr-select-3">{{ translate('forms.hpc') }}</span>
                    <span class="select-wrap__option-item select-wrap__option-item--form-page" data-select-option-items="{{ translate('forms.pet_food') }}" data-select-option-input="fsr-select-3">{{ translate('forms.pet_food') }}</span>
                    <span class="select-wrap__option-item select-wrap__option-item--form-page" data-select-option-items="{{ translate('forms.pastry') }}" data-select-option-input="fsr-select-3">{{ translate('forms.pastry') }}</span>
                    <span class="select-wrap__option-item select-wrap__option-item--form-page" data-select-option-items="{{ translate('forms.confec') }}" data-select-option-input="fsr-select-3">{{ translate('forms.confec') }}</span>
                    <span class="select-wrap__option-item select-wrap__option-item--form-page" data-select-option-items="{{ translate('forms.breakfast') }}" data-select-option-input="fsr-select-3">{{ translate('forms.breakfast') }}</span>
                    <span class="select-wrap__option-item select-wrap__option-item--form-page" data-select-option-items="{{ translate('forms.heavy') }}" data-select-option-input="fsr-select-3">{{ translate('forms.heavy') }}</span>
                    <span class="select-wrap__option-item select-wrap__option-item--form-page" data-select-option-items="{{ translate('forms.light') }}" data-select-option-input="fsr-select-3">{{ translate('forms.light') }}</span>
                    <span class="select-wrap__option-item select-wrap__option-item--form-page" data-select-option-items="{{ translate('forms.textile') }}" data-select-option-input="fsr-select-3">{{ translate('forms.textile') }}</span>
                </div>
            </div>
        </div>
        <div class="contacts-form__col contacts-form__col--half">
            <label class="contacts-form__label">{{ translate('forms.category') }}</label>
            <div class="select-wrap select-wrap--form-page">
                <div class="select-wrap__text select-wrap__text--full" data-select-wrap="fsr-select-4" data-select-focus="false">
                    <input class="select-wrap__input" type="hidden" name="category" value="Choose your info" data-select-input="fsr-select-4">
                    <div class="select-wrap__selected select-wrap__selected--grey" data-selected-item="fsr-select-4" data-selected-focus="false">{{ translate('forms.choose_category') }}</div>
                    <svg class="icon icon-arrow select-wrap__icon-svg select-wrap__icon-svg--arrow">
                        <use xlink:href="{{asset('front/images/sprite/sprite.svg#arrow')}}"></use>
                    </svg>
                </div>
                <div class="select-wrap__option select-wrap__option--form-page" data-select-option="fsr-select-4" data-select-option-opened="false" data-select-option-scroll="true">
                    <span class="select-wrap__option-item select-wrap__option-item--form-page" data-select-option-items="{{ translate('forms.qida') }}" data-select-option-input="fsr-select-4">{{ translate('forms.qida') }}</span>
                    <span class="select-wrap__option-item select-wrap__option-item--form-page" data-select-option-items="{{ translate('forms.qeyri_qida') }}" data-select-option-input="fsr-select-4">{{ translate('forms.qeyri_qida') }}</span>
                </div>
            </div>
        </div>
    </div>
    <div class="contacts-form__row">
        <div class="contacts-form__col contacts-form__col--half">
            <label class="contacts-form__label">{{ translate('forms.contact_person') }} </label>
            <input class="contacts-form__input" type="text" name="relevant_person" placeholder="{{ translate('forms.contact_person') }}" required>
        </div>
{{--        <div class="contacts-form__col contacts-form__col--half">--}}
{{--            <label class="contacts-form__label">{{ translate('forms.category') }} </label>--}}
{{--            <input class="contacts-form__input" type="text" name="contacts-page-category" placeholder="{{ translate('forms.category') }}" required>--}}
{{--        </div>--}}
    </div>
    <div class="contacts-form__row">
        <div class="contacts-form__col contacts-form__col--half">
            <div class="contact-form-loaded">
                <input class="contact-form-loaded__input" type="file" name="presentation_files[]" multiple>
                <button class="contact-form-loaded__close">
                    <svg class="icon icon-close contact-form-loaded__icon-svg">
                        <use xlink:href="{{asset('front/images/sprite/sprite.svg#close')}}"></use>
                    </svg>
                </button>
                <div class="contact-form-loaded__inner">
                    <div class="contact-form-loaded__img">
                        <img class="contact-form-loaded__icon blazy blazy--style" src="./" data-src="{{asset('front/images/main/icon/pdf_active.svg')}}" alt="bravo market">
                        <img class="contact-form-loaded__icon blazy blazy--style" src="./" data-src="{{asset('front/images/main/icon/pdf.svg')}}" alt="bravo market">
                    </div>
                    <div class="contact-form-loaded__content">
                        <h4 class="contact-form-loaded__title">{{ translate('forms.product_presentation') }} </h4>
                        <p class="contact-form-loaded__descr">{{ translate('forms.formats') }} : .pdf .png .jpeg .jpg </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="contacts-form__col contacts-form__col--half">
            <div class="contact-form-loaded">
                <input class="contact-form-loaded__input" type="file" name="price_files[]" multiple>
                <button class="contact-form-loaded__close">
                    <svg class="icon icon-close contact-form-loaded__icon-svg">
                        <use xlink:href="{{asset('front/images/sprite/sprite.svg#close')}}"></use>
                    </svg>
                </button>
                <div class="contact-form-loaded__inner">
                    <div class="contact-form-loaded__img">
                        <img class="contact-form-loaded__icon blazy blazy--style" src="./" data-src="{{asset('front/images/main/icon/pdf_active.svg')}}" alt="bravo market">
                        <img class="contact-form-loaded__icon blazy blazy--style" src="./" data-src="{{asset('front/images/main/icon/pdf.svg')}}" alt="bravo market">
                    </div>
                    <div class="contact-form-loaded__content">
                        <h4 class="contact-form-loaded__title">{{ translate('forms.product_price_list') }} </h4>
                        <p class="contact-form-loaded__descr">{{ translate('forms.formats') }} : .pdf .png .jpeg .jpg </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="contacts-form__row">
        <div class="contacts-form__col contacts-form__col--half">
            <div class="contact-form-loaded">
                <input class="contact-form-loaded__input" type="file" name="company_certificates[]" multiple>
                <button class="contact-form-loaded__close">
                    <svg class="icon icon-close contact-form-loaded__icon-svg">
                        <use xlink:href="{{asset('front/images/sprite/sprite.svg#close')}}"></use>
                    </svg>
                </button>
                <div class="contact-form-loaded__inner">
                    <div class="contact-form-loaded__img">
                        <img class="contact-form-loaded__icon blazy blazy--style" src="./" data-src="{{asset('front/images/main/icon/pdf_active.svg')}}" alt="bravo market">
                        <img class="contact-form-loaded__icon blazy blazy--style" src="./" data-src="{{asset('front/images/main/icon/pdf.svg')}}" alt="bravo market">
                    </div>
                    <div class="contact-form-loaded__content">
                        <h4 class="contact-form-loaded__title">{{ translate('forms.company_certificates') }}</h4>
                        <p class="contact-form-loaded__descr">{{ translate('forms.formats') }}: .pdf .png .jpeg .jpg </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="contacts-form__col contacts-form__col--half">
            <div class="contact-form-loaded">
                <input class="contact-form-loaded__input" type="file" name="product_certificates[]" multiple>
                <button class="contact-form-loaded__close">
                    <svg class="icon icon-close contact-form-loaded__icon-svg">
                        <use xlink:href="{{asset('front/images/sprite/sprite.svg#close')}}"></use>
                    </svg>
                </button>
                <div class="contact-form-loaded__inner">
                    <div class="contact-form-loaded__img">
                        <img class="contact-form-loaded__icon blazy blazy--style" src="./" data-src="{{asset('front/images/main/icon/pdf_active.svg')}}" alt="bravo market">
                        <img class="contact-form-loaded__icon blazy blazy--style" src="./" data-src="{{asset('front/images/main/icon/pdf.svg')}}" alt="bravo market">
                    </div>
                    <div class="contact-form-loaded__content">
                        <h4 class="contact-form-loaded__title">{{ translate('forms.product_certificates') }}</h4>
                        <p class="contact-form-loaded__descr">{{ translate('forms.formats') }}: .pdf .png .jpeg .jpg </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="contacts-form__col contacts-form__col--half">
            <div class="contact-form-loaded">
                <input class="contact-form-loaded__input" type="file" name="storage_images[]" multiple>
                <button class="contact-form-loaded__close">
                    <svg class="icon icon-close contact-form-loaded__icon-svg">
                        <use xlink:href="{{asset('front/images/sprite/sprite.svg#close')}}"></use>
                    </svg>
                </button>
                <div class="contact-form-loaded__inner">
                    <div class="contact-form-loaded__img">
                        <img class="contact-form-loaded__icon blazy blazy--style" src="./" data-src="{{asset('front/images/main/icon/pdf_active.svg')}}" alt="bravo market">
                        <img class="contact-form-loaded__icon blazy blazy--style" src="./" data-src="{{asset('front/images/main/icon/pdf.svg')}}" alt="bravo market">
                    </div>
                    <div class="contact-form-loaded__content">
                        <h4 class="contact-form-loaded__title">{{ translate('forms.storage_images') }}</h4>
                        <p class="contact-form-loaded__descr">{{ translate('forms.formats') }}: .pdf .png .jpeg .jpg .xlxs</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="contacts-form__row contacts-form__row--send-btn">
        <div class="contacts-form__col contacts-form__col--half">
            <button class="contacts-form__send-btn primary-btn primary-btn--green">
                <svg class="icon icon-send contacts-form__send-icon">
                    <use xlink:href="{{asset('front/images/sprite/sprite.svg#send')}}"></use>
                </svg>{{ translate('forms.send') }}
            </button>
        </div>
    </div>
</form>
