@extends('front.app.layout')
@section('main')
    <main class="main">
        <section class="error-page">
            <div class="container">
                <div class="row">
                    <div class="error-page__col">
                        <img class="error-page__img blazy blazy--style" src="./" data-src="{{asset('front/images/main/icon/404.svg')}}" alt="bravo market">
                        <h1 class="preloader preloader--v error-page__title">Something went wrong!</h1>
                        <a class="error-page__btn primary-btn primary-btn--green preloader preloader--v" href="{{route('home')}}">Back to main page </a>
                    </div>
                </div>
            </div>
        </section>
    </main>
@endsection
