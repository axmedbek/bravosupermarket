<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BravoForm extends Model
{
    public function files($type){
        return BravoFormImage::where('type',$type)->where('bravo_form_id',$this->id)->get();
    }
}
