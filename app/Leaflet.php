<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Leaflet extends Model
{


    public function pdf(){
        return $this->belongsTo('\Botble\Media\Models\MediaFile','pdf_media_id','id');
    }

    public function media(){
        return $this->belongsTo('\Botble\Media\Models\MediaFile','media_id','id');
    }

    public function author(){
        return $this->belongsTo('\App\User','author_id','id');
    }
}
