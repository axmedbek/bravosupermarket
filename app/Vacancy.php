<?php

namespace App;

use Botble\Media\Models\MediaFile;
use Illuminate\Database\Eloquent\Model;

class Vacancy extends Model
{

    protected $casts = [
        'media' => 'array'
    ];


    public function author(){
        return $this->belongsTo('\App\User','author_id','id');
    }


    public function applies(){
        return $this->hasMany(Apply::class);
    }

    public static function getMedia($vacancy_id){
        $vacancy = self::where('vacancy_id',$vacancy_id)->first();
        $medias = [];
        if ($vacancy->media){
            foreach ($vacancy->media as $media_id){
                $media = MediaFile::where('id',$media_id)->first();
                if($media) $medias[] = $media;
            }
        }


        return $medias;
    }
}
