<?php

namespace App;

use Botble\Media\Models\MediaFile;
use Illuminate\Database\Eloquent\Model;

class Tab extends Model
{
    protected $table = "tabs";


    public function author(){
        return $this->belongsTo(User::class,'author_id');
    }


    public static function getMedia($tab_id){
        $tab = Tab::where('tab_id',$tab_id)->first();
        $medias = [];
        foreach (json_decode($tab->media) as $media_id){
            $media = MediaFile::where('id',$media_id)->first();
            if($media) $medias[] = $media;
        }
        return $medias;
    }


    public static function getTabs($onlyActives = false)
    {
        $data = [];
        $tabs = Tab::where('parent_id',null)->where('locale',getLocale());
        if($onlyActives){
            $tabs = $tabs->where('status',true);
        }
        $tabs = $tabs->orderBy('created_at','asc')->get();

        foreach ($tabs as $tab){
            if(!Tab::hasSubTab($tab->id)){
                $data[] = $tab;
            }else{
                $subTabs = Tab::getSubTabs($tab->id);
                $tab->subTabs = $subTabs;
                $data[] = $tab;
            }

        }
        return $data;
    }


    public static function hasSubTab($tab_id){
        return Tab::where('parent_id',$tab_id)->count();
    }

    public static function getSubTabs($tab_id)
    {
        $data = [];
        $subTabs = Tab::where('parent_id',$tab_id)->where('locale',getLocale())->orderBy('created_at','asc')->get();
        foreach ($subTabs as $subTab){
            if(!Tab::hasSubTab($subTab->id)){
                $data[] = $subTab;
            }else{
                $subTabs = Tab::getSubTabs($subTab->id);
                $subTab->subTabs = $subTabs;
                $data[] = $subTab;
            }

        }
        return $data;
    }
}
