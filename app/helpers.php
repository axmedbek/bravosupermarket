<?php

use App\Language;
use App\Translation;
use App\TranslationGroup;
use Carbon\Carbon;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Storage;


if (! function_exists('experience_store_management')){
    /**
     *
     * @return string[]
     */
    function experience_store_management(){
        return [
            'have_no_experience',
            'from_0_to_6_months',
            'from_6_months_to_1_year',
            'from_1_to_3_years',
            'more_than_3_years',
        ];
    }
}

if (! function_exists('from_who')){
    function from_who(){
        return [
            'BRAVO_collaborator',
            'from_a_friend_or_family_member_who_is_not_a_BRAVO_employee',
            'facebook',
            'instagram',
            'linkedin',
        ];
    }
}


function checkRouteMenuActive($route){
    $currentRoute = Route::currentRouteName();
    return $currentRoute===$route?'active':'';
}

function checkRouteGroupActive($routeGroup=null,$groupOpened=' nav-item-open'){
    $currentRoute = Route::currentRouteName();
    return in_array($currentRoute,$routeGroup)?'nav-item-expanded'.$groupOpened:'';
}


function user(){
    $user = json_decode(json_encode(auth()->user()));
    $user->info = json_decode($user->info);
    return $user;
}

function getArrayFromChildValue($array,$childKey){
    $array = json_decode(json_encode($array),1);
    $newArray = [];
    foreach ($array as $value){
        $newArray[] = $value[$childKey];
    }
    return $newArray;
}

function getStatus($status){
    return $status?'<div class="badge badge-success">Active</div>':'<div class="badge badge-danger">Disabled</div>';
}

function getLastOrder($table){
    $last = DB::table($table)->latest('order')->first();
    return $last?$last->order+1:1;
}

function alert($message,$type='success'){
    return "
        <div class='alert alert-$type border-0 mb-0 alert-dismissible'>
            <button type='button' class='close' data-dismiss='alert'><span>×</span></button>
            {$message}
        </div>
    ";
}

function yandexUrl(){
    return "https://translate.yandex.net/api/v1.5/tr.json/translate?key=trnsl.1.1.20200303T001304Z.3dc28d2592141203.b5bf9d6f88614ae0e3e867f4fb02957d6797b7ca&format=plain";
}



function getLocale(){
    $language = session('language')??'az';
    return $language;
}

function getLocales(){
    $languages = Language::where('status',true)->get();
    $collect = collect($languages)->map(function($language){
        return $language->key;
    });
    return $collect;
}

function getLanguage(){
    $language = Language::where('key',getLocale())->first();
    return $language;
}

function getLanguages(){
    $languages = Language::where('status',true)->get();
    return $languages;
}

function getLangJson(){
    $locale = getLocale();
    $langFile = public_path("langs/$locale.json");
    $translationData = [];
    if(file_exists($langFile)) $translationData = json_decode(file_get_contents($langFile),1);
    return $translationData;
}

function translate($text){
    $data = explode('.',$text);
    $groupName = $data[0];
    $translationKey = $data[1];
    $translationData = getLangJson();
    @$group = $translationData[$groupName];
    @$translation = $group[$translationKey];
    return $translation??$text;
}

function translateWithAttribute($text,$attributes=null){
    $data = explode('.',$text);
    $groupName = $data[0];
    $translationKey = $data[1];
    $translationData = getLangJson();
    @$group = $translationData[$groupName];
    @$translation = $group[$translationKey];
    if($attributes){
        foreach ($attributes as $attribute => $value){
            $translation = preg_replace("/#{$attribute}/im","$value",$translation);
        }
    }
    return $translation??$text;
}

function jsonEncodePretty($value){
    return json_encode($value,JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES|JSON_PRETTY_PRINT);
}

function getCounts($table){
    $count = DB::table($table)->count();
    return $count;
}

function colorPart() {
    return str_pad( dechex( mt_rand( 0, 255*1000 )/1000 ), 2, '0', STR_PAD_LEFT);
}

function generateHEXColor() {
    return colorPart() . colorPart() . colorPart();
}

function generateColor($existColors = []){
    $colors = ['slate','violet','purple','indigo','blue','teal','green','orange','brown','grey','info','primary','danger','success','warning'];
    foreach ($existColors as $existColor){
        $key = array_search($existColor,$colors);
        if($key) unset($colors[$key]);
    }
    return $colors[array_rand($colors)];
}

function humanDate($date=null,$createFormat='Y-m-d H:i:s',$outputFormat='d.m.Y H:i:s'){
    Carbon::setLocale(getLocale());
    return $date?Carbon::createFromFormat($createFormat,$date)->translatedFormat($outputFormat):null;
}

function dbDate($date=null,$createFormat='d.m.Y H:i:s',$outputFormat='Y-m-d H:i:s'){
    return $date?Carbon::createFromFormat($createFormat,$date)->format($outputFormat):null;
}

function getYoutubeIdFromUrl($url) {
    $regExp = '/^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|&v=)([^#&?]*).*/';
    $match = preg_match($regExp,$url);
    $id = preg_replace($regExp,'$2',$url);
    return ($match && strlen($id) === 11) ? $id : null;
}
function generateYoutubeEmbedUrl($url){
    return "https://www.youtube.com/embed/".getYoutubeIdFromUrl($url)."?enablejsapi=1";
}

function getTemplates(){
    $templates = Storage::disk('views')->allFiles('front/app/templates');
    $templates = collect($templates)->map(function($template){
       $exploded = explode('/',$template);
       return explode('.',$exploded[count($exploded)-1])[0];
    });
    return $templates;
}


function getForms(){
    $templates = Storage::disk('views')->allFiles('front/app/forms');
    $templates = collect($templates)->map(function($template){
        $exploded = explode('/',$template);
        return explode('.',$exploded[count($exploded)-1])[0];
    });
    return $templates;
}

function textBeautify($text){
    $text = preg_replace('/[_-]/im',' ',$text);
    $text = preg_replace('/[^a-zA-Z0-9\s]/im','',$text);
    $text = ucwords($text);
    return $text;
}

function encodeJSON($data){
    $data = removeNulls($data);
    return $data?json_encode($data):null;
}
function removeNulls($array){
    return $array?array_filter($array):null;
}

function getAzMonthName($monthNumber){
    return config('settings.azMonthNames')[$monthNumber];
}

function getParamFromURL($param) {
    $parts = parse_url($_SERVER['REQUEST_URI']);
    if (isset($parts['query'])) {
        parse_str($parts['query'], $query);
        return $query[$param];
    }
    else {
        return null;
    }
}

function checkInListGivenId($id,$data){
    $status = false;
    if (strlen($data) > 0) {
        foreach (json_decode($data) as $item) {
            if ($id == $item) {
                $status = true;
                break;
            }
        }
    }
    return $status;
}


