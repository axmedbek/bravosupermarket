<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Faq extends Model
{
    public static function getFaqsWithType($type,$onlyActives = false){
        $data = [];
        $faqs = Faq::where('parent_id', null)->where('locale', getLocale());
        if ($onlyActives) {
            $faqs = $faqs->where('status', true);
        }
        if ($type) {
            $faqs = $faqs->where('target', 'LIKE', "%$type%");
        }

        $faqs = $faqs->orderBy('order', 'asc')->get();

        foreach ($faqs as $faq) {
            if (!Faq::hasSubFaqs($faq->faq_id)) {
                $data[] = $faq;
            } else {
                $subFaqs = Faq::getSubFaqs($faq->faq_id);
                $faq->subFaqs = $subFaqs;
                $data[] = $faq;
            }

        }
        return $data;
    }


    public static function getFaqs($onlyActives = false)
    {
        $data = [];
        $faqs = Faq::where('parent_id', null)->where('locale', getLocale());
        if ($onlyActives) {
            $faqs = $faqs->where('status', true);
        }
        $faqs = $faqs->orderBy('order', 'asc')->get();

        foreach ($faqs as $faq) {
            if (!Faq::hasSubFaqs($faq->faq_id)) {
                $data[] = $faq;
            } else {
                $subFaqs = Faq::getSubFaqs($faq->faq_id);
                $faq->subFaqs = $subFaqs;
                $data[] = $faq;
            }

        }
        return $data;
    }

    public static function hasSubFaqs($faq_id)
    {
        return Faq::where('parent_id', $faq_id)->count();
    }

    public static function getSubFaqs($faq_id)
    {
        $data = [];
        $subFaqs = Faq::where('parent_id', $faq_id)->where('locale', getLocale())->orderBy('order', 'asc')->get();
        foreach ($subFaqs as $subFaq) {
            if (!Faq::hasSubFaqs($subFaq->faq_id)) {
                $data[] = $subFaq;
            } else {
                $subFaqs = Faq::getSubFaqs($subFaq->faq_id);
                $subFaq->subFaqs = $subFaqs;
                $data[] = $subFaq;
            }

        }
        return $data;
    }
}
