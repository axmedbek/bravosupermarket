<?php

namespace App\Http\Controllers;

use App\Banner;
use Illuminate\Http\Request;

class BannerController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:create-banners', ['only' => ['create', 'store']]);
        $this->middleware('permission:read-banners'  , ['only' => ['index']]);
        $this->middleware('permission:update-banners', ['only' => ['edit','update']]);
        $this->middleware('permission:delete-banners', ['only' => ['destroy','destroySelecteds']]);

    }

    public function index()
    {
        $banners = Banner::orderBy('id')->where('locale', getLocale())->get();
        return view('admin.banners.index',compact('banners'));
    }


    public function create()
    {
        return view('admin.banners.create');
    }


    public function store(Request $request)
    {
        $request->validate([
            'title' => 'array|nullable',
            'text' => 'array|nullable',
            'url' => 'array|nullable',
        ]);

        $banner_id = uniqid();
        foreach (getLocales() as $locale) {
            if ($request->title[$locale] || $request->text[$locale]) {
                $banner = new Banner();
                $banner->title = $request->title[$locale]??'';
                $banner->text = $request->text[$locale]??'';
                $banner->url = $request->url[$locale]??'';
                $banner->banner_id = $banner_id;
                $banner->locale = $locale;
                $banner->target = $request->target;
                $banner->style = $request->style;
                $banner->media_id = $request->media[0];
                $banner->author_id = user()->id;
                $banner->order = $request->order;
                $banner->status = $request->status ? true : false;
                $banner->save();

            }
        }

        $data = [
            'status'    => 'success',
            'message'   => "Banner <code>#{$banner_id}</code>|banner is created successfully"
        ];
        return response()->json($data,200);
    }


    public function show()
    {
        //
    }


    public function edit(Banner $banner)
    {

        $bannerWithLocales = Banner::where('banner_id',$banner->banner_id)->get();

        foreach ($bannerWithLocales as $bannerWithLocale) {
            $banner[$bannerWithLocale['locale']] = $bannerWithLocale;
        }

        return view('admin.banners.edit')->with(compact('banner'));
    }


    public function update(Request $request,Banner $banner)
    {
        $banner_id  = $banner->banner_id;

        $request->validate([
            'title' => 'array|nullable',
            'text' => 'array|nullable',
            'url' => 'array|nullable',
        ]);

        foreach (getLocales() as $locale) {
            if ($request->title[$locale] || $request->text[$locale]) {
                $banner = Banner::where('banner_id',$banner_id)->where('locale',$locale)->first();
                if(!$banner){ // eger bu dilde banner yoxdursa yenisini yaradir
                    $banner = new Banner();
                    $banner->banner_id = $banner_id;
                    $banner->locale = $locale;
                    $banner->author_id = user()->id;
                }
                $banner->title = $request->title[$locale]??'';
                $banner->text = $request->text[$locale]??'';
                $banner->url = $request->url[$locale]??'';
                $banner->target = $request->target;
                $banner->style = $request->style;
                $banner->media_id = $request->media ? $request->media[0] : null;
                $banner->order = $request->order;
                $banner->status = $request->status ? true : false;
                $banner->save();
            }
        }

        $data = [
            'status'    => 'success',
            'message'   => "Banner <code>#{$banner->id}</code> is updated successfully"
        ];
        return response()->json($data,200);
    }


    public function destroy(Banner $banner)
    {
        $banner_id = $banner->banner_id;
        Banner::where('banner_id',$banner_id)->delete();

        $data = [
            'status'    => 'success',
            'message'   => "Post - <span class='font-weight-semibold'>#{$banner->id}</span> is deleted successfully!"
        ];
        return response()->json($data,200);
    }

    public function destroySelecteds(Request $request)
    {
        foreach ($request->selecteds as $selected) {
            $deleteData = Banner::findOrFail($selected);

            if ($deleteData) {
                Banner::where('banner_id',$deleteData->banner_id)->delete();
            }
        }

        $data = [
            'status'    => 'success',
            'message'   => "Selected <span class='font-weight-semibold'>banners</span> are deleted successfully!"
        ];
        return response()->json($data,200);
    }
}
