<?php

namespace App\Http\Controllers;

use App\Apply;
use App\Banner;
use App\Post;
use App\Vacancy;
use Illuminate\Http\Request;

class VacancyController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:create-vacancies', ['only' => ['create', 'store']]);
        $this->middleware('permission:read-vacancies'  , ['only' => ['index']]);
        $this->middleware('permission:update-vacancies', ['only' => ['edit','update']]);
        $this->middleware('permission:delete-vacancies', ['only' => ['destroy']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $vacancies = Vacancy::with('author')
            ->where('locale',getLocale())
            ->orderBy('created_at','desc')
            ->get();

        return view('admin.vacancies.index',compact('vacancies'));
    }


    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {

        return view('admin.vacancies.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required|array|check_array:1',
            'title.*' => 'string|nullable',
            'content' => 'required|array|check_array:1',
            'content.*' => 'string|nullable',
            'slug' => 'required|array|check_array:1|unique:posts',
            'slug.*' => 'string|nullable',
            'date' => 'required|date_format:d.m.Y H:i:s',
        ]);

        $vacancy_id = uniqid();

        foreach (getLocales() as $locale){
            $v = new Vacancy();
            $v->vacancy_id = $vacancy_id;
            $v->locale = $locale;
            $v->author_id = user()->id;
            $v->title = $request->title[$locale];
            $v->salary = $request->salary[$locale];
            $v->slug = $request->slug[$locale];
            $v->content = $request->{'content'}[$locale];
            $v->media = $request->media;
            $v->date = dbDate($request->date);
            $v->save();
        }

        $data = [
            'status'    => 'success',
            'message'   => "Vacancy <code>{$request->title[getLocale()]}</code> is created successfully"
        ];
        return response()->json($data,200);
    }

    /**
     * Display the specified resource.
     *
     * @param  Vacancy $vacancy
     * @return \Illuminate\Http\Response
     */
    public function show(Vacancy $vacancy)
    {

        $query = $vacancy->applies();
        $query->when(request('sart_at'),function (){

        });

        $query->when(request('end_at'),function (){

        });
        $applies = $query->get();

        return view('admin.vacancies.show',compact('applies','vacancy'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Vacancy $vacancy
     * @return \Illuminate\Http\Response
     */
    public function edit($vacancy_id)
    {
        $post_data = Vacancy::where('vacancy_id',$vacancy_id)->get();
        $vacancies = [];
        foreach ($post_data as $data){
            $vacancies[$data->locale] = $data;
        }
        return view('admin.vacancies.edit',compact('vacancies'));
    }


    public function update(Request $request, $vacancy_id)
    {


        $vacancies = Vacancy::where('vacancy_id',$vacancy_id)->get();
        $slug_validaiton = '';
        foreach ($vacancies as $vacancy) {
            $slug_validaiton .= '|unique:vacancies,slug,'.$vacancy->id;
        }
        $request->validate([
            'title' => 'required|array|check_array:1',
            'title.*' => 'string|nullable',
            'content' => 'required|array|check_array:1',
            'content.*' => 'string|nullable',
            'slug' => 'required|array|check_array:1'.$slug_validaiton,
            'slug.*' => 'string|nullable',
            'date' => 'required|date_format:d.m.Y H:i:s',
        ]);

        foreach (getLocales() as $locale){
            $v = Vacancy::where('vacancy_id',$vacancy_id)->where('locale',$locale)->first();
            if(! $v){ // eger bu dilde post yoxdursa yenisini yaradir
                $v = new Vacancy();
                $v->vacancy_id = $vacancy_id;
                $v->locale = $locale;
                $v->author_id = user()->id;
            }
            $v->title = $request->title[$locale];
            $v->slug = $request->slug[$locale];
            $v->content = $request->{'content'}[$locale];
            $v->salary = $request->{'salary'}[$locale];
            $v->media = $request->media;
            $v->date = dbDate($request->date);
            $v->save();
        }

        $data = [
            'status'    => 'success',
            'message'   => "Vacancy <code>{$request->title[getLocale()]}</code> is updated successfully"
        ];
        return response()->json($data,200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Vacancy $vacancy
     * @return \Illuminate\Http\Response
     */
    public function destroy(Vacancy $vacancy)
    {
        $vacancy->delete();

        $data = [
            'status'    => 'success',
            'message'   => "Vacancy - <span class='font-weight-semibold'>#{$vacancy->id}</span> is deleted successfully!"
        ];
        return response()->json($data,200);
    }

    public function destroySelecteds(Request $request)
    {
        foreach ($request->selecteds as $selected) {
            $deleteData = Vacancy::findOrFail($selected);
            $deleteData->delete();
        }

        $data = [
            'status'    => 'success',
            'message'   => "Selected <span class='font-weight-semibold'>vacancies</span> are deleted successfully!"
        ];
        return response()->json($data,200);
    }

    public function export($id){

        $datas = Apply::where('vacancy_id', $id)->get()->toArray();

        //dump($datas);
        $columns = [
            'ID',
            translate('table.name'),
            translate('table.phone'),
            translate('table.experience_store_management'),
            translate('table.working_now'),
            translate('table.description'),
            translate('table.date'),
        ];
        $data = [];


        foreach ($datas as $da){
            $data[] = [
                $da['id'],
                $da['name'],
                $da['phone'],
                translate('career.'.$da['experience_store_management']),
                translate('career.'.$da['working_now']),
                $da['description'],
                $da['created_at'],
            ];
        }

        $replaceDotCol=array(5);


        $this->exportExcel('Contact-ExportExcel', $columns, $data,$replaceDotCol);
    }

    function exportExcel($filename='ExportExcel',$columns=array(),$data=array(),$replaceDotCol=array()){
        header('Content-Encoding: UTF-8');
        header('Content-Type: text/plain; charset=utf-8');
        header("Content-disposition: attachment; filename=".$filename.".xls");
        echo "\xEF\xBB\xBF"; // UTF-8 BOM

        $say=count($columns);

        echo '<table border="1"><tr>';
        foreach($columns as $v){
            echo '<th style="background-color:#FFA500">'.trim($v).'</th>';
        }
        echo '</tr>';

        foreach($data as $val){
            echo '<tr>';
            for($i=0; $i < $say; $i++){

                if(in_array($i,$replaceDotCol)){
                    echo '<td>'.str_replace('.',',',$val[$i]).'</td>';
                }else{
                    echo '<td>'.$val[$i].'</td>';
                }
            }
            echo '</tr>';
        }
    }

}
