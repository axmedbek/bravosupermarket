<?php

namespace App\Http\Controllers;

use App\Menu;
use App\Page;
use Illuminate\Http\Request;

class MenuController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:create-menus', ['only' => ['create', 'store']]);
        $this->middleware('permission:read-menus'  , ['only' => ['index']]);
        $this->middleware('permission:update-menus', ['only' => ['edit','update']]);
        $this->middleware('permission:delete-menus', ['only' => ['destroy','destroySelecteds']]);
    }

    public function index()
    {
        $menus = Menu::getMenus();
        return view('admin.menus.index')->with(compact('menus'));
    }


    public function create()
    {
        $menus = Menu::getMenus();
        $pages = Page::where('status',true)->where('locale',getLocale())->get();
        return view('admin.menus.create')->with(compact('menus','pages'));
    }


    public function store(Request $request)
    {
        $request->validate([
            'parent_id' => 'required|string|nullable',
            'name' => 'required|array|check_array:1|unique:menus',
            'description' => 'array|nullable',
            'target' => 'required|array',
            'target.*' => 'required|string',
            'url' => 'string|nullable',
            'order' => 'required|integer'
        ]);

        $menu_id = uniqid();
        foreach (getLocales() as $locale) {
            if ($request->name[$locale]) {
                $menu = new Menu();
                $menu->menu_id = $menu_id;
                $menu->parent_id = $request->parent_id==='null'?null:$request->parent_id;
                $menu->locale = $locale;
                $menu->name = $request->name[$locale]??'';
                $menu->description = $request->description[$locale]??'';

                $menu->target = json_encode($request->target);
                $menu->url = $request->url;
                $menu->order = $request->order;
                $menu->status = $request->status?true:false;
                $menu->author_id = user()->id;
                $menu->save();
            }
        }

        $data = [
            'status'    => 'success',
            'message'   => "Menu - <code>{$request->name[getLocale()]}</code> is created successfully!"
        ];
        return response()->json($data,200);
    }


    public function show($id)
    {
        //
    }


    public function edit($menu_id)
    {
        $menus = Menu::getMenus();
        $menu = Menu::where('menu_id',$menu_id)->where('locale',getLocale())->first();
        $pages = Page::where('status',true)->get();
        return view('admin.menus.edit')->with(compact('menu','menus','pages'));
    }


    public function update(Request $request, Menu $menu)
    {
        $menu_id  = $menu->menu_id;

        $request->validate([
            'parent_id' => 'required|string|nullable',
            'name' => 'required|array|check_array:1|unique:menus,id,'.$menu->id,
            'description' => 'array|nullable',
            'target' => 'required|array',
            'target.*' => 'required|string',
            'url' => 'string|nullable',
            'order' => 'required|integer'
        ]);

        foreach (getLocales() as $locale) {
            if ($request->name[$locale]) {
                $menu = Menu::where('menu_id',$menu_id)->where('locale',$locale)->first();
                if(!$menu){ // eger bu dilde menu yoxdursa yenisini yaradir
                    $menu = new Menu();
                    $menu->menu_id = $menu_id;
                    $menu->locale = $locale;
                    $menu->author_id = user()->id;
                }

                $oldData = $menu->name;
                $menu->parent_id = $request->parent_id==='null'?null:$request->parent_id;
                $menu->name = $request->name[$locale]??'';
                $menu->description = $request->description[$locale]??'';
                $menu->order = $request->order;
                $menu->status = $request->status?true:false;
                $menu->target = json_encode($request->target);
                $menu->url = $request->url;
                $menu->save();
            }
        }


        $data = [
            'status'    => 'success',
            'message'   => "Menu - <span class='font-weight-semibold'>{$oldData}</span> is updated successfully!"
        ];
        return response()->json($data,200);
    }


    public function destroy(Menu $menu)
    {
        $oldData = $menu->name;
        $menu->delete();

        $data = [
            'status'    => 'success',
            'message'   => "Menu - <span class='font-weight-semibold'>{$oldData}</span> is deleted successfully!"
        ];
        return response()->json($data,200);
    }

    public function destroySelecteds(Request $request)
    {
        foreach ($request->selecteds as $selected) {
            if($deleteData = Menu::find($selected)){
                $deleteData->delete();
            }
        }

        $data = [
            'status'    => 'success',
            'message'   => "Selected <span class='font-weight-semibold'>menus</span> are deleted successfully!"
        ];
        return response()->json($data,200);
    }

}
