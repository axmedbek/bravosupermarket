<?php

namespace App\Http\Controllers;

use App\Student;
use Illuminate\Http\Request;

class StudentController extends Controller
{
    public function index(){
        $students = Student::where('locale',getLocale())->get();
        return view('admin.students.index',compact('students'));
    }


    public function create()
    {
        return view('admin.students.create');
    }


    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|array|check_array:1',
            'name.*' => 'string|nullable',
            'title' => 'required|array|check_array:1',
            'title.*' => 'string|nullable',
            'content' => 'required|array',
            'content.*' => 'string|nullable',
            'group' => 'required|string',
            'media' => 'required|array',
            'media.*' => 'integer|nullable',
            'date' => 'required|date_format:d.m.Y H:i:s',
        ]);


        $student_id = uniqid();
        foreach (getLocales() as $locale) {
            if ($request->name[$locale]) {
                $student = new Student();
                $student->student_id = $student_id;
                $student->locale = $locale;
                $student->group = $request->group;
                $student->author_id = user()->id;
                $student->title = $request->title[$locale] ?? '';
                $student->name = $request->name[$locale] ?? '';
                $student->content = $request->{'content'}[$locale] ?? '';
                $student->media = json_encode($request->media);
                $student->date = dbDate($request->date);
                $student->order = $request->order;
                $student->save();
            }
        }


        $data = [
            'status' => 'success',
            'message' => "Student <code>{$request->name[getLocale()]}</code> is created successfully"
        ];
        return response()->json($data, 200);
    }


    public function show(Student $student)
    {
        //
    }


    public function edit($student_id)
    {
        $page_data = Student::where('student_id', $student_id)->get();
        $students = [];
        foreach ($page_data as $data) {
            $students[$data->locale] = $data;
        }

        return view('admin.students.edit',compact('students'));
    }


    public function update(Request $request, $student_id)
    {
        $request->validate([
            'name' => 'required|array|check_array:1',
            'name.*' => 'string|nullable',
            'title' => 'required|array|check_array:1',
            'title.*' => 'string|nullable',
            'content' => 'required|array',
            'content.*' => 'string|nullable',
            'group' => 'required|string',
            'media' => 'required|array',
            'media.*' => 'integer|nullable',
            'date' => 'required|date_format:d.m.Y H:i:s',
        ]);


        foreach (getLocales() as $locale) {
            if ($request->name[$locale]) {
                $student = Student::where('student_id', $student_id)->where('locale', $locale)->first();
                if (!$student) { // eger bu dilde student yoxdursa yenisini yaradir
                    $student = new Student();
                    $student->student_id = $student_id;
                    $student->locale = $locale;
                    $student->author_id = user()->id;
                }
                $student->student_id = $student_id;
                $student->locale = $locale;
                $student->group = $request->group;
                $student->author_id = user()->id;
                $student->title = $request->title[$locale] ?? '';
                $student->name = $request->name[$locale] ?? '';
                $student->content = $request->{'content'}[$locale] ?? '';
                $student->media = json_encode($request->media);
                $student->date = dbDate($request->date);
                $student->order = $request->order;
                $student->save();
            }
        }


        $data = [
            'status' => 'success',
            'message' => "Student <code>{$request->name[getLocale()]}</code> is updated successfully"
        ];
        return response()->json($data, 200);
    }


    public function destroy(Student $student)
    {
        $oldData = $student->name;

        $students  = Student::where('student_id',$student->student_id)->get();

        if (count($students) > 0) {
            Student::where('student_id',$student->student_id)->delete();
        }

        $data = [
            'status' => 'success',
            'message' => "Student - <span class='font-weight-semibold'>{$oldData}</span> is deleted successfully!"
        ];
        return response()->json($data, 200);
    }

    public function destroySelecteds(Request $request)
    {
        foreach ($request->selecteds as $selected) {
            Student::where('student_id', $selected)->delete();
        }

        $data = [
            'status' => 'success',
            'message' => "Selected <span class='font-weight-semibold'>students</span> are deleted successfully!"
        ];
        return response()->json($data, 200);
    }
}
