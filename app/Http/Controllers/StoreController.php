<?php

namespace App\Http\Controllers;

use App\Store;
use Illuminate\Http\Request;

class StoreController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:create-stores', ['only' => ['create', 'store']]);
        $this->middleware('permission:read-stores'  , ['only' => ['index']]);
        $this->middleware('permission:update-stores', ['only' => ['edit','update']]);
        $this->middleware('permission:delete-stores', ['only' => ['destroy','destroySelecteds']]);
    }

    public function index()
    {
        $stores = Store::where('locale',getLocale())->get();
        return view('admin.stores.index',compact('stores'));
    }


    public function create()
    {
        return view('admin.stores.create');
    }


    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|array|check_array:1|unique:stores,name',
            'address' => 'array|nullable',
            'code' => 'nullable|string',
            'format' => 'nullable|string',
            'phone' => 'nullable|string',
            'work_times' => 'nullable|string',
            'longitude' => 'required|string',
            'latitude' => 'required|string',
            'order' => 'required|integer'
        ]);

        $store_id = uniqid();
        foreach (getLocales() as $locale) {
            if ($request->name[$locale]) {
                $store = new Store();
                $store->store_id = $store_id;
                $store->locale = $locale;
                $store->name = $request->name[$locale]??'';
                $store->address = $request->address[$locale]??'';

                $store->code = $request->code;
                $store->format = $request->format;
                $store->phone = $request->phone;
                $store->work_times = $request->work_times;

                $store->latitude = $request->latitude;
                $store->longitude = $request->longitude;

                $store->order = $request->order;
                $store->status = $request->status?true:false;
                $store->author_id = user()->id;
                $store->save();
            }
        }

        $data = [
            'status'    => 'success',
            'message'   => "Store - <code>{$request->name[getLocale()]}</code> is created successfully!"
        ];
        return response()->json($data,200);
    }


    public function show($id)
    {
        //
    }


    public function edit(Store $store)
    {
        $stores = Store::where('store_id',$store->store_id)->get();
        foreach ($stores as $storeItem) {
            $store[$storeItem['locale']] = $storeItem;
        }
//        $menus = Menu::getMenus();
//        $menu = Menu::where('store_id',$store_id)->where('locale',getLocale())->first();
//        $pages = Page::where('status',true)->get();
        return view('admin.stores.edit')->with(compact('store'));
    }


    public function update(Request $request, Store $store)
    {
        $store_id  = $store->store_id;

        $request->validate([
            'name' => 'required|array|check_array:1|unique:stores,id,'.$store->id,
            'address' => 'array|nullable',
            'code' => 'nullable|string',
            'phone' => 'nullable|string',
            'work_times' => 'nullable|string',
            'format' => 'nullable|string',
            'longitude' => 'required|string',
            'latitude' => 'required|string',
            'order' => 'required|integer'
        ]);

        foreach (getLocales() as $locale) {
            if ($request->name[$locale]) {
                $store = Store::where('store_id',$store_id)->where('locale',$locale)->first();
                if(!$store){ // eger bu dilde menu yoxdursa yenisini yaradir
                    $store = new Store();
                    $store->store_id = $store_id;
                    $store->locale = $locale;
                    $store->author_id = user()->id;
                }

                $oldData = $store->name;
                $store->name = $request->name[$locale]??'';
                $store->address = $request->address[$locale]??'';
                $store->order = $request->order;
                $store->code = $request->code;
                $store->phone = $request->phone;
                $store->work_times = $request->work_times;
                $store->format = $request->format;
                $store->status = $request->status?true:false;

                $store->latitude = $request->latitude;
                $store->longitude = $request->longitude;
                $store->save();
            }
        }


        $data = [
            'status'    => 'success',
            'message'   => "Store - <span class='font-weight-semibold'>{$oldData}</span> is updated successfully!"
        ];
        return response()->json($data,200);
    }


    public function destroy(Store $store)
    {
        $oldData = $store->name;

        Store::where('store_id',$store->store_id)->delete();

        $data = [
            'status'    => 'success',
            'message'   => "Store - <span class='font-weight-semibold'>{$oldData}</span> is deleted successfully!"
        ];
        return response()->json($data,200);
    }

    public function destroySelecteds(Request $request)
    {
        foreach ($request->selecteds as $selected) {
            if($deleteData = Store::find($selected)){
                $deleteData->delete();
            }
        }

        $data = [
            'status'    => 'success',
            'message'   => "Selected <span class='font-weight-semibold'>stores</span> are deleted successfully!"
        ];
        return response()->json($data,200);
    }
}
