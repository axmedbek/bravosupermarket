<?php

namespace App\Http\Controllers;

use App\Offer;
use App\OfferProduct;
use Illuminate\Http\Request;

class OfferProductController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:create-offer-products', ['only' => ['create', 'store']]);
        $this->middleware('permission:read-offer-products'  , ['only' => ['index']]);
        $this->middleware('permission:update-offer-products', ['only' => ['edit','update']]);
        $this->middleware('permission:delete-offer-products', ['only' => ['destroy','destroySelecteds']]);

    }

    public function index()
    {
        $offerProducts = OfferProduct::where('locale',getLocale())->orderBy('order')->get();
        return view('admin.offer_products.index',compact('offerProducts'));

    }


    public function create()
    {
        return view('admin.offer_products.create');
    }


    public function store(Request $request)
    {
        $request->validate([
            'title' => 'array|check_array:1',
            'title.*' => 'string|nullable',
            'text' => 'array|check_array:1',
            'text.*' => 'string|nullable',
            'media' => 'required',
        ]);

        $offer_product_id = uniqid();
        foreach (getLocales() as $locale){
            $order = getLastOrder('offer_products');
            if($request->title[$locale]){
                $offerProduct = new OfferProduct();
                $offerProduct->offer_product_id = $offer_product_id;
                $offerProduct->locale = $locale;
                $offerProduct->title = $request->title[$locale];
                $offerProduct->text = $request->text[$locale];
                $offerProduct->media_id = $request->media[0];
                $offerProduct->order = $order;
                $offerProduct->status = $request->status?true:false;
                $offerProduct->author_id = user()->id;
                $offerProduct->date = dbDate($request->date,'d.m.Y','Y-m-d');
                $offerProduct->save();
            }
        }


        $data = [
            'status'    => 'success',
            'message'   => "Offer Product<code>{$request->title[getLocale()]}</code> is created successfully"
        ];
        return response()->json($data,200);
    }


    public function show()
    {
        //
    }


    public function edit($offer_product_id)
    {
        $offer_products_data = OfferProduct::where('offer_product_id',$offer_product_id)->get();
        $offerProducts = [];
        foreach ($offer_products_data as $offer_product){
            $offerProducts[$offer_product->locale] = $offer_product;
        }
        $offers = Offer::where('status',true)->where('locale',getLocale())->get();
        return view('admin.offer_products.edit')->with(compact('offers','offerProducts'));
    }


    public function update(Request $request,$offer_product_id)
    {
        $request->validate([
            'title' => 'array|check_array:1',
            'title.*' => 'string|nullable',
            'text' => 'array|check_array:1',
            'text.*' => 'string|nullable',
            'media' => 'required',
        ]);


        foreach (getLocales() as $locale){
            if($request->title[$locale]){
                $offerProduct = OfferProduct::where('offer_product_id',$offer_product_id)->where('locale',$locale)->first();
                if(!$offerProduct){ // eger bu dilde offer yoxdursa yenisini yaradir
                    $offerProduct = new OfferProduct();
                    $offerProduct->offer_product_id = $offer_product_id;
                    $offerProduct->author_id = user()->id;
                }
                $offerProduct->locale = $locale;
                $offerProduct->title = $request->title[$locale];
                $offerProduct->text = $request->text[$locale];
                $offerProduct->media_id = $request->media[0];
                $offerProduct->status = $request->status?true:false;
                $offerProduct->date = dbDate($request->date,'d.m.Y','Y-m-d');
                $offerProduct->save();
            }
        }


        $data = [
            'status'    => 'success',
            'message'   => "Offer Product <code>{$request->title[getLocale()]}</code> is updated successfully"
        ];
        return response()->json($data,200);
    }


    public function destroy($offer_product_id)
    {
        $offerProductData = OfferProduct::where('offer_product_id',$offer_product_id)->where('locale',getLocale())->first();
        $offerProducts = OfferProduct::where('offer_product_id',$offer_product_id)->get();
        foreach ($offerProducts as $offerProduct){
            $offerProduct->delete();
        }

        $data = [
            'status'    => 'success',
            'message'   => "OfferProduct - <span class='font-weight-semibold'>{$offerProductData->title}</span> is deleted successfully!"
        ];
        return response()->json($data,200);
    }

    public function destroySelecteds(Request $request)
    {
        foreach ($request->selecteds as $selected) {
            $deleteDatas = OfferProduct::where('offer_product_id',$selected)->get();
            foreach ($deleteDatas as $deleteData){
                $deleteData->delete();
            }
        }

        $data = [
            'status'    => 'success',
            'message'   => "Selected <span class='font-weight-semibold'>Offer Products</span> are deleted successfully!"
        ];
        return response()->json($data,200);
    }
}
