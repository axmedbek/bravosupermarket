<?php

namespace App\Http\Controllers;

use App\Apply;
use App\Vacancy;
use Illuminate\Http\Request;

class CareerController extends Controller
{
    public function index(){

        $vacancies = Vacancy::where('locale', getLocale())->where('status', 1)
            ->get();
        return view('front.app.templates.staff',compact('vacancies'));
    }

    public function applyGet(){
        $vacancies = Vacancy::where('locale', getLocale())->where('status', 1)
            ->get();
        return view('front.app.templates.apply',compact('vacancies'));
    }

    public function applyStore(Request $request){

        $request->validate([
            'vacancy_id' => 'required|numeric',
            'name' => 'required|string|max:191',
            'phone' => 'required|string|max:15',
            'experience_store_management' => 'required|string',
            'working_now' => 'required|string',
            'description' => 'required|string',
            'from_who' => 'required|string',
            'file_first' => 'required|mimes:jpeg,jpg,pdf,docx,doc,png',
            'file_second' => 'mimes:jpeg,jpg,pdf,docx,doc,png'
        ]);

        $a = new Apply;
        $a->vacancy_id = $request->vacancy_id;
        $a->name = $request->name;
        $a->phone = $request->phone;
        $a->description = $request->description;
        $a->experience_store_management = $request->experience_store_management;
        $a->working_now = $request->working_now;
        $a->from_who = $request->from_who;
        $a->save();

        if ($request->file('file_first')){
            $file_first = $request->file('file_first')->store('cv',['disk' => 'uploads']);
            $a->file_first = $file_first;
            $a->save();
        }

        if ($request->file('file_second')){
            $file_second = $request->file('file_second')->store('cv',['disk' => 'uploads']);
            $a->file_second = $file_second;
            $a->save();
        }

        return back()->with(['message' => 'Müraciyətiniz qeydə alındı. Sizinlə tezliklə əlaqə saxlaycağıq.']);
    }
}
