<?php

namespace App\Http\Controllers;

use App\Category;
use App\Language;
use App\Card;
use App\Post;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class CardController extends Controller
{

    public function __construct()
    {
        $this->middleware('permission:create-cards', ['only' => ['create', 'store']]);
        $this->middleware('permission:read-cards'  , ['only' => ['index']]);
        $this->middleware('permission:update-cards', ['only' => ['edit','update']]);
        $this->middleware('permission:delete-cards', ['only' => ['destroy','destroySelecteds']]);
    }

    public function index()
    {
        $cards = Card::with('author')
            ->where('locale',getLocale())
            ->orderBy('created_at','desc')
            ->get();

        return view('admin.cards.index',compact('cards'));

    }


    public function create()
    {
        return view('admin.cards.create');
    }


    public function store(Request $request)
    {

        $request->validate([
            'title' => 'required|array|check_array:1',
            'title.*' => 'string|nullable',
            'slug' => 'required|array|check_array:1|unique:posts',
            'slug.*' => 'string|nullable',
            'content' => 'required|array|check_array:1',
            'content.*' => 'string|nullable',
        ]);


        $card_id = uniqid();
        foreach (getLocales() as $locale){
            $card = new Card();
            $card->card_id = $card_id;
            $card->locale = $locale;
            $card->author_id = user()->id;
            $card->title = $request->title[$locale];
            $card->content = $request->get('content')[$locale];
            $card->slug = $request->slug[$locale];
            $card->media_id = $request->media[0];
            $card->date = dbDate($request->date);
            $card->status = $request->status == 'on' ? true : false;
            $card->save();
        }

        $data = [
            'status'    => 'success',
            'message'   => "Card <code>{$request->title[getLocale()]}</code> is created successfully"
        ];
        return response()->json($data,200);
    }


    public function show(Post $post)
    {
        //
    }


    public function edit($card_id)
    {
        $card_data = Card::with('media')->where('card_id',$card_id)->get();
        $cards = [];
        foreach ($card_data as $card){
            $cards[$card->locale] = $card;
        }
        return view('admin.cards.edit',compact('cards'));
    }


    public function update(Request $request, $card_id)
    {
        $cards = Card::where('card_id',$card_id)->get();
        $slug_validaiton = '';
        foreach ($cards as $Card) {
            $slug_validaiton .= '|unique:posts,slug,'.$Card->id;
        }
        $request->validate([
            'title' => 'required|array|check_array:1',
            'title.*' => 'string|nullable',
            'slug' => 'required|array|check_array:1'.$slug_validaiton,
            'slug.*' => 'string|nullable',
            'content' => 'required|array|check_array:1',
            'content.*' => 'string|nullable',
        ]);


        foreach (getLocales() as $locale){
            $card = Card::where('card_id',$card_id)->where('locale',$locale)->first();
            if(!$card){ // eger bu dilde post yoxdursa yenisini yaradir
                $card = new Card();
                $card->card_id = $card_id;
                $card->locale = $locale;
                $card->author_id = user()->id;
            }
            $card->title = $request->title[$locale];
            $card->slug = $request->slug[$locale];
            $card->media_id = $request->media[0];
            $card->content = $request->get('content')[$locale];
            $card->status = $request->status == 'on' ? true : false;
            $card->date = dbDate($request->date);

            $card->save();
        }

        $data = [
            'status'    => 'success',
            'message'   => "Card <code>{$request->title[getLocale()]}</code> is updated successfully"
        ];
        return response()->json($data,200);
    }


    public function destroy(Card $card)
    {
        $oldData = $card->title;
        $card->delete();

        $data = [
            'status'    => 'success',
            'message'   => "Card - <span class='font-weight-semibold'>{$oldData}</span> is deleted successfully!"
        ];
        return response()->json($data,200);
    }

    public function destroySelecteds(Request $request)
    {
        foreach ($request->selecteds as $selected) {
            $deleteData = Card::where('card_id',$selected)->first();
            $deleteData->delete();
        }

        $data = [
            'status'    => 'success',
            'message'   => "Selected <span class='font-weight-semibold'>Card</span> are deleted successfully!"
        ];
        return response()->json($data,200);
    }

}
