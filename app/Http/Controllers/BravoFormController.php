<?php

namespace App\Http\Controllers;

use App\BravoForm;
use App\BravoFormImage;
use Illuminate\Http\Request;

class BravoFormController extends Controller
{
    public function addReklamForm(Request $request)
    {
        $validator = validator($request->all(), [
            'company_name' => 'required|string',
            'relevant_person' => 'required|string',
            'email' => 'required|string',
            'phone' => 'required|string',
        ]);

        if ($validator->fails()) {
            session()->flash('error', 'Reklam tələbiniz göndərilə bilmədi.Bütün sahələri doldurduğunuza əmin olun');
            return redirect()->back();
        } else {
            $bravoForm = new BravoForm();
            $bravoForm->company_name = $request->company_name;
            $bravoForm->related_person = $request->relevant_person;
            $bravoForm->email = $request->email;
            $bravoForm->phone = $request->phone;
            $bravoForm->form_type = "reklam";
            $bravoForm->save();


            session()->flash('success', 'Reklam tələbiniz göndərilmişdir');

            return redirect()->back();
        }
    }

    public function addContactFeedback(Request $request){
        $validator = validator($request->all(), [
            'firstname' => 'required|string',
            'lastname' => 'required|string',
            'email' => 'required|string',
            'phone' => 'required|string',
            'date' => 'required|string',
            'request_type' => 'required|string',
            'store' => 'required|string',
            'message' => 'required|string',
        ]);

        if ($validator->fails()) {
            session()->flash('error', 'Feedback müraciətiniz göndərilə bilmədi.Bütün sahələri doldurduğunuza əmin olun');
            return redirect()->back();
        } else {
            $bravoForm = new BravoForm();
            $bravoForm->firstname = $request->firstname;
            $bravoForm->lastname = $request->lastname;
            $bravoForm->email = $request->email;
            $bravoForm->phone = $request->phone;
            $bravoForm->request_type = $request->request_type;
            $bravoForm->store = $request->store;
            $bravoForm->comment = $request->message;
            $bravoForm->date = $request->date;
            $bravoForm->form_type = "contact_feedback";
            $bravoForm->save();


            session()->flash('success', 'Feedback müraciətiniz göndərilmişdir');

            return redirect()->back();
        }
    }

    public function addCustomerFeedback(Request $request){
        $validator = validator($request->all(), [
            'firstname' => 'required|string',
            'lastname' => 'required|string',
            'email' => 'required|string',
            'phone' => 'required|string',
            'request_type' => 'required|string',
            'request_subject' => 'required|string',
            'message' => 'required|string',
        ]);

        if ($validator->fails()) {
            session()->flash('error', 'Feedback müraciətiniz göndərilə bilmədi.Bütün sahələri doldurduğunuza əmin olun');
            return redirect()->back();
        } else {
            $bravoForm = new BravoForm();
            $bravoForm->firstname = $request->firstname;
            $bravoForm->lastname = $request->lastname;
            $bravoForm->email = $request->email;
            $bravoForm->phone = $request->phone;
            $bravoForm->request_type = $request->request_type;
            $bravoForm->subject = $request->request_subject;
            $bravoForm->comment = $request->message;
            $bravoForm->form_type = "customer_feedback";
            $bravoForm->save();


            if ($request->hasFile('images')) {
                foreach ($request->file('images') as $file) {
                    $fileName ='customer-feedback-'.time().'.'.$file->getClientOriginalExtension();
                    $file->move(public_path('uploads'), $fileName);

                    $bravoFormImage = new BravoFormImage();
                    $bravoFormImage->type = 'customer_feedback';
                    $bravoFormImage->bravo_form_id = $bravoForm->id;
                    $bravoFormImage->path = $fileName;
                    $bravoFormImage->file_type = $file->getClientOriginalExtension();
                    $bravoFormImage->save();
                }
            }

            session()->flash('success', 'Feedback müraciətiniz göndərilmişdir');

            return redirect()->back();
        }
    }


    public function addMediaForm(Request $request)
    {
        $validator = validator($request->all(), [
            'company_name' => 'required|string',
            'relevant_person' => 'required|string',
            'email' => 'required|string',
            'phone' => 'required|string',
            'address' => 'required|string',
            'subject' => 'required|string',
            'message' => 'required|string',
        ]);

        if ($validator->fails()) {
            session()->flash('error', 'Media tələbiniz göndərilə bilmədi.Bütün sahələri doldurduğunuza əmin olun');
            return redirect()->back();
        } else {
            $bravoForm = new BravoForm();
            $bravoForm->company_name = $request->company_name;
            $bravoForm->related_person = $request->relevant_person;
            $bravoForm->email = $request->email;
            $bravoForm->phone = $request->phone;
            $bravoForm->address = $request->address;
            $bravoForm->subject = $request->subject;
            $bravoForm->comment = $request->message;
            $bravoForm->form_type = "media";
            $bravoForm->save();


            session()->flash('success', 'Media tələbiniz göndərilmişdir');

            return redirect()->back();
        }
    }

    public function addB2bForm(Request $request)
    {
        $validator = validator($request->all(), [
            'company_name' => 'required|string',
            'company_desc' => 'required|string',
            'relevant_person' => 'required|string',
            'district' => 'required|string',
            'city' => 'required|string',
            'country' => 'required|string',
            'zip_code' => 'required|string',
            'email' => 'required|string',
            'phone' => 'required|string',
            'address' => 'required|string',
            'family' => 'required|string',
            'category' => 'required|string'
        ]);

        if ($validator->fails()) {
            session()->flash('error', 'Topdan satış tələbiniz göndərilə bilmədi.Bütün sahələri doldurduğunuza əmin olun');
            return redirect()->back();
        } else {
            $bravoForm = new BravoForm();
            $bravoForm->company_name = $request->company_name;
            $bravoForm->related_person = $request->relevant_person;

            $bravoForm->company_info = $request->company_desc;
            $bravoForm->category = $request->category;
            $bravoForm->product_family = $request->family;
            $bravoForm->category = $request->category;
            $bravoForm->region = $request->district;
            $bravoForm->city = $request->city;
            $bravoForm->country = $request->country;
            $bravoForm->zip_code = $request->zip_code;

            $bravoForm->email = $request->email;
            $bravoForm->phone = $request->phone;
            $bravoForm->address = $request->address;
            $bravoForm->form_type = "b2b";
            $bravoForm->save();


            session()->flash('success', 'Topdan satış tələbiniz göndərilmişdir');

            return redirect()->back();
        }
    }


    public function addSupplierForm(Request $request)
    {
        $validator = validator($request->all(), [
            'company_name' => 'required|string',
            'company_desc' => 'required|string',
            'voen' => 'required|string',
            'relevant_person' => 'required|string',
            'district' => 'required|string',
            'city' => 'required|string',
            'country' => 'required|string',
            'zip_code' => 'required|string',
            'email' => 'required|string',
            'phone' => 'required|string',
            'address' => 'required|string',
            'family' => 'required|string',
            'category' => 'required|string'
        ]);

        if ($validator->fails()) {
            session()->flash('error', 'Potensial təhcizatçı tələbiniz göndərilə bilmədi.Bütün sahələri doldurduğunuza əmin olun');
            return redirect()->back();
        } else {
            $bravoForm = new BravoForm();
            $bravoForm->company_name = $request->company_name;
            $bravoForm->related_person = $request->relevant_person;
            $bravoForm->voen = $request->voen;

            $bravoForm->company_info = $request->company_desc;
            $bravoForm->category = $request->category;
            $bravoForm->product_family = $request->category;
            $bravoForm->category = $request->family;
            $bravoForm->region = $request->district;
            $bravoForm->city = $request->city;
            $bravoForm->country = $request->country;
            $bravoForm->zip_code = $request->zip_code;

            $bravoForm->email = $request->email;
            $bravoForm->phone = $request->phone;
            $bravoForm->address = $request->address;
            $bravoForm->form_type = "supplier";
            $bravoForm->save();

            if ($request->hasFile('presentation_files')) {
                foreach ($request->file('presentation_files') as $file) {
                    $fileName ='presentation_file-'.time().'.'.$file->getClientOriginalExtension();
                    $file->move(public_path('uploads'), $fileName);

                    $bravoFormImage = new BravoFormImage();
                    $bravoFormImage->type = 'presentation';
                    $bravoFormImage->bravo_form_id = $bravoForm->id;
                    $bravoFormImage->path = $fileName;
                    $bravoFormImage->file_type = $file->getClientOriginalExtension();
                    $bravoFormImage->save();
                }
            }

            if ($request->hasFile('price_files')) {
                foreach ($request->file('price_files') as $file) {
                    $fileName = 'price-file-'.time().'.'.$file->getClientOriginalExtension();
                    $file->move(public_path('uploads'), $fileName);

                    $bravoFormImage = new BravoFormImage();
                    $bravoFormImage->type = 'price';
                    $bravoFormImage->bravo_form_id = $bravoForm->id;
                    $bravoFormImage->path = $fileName;
                    $bravoFormImage->file_type = $file->getClientOriginalExtension();
                    $bravoFormImage->save();
                }
            }


            if ($request->hasFile('company_certificates')) {
                foreach ($request->file('company_certificates') as $file) {
                    $fileName = 'company-cert-'.time().'.'.$file->getClientOriginalExtension();
                    $file->move(public_path('uploads'), $fileName);

                    $bravoFormImage = new BravoFormImage();
                    $bravoFormImage->type = 'company_certificate';
                    $bravoFormImage->bravo_form_id = $bravoForm->id;
                    $bravoFormImage->path = $fileName;
                    $bravoFormImage->file_type = $file->getClientOriginalExtension();
                    $bravoFormImage->save();
                }
            }

            if ($request->hasFile('product_certificates')) {
                foreach ($request->file('product_certificates') as $file) {
                    $fileName = 'product-cert-'.time().'.'.$file->getClientOriginalExtension();
                    $file->move(public_path('uploads'), $fileName);

                    $bravoFormImage = new BravoFormImage();
                    $bravoFormImage->type = 'product_certificate';
                    $bravoFormImage->bravo_form_id = $bravoForm->id;
                    $bravoFormImage->path = $fileName;
                    $bravoFormImage->file_type = $file->getClientOriginalExtension();
                    $bravoFormImage->save();
                }
            }

            if ($request->hasFile('storage_images')) {
                foreach ($request->file('storage_images') as $file) {
                    $fileName = 'storage-'.time().'.'.$file->getClientOriginalExtension();
                    $file->move(public_path('uploads'), $fileName);

                    $bravoFormImage = new BravoFormImage();
                    $bravoFormImage->type = 'storage';
                    $bravoFormImage->bravo_form_id = $bravoForm->id;
                    $bravoFormImage->path = $fileName;
                    $bravoFormImage->file_type = $file->getClientOriginalExtension();
                    $bravoFormImage->save();
                }
            }


            session()->flash('success', 'Potensial təhcizatçı tələbiniz göndərilmişdir');

            return redirect()->back();
        }
    }

    public function getAdminPanel($type)
    {
        switch ($type) {
            case 'reklam' :
            {
                $items = BravoForm::where('form_type', 'reklam')->get();
                return view('admin.requests.reklam', compact('items'));
            }
            case 'media' :
            {
                $items = BravoForm::where('form_type', 'media')->get();
                return view('admin.requests.media', compact('items'));
            }
            case 'b2b' :
            {
                $items = BravoForm::where('form_type', 'b2b')->get();
                return view('admin.requests.b2b', compact('items'));
            }
            case 'supplier' :
            {
                $items = BravoForm::where('form_type', 'supplier')->get();
                return view('admin.requests.supplier', compact('items'));
            }
            case 'contact_feedback' :
            {
                $items = BravoForm::where('form_type', 'contact_feedback')->get();
                return view('admin.requests.contact_feedback', compact('items'));
            }
            case 'customer_feedback' :
            {
                $items = BravoForm::where('form_type', 'customer_feedback')->get();
                return view('admin.requests.customer_feedback', compact('items'));
            }
            default :
            {
//                return view('admin.requests.reklam');
            }
        }
    }
}
