<?php

namespace App\Http\Controllers;


use App\Page;
use App\Tab;
use Illuminate\Http\Request;

class TabController extends Controller
{

    public function __construct()
    {
        $this->middleware('permission:create-tabs', ['only' => ['create', 'store']]);
        $this->middleware('permission:read-tabs'  , ['only' => ['index']]);
        $this->middleware('permission:update-tabs', ['only' => ['edit','update']]);
        $this->middleware('permission:delete-tabs', ['only' => ['destroy','destroySelecteds']]);
    }

    public function index()
    {
        $tabs = Tab::where('locale',getLocale())->orderBy('date','desc')->get();
        return view('admin.tabs.index',compact('tabs'));

    }


    public function create()
    {
        $tabs = Tab::where('locale',app()->getLocale())->get();
        return view('admin.tabs.create',compact('tabs'));
    }


    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required|array|check_array:1',
            'title.*' => 'string|nullable',
            'content' => 'required|array',
            'content.*' => 'string|nullable',
            'menu' => 'nullable|string',
            'group' => 'string',
            'media.*' => 'integer|nullable',
            'date' => 'required|date_format:d.m.Y H:i:s',
        ]);


        $tab_id = uniqid();
        foreach (getLocales() as $locale){
            if($request->title[$locale]){
                $tab = new Tab();
                $tab->tab_id = $tab_id;
                $tab->locale = $locale;
                $tab->parent_id = $request->menu;
                $tab->author_id = user()->id;
                $tab->title = $request->title[$locale]??'';
                $tab->content = $request->{'content'}[$locale]??'';
                $tab->group = $request->group;
                $tab->form_template = $request->template;
                $tab->date = dbDate($request->date);
                $tab->save();
            }
        }

        $data = [
            'status'    => 'success',
            'message'   => "Tab <code>{$request->title[getLocale()]}</code> is created successfully"
        ];
        return response()->json($data,200);
    }


//    public function show(Page $page)
//    {
//        //
//    }


    public function edit($tab_id)
    {
        $tabs = Tab::where('locale',app()->getLocale())->get();
        $tab = Tab::where('tab_id',$tab_id)->where('locale',getLocale())->first();
        return view('admin.tabs.edit')->with(compact('tabs','tab'));
    }


    public function update(Request $request, $tab_id)
    {
        $request->validate([
            'title' => 'required|array|check_array:1',
            'title.*' => 'string|nullable',
            'content' => 'required|array',
            'content.*' => 'string|nullable',
            'menu' => 'nullable|string',
            'group' => 'string',
            'media.*' => 'integer|nullable',
            'date' => 'required|date_format:d.m.Y H:i:s',
        ]);


        foreach (getLocales() as $locale){
            if($request->title[$locale]){
                $tab = Tab::where('tab_id',$tab_id)->where('locale',$locale)->first();
                if(!$tab){ // eger bu dilde tab yoxdursa yenisini yaradir
                    $tab = new Tab();
                    $tab->tab_id = $tab_id;
                    $tab->locale = $locale;
                    $tab->author_id = user()->id;
                }
                $tab->parent_id = $request->menu;
                $tab->title = $request->title[$locale];
                $tab->content = $request->{'content'}[$locale];
                $tab->group = $request->group;
                $tab->form_template = $request->template;
                $tab->date = dbDate($request->date);
                $tab->save();

            }
        }

        $data = [
            'status'    => 'success',
            'message'   => "Tab <code>{$request->title[getLocale()]}</code> is updated successfully"
        ];
        return response()->json($data,200);
    }


    public function destroy(Tab $tab)
    {
        $oldData = $tab->title;

        $tabs  = Tab::where('tab_id',$tab->tab_id)->get();

        if (count($tabs) > 0) {
            Tab::where('tab_id',$tab->tab_id)->delete();
        }

        $data = [
            'status'    => 'success',
            'message'   => "Tab - <span class='font-weight-semibold'>{$oldData}</span> is deleted successfully!"
        ];
        return response()->json($data,200);
    }

    public function destroySelecteds(Request $request)
    {
        foreach ($request->selecteds as $selected) {
            $deleteData = Tab::where('tab_id',$selected)->first();
            $deleteData->delete();
        }

        $data = [
            'status'    => 'success',
            'message'   => "Selected <span class='font-weight-semibold'>tabs</span> are deleted successfully!"
        ];
        return response()->json($data,200);
    }

}
