<?php

namespace App\Http\Controllers;

use App\Faq;
use App\Menu;
use Illuminate\Http\Request;

class FaqController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:create-faqs', ['only' => ['create', 'store']]);
        $this->middleware('permission:read-faqs'  , ['only' => ['index']]);
        $this->middleware('permission:update-faqs', ['only' => ['edit','update']]);
        $this->middleware('permission:delete-faqs', ['only' => ['destroy','destroySelecteds']]);
    }

    public function index()
    {
        $faqs = Faq::where('locale',getLocale())->orderBy('date','desc')->get();
        return view('admin.faqs.index',compact('faqs'));

    }


    public function create()
    {
        $faqs = Faq::where('locale',app()->getLocale())->whereNull('parent_id')->get();
        return view('admin.faqs.create',compact('faqs'));
    }


    public function store(Request $request)
    {
        $request->validate([
            'parent' => 'required|string|nullable',
            'question' => 'required|array|check_array:1|unique:faqs',
            'answer' => 'array|nullable',
            'target' => 'nullable|array',
            'target.*' => 'nullable|string',
            'order' => 'required|integer'
        ]);

        $faq_id = uniqid();
        foreach (getLocales() as $locale) {
            if ($request->question[$locale]) {
                $faq = new Faq();
                $faq->faq_id = $faq_id;
                $faq->parent_id = $request->parent==='null'?null:$request->parent;
                $faq->locale = $locale;
                $faq->question = $request->question[$locale]??'';
                $faq->answer = $request->answer[$locale]??'';
                $faq->target = json_encode($request->target);

                $faq->order = $request->order;
                $faq->status = $request->status?true:false;
                $faq->author_id = user()->id;
                $faq->save();
            }
        }

        $data = [
            'status'    => 'success',
            'message'   => "Faq - <code>{$request->question[getLocale()]}</code> is created successfully!"
        ];
        return response()->json($data,200);
    }


    public function show($id)
    {
        //
    }


    public function edit(Faq $faq)
    {
        $faqsWithLocales = Faq::where('faq_id',$faq->faq_id)->get();

        foreach ($faqsWithLocales as $faqsWithLocale) {
            $faq[$faqsWithLocale['locale']] = $faqsWithLocale;
        }

        $faqs = Faq::where('locale',app()->getLocale())->whereNull('parent_id')->get();

        return view('admin.faqs.edit')->with(compact('faq','faqs'));
    }


    public function update(Request $request, Faq $faq)
    {
        $faq_id  = $faq->faq_id;

        $request->validate([
            'parent' => 'required|string|nullable',
            'question' => 'required|array|check_array:1|unique:faqs,id,'.$faq->id,
            'answer' => 'array|nullable',
            'target' => 'nullable|array',
            'target.*' => 'nullable|string',
            'order' => 'required|integer'
        ]);

        foreach (getLocales() as $locale) {
            if ($request->question[$locale]) {
                $faq = Faq::where('faq_id',$faq_id)->where('locale',$locale)->first();
                if(!$faq){ // eger bu dilde faq yoxdursa yenisini yaradir
                    $faq = new Faq();
                    $faq->faq_id = $faq_id;
                    $faq->locale = $locale;
                    $faq->author_id = user()->id;
                }

                $oldData = $faq->question;
                $faq->parent_id = $request->parent==='null'?null:$request->parent;
                $faq->question = $request->question[$locale]??'';
                $faq->answer = $request->answer[$locale]??'';
                $faq->order = $request->order;
                $faq->target = json_encode($request->target);
                $faq->status = $request->status?true:false;
                $faq->save();
            }
        }


        $data = [
            'status'    => 'success',
            'message'   => "Faq - <span class='font-weight-semibold'>{$oldData}</span> is updated successfully!"
        ];
        return response()->json($data,200);
    }


    public function destroy(Faq $faq)
    {
        $oldData = $faq->question;

        $faqs  = Faq::where('faq_id',$faq->faq_id)->get();

        if (count($faqs) > 0) {
            Faq::where('faq_id',$faq->faq_id)->delete();
        }


        $data = [
            'status'    => 'success',
            'message'   => "Faq - <span class='font-weight-semibold'>{$oldData}</span> is deleted successfully!"
        ];
        return response()->json($data,200);
    }

    public function destroySelecteds(Request $request)
    {
        foreach ($request->selecteds as $selected) {
            if($deleteData = Faq::where('faq_id',$selected)->first()){
                $deleteData->delete();
            }
        }

        $data = [
            'status'    => 'success',
            'message'   => "Selected <span class='font-weight-semibold'>faqs</span> are deleted successfully!"
        ];
        return response()->json($data,200);
    }
}
