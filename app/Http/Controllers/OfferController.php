<?php

namespace App\Http\Controllers;

use App\Offer;
use App\OfferProduct;
use Carbon\Carbon;
use Illuminate\Http\Request;

class OfferController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:create-offers', ['only' => ['create', 'store']]);
        $this->middleware('permission:read-offers'  , ['only' => ['index']]);
        $this->middleware('permission:update-offers', ['only' => ['edit','update']]);
        $this->middleware('permission:delete-offers', ['only' => ['destroy','destroySelecteds']]);

    }

    public function index()
    {
        $offers = Offer::where('locale',getLocale())->orderBy('order','asc')->where('date','<',Carbon::now())->get();
        $upComingOffers = Offer::where('locale',getLocale())->orderBy('order','asc')->where('date','>',Carbon::now())->get();
        return view('admin.offers.index',compact('offers','upComingOffers'));

    }


    public function create()
    {
        $offerProducts = OfferProduct::where('locale',getLocale())->where('status',true)->orderBy('order')->get();
        return view('admin.offers.create',compact('offerProducts'));
    }


    public function store(Request $request)
    {
        $request->validate([
            'title' => 'array|check_array:1',
            'title.*' => 'string|nullable',
            'text' => 'array|check_array:1',
            'text.*' => 'string|nullable',
            'offer_products' => 'array|check_array:1',
            'offer_products.*' => 'string|nullable',
            'media' => 'required',
        ]);

        $offer_id = uniqid();
        foreach (getLocales() as $locale){
            if($request->title[$locale]){
                $offer = new Offer();
                $offer->offer_id = $offer_id;
                $offer->locale = $locale;
                $offer->title = $request->title[$locale];
                $offer->text = $request->text[$locale];
                $offer->order = $request->order;
                $offer->media_id = $request->media[0];
                $offer->offer_products = encodeJSON($request->offer_products);
                $offer->order = getLastOrder('offers');
                $offer->status = $request->status?true:false;
                $offer->author_id = user()->id;
                $offer->date = dbDate($request->date,'d.m.Y','Y-m-d');
                $offer->save();
            }
        }


        $data = [
            'status'    => 'success',
            'message'   => "Offer <code>{$request->title[getLocale()]}</code> is created successfully"
        ];
        return response()->json($data,200);
    }


    public function show()
    {
        //
    }


    public function edit($offer_id)
    {
        $offerProducts = OfferProduct::where('locale',getLocale())->where('status',true)->orderBy('order')->get();
        $offer_data = Offer::where('offer_id',$offer_id)->get();
        $offers = [];
        foreach ($offer_data as $offer){
            $offers[$offer->locale] = $offer;
        }
        return view('admin.offers.edit')->with(compact('offers','offerProducts'));
    }


    public function update(Request $request,$offer_id)
    {
        $request->validate([
            'title' => 'array|check_array:1',
            'title.*' => 'string|nullable',
            'text' => 'array|check_array:1',
            'text.*' => 'string|nullable',
            'offer_products' => 'array|check_array:1',
            'offer_products.*' => 'string|nullable',
            'media' => 'required',
        ]);


        foreach (getLocales() as $locale){
            if($request->title[$locale]){
                $offer = Offer::where('offer_id',$offer_id)->where('locale',$locale)->first();
                if(!$offer){ // eger bu dilde offer yoxdursa yenisini yaradir
                    $offer = new Offer();
                    $offer->offer_id = $offer_id;
                    $offer->locale = $locale;
                    $offer->author_id = user()->id;
                }
                $offer->locale = $locale;
                $offer->title = $request->title[$locale];
                $offer->text = $request->text[$locale];
                $offer->media_id = $request->media[0];
                $offer->offer_products = encodeJSON($request->offer_products);
                $offer->status = $request->status?true:false;
                $offer->date = dbDate($request->date,'d.m.Y','Y-m-d');
                $offer->save();
            }
        }


        $data = [
            'status'    => 'success',
            'message'   => "Offer <code>{$request->title[getLocale()]}</code> is updated successfully"
        ];
        return response()->json($data,200);
    }


    public function destroy($offer_id)
    {
        $offerData = Offer::where('offer_id',$offer_id)->where('locale',getLocale())->first();
        $offers = Offer::where('offer_id',$offer_id)->get();
        foreach ($offers as $offer){
            $offer->delete();
        }

        $data = [
            'status'    => 'success',
            'message'   => "Offer - <span class='font-weight-semibold'>{$offerData->title}</span> is deleted successfully!"
        ];
        return response()->json($data,200);
    }

    public function destroySelecteds(Request $request)
    {
        foreach ($request->selecteds as $selected) {
            $deleteDatas = Offer::where('offer_id',$selected)->get();
            foreach ($deleteDatas as $deleteData){
                $deleteData->delete();
            }
        }

        $data = [
            'status'    => 'success',
            'message'   => "Selected <span class='font-weight-semibold'>offers</span> are deleted successfully!"
        ];
        return response()->json($data,200);
    }
}
