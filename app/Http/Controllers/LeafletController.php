<?php

namespace App\Http\Controllers;

use App\Category;
use App\Language;
use App\Leaflet;
use App\Post;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class LeafletController extends Controller
{

    public function __construct()
    {
        $this->middleware('permission:create-leaflets', ['only' => ['create', 'store']]);
        $this->middleware('permission:read-leaflets'  , ['only' => ['index']]);
        $this->middleware('permission:update-leaflets', ['only' => ['edit','update']]);
        $this->middleware('permission:delete-leaflets', ['only' => ['destroy','destroySelecteds']]);

    }

    public function index()
    {
        $leaflets = Leaflet::with('author')
            ->where('locale',getLocale())
            ->orderBy('created_at','desc')
            ->get();

        return view('admin.leaflets.index',compact('leaflets'));

    }


    public function create()
    {
        return view('admin.leaflets.create');
    }


    public function store(Request $request)
    {

        $request->validate([
            'title' => 'required|array|check_array:1',
            'title.*' => 'string|nullable',
            'slug' => 'required|array|check_array:1|unique:posts',
            'slug.*' => 'string|nullable',
            'start_at' => 'required|date_format:d.m.Y H:i:s',
            'end_at' => 'required|date_format:d.m.Y H:i:s',
        ]);


        $leaflet_id = uniqid();
        foreach (getLocales() as $locale){
            $post = new Leaflet();
            $post->leaflet_id = $leaflet_id;
            $post->locale = $locale;
            $post->author_id = user()->id;
            $post->title = $request->title[$locale];
            $post->slug = $request->slug[$locale];
            $post->media_id = $request->media[0];
            $post->pdf_media_id = $request->pdf_media_id[0];
            $post->status = $request->status == 'on' ? true : false;
            $post->start_at = dbDate($request->start_at);
            $post->end_at = dbDate($request->end_at);
            $post->save();
        }

        $data = [
            'status'    => 'success',
            'message'   => "Leaflet <code>{$request->title[getLocale()]}</code> is created successfully"
        ];
        return response()->json($data,200);
    }


    public function show(Post $post)
    {
        //
    }


    public function edit($leaflet_id)
    {
        $leaflet_data = Leaflet::where('leaflet_id',$leaflet_id)->get();
        $leaflets = [];
        foreach ($leaflet_data as $data){
            $leaflets[$data->locale] = $data;
        }
        return view('admin.leaflets.edit')->with(compact('leaflets'));
    }


    public function update(Request $request, $leaflet_id)
    {
        $leaflets = Leaflet::where('leaflet_id',$leaflet_id)->get();
        $slug_validaiton = '';
        foreach ($leaflets as $leaflet) {
            $slug_validaiton .= '|unique:posts,slug,'.$leaflet->id;
        }
        $request->validate([
            'title' => 'required|array|check_array:1',
            'title.*' => 'string|nullable',
            'slug' => 'required|array|check_array:1'.$slug_validaiton,
            'slug.*' => 'string|nullable',
            'start_at' => 'required|date_format:d.m.Y H:i:s',
            'end_at' => 'required|date_format:d.m.Y H:i:s',
        ]);


        foreach (getLocales() as $locale){
            $post = Leaflet::where('leaflet_id',$leaflet_id)->where('locale',$locale)->first();
            if(!$post){ // eger bu dilde post yoxdursa yenisini yaradir
                $post = new Leaflet();
                $post->leaflet_id = $leaflet_id;
                $post->locale = $locale;
                $post->author_id = user()->id;
            }
            $post->title = $request->title[$locale];
            $post->slug = $request->slug[$locale];
            $post->media_id = $request->media[0];
            $post->pdf_media_id = $request->pdf_media_id[0];
            $post->status = $request->status == 'on' ? true : false;
            $post->start_at = dbDate($request->start_at);
            $post->end_at = dbDate($request->end_at);
            $post->save();
        }

        $data = [
            'status'    => 'success',
            'message'   => "Leaflet <code>{$request->title[getLocale()]}</code> is updated successfully"
        ];
        return response()->json($data,200);
    }


    public function destroy(Leaflet $leaflet)
    {
        $oldData = $leaflet->title;
        $leaflet->delete();

        $data = [
            'status'    => 'success',
            'message'   => "Leaflet - <span class='font-weight-semibold'>{$oldData}</span> is deleted successfully!"
        ];
        return response()->json($data,200);
    }

    public function destroySelecteds(Request $request)
    {
        foreach ($request->selecteds as $selected) {
            $deleteData = Leaflet::where('leaflet_id',$selected)->first();
            $deleteData->delete();
        }

        $data = [
            'status'    => 'success',
            'message'   => "Selected <span class='font-weight-semibold'>leaflet</span> are deleted successfully!"
        ];
        return response()->json($data,200);
    }

}
