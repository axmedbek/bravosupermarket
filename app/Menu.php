<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    public function pages()
    {
        return $this->hasMany('App\Page', 'menu_id', 'id');
    }

    public function page()
    {
        return $this->hasOne('App\Page', 'menu_id', 'menu_id')->where('locale',getLocale());
    }

    public static function getMenus($onlyActives = false)
    {
        $data = [];
        $menus = Menu::where('parent_id', null)->where('locale', getLocale());
        if ($onlyActives) {
            $menus = $menus->where('status', true);
        }
        $menus = $menus->orderBy('order', 'asc')->get();
        foreach ($menus as $menu) {
            if (!Menu::hasSubMenus($menu->menu_id)) {
                $data[] = $menu;
            } else {
                $subMenus = Menu::getSubMenus($menu->menu_id);
                $menu->subMenus = $subMenus;
                $data[] = $menu;
            }

        }
        return $data;
    }

    public static function hasSubMenus($menu_id)
    {
        return Menu::where('parent_id', $menu_id)->count();
    }

    public static function getSubMenus($menu_id)
    {
        $data = [];
        $subMenus = Menu::where('parent_id', $menu_id)->where('locale', getLocale())->orderBy('order', 'asc')->get();
        foreach ($subMenus as $subMenu) {
            if (!Menu::hasSubMenus($subMenu->menu_id)) {
                $data[] = $subMenu;
            } else {
                $subMenus = Menu::getSubMenus($subMenu->menu_id);
                $subMenu->subMenus = $subMenus;
                $data[] = $subMenu;
            }

        }
//        if($menu_id==='5ee9f7c60de5c') dd($data);
        return $data;
    }

    public static function getTargetMenus($target)
    {
        $menus = Menu::where('status', true)
            ->with('pages')
            ->orderBy('order', 'asc')
            ->where('target', 'LIKE', "%$target%")
            ->where('locale', getLocale())->get();
        return $menus;
    }


    public static function getTargetLocationMenu()
    {
        return Menu::where('status', true)
            ->with('pages')
            ->where('target', 'LIKE', "%targetStoreLocation%")
            ->where('locale', getLocale())->first();
    }
}
