<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PageTab extends Model
{
    protected $table = "page_tabs";
}
