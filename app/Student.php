<?php

namespace App;

use Botble\Media\Models\MediaFile;
use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    public function author(){
        return $this->belongsTo('\App\User','author_id','id');
    }

    public static function getMedia($student_id){
        $student = Student::where('student_id',$student_id)->first();
        $medias = [];
        foreach (json_decode($student->media) as $media_id){
            $media = MediaFile::where('id',$media_id)->first();
            if($media) $medias[] = $media;
        }
        return $medias;
    }
}
