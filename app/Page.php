<?php

namespace App;

use Botble\Media\Models\MediaFile;
use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    public function menu(){
        return $this->belongsTo('\App\Menu','menu_id','id');
    }

    public function author(){
        return $this->belongsTo('\App\User','author_id','id');
    }

    public static function getPageTabs($page_id){
        return Tab::join('page_tabs','page_tabs.tab_id','tabs.tab_id')
            ->where('tabs.locale',getLocale())
            ->where('page_tabs.page_id',$page_id)
            ->whereNull('tabs.parent_id')
            ->orderBy('tabs.order')
            ->get(['tabs.*']);
    }

    public static function getSubTabs($tab_id){
        return Tab::leftJoin('page_tabs','page_tabs.tab_id','tabs.tab_id')
             ->where('tabs.locale',getLocale())
             ->where('tabs.parent_id',$tab_id)
             ->get(['tabs.*']);
    }


    public function pageTabs(){
        return $this->hasMany('\App\PageTab','page_id','page_id')->where('locale',getLocale());
    }

    public static function getMedia($page_id){
        $page = Page::where('page_id',$page_id)->first();
        $medias = [];
        foreach (json_decode($page->media) as $media_id){
            $media = MediaFile::where('id',$media_id)->first();
            if($media) $medias[] = $media;
        }
        return $medias;
    }
}
