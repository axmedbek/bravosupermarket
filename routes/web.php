<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*********************Front**********************/

Auth::routes(['register' => false]);

Route::get('/', 'HomeController@index')->name('home');
Route::get('/page/{slug}-{id}', 'HomeController@page')->name('page')->where(['slug' => '[A-Za-z0-9_\-]+','id' => '\d+']);
Route::get('/offers/{slug}-{offer_id}', 'HomeController@offers')->name('offers')->where(['slug' => '[A-Za-z0-9_\-]+','id' => '\d+']);
Route::get('/leaflets', 'HomeController@leaflet')->name('leaflet');
Route::get('/leaflets/{slug}', 'HomeController@leafletGet')->name('leaflet.Get');

Route::post('/form/ad', 'BravoFormController@addReklamForm')->name('form.ad');
Route::post('/form/media', 'BravoFormController@addMediaForm')->name('form.media');
Route::post('/form/b2b', 'BravoFormController@addB2bForm')->name('form.b2b');
Route::post('/form/supplier', 'BravoFormController@addSupplierForm')->name('form.supplier');
Route::post('/form/contact_feedback', 'BravoFormController@addContactFeedback')->name('form.contact_feedback');
Route::post('/form/customer_feedback', 'BravoFormController@addCustomerFeedback')->name('form.customer_feedback');


Route::get('/language/{language?}', 'HomeController@setLocale')->name('setLocale');

Route::group(['prefix' => 'career', 'as' => 'career.'],function (){
    Route::get('/', 'CareerController@index')->name('index');
    Route::get('/apply', 'CareerController@applyGet')->name('applyGet');
    Route::post('/apply', 'CareerController@applyStore')->name('applyStore');
});

Route::get('/cards', 'HomeController@card')->name('card');
Route::get('/page/students/{group_id}', 'HomeController@getStudentsWithGroup')->name('get.students.group');

Route::get('/v1/api/stores', 'HomeController@getAllStores')->name('api.stores');
Route::get('/v1/api/store/{id}', 'HomeController@getStoreById')->name('api.store');
Route::get('/v1/api/search/store', 'HomeController@searchStore')->name('api.store.search');



/*********************Panel**********************/
Route::group(['prefix' => 'admin', 'middleware' => ['auth','lang'], 'as' => 'admin.'],function(){
    Route::get('/', 'AdminController@index')->name('dashboard');



    //Rauf Abbas
    Route::delete('/vacancies/deleteSelecteds', 'BannerController@destroySelecteds');
    Route::resource('/vacancies', 'VacancyController');
    Route::get('/vacancies/export/{id}', 'VacancyController@export')->name('vacancies.export');

    Route::delete('/cards/deleteSelecteds', 'CardController@destroySelecteds');
    Route::resource('/cards', 'CardController');

    Route::delete('/stores/deleteSelecteds', 'StoreController@destroySelecteds');
    Route::resource('/stores', 'StoreController');

    Route::delete('/students/deleteSelecteds', 'StudentController@destroySelecteds');
    Route::resource('/students', 'StudentController');


    Route::resource('/leaflets', 'LeafletController');
    Route::delete('/leaflets/deleteSelecteds', 'LeafletController@destroySelecteds');


    Route::delete('/categories/deleteSelecteds', 'CategoryController@destroySelecteds');
    Route::resource('/categories', 'CategoryController');

    Route::delete('/posts/deleteSelecteds', 'PostController@destroySelecteds');
    Route::resource('/posts', 'PostController');

    Route::resource('/tabs/deleteSelecteds', 'TabController@destroySelecteds');
    Route::resource('/tabs', 'TabController');

    Route::delete('/faqs/deleteSelecteds', 'FaqController@destroySelecteds');
    Route::resource('/faqs', 'FaqController');

    Route::delete('/menus/deleteSelecteds', 'MenuController@destroySelecteds');
    Route::resource('/menus', 'MenuController');

    Route::delete('/pages/deleteSelecteds', 'PageController@destroySelecteds');
    Route::resource('/pages', 'PageController');

    Route::delete('/sliders/deleteSelecteds', 'SliderController@destroySelecteds');
    Route::resource('/sliders', 'SliderController');

    Route::delete('/banners/deleteSelecteds', 'BannerController@destroySelecteds');
    Route::resource('/banners', 'BannerController');

    Route::delete('/offers/deleteSelecteds', 'OfferController@destroySelecteds');
    Route::resource('/offers', 'OfferController');

    Route::delete('/offers/deleteSelecteds', 'OfferProductController@destroySelecteds');
    Route::resource('/offer-products', 'OfferProductController');

    Route::delete('/users/deleteSelecteds', 'UserController@destroySelecteds');
    Route::resource('/users', 'UserController');

    Route::delete('/roles/deleteSelecteds', 'RoleController@destroySelecteds');
    Route::resource('/roles', 'RoleController');

    Route::delete('/permissions/deleteSelecteds', 'PermissionController@destroySelecteds');
    Route::resource('/permissions', 'PermissionController');

    Route::delete('/languages/deleteSelecteds', 'LanguageController@destroySelecteds');
    Route::resource('/languages', 'LanguageController');

    Route::get('/translation_group/{translation_group}/translations', 'TranslationGroupController@translations')->name('translation_group.translations');
    Route::delete('/translation_groups/deleteSelecteds', 'TranslationGroupController@destroySelecteds');
    Route::resource('/translation_groups', 'TranslationGroupController');

    Route::get('/translations/exportView', 'TranslationController@exportView')->name('translations.exportView');
    Route::get('/translations/export', 'TranslationController@export')->name('translations.export');
    Route::get('/translations/create/{group?}', 'TranslationController@create');
    Route::delete('/translations/deleteSelecteds', 'TranslationController@destroySelecteds');
    Route::resource('/translations', 'TranslationController');

    Route::post('/mediaUpload', 'AdminController@mediaUpload')->name('mediaUpload');
    Route::post('/generateSlug', 'AdminController@generateSlug')->name('generateSlug');
    Route::post('/generateOrderNumberInParent', 'AdminController@generateOrderNumberInParent')->name('generateOrderNumberInParent');
    Route::post('/reorder', 'AdminController@reorder')->name('reorder');

    Route::get('/config', 'AdminController@index')->name('config');
    Route::get('/site', 'AdminController@index')->name('site');

    Route::get('/requests/{type}', 'BravoFormController@getAdminPanel')->name('requests.index');
});






