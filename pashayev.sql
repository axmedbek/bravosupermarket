-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jun 12, 2020 at 09:48 PM
-- Server version: 5.7.30-0ubuntu0.18.04.1
-- PHP Version: 7.3.18-1+ubuntu18.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pashayev`
--

-- --------------------------------------------------------

--
-- Table structure for table `banners`
--

CREATE TABLE `banners` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `target` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `media_id` int(10) UNSIGNED DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `text` text COLLATE utf8mb4_unicode_ci,
  `url` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order` int(11) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `author_id` bigint(20) UNSIGNED NOT NULL,
  `style` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'noText',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `banners`
--

INSERT INTO `banners` (`id`, `target`, `media_id`, `title`, `text`, `url`, `order`, `status`, `author_id`, `style`, `created_at`, `updated_at`) VALUES
(1, 'media', 40, 'Günün təklifi.', 'Möcüzə qarğıdalı yağı 8.19 AZN deyil, 6.55 AZN-ə.', NULL, NULL, 1, 4, 'noText', '2020-06-11 09:28:27', '2020-06-12 04:03:04'),
(2, 'media', 41, 'Günün təklifi.', 'Bazar ertesi.950 ml. 3.2%-li Prostokvaşino süd 2.80 AZN deyil, 1.89 AZN-ə.', NULL, NULL, 1, 4, 'noText', '2020-06-11 09:30:13', '2020-06-12 04:04:15'),
(3, 'media', 42, 'Gündəlik təklifi.', '2 iyun, çərşənbə axşamı. Mal ətindən qiymə 9 AZN deyil, 7.49 AZN-ə', NULL, NULL, 1, 4, 'noText', '2020-06-11 09:32:25', '2020-06-12 04:05:30'),
(4, 'aboveBravoBanner', 32, 'Careers', 'Biz əmək bazarında özünə güvənən, pərakəndə satış sahəsində karyera qurmaq istəyən aktiv, dürüst, öyrənməyə meylli namizədləri hər zaman axtarırıq.  Bu bölmədə siz bizim şirkətdə karyera imkanlarından məlumat ala bilərsiniz.', NULL, 4, 1, 4, 'textLeft', '2020-06-11 09:47:35', '2020-06-11 10:02:12'),
(5, 'belowBravoBanner', 27, 'Bravo Çağrı Mərkəzini təqdim edir', 'Artıq siz *2266 qısa nömrəsinə zəng edərək Bravo Müştəri Xidmətlərinə müraciət edə bilərsiniz.', NULL, NULL, 1, 4, 'textRight', '2020-06-11 09:49:05', '2020-06-12 04:09:25'),
(6, 'shopAtBravo', 57, 'Hədiyyəli məhsullar', 'BRAVO-DA BAĞ MÖVSÜMÜ', NULL, NULL, 1, 4, 'noText', '2020-06-11 09:55:31', '2020-06-12 09:05:53'),
(8, 'shopAtBravo', 38, 'Uşaq üçün yay kolleksiyası', '28 may - 10 iyun', NULL, 7, 1, 4, 'noText', '2020-06-11 10:02:16', '2020-06-11 10:08:21'),
(9, 'shopAtBravo', 37, 'Elektronika məhsulları', 'Festival!', NULL, 8, 1, 4, 'noText', '2020-06-11 10:04:50', '2020-06-11 10:08:21'),
(10, 'bravoBanner', 43, NULL, NULL, NULL, NULL, 1, 4, 'noText', '2020-06-11 10:06:59', '2020-06-12 04:11:56');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `parent_id` bigint(20) UNSIGNED DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `parent_id`, `name`, `description`, `order`, `status`, `created_at`, `updated_at`) VALUES
(1, NULL, 'test', 'test', 1, 1, '2020-06-10 01:45:57', '2020-06-10 01:45:57'),
(2, 1, 'test 1.1', 'test 1.1', 1, 1, '2020-06-10 01:46:10', '2020-06-10 01:46:10');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `languages`
--

CREATE TABLE `languages` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `languages`
--

INSERT INTO `languages` (`id`, `key`, `name`, `description`, `order`, `status`, `created_at`, `updated_at`) VALUES
(1, 'az', 'Azərbaycan', 'Azərbaycan dili', 1, 1, '2020-06-08 08:07:48', '2020-06-08 08:07:48'),
(2, 'en', 'English', 'English', 2, 1, '2020-06-12 05:39:33', '2020-06-12 05:39:33');

-- --------------------------------------------------------

--
-- Table structure for table `media_files`
--

CREATE TABLE `media_files` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `folder_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `mime_type` varchar(120) COLLATE utf8mb4_unicode_ci NOT NULL,
  `size` int(11) NOT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `options` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `media_files`
--

INSERT INTO `media_files` (`id`, `user_id`, `name`, `folder_id`, `mime_type`, `size`, `url`, `options`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 4, '7rvxBxvi1QRCmXYTuhd7Ik3IEyY1NqdTqdbQBOhP', 0, 'image/jpeg', 187855, '/uploads/4/7rvxbxvi1qrcmxytuhd7ik3ieyy1nqdtqdbqbohp.jpg', '[]', '2020-06-09 04:10:40', '2020-06-11 08:15:37', '2020-06-11 08:15:37'),
(2, 4, '8G6JriI08Iz3YRvok90OSa9psekGzOmlsWcUTR7B', 0, 'image/jpeg', 658557, '/uploads/4/8g6jrii08iz3yrvok90osa9psekgzomlswcutr7b.jpg', '[]', '2020-06-09 04:10:40', '2020-06-11 08:15:37', '2020-06-11 08:15:37'),
(3, 4, '8suTYmJUZLheDqWjrr02SOHRCJUY8h6vgOMrkIpT', 0, 'image/jpeg', 54843, '/uploads/4/8sutymjuzlhedqwjrr02sohrcjuy8h6vgomrkipt.jpg', '[]', '2020-06-09 04:10:42', '2020-06-11 08:15:37', '2020-06-11 08:15:37'),
(4, 4, '8yFMFRJ6KnwJf1vPgn4qPNygecxQvnQ4IGxYdiry', 0, 'image/png', 40457, '/uploads/4/8yfmfrj6knwjf1vpgn4qpnygecxqvnq4igxydiry.png', '[]', '2020-06-09 04:10:42', '2020-06-11 08:15:37', '2020-06-11 08:15:37'),
(5, 4, '50l5vo8qM3FMuF0s8sPEZm2zwRFFoOl7uC6bXeG5', 0, 'image/jpeg', 69393, '/uploads/4/50l5vo8qm3fmuf0s8spezm2zwrffool7uc6bxeg5.jpg', '[]', '2020-06-09 04:10:43', '2020-06-11 08:15:37', '2020-06-11 08:15:37'),
(6, 4, 'AQjfthXzlLWEbAOVn14Vc4e8r9WNcS1r9qPYm7Tq', 0, 'image/jpeg', 65818, '/uploads/4/aqjfthxzllwebaovn14vc4e8r9wncs1r9qpym7tq.jpg', '[]', '2020-06-09 04:10:43', '2020-06-11 08:15:37', '2020-06-11 08:15:37'),
(7, 4, 'ASh2RpuOfppYZVogsFzIXW38U2DQpei2yJ423rCX', 0, 'image/jpeg', 74761, '/uploads/4/ash2rpuofppyzvogsfzixw38u2dqpei2yj423rcx.jpeg', '[]', '2020-06-09 04:10:43', '2020-06-11 08:15:37', '2020-06-11 08:15:37'),
(8, 4, 'AZl6FyHdyjCKaUolqKXCKsZOfwT7iiBHKYRGx82S', 0, 'image/jpeg', 638443, '/uploads/4/azl6fyhdyjckauolqkxckszofwt7iibhkyrgx82s.jpg', '[]', '2020-06-09 04:10:44', '2020-06-11 08:15:37', '2020-06-11 08:15:37'),
(9, 4, 'cPK7KxiiAMa2tdEDw9P5E1Ebp4rvbshFet8cxydq', 0, 'image/jpeg', 65818, '/uploads/4/cpk7kxiiama2tdedw9p5e1ebp4rvbshfet8cxydq.jpg', '[]', '2020-06-09 04:10:45', '2020-06-11 08:15:37', '2020-06-11 08:15:37'),
(10, 4, 'CSamnoespuOfX6bx8dXENZZr1LDjPPbDG9ulBZ3a', 0, 'image/png', 88260, '/uploads/4/csamnoespuofx6bx8dxenzzr1ldjppbdg9ulbz3a.png', '[]', '2020-06-09 04:10:46', '2020-06-11 08:15:37', '2020-06-11 08:15:37'),
(11, 4, 'CWOy0O0yMZ0AKgEkjnPl68rC883PFewIWCRwQnEw', 0, 'image/jpeg', 31806, '/uploads/4/cwoy0o0ymz0akgekjnpl68rc883pfewiwcrwqnew.jpg', '[]', '2020-06-09 04:10:46', '2020-06-11 08:15:37', '2020-06-11 08:15:37'),
(12, 4, 'DjRWLYTGgMpBApAWc5XpAxmeOGM7ShfkHyY7senu', 0, 'image/png', 7234, '/uploads/4/djrwlytggmpbapawc5xpaxmeogm7shfkhyy7senu.png', '[]', '2020-06-09 04:10:46', '2020-06-11 08:15:37', '2020-06-11 08:15:37'),
(13, 4, 'DoBx761KkFrGK3SVkPBPnBJvTO8BpfC2KVRjezML', 0, 'image/jpeg', 29205, '/uploads/4/dobx761kkfrgk3svkpbpnbjvto8bpfc2kvrjezml.jpg', '[]', '2020-06-09 04:10:47', '2020-06-11 08:15:37', '2020-06-11 08:15:37'),
(14, 4, 'EXHFgmPvKsjlDzqpfG6lMPVTAcsOEa0NxXLnTTHy', 0, 'image/jpeg', 145638, '/uploads/4/exhfgmpvksjldzqpfg6lmpvtacsoea0nxxlntthy.jpg', '[]', '2020-06-09 04:10:47', '2020-06-11 08:15:37', '2020-06-11 08:15:37'),
(15, 4, 'faVEdmG9LgvWaFU3TOgwHwC2W3fEjkoAIFAFJ0TW', 0, 'image/jpeg', 21152, '/uploads/4/favedmg9lgvwafu3togwhwc2w3fejkoaifafj0tw.jpg', '[]', '2020-06-09 04:10:47', '2020-06-11 08:15:37', '2020-06-11 08:15:37'),
(16, 4, 'Ff9TPDD0uo41qSwkmupjOV2l4Ovvw3DVtrjagMNn', 0, 'image/jpeg', 127074, '/uploads/4/ff9tpdd0uo41qswkmupjov2l4ovvw3dvtrjagmnn.jpg', '[]', '2020-06-09 04:10:47', '2020-06-11 08:15:37', '2020-06-11 08:15:37'),
(17, 4, 'G4r0anGAPSHhZqAG4JHjnYNffR9lGuCYie6Se9zR', 0, 'image/jpeg', 606770, '/uploads/4/g4r0angapshhzqag4jhjnynffr9lgucyie6se9zr.jpg', '[]', '2020-06-09 04:10:48', '2020-06-11 08:15:37', '2020-06-11 08:15:37'),
(18, 4, 'ga6M8Oxom7BA0LY9CEzPXPWfzKFoLOvbJivSLkvf', 0, 'image/jpeg', 69886, '/uploads/4/ga6m8oxom7ba0ly9cezpxpwfzkfolovbjivslkvf.jpg', '[]', '2020-06-09 04:10:48', '2020-06-11 08:15:37', '2020-06-11 08:15:37'),
(19, 4, 'Customer Service', 0, 'image/jpeg', 272202, '/uploads/4/customer-service.jpg', '[]', '2020-06-09 09:17:00', '2020-06-11 08:15:37', '2020-06-11 08:15:37'),
(20, 4, 'TİK TOK VİDEOLARI İZLEDİM #15', 0, 'youtube', 0, 'https://www.youtube.com/watch?v=30V1zmPcEkI', '{\"thumb\":\"https:\\/\\/img.youtube.com\\/vi\\/30V1zmPcEkI\\/maxresdefault.jpg\"}', '2020-06-10 04:10:25', '2020-06-11 08:15:37', '2020-06-11 08:15:37'),
(21, 4, 'document', 0, 'application/pdf', 175825, '/uploads/4/document.pdf', '[]', '2020-06-10 05:04:42', '2020-06-11 08:15:37', '2020-06-11 08:15:37'),
(22, 4, 'CSR activities', 0, 'image/jpeg', 746216, '/uploads/4/csr-activities.jpg', '[]', '2020-06-11 07:56:23', '2020-06-11 08:15:37', '2020-06-11 08:15:37'),
(23, 4, 'Praktikum', 0, 'image/jpeg', 432677, '/uploads/4/praktikum.jpg', '[]', '2020-06-11 07:56:24', '2020-06-11 08:15:37', '2020-06-11 08:15:37'),
(24, 4, 'Special offers', 0, 'image/jpeg', 897667, '/uploads/4/special-offers.jpg', '[]', '2020-06-11 07:56:24', '2020-06-11 08:15:37', '2020-06-11 08:15:37'),
(25, 4, 'Customer Service - main page', 0, 'image/jpeg', 142256, '/uploads/4/customer-service-main-page.jpg', '[]', '2020-06-11 08:13:08', '2020-06-11 08:15:37', '2020-06-11 08:15:37'),
(26, 4, 'CSR activities-1', 0, 'image/jpeg', 746216, '/uploads/4/csr-activities-1.jpg', '[]', '2020-06-11 08:15:45', '2020-06-11 08:15:45', NULL),
(27, 4, 'Customer Service-1', 0, 'image/jpeg', 272202, '/uploads/4/customer-service-1.jpg', '[]', '2020-06-11 08:15:45', '2020-06-11 08:15:45', NULL),
(28, 4, 'Praktikum-1', 0, 'image/jpeg', 432677, '/uploads/4/praktikum-1.jpg', '[]', '2020-06-11 08:15:45', '2020-06-11 08:15:45', NULL),
(29, 4, 'Special offers-1', 0, 'image/jpeg', 897667, '/uploads/4/special-offers-1.jpg', '[]', '2020-06-11 08:15:45', '2020-06-11 08:15:45', NULL),
(30, 4, 'TIMELAPSE OF THE ENTIRE UNIVERSE', 0, 'youtube', 0, 'https://www.youtube.com/watch?v=TBikbn5XJhg', '{\"thumb\":\"https:\\/\\/img.youtube.com\\/vi\\/TBikbn5XJhg\\/maxresdefault.jpg\"}', '2020-06-11 09:25:02', '2020-06-11 09:25:02', NULL),
(31, 4, 'Günün təklifi. 11 iyun keçərli olacaq. 1 kq. Westgold kərə yağı 15.80 AZN deyil, 14.25 AZN-ə.', 0, 'youtube', 0, 'https://www.youtube.com/watch?v=tg4C4NBETws', '{\"thumb\":\"https:\\/\\/img.youtube.com\\/vi\\/tg4C4NBETws\\/maxresdefault.jpg\"}', '2020-06-11 09:30:33', '2020-06-11 09:30:33', NULL),
(32, 4, 'IMG_8981 1', 0, 'image/png', 419833, '/uploads/4/img-8981-1.png', '[]', '2020-06-11 09:47:22', '2020-06-11 09:47:22', NULL),
(33, 4, 'Mask Group', 0, 'image/png', 179671, '/uploads/4/mask-group.png', '[]', '2020-06-11 09:48:56', '2020-06-11 09:48:56', NULL),
(34, 4, '2 iyun', 0, 'image/jpeg', 396106, '/uploads/4/2-iyun.jpg', '[]', '2020-06-11 09:55:47', '2020-06-11 09:55:47', NULL),
(35, 4, 'Home page main banner -2 1180x535', 0, 'image/jpeg', 520988, '/uploads/4/home-page-main-banner-2-1180x535.jpg', '[]', '2020-06-11 09:57:06', '2020-06-11 09:57:06', NULL),
(36, 4, 'Home page main banner 1180x535', 0, 'image/jpeg', 373584, '/uploads/4/home-page-main-banner-1180x535.jpg', '[]', '2020-06-11 09:57:06', '2020-06-11 09:57:06', NULL),
(37, 4, 'Shop at Bravo - 2', 0, 'image/jpeg', 651869, '/uploads/4/shop-at-bravo-2.jpg', '[]', '2020-06-11 09:58:41', '2020-06-11 09:58:41', NULL),
(38, 4, 'Shop at Bravo -1', 0, 'image/jpeg', 382911, '/uploads/4/shop-at-bravo-1.jpg', '[]', '2020-06-11 10:00:57', '2020-06-11 10:00:57', NULL),
(39, 4, 'Rectangle 147', 0, 'image/png', 247066, '/uploads/4/rectangle-147.png', '[]', '2020-06-11 10:06:52', '2020-06-11 10:06:52', NULL),
(40, 4, 'Günün təklifi. 12 iyun. 2 l. Möcüzə qarğıdalı yağı 8.19 AZN deyil, 6.55 AZN-ə.', 0, 'youtube', 0, 'https://www.youtube.com/watch?v=nGeaBetB3RQ', '{\"thumb\":\"https:\\/\\/img.youtube.com\\/vi\\/nGeaBetB3RQ\\/maxresdefault.jpg\"}', '2020-06-12 04:02:24', '2020-06-12 04:02:24', NULL),
(41, 4, 'Günün təklifi.9 iyun, çərşənbə axşamı. 500 qr. Champion qara çay 9.70 AZN deyil, 7.75 AZN-ə.', 0, 'youtube', 0, 'https://www.youtube.com/watch?v=DmFHDvCuim8', '{\"thumb\":\"https:\\/\\/img.youtube.com\\/vi\\/DmFHDvCuim8\\/maxresdefault.jpg\"}', '2020-06-12 04:04:04', '2020-06-12 04:04:04', NULL),
(42, 4, 'Gündəlik təklifi. 2 iyun, çərşənbə axşamı. Mal ətindən qiymə 9 AZN deyil, 7.49 AZN-ə', 0, 'youtube', 0, 'https://www.youtube.com/watch?v=lB4LENL7gL8', '{\"thumb\":\"https:\\/\\/img.youtube.com\\/vi\\/lB4LENL7gL8\\/maxresdefault.jpg\"}', '2020-06-12 04:05:21', '2020-06-12 04:05:21', NULL),
(43, 4, 'Home page middle banner 1180x355', 0, 'image/jpeg', 251127, '/uploads/4/home-page-middle-banner-1180x355.jpg', '[]', '2020-06-12 04:11:50', '2020-06-12 04:11:50', NULL),
(44, 4, 'About us - 2', 0, 'image/jpeg', 598867, '/uploads/4/about-us-2.jpg', '[]', '2020-06-12 06:25:23', '2020-06-12 06:25:23', NULL),
(45, 4, 'Cards', 0, 'image/jpeg', 525685, '/uploads/4/cards.jpg', '[]', '2020-06-12 06:25:23', '2020-06-12 06:25:23', NULL),
(46, 4, 'Careers', 0, 'image/jpeg', 588256, '/uploads/4/careers.jpg', '[]', '2020-06-12 06:25:24', '2020-06-12 06:25:24', NULL),
(47, 4, 'Contact us', 0, 'image/jpeg', 449158, '/uploads/4/contact-us.jpg', '[]', '2020-06-12 06:25:24', '2020-06-12 06:25:24', NULL),
(48, 4, 'CSR activities-2', 0, 'image/jpeg', 746216, '/uploads/4/csr-activities-2.jpg', '[]', '2020-06-12 06:25:24', '2020-06-12 06:25:24', NULL),
(49, 4, 'Customer Service-2', 0, 'image/jpeg', 272202, '/uploads/4/customer-service-2.jpg', '[]', '2020-06-12 06:25:25', '2020-06-12 06:25:25', NULL),
(50, 4, 'Leaflet', 0, 'image/jpeg', 996007, '/uploads/4/leaflet.jpg', '[]', '2020-06-12 06:25:25', '2020-06-12 06:25:25', NULL),
(51, 4, 'Legal disclaimer', 0, 'image/jpeg', 594302, '/uploads/4/legal-disclaimer.jpg', '[]', '2020-06-12 06:25:26', '2020-06-12 06:25:26', NULL),
(52, 4, 'Our stores - 1', 0, 'image/jpeg', 641390, '/uploads/4/our-stores-1.jpg', '[]', '2020-06-12 06:25:26', '2020-06-12 06:25:26', NULL),
(53, 4, 'Our stores - 2', 0, 'image/jpeg', 589737, '/uploads/4/our-stores-2.jpg', '[]', '2020-06-12 06:25:27', '2020-06-12 06:25:27', NULL),
(54, 4, 'Praktikum-2', 0, 'image/jpeg', 432677, '/uploads/4/praktikum-2.jpg', '[]', '2020-06-12 06:25:27', '2020-06-12 06:25:27', NULL),
(55, 4, 'Special offers-2', 0, 'image/jpeg', 897667, '/uploads/4/special-offers-2.jpg', '[]', '2020-06-12 06:25:27', '2020-06-12 06:25:27', NULL),
(56, 4, 'About us - 1', 0, 'image/jpeg', 493880, '/uploads/4/about-us-1.jpg', '[]', '2020-06-12 06:25:37', '2020-06-12 06:25:37', NULL),
(57, 4, 'Shop at Bravo - 3', 0, 'image/jpeg', 187158, '/uploads/4/shop-at-bravo-3.jpg', '[]', '2020-06-12 09:05:47', '2020-06-12 09:05:47', NULL),
(58, 4, 'thumb', 0, 'image/jpeg', 42492, '/uploads/4/thumb.jpg', '[]', '2020-06-12 12:52:19', '2020-06-12 12:52:19', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `media_folders`
--

CREATE TABLE `media_folders` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `media_settings`
--

CREATE TABLE `media_settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `key` varchar(120) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci,
  `media_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `menus`
--

CREATE TABLE `menus` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `parent_id` bigint(20) UNSIGNED DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `target` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `url` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `author_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `menus`
--

INSERT INTO `menus` (`id`, `parent_id`, `name`, `description`, `target`, `url`, `order`, `status`, `author_id`, `created_at`, `updated_at`) VALUES
(1, 8, 'About Us', 'About Us', '[\"targetTop\",\"targetBottom\"]', '#', 1, 1, 1, '2020-06-08 08:09:23', '2020-06-08 10:27:48'),
(2, 8, 'Contact Us', 'Contact Us', '[\"targetTop\",\"targetBottom\"]', '#', 2, 1, 1, '2020-06-08 10:11:09', '2020-06-08 11:04:26'),
(3, NULL, 'Careers', 'Careers', '[\"targetTop\"]', '#', 3, 1, 1, '2020-06-08 10:11:24', '2020-06-08 10:11:24'),
(4, NULL, 'Customer Service', 'Customer Service', '[\"targetTop\"]', '#', 4, 1, 1, '2020-06-08 10:11:39', '2020-06-08 10:11:39'),
(5, NULL, 'Leaflet', 'Leaflet', '[\"targetTop\"]', '#', 5, 1, 1, '2020-06-08 10:11:54', '2020-06-08 10:11:54'),
(6, NULL, 'Cards', 'Cards', '[\"targetTop\"]', '#', 6, 1, 1, '2020-06-08 10:12:06', '2020-06-08 10:12:06'),
(7, NULL, 'CSR Activities', 'CSR Activities', '[\"targetTop\"]', '#', 7, 1, 1, '2020-06-08 10:12:20', '2020-06-08 10:12:20'),
(8, NULL, 'Quick Links', 'Quick Links', '[\"targetBottom\"]', '#', 8, 1, 1, '2020-06-08 10:17:11', '2020-06-08 10:17:11'),
(9, 8, 'Promotions', 'Promotions', '[\"targetBottom\"]', '#', 2, 1, 1, '2020-06-08 10:28:35', '2020-06-08 10:28:35'),
(10, NULL, 'About Bravo', 'About Bravo', '[\"targetBottom\"]', '#', 9, 1, 1, '2020-06-08 10:29:09', '2020-06-08 10:29:09'),
(11, 10, 'History', 'History', '[\"targetBottom\"]', '#', 1, 1, 1, '2020-06-08 10:31:12', '2020-06-08 10:32:22'),
(12, 10, 'Mission & values', 'Mission & values', '[\"targetBottom\"]', '#', 2, 1, 1, '2020-06-08 10:31:23', '2020-06-08 10:32:32'),
(13, NULL, 'Products & Services', 'Products & Services', '[\"targetBottom\"]', '#', 11, 1, 1, '2020-06-08 10:32:02', '2020-06-08 10:32:02'),
(14, 13, 'Departments', 'Departments', '[\"targetBottom\"]', '#', 1, 1, 1, '2020-06-08 10:32:58', '2020-06-08 10:32:58'),
(15, 13, 'Quality standards', 'Quality standards', '[\"targetBottom\"]', '#', 2, 1, 1, '2020-06-08 10:33:09', '2020-06-08 10:33:09'),
(16, NULL, 'Customer care', 'Customer care', '[\"targetBottom\"]', '#', 12, 1, 1, '2020-06-08 10:33:33', '2020-06-08 10:33:33'),
(17, 16, 'Potential suppliers', 'Potential suppliers', '[\"targetBottom\"]', '#', 1, 1, 1, '2020-06-08 10:33:54', '2020-06-08 10:33:54'),
(18, 16, 'Real estate', 'Real estate', '[\"targetBottom\"]', '#', 2, 1, 1, '2020-06-08 10:34:02', '2020-06-08 10:34:02'),
(19, NULL, 'Get Bravo app', 'Get Bravo app', '[\"targetFooter\"]', '#', 13, 1, 1, '2020-06-08 10:40:09', '2020-06-08 10:40:09'),
(20, NULL, 'Legal', 'Legal', '[\"targetFooter\"]', '#', 14, 1, 1, '2020-06-08 10:40:43', '2020-06-08 10:40:43'),
(21, 19, 'Learn more', 'Learn more', '[\"targetFooter\"]', '#', 1, 1, 1, '2020-06-08 10:41:04', '2020-06-08 10:41:04'),
(22, 19, 'Google play', 'Google play', '[\"targetFooter\"]', '#', 2, 1, 1, '2020-06-08 10:41:12', '2020-06-08 10:41:12'),
(23, 19, 'App store', 'App store', '[\"targetFooter\"]', '#', 3, 1, 1, '2020-06-08 10:41:21', '2020-06-08 10:41:21'),
(24, 20, 'Privacy policy', 'Privacy policy', '[\"targetFooter\"]', '#', 1, 1, 1, '2020-06-08 10:41:33', '2020-06-08 10:41:33'),
(25, 20, 'Terms of use', 'Terms of use', '[\"targetFooter\"]', '#', 2, 1, 1, '2020-06-08 10:41:45', '2020-06-08 10:41:45'),
(26, 20, 'Contest rules and regulations', 'Contest rules and regulations', '[\"targetFooter\"]', '#', 3, 1, 1, '2020-06-08 10:41:55', '2020-06-08 10:41:55'),
(27, 8, 'Press Center', '/', '[\"targetBottom\"]', NULL, 3, 1, 4, '2020-06-08 10:50:25', '2020-06-08 11:02:24'),
(28, 8, 'Recipes', NULL, '[\"targetBottom\"]', NULL, 4, 1, 4, '2020-06-08 10:50:51', '2020-06-08 10:57:53'),
(29, 8, 'Our Stories', '/', '[\"targetBottom\"]', NULL, 15, 1, 4, '2020-06-08 10:56:30', '2020-06-08 10:58:03'),
(30, 10, 'Local charity', NULL, '[\"targetBottom\"]', NULL, 3, 1, 4, '2020-06-08 11:09:03', '2020-06-08 11:09:03'),
(31, 10, 'Sustainability', NULL, '[\"targetBottom\"]', NULL, 4, 1, 4, '2020-06-08 11:09:28', '2020-06-08 11:09:28'),
(32, 10, 'Headquarters', NULL, '[\"targetBottom\"]', NULL, 5, 1, 4, '2020-06-08 11:09:51', '2020-06-08 11:09:51'),
(33, 10, 'Countries of operation', NULL, '[\"targetBottom\"]', NULL, 6, 1, 4, '2020-06-08 11:11:14', '2020-06-08 11:11:14'),
(34, 10, 'Compliance', NULL, '[\"targetBottom\"]', NULL, 7, 1, 4, '2020-06-08 11:12:53', '2020-06-08 11:12:53'),
(35, 13, 'Food safety', NULL, '[\"targetBottom\"]', NULL, 3, 1, 4, '2020-06-08 11:13:33', '2020-06-08 11:13:33'),
(36, 13, 'Product manuals', '/', '[\"targetBottom\"]', NULL, 4, 1, 4, '2020-06-08 11:14:16', '2020-06-08 11:14:16'),
(37, 13, 'Product videos', NULL, '[\"targetBottom\"]', NULL, 5, 1, 4, '2020-06-08 11:14:40', '2020-06-08 11:14:40'),
(38, 16, 'FAQ', NULL, '[\"targetBottom\"]', NULL, 3, 1, 4, '2020-06-08 11:15:11', '2020-06-08 11:15:11'),
(39, 16, 'Product recalls', NULL, '[\"targetBottom\"]', NULL, 4, 1, 4, '2020-06-08 11:15:45', '2020-06-08 11:16:24'),
(40, NULL, 'Job Opportunities', NULL, '[\"targetCareersTop\"]', NULL, 1, 1, 4, '2020-06-12 10:57:38', '2020-06-12 10:57:38'),
(41, NULL, 'PRAKTIKUM Summer Internship Program', NULL, '[\"targetCareersTop\"]', NULL, 2, 1, 4, '2020-06-12 10:57:54', '2020-06-12 10:57:54'),
(42, NULL, 'Other projects', NULL, '[\"targetCareersTop\"]', NULL, 3, 1, 4, '2020-06-12 10:58:16', '2020-06-12 10:58:16');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(11, '2020_02_28_124337_create_skills_table', 1),
(28, '2014_10_12_000000_create_users_table', 2),
(29, '2014_10_12_100000_create_password_resets_table', 2),
(30, '2017_05_09_070343_create_media_tables', 2),
(31, '2019_08_19_000000_create_failed_jobs_table', 2),
(32, '2020_01_29_093707_create_translation_groups_table', 2),
(33, '2020_01_29_093708_create_categories_table', 2),
(34, '2020_02_26_130934_laratrust_setup_tables', 2),
(35, '2020_02_26_133237_create_translations_table', 2),
(36, '2020_02_26_133801_create_settings_table', 2),
(37, '2020_02_27_093415_create_languages_table', 2),
(38, '2020_04_02_141016_create_menus_table', 2),
(41, '2020_05_29_111555_create_posts_table', 3),
(55, '2020_06_09_063721_create_sliders_table', 4),
(58, '2020_06_10_091632_create_offers_table', 5),
(61, '2020_06_09_063732_create_banners_table', 6),
(67, '2020_06_07_133105_create_pages_table', 7);

-- --------------------------------------------------------

--
-- Table structure for table `offers`
--

CREATE TABLE `offers` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `parent_id` bigint(20) UNSIGNED DEFAULT NULL,
  `media_id` int(10) UNSIGNED DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `text` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `target` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `author_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `date` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `offers`
--

INSERT INTO `offers` (`id`, `parent_id`, `media_id`, `title`, `text`, `target`, `order`, `status`, `author_id`, `created_at`, `updated_at`, `date`) VALUES
(42, NULL, NULL, 'Novruz bayram', 'Novruz bayram is coming', NULL, 1, 1, 4, '2020-06-11 08:28:26', '2020-06-11 13:29:07', '2020-06-10 20:00:00'),
(47, NULL, NULL, 'Easter Feasts', 'Easter Feasts', NULL, 4, 1, 4, '2020-06-11 08:29:01', '2020-06-11 13:29:20', '2020-06-18 20:00:00'),
(52, NULL, NULL, 'Easter Treats', 'Easter Treats', NULL, 5, 1, 4, '2020-06-11 08:29:43', '2020-06-11 13:29:29', '2020-06-29 20:00:00'),
(57, NULL, NULL, 'Garden Event', 'Garden Event', NULL, 6, 1, 4, '2020-06-11 08:30:05', '2020-06-11 13:29:45', '2020-06-14 20:00:00'),
(74, 42, 26, '', '', NULL, 0, 1, 4, '2020-06-11 13:29:07', '2020-06-11 13:29:07', NULL),
(75, 42, 27, '', '', NULL, 1, 1, 4, '2020-06-11 13:29:07', '2020-06-11 13:29:07', NULL),
(76, 42, 28, '', '', NULL, 2, 1, 4, '2020-06-11 13:29:07', '2020-06-11 13:29:07', NULL),
(77, 42, 29, '', '', NULL, 3, 1, 4, '2020-06-11 13:29:07', '2020-06-11 13:29:07', NULL),
(78, 47, 26, '', '', NULL, 0, 1, 4, '2020-06-11 13:29:20', '2020-06-11 13:29:20', NULL),
(79, 47, 27, '', '', NULL, 1, 1, 4, '2020-06-11 13:29:20', '2020-06-11 13:29:20', NULL),
(80, 47, 28, '', '', NULL, 2, 1, 4, '2020-06-11 13:29:20', '2020-06-11 13:29:20', NULL),
(81, 47, 29, '', '', NULL, 3, 1, 4, '2020-06-11 13:29:20', '2020-06-11 13:29:20', NULL),
(82, 52, 26, '', '', NULL, 0, 1, 4, '2020-06-11 13:29:29', '2020-06-11 13:29:29', NULL),
(83, 52, 27, '', '', NULL, 1, 1, 4, '2020-06-11 13:29:29', '2020-06-11 13:29:29', NULL),
(84, 52, 28, '', '', NULL, 2, 1, 4, '2020-06-11 13:29:29', '2020-06-11 13:29:29', NULL),
(85, 52, 29, '', '', NULL, 3, 1, 4, '2020-06-11 13:29:29', '2020-06-11 13:29:29', NULL),
(86, 57, 26, '', '', NULL, 0, 1, 4, '2020-06-11 13:29:45', '2020-06-11 13:29:45', NULL),
(87, 57, 27, '', '', NULL, 1, 1, 4, '2020-06-11 13:29:45', '2020-06-11 13:29:45', NULL),
(88, 57, 28, '', '', NULL, 2, 1, 4, '2020-06-11 13:29:45', '2020-06-11 13:29:45', NULL),
(89, 57, 29, '', '', NULL, 3, 1, 4, '2020-06-11 13:29:45', '2020-06-11 13:29:45', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE `pages` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `page_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `locale` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `menu_id` bigint(20) UNSIGNED DEFAULT NULL,
  `author_id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8mb4_unicode_ci,
  `media` text COLLATE utf8mb4_unicode_ci,
  `template` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date` timestamp NULL DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`id`, `page_id`, `locale`, `menu_id`, `author_id`, `title`, `slug`, `content`, `media`, `template`, `date`, `status`, `created_at`, `updated_at`) VALUES
(1, '5ee3948f1c09d', 'az', 1, 4, 'Haqqımızda', 'haqqimizda', '<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"439\">\r\n	<tbody>\r\n		<tr height=\"20\">\r\n			<td height=\"20\" width=\"439\">&nbsp;</td>\r\n		</tr>\r\n		<tr height=\"21\">\r\n			<td height=\"21\" width=\"439\"><span style=\"color:#1ABC9C;\"><strong>Vibrant comfortable store environment</strong></span></td>\r\n		</tr>\r\n		<tr height=\"20\">\r\n			<td height=\"20\" width=\"439\">Innovative store design</td>\r\n		</tr>\r\n		<tr height=\"20\">\r\n			<td height=\"20\" width=\"439\">Wide spacious aisles</td>\r\n		</tr>\r\n		<tr height=\"20\">\r\n			<td height=\"20\" width=\"439\">Modern wide cash desks to enhance the checkout experience</td>\r\n		</tr>\r\n		<tr height=\"20\">\r\n			<td height=\"20\" style=\"height: 30px;\" width=\"439\">&nbsp;</td>\r\n		</tr>\r\n		<tr height=\"21\">\r\n			<td height=\"21\" width=\"439\"><strong><span style=\"color:#1ABC9C;\">Excellence in store standards</span></strong></td>\r\n		</tr>\r\n		<tr height=\"38\">\r\n			<td height=\"38\" width=\"439\">Impactful displays of well merchandised products with the highest quality and freshness</td>\r\n		</tr>\r\n		<tr height=\"20\">\r\n			<td height=\"20\" width=\"439\">Well organized displays with full availability of stock</td>\r\n		</tr>\r\n		<tr height=\"20\">\r\n			<td height=\"20\" width=\"439\">Trusted for hygiene and food safety</td>\r\n		</tr>\r\n		<tr height=\"20\">\r\n			<td height=\"20\" width=\"439\">Clear promotional and price message</td>\r\n		</tr>\r\n		<tr height=\"21\">\r\n			<td height=\"21\" width=\"439\">&nbsp;</td>\r\n		</tr>\r\n		<tr height=\"20\">\r\n			<td height=\"20\" width=\"439\"><span style=\"color:#1ABC9C;\"><strong>Best in service</strong></span></td>\r\n		</tr>\r\n		<tr height=\"38\">\r\n			<td height=\"38\" width=\"439\"><font>Knowledgeable team members trained and motivated to give great service</font><font>&nbsp;</font></td>\r\n		</tr>\r\n		<tr height=\"20\">\r\n			<td height=\"20\" width=\"439\">Always happy to help</td>\r\n		</tr>\r\n	</tbody>\r\n</table>', '[\"56\"]', 'about', '2020-06-12 14:34:14', 1, '2020-06-12 10:43:27', '2020-06-12 10:43:27'),
(2, '5ee3948f1c09d', 'en', 1, 4, 'About Us', 'about-us', '<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"439\">\r\n	<tbody>\r\n		<tr height=\"20\">\r\n			<td height=\"20\" width=\"439\">&nbsp;</td>\r\n		</tr>\r\n		<tr height=\"21\">\r\n			<td height=\"21\" width=\"439\"><span style=\"color:#1ABC9C;\"><strong>Vibrant comfortable store environment</strong></span></td>\r\n		</tr>\r\n		<tr height=\"20\">\r\n			<td height=\"20\" width=\"439\">Innovative store design</td>\r\n		</tr>\r\n		<tr height=\"20\">\r\n			<td height=\"20\" width=\"439\">Wide spacious aisles</td>\r\n		</tr>\r\n		<tr height=\"20\">\r\n			<td height=\"20\" width=\"439\">Modern wide cash desks to enhance the checkout experience</td>\r\n		</tr>\r\n		<tr height=\"20\">\r\n			<td height=\"20\" style=\"height: 30px;\" width=\"439\">&nbsp;</td>\r\n		</tr>\r\n		<tr height=\"21\">\r\n			<td height=\"21\" width=\"439\"><strong><span style=\"color:#1ABC9C;\">Excellence in store standards</span></strong></td>\r\n		</tr>\r\n		<tr height=\"38\">\r\n			<td height=\"38\" width=\"439\">Impactful displays of well merchandised products with the highest quality and freshness</td>\r\n		</tr>\r\n		<tr height=\"20\">\r\n			<td height=\"20\" width=\"439\">Well organized displays with full availability of stock</td>\r\n		</tr>\r\n		<tr height=\"20\">\r\n			<td height=\"20\" width=\"439\">Trusted for hygiene and food safety</td>\r\n		</tr>\r\n		<tr height=\"20\">\r\n			<td height=\"20\" width=\"439\">Clear promotional and price message</td>\r\n		</tr>\r\n		<tr height=\"21\">\r\n			<td height=\"21\" width=\"439\">&nbsp;</td>\r\n		</tr>\r\n		<tr height=\"20\">\r\n			<td height=\"20\" width=\"439\"><span style=\"color:#1ABC9C;\"><strong>Best in service</strong></span></td>\r\n		</tr>\r\n		<tr height=\"38\">\r\n			<td height=\"38\" width=\"439\"><font>Knowledgeable team members trained and motivated to give great service</font><font>&nbsp;</font></td>\r\n		</tr>\r\n		<tr height=\"20\">\r\n			<td height=\"20\" width=\"439\">Always happy to help</td>\r\n		</tr>\r\n	</tbody>\r\n</table>', '[\"56\"]', 'about', '2020-06-12 14:34:14', 1, '2020-06-12 10:43:27', '2020-06-12 10:43:27'),
(3, '5ee3956ab1024', 'az', 6, 4, 'Kartlar', 'kartlar', '', '[\"45\"]', 'cards', '2020-06-12 14:44:48', 1, '2020-06-12 10:47:06', '2020-06-12 10:47:06'),
(4, '5ee3956ab1024', 'en', 6, 4, 'Cards', 'cards', '', '[\"45\"]', 'cards', '2020-06-12 14:44:48', 1, '2020-06-12 10:47:06', '2020-06-12 10:47:06'),
(5, '5ee395f39cda2', 'az', 3, 4, 'Karyera', 'karyera', '', '[\"46\"]', 'careers', '2020-06-12 14:47:36', 1, '2020-06-12 10:49:23', '2020-06-12 10:49:23'),
(6, '5ee395f39cda2', 'en', 3, 4, 'Careers', 'careers', '', '[\"46\"]', 'careers', '2020-06-12 14:47:36', 1, '2020-06-12 10:49:23', '2020-06-12 10:49:23'),
(7, '5ee396d99d9d4', 'az', 2, 4, 'Əlaqə', 'elaqe', '', '[\"47\"]', 'contacts', '2020-06-12 14:50:04', 1, '2020-06-12 10:53:13', '2020-06-12 10:53:13'),
(8, '5ee396d99d9d4', 'en', 2, 4, 'Contact us', 'contact-us', '', '[\"47\"]', 'contacts', '2020-06-12 14:50:04', 1, '2020-06-12 10:53:13', '2020-06-12 10:53:13'),
(9, '5ee39b4d8869b', 'az', 7, 4, 'KSM fəaliyyətləri', 'ksm-fealiyyetleri', '', '[\"48\"]', 'csr_activities', '2020-06-12 15:10:28', 1, '2020-06-12 11:12:13', '2020-06-12 11:12:13'),
(10, '5ee39b4d8869b', 'en', 7, 4, 'CSR Activities', 'csr-activities', '', '[\"48\"]', 'csr_activities', '2020-06-12 15:10:28', 1, '2020-06-12 11:12:13', '2020-06-12 11:12:13'),
(11, '5ee3a051f039f', 'az', 4, 4, 'Müştəri xidməti', 'musteri-xidmeti', '', '[\"49\"]', 'customer_services', '2020-06-12 15:31:53', 1, '2020-06-12 11:33:37', '2020-06-12 11:33:37'),
(12, '5ee3a051f039f', 'en', 4, 4, 'Customer Service', 'customer-service', '', '[\"49\"]', 'customer_services', '2020-06-12 15:31:53', 1, '2020-06-12 11:33:37', '2020-06-12 11:33:37'),
(13, '5ee3a30890035', 'az', 5, 4, 'Buklet', 'buklet', '', '[\"50\"]', 'leaflet', '2020-06-12 15:39:12', 1, '2020-06-12 11:45:12', '2020-06-12 11:45:12'),
(14, '5ee3a30890035', 'en', 5, 4, 'Leaflet', 'leaflet', '', '[\"50\"]', 'leaflet', '2020-06-12 15:39:12', 1, '2020-06-12 11:45:12', '2020-06-12 11:45:12'),
(19, '5ee3b433bb0f9', 'az', NULL, 4, 'Xüsusi təkliflər', 'xususi-teklifler', '', '[\"55\"]', 'offers', '2020-06-12 16:55:28', 1, '2020-06-12 12:58:27', '2020-06-12 12:58:27'),
(20, '5ee3b433bb0f9', 'en', NULL, 4, 'Special Offers', 'special-offers', '', '[\"55\"]', 'offers', '2020-06-12 16:55:28', 1, '2020-06-12 12:58:27', '2020-06-12 12:58:27');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `module` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `display_name`, `description`, `module`, `created_at`, `updated_at`) VALUES
(1, 'create-categories', 'Create Categories', 'Create Categories', 'categories', '2020-06-08 08:07:47', '2020-06-08 08:07:47'),
(2, 'read-categories', 'Read Categories', 'Read Categories', 'categories', '2020-06-08 08:07:47', '2020-06-08 08:07:47'),
(3, 'update-categories', 'Update Categories', 'Update Categories', 'categories', '2020-06-08 08:07:47', '2020-06-08 08:07:47'),
(4, 'delete-categories', 'Delete Categories', 'Delete Categories', 'categories', '2020-06-08 08:07:47', '2020-06-08 08:07:47'),
(5, 'create-languages', 'Create Languages', 'Create Languages', 'languages', '2020-06-08 08:07:47', '2020-06-08 08:07:47'),
(6, 'read-languages', 'Read Languages', 'Read Languages', 'languages', '2020-06-08 08:07:47', '2020-06-08 08:07:47'),
(7, 'update-languages', 'Update Languages', 'Update Languages', 'languages', '2020-06-08 08:07:47', '2020-06-08 08:07:47'),
(8, 'delete-languages', 'Delete Languages', 'Delete Languages', 'languages', '2020-06-08 08:07:47', '2020-06-08 08:07:47'),
(9, 'create-products', 'Create Products', 'Create Products', 'products', '2020-06-08 08:07:47', '2020-06-08 08:07:47'),
(10, 'read-products', 'Read Products', 'Read Products', 'products', '2020-06-08 08:07:47', '2020-06-08 08:07:47'),
(11, 'update-products', 'Update Products', 'Update Products', 'products', '2020-06-08 08:07:47', '2020-06-08 08:07:47'),
(12, 'delete-products', 'Delete Products', 'Delete Products', 'products', '2020-06-08 08:07:47', '2020-06-08 08:07:47'),
(13, 'create-roles', 'Create Roles', 'Create Roles', 'roles', '2020-06-08 08:07:47', '2020-06-08 08:07:47'),
(14, 'read-roles', 'Read Roles', 'Read Roles', 'roles', '2020-06-08 08:07:47', '2020-06-08 08:07:47'),
(15, 'update-roles', 'Update Roles', 'Update Roles', 'roles', '2020-06-08 08:07:47', '2020-06-08 08:07:47'),
(16, 'delete-roles', 'Delete Roles', 'Delete Roles', 'roles', '2020-06-08 08:07:47', '2020-06-08 08:07:47'),
(17, 'create-permissions', 'Create Permissions', 'Create Permissions', 'permissions', '2020-06-08 08:07:47', '2020-06-08 08:07:47'),
(18, 'read-permissions', 'Read Permissions', 'Read Permissions', 'permissions', '2020-06-08 08:07:47', '2020-06-08 08:07:47'),
(19, 'update-permissions', 'Update Permissions', 'Update Permissions', 'permissions', '2020-06-08 08:07:47', '2020-06-08 08:07:47'),
(20, 'delete-permissions', 'Delete Permissions', 'Delete Permissions', 'permissions', '2020-06-08 08:07:47', '2020-06-08 08:07:47'),
(21, 'create-settings', 'Create Settings', 'Create Settings', 'settings', '2020-06-08 08:07:47', '2020-06-08 08:07:47'),
(22, 'read-settings', 'Read Settings', 'Read Settings', 'settings', '2020-06-08 08:07:47', '2020-06-08 08:07:47'),
(23, 'update-settings', 'Update Settings', 'Update Settings', 'settings', '2020-06-08 08:07:47', '2020-06-08 08:07:47'),
(24, 'delete-settings', 'Delete Settings', 'Delete Settings', 'settings', '2020-06-08 08:07:47', '2020-06-08 08:07:47'),
(25, 'create-skills', 'Create Skills', 'Create Skills', 'skills', '2020-06-08 08:07:47', '2020-06-08 08:07:47'),
(26, 'read-skills', 'Read Skills', 'Read Skills', 'skills', '2020-06-08 08:07:47', '2020-06-08 08:07:47'),
(27, 'update-skills', 'Update Skills', 'Update Skills', 'skills', '2020-06-08 08:07:47', '2020-06-08 08:07:47'),
(28, 'delete-skills', 'Delete Skills', 'Delete Skills', 'skills', '2020-06-08 08:07:47', '2020-06-08 08:07:47'),
(29, 'create-translations', 'Create Translations', 'Create Translations', 'translations', '2020-06-08 08:07:47', '2020-06-08 08:07:47'),
(30, 'read-translations', 'Read Translations', 'Read Translations', 'translations', '2020-06-08 08:07:47', '2020-06-08 08:07:47'),
(31, 'update-translations', 'Update Translations', 'Update Translations', 'translations', '2020-06-08 08:07:47', '2020-06-08 08:07:47'),
(32, 'delete-translations', 'Delete Translations', 'Delete Translations', 'translations', '2020-06-08 08:07:47', '2020-06-08 08:07:47'),
(33, 'export-translations', 'Export Translations', 'Export Translations', 'translations', '2020-06-08 08:07:47', '2020-06-08 08:07:47'),
(34, 'create-translation-groups', 'Create Translation-groups', 'Create Translation-groups', 'translation-groups', '2020-06-08 08:07:47', '2020-06-08 08:07:47'),
(35, 'read-translation-groups', 'Read Translation-groups', 'Read Translation-groups', 'translation-groups', '2020-06-08 08:07:47', '2020-06-08 08:07:47'),
(36, 'update-translation-groups', 'Update Translation-groups', 'Update Translation-groups', 'translation-groups', '2020-06-08 08:07:47', '2020-06-08 08:07:47'),
(37, 'delete-translation-groups', 'Delete Translation-groups', 'Delete Translation-groups', 'translation-groups', '2020-06-08 08:07:47', '2020-06-08 08:07:47'),
(38, 'create-users', 'Create Users', 'Create Users', 'users', '2020-06-08 08:07:47', '2020-06-08 08:07:47'),
(39, 'read-users', 'Read Users', 'Read Users', 'users', '2020-06-08 08:07:47', '2020-06-08 08:07:47'),
(40, 'update-users', 'Update Users', 'Update Users', 'users', '2020-06-08 08:07:47', '2020-06-08 08:07:47'),
(41, 'delete-users', 'Delete Users', 'Delete Users', 'users', '2020-06-08 08:07:47', '2020-06-08 08:07:47'),
(42, 'create-menus', 'Create Menus', 'Create Menus', 'menus', '2020-06-08 08:07:47', '2020-06-08 08:07:47'),
(43, 'read-menus', 'Read Menus', 'Read Menus', 'menus', '2020-06-08 08:07:47', '2020-06-08 08:07:47'),
(44, 'update-menus', 'Update Menus', 'Update Menus', 'menus', '2020-06-08 08:07:47', '2020-06-08 08:07:47'),
(45, 'delete-menus', 'Delete Menus', 'Delete Menus', 'menus', '2020-06-08 08:07:47', '2020-06-08 08:07:47'),
(46, 'create-pages', 'Create Pages', 'Create Pages', 'pages', '2020-06-08 08:07:47', '2020-06-08 08:07:47'),
(47, 'read-pages', 'Read Pages', 'Read Pages', 'pages', '2020-06-08 08:07:47', '2020-06-08 08:07:47'),
(48, 'update-pages', 'Update Pages', 'Update Pages', 'pages', '2020-06-08 08:07:47', '2020-06-08 08:07:47'),
(49, 'delete-pages', 'Delete Pages', 'Delete Pages', 'pages', '2020-06-08 08:07:47', '2020-06-08 08:07:47'),
(50, 'create-posts', 'Create Posts', 'Create Posts', 'posts', '2020-06-08 08:07:47', '2020-06-08 08:07:47'),
(51, 'read-posts', 'Read Posts', 'Read Posts', 'posts', '2020-06-08 08:07:47', '2020-06-08 08:07:47'),
(52, 'update-posts', 'Update Posts', 'Update Posts', 'posts', '2020-06-08 08:07:47', '2020-06-08 08:07:47'),
(53, 'delete-posts', 'Delete Posts', 'Delete Posts', 'posts', '2020-06-08 08:07:47', '2020-06-08 08:07:47'),
(54, 'read-media', 'Read Media', 'Read Media', 'media', '2020-06-08 08:07:47', '2020-06-08 08:07:47'),
(55, 'read-profile', 'Read Profile', 'Read Profile', 'profile', '2020-06-08 08:07:47', '2020-06-08 08:07:47'),
(56, 'update-profile', 'Update Profile', 'Update Profile', 'profile', '2020-06-08 08:07:47', '2020-06-08 08:07:47'),
(57, 'create-sliders', 'Create Sliders', 'Create Sliders', 'sliders', '2020-06-09 03:57:04', '2020-06-09 03:57:04'),
(58, 'read-sliders', 'Read Sliders', 'Read Sliders', 'sliders', '2020-06-09 03:57:12', '2020-06-09 03:57:12'),
(59, 'update-sliders', 'Update Sliders', 'Update Sliders', 'sliders', '2020-06-09 03:57:21', '2020-06-09 03:57:21'),
(60, 'delete-sliders', 'Delete Sliders', 'Delete Sliders', 'sliders', '2020-06-09 03:57:32', '2020-06-09 03:57:32'),
(61, 'create-banners', 'Create Banners', 'Create Banners', 'banners', '2020-06-09 03:57:53', '2020-06-09 03:57:53'),
(62, 'read-banners', 'Read Banners', 'Read Banners', 'banners', '2020-06-09 03:58:01', '2020-06-09 03:58:01'),
(63, 'update-banners', 'Update Banners', 'Update Banners', 'banners', '2020-06-09 03:58:11', '2020-06-09 03:58:11'),
(64, 'delete-banners', 'Delete Banners', 'Delete Banners', 'banners', '2020-06-09 03:58:19', '2020-06-09 03:58:19'),
(65, 'create-offers', 'Create Offers', 'Create Offers', 'offers', '2020-06-10 05:44:19', '2020-06-10 05:44:19'),
(66, 'read-offers', 'Read Offers', 'Read Offers', 'offers', '2020-06-10 05:44:28', '2020-06-10 05:44:28'),
(67, 'update-offers', 'Update Offers', 'Update Offers', 'offers', '2020-06-10 05:44:37', '2020-06-10 05:45:00'),
(68, 'delete-offers', 'Delete Offers', 'Delete Offers', 'offers', '2020-06-10 05:44:48', '2020-06-10 05:44:48');

-- --------------------------------------------------------

--
-- Table structure for table `permission_role`
--

CREATE TABLE `permission_role` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permission_role`
--

INSERT INTO `permission_role` (`permission_id`, `role_id`) VALUES
(1, 1),
(2, 1),
(3, 1),
(4, 1),
(5, 1),
(6, 1),
(7, 1),
(8, 1),
(9, 1),
(10, 1),
(11, 1),
(12, 1),
(13, 1),
(14, 1),
(15, 1),
(16, 1),
(17, 1),
(18, 1),
(19, 1),
(20, 1),
(21, 1),
(22, 1),
(23, 1),
(24, 1),
(25, 1),
(26, 1),
(27, 1),
(28, 1),
(29, 1),
(30, 1),
(31, 1),
(32, 1),
(33, 1),
(34, 1),
(35, 1),
(36, 1),
(37, 1),
(38, 1),
(39, 1),
(40, 1),
(41, 1),
(42, 1),
(43, 1),
(44, 1),
(45, 1),
(46, 1),
(47, 1),
(48, 1),
(49, 1),
(50, 1),
(51, 1),
(52, 1),
(53, 1),
(54, 1),
(55, 1),
(56, 1),
(57, 1),
(58, 1),
(59, 1),
(60, 1),
(61, 1),
(62, 1),
(63, 1),
(64, 1),
(65, 1),
(66, 1),
(67, 1),
(68, 1),
(1, 2),
(2, 2),
(3, 2),
(5, 2),
(6, 2),
(7, 2),
(9, 2),
(10, 2),
(11, 2),
(22, 2),
(25, 2),
(26, 2),
(27, 2),
(28, 2),
(29, 2),
(30, 2),
(31, 2),
(32, 2),
(33, 2),
(34, 2),
(35, 2),
(36, 2),
(37, 2),
(38, 2),
(39, 2),
(40, 2),
(42, 2),
(43, 2),
(44, 2),
(46, 2),
(47, 2),
(48, 2),
(50, 2),
(51, 2),
(52, 2),
(54, 2),
(55, 2),
(56, 2),
(57, 2),
(58, 2),
(59, 2),
(61, 2),
(62, 2),
(63, 2),
(9, 3),
(10, 3),
(11, 3),
(12, 3),
(25, 3),
(26, 3),
(27, 3),
(28, 3),
(29, 3),
(30, 3),
(31, 3),
(32, 3),
(33, 3),
(34, 3),
(35, 3),
(36, 3),
(37, 3),
(43, 3),
(47, 3),
(50, 3),
(51, 3),
(52, 3),
(54, 3),
(55, 3),
(56, 3),
(58, 3),
(59, 3),
(62, 3),
(63, 3);

-- --------------------------------------------------------

--
-- Table structure for table `permission_user`
--

CREATE TABLE `permission_user` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `user_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `post_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `locale` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `category_id` bigint(20) UNSIGNED NOT NULL,
  `author_id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8mb4_unicode_ci,
  `media` text COLLATE utf8mb4_unicode_ci,
  `date` timestamp NULL DEFAULT NULL,
  `read` bigint(20) UNSIGNED NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `display_name`, `description`, `created_at`, `updated_at`) VALUES
(1, 'owner', 'Owner', 'Owner', '2020-06-08 08:07:47', '2020-06-08 08:07:47'),
(2, 'admin', 'Admin', 'Admin', '2020-06-08 08:07:47', '2020-06-08 08:07:47'),
(3, 'operator', 'Operator', 'Operator', '2020-06-08 08:07:48', '2020-06-08 08:07:48');

-- --------------------------------------------------------

--
-- Table structure for table `role_user`
--

CREATE TABLE `role_user` (
  `role_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `user_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `role_user`
--

INSERT INTO `role_user` (`role_id`, `user_id`, `user_type`) VALUES
(1, 1, 'App\\User'),
(1, 4, 'App\\User');

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `skills`
--

CREATE TABLE `skills` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `sliders`
--

CREATE TABLE `sliders` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `parent_id` bigint(20) UNSIGNED DEFAULT NULL,
  `media_id` int(10) UNSIGNED DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `target` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `author_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sliders`
--

INSERT INTO `sliders` (`id`, `parent_id`, `media_id`, `title`, `target`, `status`, `author_id`, `created_at`, `updated_at`) VALUES
(155, NULL, NULL, 'homepage slider', 'noTarget', 1, 4, '2020-06-11 07:54:52', '2020-06-12 10:55:01'),
(184, 155, 35, 'Supermanat', NULL, 1, 4, '2020-06-12 10:55:01', '2020-06-12 10:55:01'),
(185, 155, 36, 'Bravomarket 24/7', NULL, 1, 4, '2020-06-12 10:55:01', '2020-06-12 10:55:01');

-- --------------------------------------------------------

--
-- Table structure for table `translations`
--

CREATE TABLE `translations` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `group_id` bigint(20) UNSIGNED NOT NULL,
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `translations`
--

INSERT INTO `translations` (`id`, `group_id`, `key`, `value`, `created_at`, `updated_at`) VALUES
(1, 1, 'dashboard', '{\"az\":\"Panel\"}', '2020-06-08 08:07:48', '2020-06-08 08:07:48'),
(2, 1, 'modules', '{\"az\":\"Modullar\"}', '2020-06-08 08:07:48', '2020-06-08 08:07:48'),
(3, 1, 'menus', '{\"az\":\"Menyular\"}', '2020-06-08 08:07:48', '2020-06-08 08:07:48'),
(4, 1, 'portfolio', '{\"az\":\"Portfel\"}', '2020-06-08 08:07:48', '2020-06-08 08:07:48'),
(5, 1, 'products', '{\"az\":\"Məhsullar\"}', '2020-06-08 08:07:48', '2020-06-08 08:07:48'),
(6, 1, 'categories', '{\"az\":\"Kateqoriyalar\"}', '2020-06-08 08:07:48', '2020-06-08 08:07:48'),
(7, 1, 'users', '{\"az\":\"İstifadəçilər\"}', '2020-06-08 08:07:48', '2020-06-08 08:07:48'),
(8, 1, 'roles', '{\"az\":\"Vəzifələr\"}', '2020-06-08 08:07:48', '2020-06-08 08:07:48'),
(9, 1, 'permissions', '{\"az\":\"İcazələr\"}', '2020-06-08 08:07:48', '2020-06-08 08:07:48'),
(10, 1, 'localization', '{\"az\":\"Lokalizasiya\"}', '2020-06-08 08:07:48', '2020-06-08 08:07:48'),
(11, 1, 'languages', '{\"az\":\"Dillər\"}', '2020-06-08 08:07:48', '2020-06-08 08:07:48'),
(12, 1, 'translation_groups', '{\"az\":\"Tərcümə qrupları\"}', '2020-06-08 08:07:48', '2020-06-08 08:07:48'),
(13, 1, 'translations', '{\"az\":\"Tərcümələr\"}', '2020-06-08 08:07:48', '2020-06-08 08:07:48'),
(14, 1, 'settings', '{\"az\":\"Tənzimləmələr\"}', '2020-06-08 08:07:48', '2020-06-08 08:07:48'),
(15, 1, 'config', '{\"az\":\"Konfiqurasiya\"}', '2020-06-08 08:07:48', '2020-06-08 08:07:48'),
(16, 1, 'site', '{\"az\":\"Sayt\"}', '2020-06-08 08:07:48', '2020-06-08 08:07:48'),
(17, 2, 'main', '{\"az\":\"Əsas\"}', '2020-06-08 08:07:48', '2020-06-08 08:07:48'),
(18, 2, 'other', '{\"az\":\"Digər\"}', '2020-06-08 08:07:48', '2020-06-08 08:07:48');

-- --------------------------------------------------------

--
-- Table structure for table `translation_groups`
--

CREATE TABLE `translation_groups` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `translation_groups`
--

INSERT INTO `translation_groups` (`id`, `name`, `description`, `status`, `created_at`, `updated_at`) VALUES
(1, 'navigation', 'Menu Group - created by Pashayev panel', 1, '2020-06-08 08:07:48', '2020-06-08 08:07:48'),
(2, 'menuLabels', 'Menu Labels Group - created by Pashayev panel', 1, '2020-06-08 08:07:48', '2020-06-08 08:07:48');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `info` text COLLATE utf8mb4_unicode_ci,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `info`, `status`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Fuad Pashayev', 'fuad@pashayev.info', '$2y$10$xiSaeJFbLzjNeuroFCrODOp..vftMoTm8.qdSez4LbcMDbA3qU/aK', '{\"address\":\"Baku, Azerbaijan\"}', 1, NULL, '2020-06-08 08:07:47', '2020-06-08 08:07:47'),
(4, 'Gamidov Studio', 'dev@gamidov.com', '$2y$10$hJqrwq1lJ.U.89jF3tnnMedTRgaOyUVOw1k2zwncwKq5OPhPrmeX.', '{\"address\":\"Baku, Azerbaijan\"}', 1, NULL, '2020-06-08 08:42:07', '2020-06-08 08:42:07');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `banners`
--
ALTER TABLE `banners`
  ADD PRIMARY KEY (`id`),
  ADD KEY `banners_author_id_foreign` (`author_id`),
  ADD KEY `banners_media_id_foreign` (`media_id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `categories_parent_id_foreign` (`parent_id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `languages`
--
ALTER TABLE `languages`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `languages_key_unique` (`key`);

--
-- Indexes for table `media_files`
--
ALTER TABLE `media_files`
  ADD PRIMARY KEY (`id`),
  ADD KEY `media_files_user_id_index` (`user_id`);

--
-- Indexes for table `media_folders`
--
ALTER TABLE `media_folders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `media_folders_user_id_index` (`user_id`);

--
-- Indexes for table `media_settings`
--
ALTER TABLE `media_settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menus`
--
ALTER TABLE `menus`
  ADD PRIMARY KEY (`id`),
  ADD KEY `menus_author_id_foreign` (`author_id`),
  ADD KEY `menus_parent_id_foreign` (`parent_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `offers`
--
ALTER TABLE `offers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `offers_author_id_foreign` (`author_id`),
  ADD KEY `offers_media_id_foreign` (`media_id`),
  ADD KEY `offers_parent_id_foreign` (`parent_id`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `pages_slug_unique` (`slug`),
  ADD KEY `pages_author_id_foreign` (`author_id`),
  ADD KEY `pages_menu_id_foreign` (`menu_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `permissions_name_unique` (`name`);

--
-- Indexes for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `permission_role_role_id_foreign` (`role_id`);

--
-- Indexes for table `permission_user`
--
ALTER TABLE `permission_user`
  ADD PRIMARY KEY (`user_id`,`permission_id`,`user_type`),
  ADD KEY `permission_user_permission_id_foreign` (`permission_id`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `posts_slug_unique` (`slug`),
  ADD KEY `posts_author_id_foreign` (`author_id`),
  ADD KEY `posts_category_id_foreign` (`category_id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_name_unique` (`name`);

--
-- Indexes for table `role_user`
--
ALTER TABLE `role_user`
  ADD PRIMARY KEY (`user_id`,`role_id`,`user_type`),
  ADD KEY `role_user_role_id_foreign` (`role_id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `skills`
--
ALTER TABLE `skills`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sliders`
--
ALTER TABLE `sliders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sliders_author_id_foreign` (`author_id`),
  ADD KEY `sliders_media_id_foreign` (`media_id`),
  ADD KEY `sliders_parent_id_foreign` (`parent_id`);

--
-- Indexes for table `translations`
--
ALTER TABLE `translations`
  ADD PRIMARY KEY (`id`),
  ADD KEY `translations_group_id_foreign` (`group_id`);

--
-- Indexes for table `translation_groups`
--
ALTER TABLE `translation_groups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `translation_groups_name_unique` (`name`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `banners`
--
ALTER TABLE `banners`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `languages`
--
ALTER TABLE `languages`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `media_files`
--
ALTER TABLE `media_files`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=59;
--
-- AUTO_INCREMENT for table `media_folders`
--
ALTER TABLE `media_folders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `media_settings`
--
ALTER TABLE `media_settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `menus`
--
ALTER TABLE `menus`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=68;
--
-- AUTO_INCREMENT for table `offers`
--
ALTER TABLE `offers`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=90;
--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=69;
--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `skills`
--
ALTER TABLE `skills`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sliders`
--
ALTER TABLE `sliders`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=186;
--
-- AUTO_INCREMENT for table `translations`
--
ALTER TABLE `translations`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `translation_groups`
--
ALTER TABLE `translation_groups`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `banners`
--
ALTER TABLE `banners`
  ADD CONSTRAINT `banners_author_id_foreign` FOREIGN KEY (`author_id`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `banners_media_id_foreign` FOREIGN KEY (`media_id`) REFERENCES `media_files` (`id`);

--
-- Constraints for table `categories`
--
ALTER TABLE `categories`
  ADD CONSTRAINT `categories_parent_id_foreign` FOREIGN KEY (`parent_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `menus`
--
ALTER TABLE `menus`
  ADD CONSTRAINT `menus_author_id_foreign` FOREIGN KEY (`author_id`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `menus_parent_id_foreign` FOREIGN KEY (`parent_id`) REFERENCES `menus` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `offers`
--
ALTER TABLE `offers`
  ADD CONSTRAINT `offers_author_id_foreign` FOREIGN KEY (`author_id`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `offers_media_id_foreign` FOREIGN KEY (`media_id`) REFERENCES `media_files` (`id`),
  ADD CONSTRAINT `offers_parent_id_foreign` FOREIGN KEY (`parent_id`) REFERENCES `offers` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `pages`
--
ALTER TABLE `pages`
  ADD CONSTRAINT `pages_author_id_foreign` FOREIGN KEY (`author_id`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `pages_menu_id_foreign` FOREIGN KEY (`menu_id`) REFERENCES `menus` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `permission_user`
--
ALTER TABLE `permission_user`
  ADD CONSTRAINT `permission_user_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `posts`
--
ALTER TABLE `posts`
  ADD CONSTRAINT `posts_author_id_foreign` FOREIGN KEY (`author_id`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `posts_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `role_user`
--
ALTER TABLE `role_user`
  ADD CONSTRAINT `role_user_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `sliders`
--
ALTER TABLE `sliders`
  ADD CONSTRAINT `sliders_author_id_foreign` FOREIGN KEY (`author_id`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `sliders_media_id_foreign` FOREIGN KEY (`media_id`) REFERENCES `media_files` (`id`),
  ADD CONSTRAINT `sliders_parent_id_foreign` FOREIGN KEY (`parent_id`) REFERENCES `sliders` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `translations`
--
ALTER TABLE `translations`
  ADD CONSTRAINT `translations_group_id_foreign` FOREIGN KEY (`group_id`) REFERENCES `translation_groups` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
