<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTabsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tabs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('tab_id');
            $table->string('locale');
            $table->string('parent_id')->nullable();
            $table->bigInteger('author_id')->unsigned();
            $table->string('title')->nullable();
            $table->text('content')->nullable();
            $table->text('media')->nullable();
            $table->integer('order')->default(0);
            $table->string('form_template')->nullable();
            $table->string('form_position')->nullable();
            $table->string('group');
            $table->timestamp('date')->nullable();
            $table->boolean('status')->default(true);
            $table->timestamps();
            $table->foreign('author_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tabs');
    }
}
