<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBravoFormsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bravo_forms', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('form_type');
            $table->string('company_name')->nullable();
            $table->string('company_info')->nullable();
            $table->string('voen')->nullable();
            $table->string('region')->nullable();
            $table->string('subject')->nullable();
            $table->string('city')->nullable();
            $table->string('country')->nullable();
            $table->string('zip_code')->nullable();
            $table->string('product_family')->nullable();
            $table->string('email')->nullable();
            $table->string('phone')->nullable();
            $table->string('related_person')->nullable();
            $table->string('category')->nullable();
            $table->text('comment')->nullable();
            $table->text('address')->nullable();

            $table->string('firstname')->nullable();
            $table->string('lastname')->nullable();
            $table->string('date')->nullable();
            $table->string('request_type')->nullable();
            $table->string('store')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bravo_forms');
    }
}
