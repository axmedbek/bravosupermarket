<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOfferProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('offer_products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('offer_product_id');
            $table->string('locale');
            $table->string('title')->nullable();
            $table->string('text')->nullable();
            $table->integer('media_id')->unsigned()->nullable();
            $table->integer('order');
            $table->boolean('status')->default(true);
            $table->bigInteger('author_id')->unsigned();
            $table->timestamp('date')->nullable();
            $table->timestamps();

            $table->foreign('author_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('offer_products');
    }
}
