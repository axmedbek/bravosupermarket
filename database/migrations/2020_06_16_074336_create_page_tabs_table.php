<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePageTabsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('page_tabs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('tab_id');
            $table->string('page_id');
            $table->timestamps();

//            $table->foreign('tab_id')->references('id')->on('tabs');
//            $table->foreign('page_id')->references('id')->on('pages');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('page_tabs');
    }
}
