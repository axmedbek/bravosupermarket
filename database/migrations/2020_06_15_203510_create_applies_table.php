<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAppliesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('applies', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('vacancy_id');
            $table->string('name')->nullable();
            $table->string('phone')->nullable();
            $table->string('age')->default('18-25');
            $table->string('education_degree')->nullable(); // Təhsil
            $table->string('experience_store_management')->nullable(); //Mağaza idarə heyətində təcrübəm:
            $table->string('working_now')->nullable(); //Hal hazirda isleryisinizmi//  Hə / Yox
            $table->text('description')->nullable(); //Hal-hazırda və ya ən son çalışdığıınız vəzifə
            $table->string('from_who')->nullable(); //Bizi hardan duydunuz
            $table->string('file_first')->nullable();
            $table->string('file_second')->nullable();
            $table->timestamps();
            $table->foreign('vacancy_id')->references('id')->on('vacancies');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('applies');
    }
}
