<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBravoFormImagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bravo_form_images', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('bravo_form_id');
            $table->string('type')->nullable();
            $table->string('path');
            $table->string('file_type')->nullable();
            $table->timestamps();
            $table->foreign('bravo_form_id')->references('id')->on('bravo_forms')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bravo_form_images');
    }
}
