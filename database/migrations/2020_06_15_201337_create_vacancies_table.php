<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVacanciesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vacancies', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('author_id');
            $table->string('vacancy_id')->nullable();
            $table->string('locale')->index();
            $table->string('title')->nullable();
            $table->text('content')->nullable();
            $table->string('salary')->nullable();
            $table->bigInteger('read')->default(0);
            $table->string('slug')->unique()->nullable();
            $table->boolean('status')->default(true);
            $table->text('media')->nullable()->nullable();
            $table->integer('order')->nullable();
            $table->timestamp('date')->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->index(['vacancy_id','locale']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vacancies');
    }
}
