<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stores', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('code')->nullable();
            $table->string('format')->nullable();
            $table->string('store_id');
            $table->string('locale');
            $table->string('name');
            $table->text('address');
            $table->string('longitude')->nullable();
            $table->string('phone')->nullable();
            $table->string('work_times')->nullable();
            $table->string('latitude')->nullable();
            $table->unsignedBigInteger('author_id');
            $table->integer('order')->nullable();
            $table->boolean('status')->default(true);
            $table->timestamp('date')->nullable();
            $table->timestamps();
            $table->foreign('author_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stores');
    }
}
