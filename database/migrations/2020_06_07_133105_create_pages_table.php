<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePagesTable extends Migration
{

    public function up()
    {
        Schema::create('pages', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('page_id');
            $table->string('locale');
            $table->string('menu_id')->nullable();
            $table->bigInteger('author_id')->unsigned();
            $table->string('title')->nullable();
            $table->string('slug')->unique()->nullable();
            $table->text('content')->nullable();
            $table->text('media')->nullable();
            $table->string('template');
            $table->timestamp('date')->nullable();
            $table->boolean('status')->default(true);
            $table->timestamps();
            $table->foreign('author_id')->references('id')->on('users');
//            $table->foreign('menu_id')->references('menu_id')->on('menus')
//                ->onUpdate('cascade')->onDelete('cascade');
        });

    }


    public function down()
    {
        Schema::dropIfExists('pages');
    }
}
