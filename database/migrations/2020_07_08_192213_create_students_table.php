<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStudentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('students', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('student_id');
            $table->string('name');
            $table->string('title')->nullable();
            $table->string('locale');
            $table->text('content')->nullable();
            $table->text('media')->nullable();
            $table->string('group');
            $table->unsignedBigInteger('author_id');
            $table->integer('order')->nullable();
            $table->boolean('status')->default(true);
            $table->timestamp('date')->nullable();
            $table->foreign('author_id')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('students');
    }
}
