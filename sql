
INSERT INTO `translations` ( `group_id`, `key`, `value`) VALUES
(3,'image', 'Image', '2019-08-20 05:05:05', '2019-08-30 09:14:57', NULL),
(3,'video', 'Video', '2019-08-20 05:05:05', '2019-08-30 09:14:57', NULL),
(3,'document', 'Document', '2019-08-20 05:05:05', '2019-08-30 09:14:57', NULL),
(3,'view_in', 'View in', '2019-08-20 05:05:05', '2019-08-30 09:14:57', NULL),
(3,'all_media', 'All media', '2019-08-20 05:05:05', '2019-08-30 09:14:57', NULL),
(3,'trash', 'Trash', '2019-08-20 05:05:05', '2019-08-30 09:14:57', NULL),
(3,'recent', 'Recent', '2019-08-20 05:05:05', '2019-08-30 09:14:57', NULL),
(3,'favorites', 'Favorites', '2019-08-20 05:05:05', '2019-08-30 09:14:57', NULL),
(3,'upload', 'Upload', '2019-08-20 05:05:05', '2019-08-30 09:14:57', NULL),
(3,'add_from', 'Add from', '2019-08-20 05:05:05', '2019-08-30 09:14:57', NULL),
(3,'youtube', 'Youtube', '2019-08-20 05:05:05', '2019-08-30 09:14:57', NULL),
(3,'vimeo', 'Vimeo', '2019-08-20 05:05:05', '2019-08-30 09:14:57', NULL),
(3,'vine', 'Vine', '2019-08-20 05:05:05', '2019-08-30 09:14:57', NULL),
(3,'daily_motion', 'Dailymotion', '2019-08-20 05:05:05', '2019-08-30 09:14:57', NULL),
(3,'create_folder', 'Create folder', '2019-08-20 05:05:05', '2019-08-30 09:14:57', NULL),
(3,'refresh', 'Refresh', '2019-08-20 05:05:05', '2019-08-30 09:14:57', NULL),
(3,'empty_trash', 'Empty trash', '2019-08-20 05:05:05', '2019-08-30 09:14:57', NULL),
(3,'search_file_and_folder', 'Search file and folder', '2019-08-20 05:05:05', '2019-08-30 09:14:57', NULL),
(3,'sort', 'Sort', '2019-08-20 05:05:05', '2019-08-30 09:14:57', NULL),
(3,'file_name_asc', 'File name - ASC', '2019-08-20 05:05:05', '2019-08-30 09:14:57', NULL),
(3,'file_name_desc', 'File name - DESC', '2019-08-20 05:05:05', '2019-08-30 09:14:57', NULL),
(3,'created_date_asc', 'Created date - ASC', '2019-08-20 05:05:05', '2019-08-30 09:14:57', NULL),
(3,'created_date_desc', 'Created_date - DESC', '2019-08-20 05:05:05', '2019-08-30 09:14:57', NULL),
(3,'uploaded_date_asc', 'Uploaed date - ASC', '2019-08-20 05:05:05', '2019-08-30 09:14:57', NULL),
(3,'uploaded_date_desc', 'Uploaded date - DESC', '2019-08-20 05:05:05', '2019-08-30 09:14:57', NULL),
(3,'size_asc', 'Size - ASC', '2019-08-20 05:05:05', '2019-08-30 09:14:57', NULL),
(3,'size_desc', 'Size - DESC', '2019-08-20 05:05:05', '2019-08-30 09:14:57', NULL),
(3,'actions', 'Actions', '2019-08-20 05:05:05', '2019-08-30 09:14:57', NULL),
(3,'nothing_is_selected', 'Nothing is selected', '2019-08-20 05:05:05', '2019-08-30 09:14:57', NULL),
(3,'insert', 'Insert', '2019-08-20 05:05:05', '2019-08-30 09:14:57', NULL),
(3,'coming_soon', 'Coming soon', '2019-08-20 05:05:05', '2019-08-30 09:14:57', NULL),
(3,'add_from_youtube', 'Add from youtube', '2019-08-20 05:05:05', '2019-08-30 09:14:57', NULL),
(3,'playlist', 'Playlist', '2019-08-20 05:05:05', '2019-08-30 09:14:57', NULL),
(3,'add_url', 'Add URL', '2019-08-20 05:05:05', '2019-08-30 09:14:57', NULL),
(3,'folder_name', 'Folder name', '2019-08-20 05:05:05', '2019-08-30 09:14:57', NULL),
(3,'create', 'Create', '2019-08-20 05:05:05', '2019-08-30 09:14:57', NULL),
(3,'rename', 'Rename', '2019-08-20 05:05:05', '2019-08-30 09:14:57', NULL),
(3,'close', 'Close', '2019-08-20 05:05:05', '2019-08-30 09:14:57', NULL),
(3,'save_changes', 'Save changes', '2019-08-20 05:05:05', '2019-08-30 09:14:57', NULL),
(3,'move_to_trash', 'Move items to trash', '2019-08-20 05:05:05', '2019-08-30 09:14:57', NULL),
(3,'confirm_trash', 'Are you sure you want to move these items to trash?', '2019-08-20 05:05:05', '2019-08-30 09:14:57', NULL),
(3,'confirm', 'Confirm', '2019-08-20 05:05:05', '2019-08-30 09:14:57', NULL),
(3,'confirm_delete', 'Delete item(s)', '2019-08-20 05:05:05', '2019-08-30 09:14:57', NULL),
(3,'confirm_delete_description', 'Your request cannot rollback. Are you sure you wanna delete these items?', '2019-08-20 05:05:05', '2019-08-30 09:14:57', NULL),
(3,'empty_trash_title', 'Empty trash', '2019-08-20 05:05:05', '2019-08-30 09:14:57', NULL),
(3,'empty_trash_description', 'Your request cannot rollback.Are you sure you wanna remove all items in trash?', '2019-08-20 05:05:05', '2019-08-30 09:14:57', NULL),
(3,'up_level', 'Up one level', '2019-08-20 05:05:05', '2019-08-30 09:14:57', NULL),
(3,'upload_progress', 'Upload progress', '2019-08-20 05:05:05', '2019-08-30 09:14:57', NULL),
(3,'folder_created', 'Folder is created successfully!', '2019-08-20 05:05:05', '2019-08-30 09:14:57', NULL),
(3,'gallery', 'Media gallery', '2019-08-20 05:05:05', '2019-08-30 09:14:57', NULL),
(3,'trash_error', 'Error when delete selected item(s)', '2019-08-20 05:05:05', '2019-08-30 09:14:57', NULL),
(3,'trash_success', 'Moved selected item(s) to trash successfully!', '2019-08-20 05:05:05', '2019-08-30 09:14:57', NULL),
(3,'restore_error', 'Error when restore selected item(s)', '2019-08-20 05:05:05', '2019-08-30 09:14:57', NULL),
(3,'restore_success', 'Restore selected item(s) successfully!', '2019-08-20 05:05:05', '2019-08-30 09:14:57', NULL),
(3,'copy_success', 'Copied selected item(s) successfully!', '2019-08-20 05:05:05', '2019-08-30 09:14:57', NULL),
(3,'delete_success', 'Deleted selected item(s) successfully!', '2019-08-20 05:05:05', '2019-08-30 09:14:57', NULL),
(3,'favorite_success', 'Favorite selected item(s) successfully!', '2019-08-20 05:05:05', '2019-08-30 09:14:57', NULL),
(3,'remove_favorite_success', 'Remove selected item(s) from favorites successfully!', '2019-08-20 05:05:05', '2019-08-30 09:14:57', NULL),
(3,'rename_error', 'Error when rename item(s)', '2019-08-20 05:05:05', '2019-08-30 09:14:57', NULL),
(3,'rename_success', 'Rename selected item(s) successfully!', '2019-08-20 05:05:05', '2019-08-30 09:14:57', NULL),
(3,'empty_trash_success', 'Empty trash successfully!', '2019-08-20 05:05:05', '2019-08-30 09:14:57', NULL),
(3,'invalid_action', 'Invalid action!', '2019-08-20 05:05:05', '2019-08-30 09:14:57', NULL),
(3,'file_not_exists', 'File is not exists!', '2019-08-20 05:05:05', '2019-08-30 09:14:57', NULL),
(3,'download_file_error', 'Error when downloading files!', '2019-08-20 05:05:05', '2019-08-30 09:14:57', NULL),
(3,'missing_zip_archive_extension', 'Please enable ZipArchive extension to download file!', '2019-08-20 05:05:05', '2019-08-30 09:14:57', NULL),
(3,'can_not_download_file', 'Can not download this file!', '2019-08-20 05:05:05', '2019-08-30 09:14:57', NULL),
(3,'invalid_request', 'Invalid request!', '2019-08-20 05:05:05', '2019-08-30 09:14:57', NULL),
(3,'add_success', 'Add item successfully!', '2019-08-20 05:05:05', '2019-08-30 09:14:57', NULL),
(3,'file_too_big', 'File too big. Max file upload is :size bytes', '2019-08-20 05:05:05', '2019-08-30 09:14:57', NULL),
(3,'can_not_detect_file_type', 'File type is not allowed or can not detect file type!', '2019-08-20 05:05:05', '2019-08-30 09:14:57', NULL),
(3,'upload_failed', 'The file is NOT uploaded completely. The server allows max upload file size is :size . Please check your file size OR try to upload again in case of having network errors', '2019-08-20 05:05:05', '2019-08-30 09:14:57', NULL),
(3,'menu_name', 'Media', '2019-08-20 05:05:05', '2019-08-30 09:14:57', NULL),
(3,'add', 'Add media', '2019-08-20 05:05:05', '2019-08-30 09:14:57', NULL),
(3,'javascript.name', 'Name', '2019-08-20 05:05:05', '2019-08-30 09:14:57', NULL),
(3,'javascript.url', 'Url', '2019-08-20 05:05:05', '2019-08-30 09:14:57', NULL),
(3,'javascript.full_url', 'Full url', '2019-08-20 05:05:05', '2019-08-30 09:14:57', NULL),
(3,'javascript.size', 'Size', '2019-08-20 05:05:05', '2019-08-30 09:14:57', NULL),
(3,'javascript.mime_type', 'Type', '2019-08-20 05:05:05', '2019-08-30 09:14:57', NULL),
(3,'javascript.created_at', 'Uploaded at', '2019-08-20 05:05:05', '2019-08-30 09:14:57', NULL),
(3,'javascript.updated_at', 'Modified at', '2019-08-20 05:05:05', '2019-08-30 09:14:57', NULL),
(3,'javascript.nothing_selected', 'Nothing is selected', '2019-08-20 05:05:05', '2019-08-30 09:14:57', NULL),
(3,'javascript.visit_link', 'Open link', '2019-08-20 05:05:05', '2019-08-30 09:14:57', NULL),
(3,'javascript.add_from.youtube.original_msg', 'Please input Youtube URL', '2019-08-20 05:05:05', '2019-08-30 09:14:57', NULL),
(3,'javascript.add_from.youtube.no_api_key_msg', 'Please specify the Youtube API key', '2019-08-20 05:05:05', '2019-08-30 09:14:57', NULL),
(3,'javascript.add_from.youtube.invalid_url_msg', 'Your link is not belongs to Youtube', '2019-08-20 05:05:05', '2019-08-30 09:14:57', NULL),
(3,'javascript.add_from.youtube.error_msg', 'Some error occurred...', '2019-08-20 05:05:05', '2019-08-30 09:14:57', NULL),
(3,'javascript.no_item.all_media.icon', 'fa fa-cloud-upload', '2019-08-20 05:05:05', '2019-08-30 09:14:57', NULL),
(3,'javascript.no_item.all_media.title', 'Drop files and folders here', '2019-08-20 05:05:05', '2019-08-30 09:14:57', NULL),
(3,'javascript.no_item.all_media.message', 'Or use the upload button above', '2019-08-20 05:05:05', '2019-08-30 09:14:57', NULL),
(3,'javascript.no_item.trash.icon', 'fa fa-trash', '2019-08-20 05:05:05', '2019-08-30 09:14:57', NULL),
(3,'javascript.no_item.trash.title', 'There is nothing in your trash currently', '2019-08-20 05:05:05', '2019-08-30 09:14:57', NULL),
(3,'javascript.no_item.trash.message', 'Delete files to move them to trash automatically. Delete files from trash to remove them permanently', '2019-08-20 05:05:05', '2019-08-30 09:14:57', NULL),
(3,'javascript.no_item.favorites.icon', 'fa fa-star', '2019-08-20 05:05:05', '2019-08-30 09:14:57', NULL),
(3,'javascript.no_item.favorites.title', 'You have not added anything to your favorites yet', '2019-08-20 05:05:05', '2019-08-30 09:14:57', NULL),
(3,'javascript.no_item.favorites.message', 'Add files to favorites to easily find them later', '2019-08-20 05:05:05', '2019-08-30 09:14:57', NULL),
(3,'javascript.no_item.recent.icon', 'fa fa-clock-o', '2019-08-20 05:05:05', '2019-08-30 09:14:57', NULL),
(3,'javascript.no_item.recent.title', 'You did not opened anything yet', '2019-08-20 05:05:05', '2019-08-30 09:14:57', NULL),
(3,'javascript.no_item.recent.message', 'All recent files that you opened will be appeared here', '2019-08-20 05:05:05', '2019-08-30 09:14:57', NULL),
(3,'javascript.no_item.default.icon', 'fa fa-refresh', '2019-08-20 05:05:05', '2019-08-30 09:14:57', NULL),
(3,'javascript.no_item.default.title', 'No items', '2019-08-20 05:05:05', '2019-08-30 09:14:57', NULL),
(3,'javascript.no_item.default.message', 'This directory has no item', '2019-08-20 05:05:05', '2019-08-30 09:14:57', NULL),
(3,'javascript.clipboard.success', 'These file links has been copied to clipboard', '2019-08-20 05:05:05', '2019-08-30 09:14:57', NULL),
(3,'javascript.message.error_header', 'Error', '2019-08-20 05:05:05', '2019-08-30 09:14:57', NULL),
(3,'javascript.message.success_header', 'Success', '2019-08-20 05:05:05', '2019-08-30 09:14:57', NULL),
(3,'javascript.download.error', 'No files selected or cannot download these files', '2019-08-20 05:05:05', '2019-08-30 09:14:57', NULL),
(3,'javascript.actions_list.basic.preview', 'Preview', '2019-08-20 05:05:05', '2019-08-30 09:14:57', NULL),
(3,'javascript.actions_list.file.copy_link', 'Copy link', '2019-08-20 05:05:05', '2019-08-30 09:14:57', NULL),
(3,'javascript.actions_list.file.rename', 'Rename', '2019-08-20 05:05:05', '2019-08-30 09:14:57', NULL),
(3,'javascript.actions_list.file.make_copy', 'Make a copy', '2019-08-20 05:05:05', '2019-08-30 09:14:57', NULL),
(3,'javascript.actions_list.user.favorite', 'Add to favorite', '2019-08-20 05:05:05', '2019-08-30 09:14:57', NULL),
(3,'javascript.actions_list.user.remove_favorite', 'Remove favorite', '2019-08-20 05:05:05', '2019-08-30 09:14:57', NULL),
(3,'javascript.actions_list.other.download', 'Download', '2019-08-20 05:05:05', '2019-08-30 09:14:57', NULL),
(3,'javascript.actions_list.other.trash', 'Move to trash', '2019-08-20 05:05:05', '2019-08-30 09:14:57', NULL),
(3,'javascript.actions_list.other.delete', 'Delete permanently', '2019-08-20 05:05:05', '2019-08-30 09:14:57', NULL),
(3,'javascript.actions_list.other.restore', 'Restore', '2019-08-20 05:05:05', '2019-08-30 09:14:57', NULL),
